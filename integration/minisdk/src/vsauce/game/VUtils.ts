namespace vsauce {
    export function getGame(): VGame {
        return plume.Game.get() as VGame;
    }
    export function getGui(): plume.CanvasGui {
        return plume.Game.get().getGui() as plume.CanvasGui;
    }
}