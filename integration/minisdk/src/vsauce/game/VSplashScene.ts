
module vsauce {

    export abstract class VSplashScene extends plume.GuiScene {

        protected loader: plume.Loader3d;

        constructor() {
            super();

            let game = this.game as VGame;
            let spashduration = 0; // get from config

            this.loader = game.engine.loader;

            this.loadModels(this.loader);

            let self = this;
            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, spashduration);
        }

        private ensureModelLoadedAndSwitch() {
            if (this.areModelsLoaded(this.loader)) {
                this.goToMenu();
                return;
            }

            let self = this;
            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, 100);
        }

        // Can be overload
        protected areModelsLoaded(loader: plume.Loader3d): boolean {
            return loader.isLoaded();
        }

        protected abstract loadModels(loader: plume.Loader3d): void;
        protected abstract goToMenu(): void;

    }


}