namespace vsauce {

    export type DataObject = {
        [key: string]: any
    }
    
    export type VPlayer = {
        id: string,
        name?: string,
        photo?: string
    }

    export interface VDataManager {
        getDataAsync(keys: string[], fn: (data: DataObject) => void): void;
        setDataAsync(data: DataObject): void;
        logEvent(eventName: string, valueToSum?: number, parameters?: Object): void;
        getPlayer() : VPlayer; // available after start()
    }
    
    export abstract class VDefaultDataManager implements VDataManager {
        
        constructor() {
        }
        
        abstract getDataAsync(keys: string[], fn: (data: DataObject) => void): void;
        abstract setDataAsync(data: DataObject): void;
        abstract logEvent(eventName: string, valueToSum?: number, parameters?: Object): void;
        abstract getPlayer(): VPlayer;
    }

    export class VDummyDataManager extends VDefaultDataManager {

        private _localStorage: plume.LocalStorage;

        constructor() {
            super();

            let game = getGame();
            this._localStorage = new plume.LocalStorage(game.config.gameId);
        }

        getDataAsync(keys: string[], fn: (data: DataObject) => void): void {
            let res: DataObject = {};
            for (let i = 0; i < keys.length; i++) {
                let k = keys[i];
                let v = this._localStorage.get(k);
                res[k] = v;
            }

            setTimeout(function () {
                fn(res);
            }, 100);
        }

        setDataAsync(data: DataObject): void {
            for (let k in data) {
                let v = data[k];
                this._localStorage.put(k, v);
            }
        }

        logEvent(eventName: string, valueToSum?: number, parameters?: Object): void {
            logger.debug("event:" + eventName + ", v=" + valueToSum + " ,params=" + (parameters == null ? "null" : JSON.stringify(parameters)));
        }

        getPlayer(): VPlayer {
            let pid = this._localStorage.get("playerId", null);
            if (pid == null) {
                let now = Date.now();
                pid = "player" + now;
                this._localStorage.put("playerId", pid);
                this._localStorage.put("playerName", "guest" + now);
            }
            let name = this._localStorage.get("playerName", pid);
            return {
                id: pid,
                name: name,
            };
        }
    }
}