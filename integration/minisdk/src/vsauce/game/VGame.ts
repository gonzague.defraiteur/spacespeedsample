/// <reference path="../../../../../plume3d/export/latest/plume3d.core.d.ts" />
/// <reference path="../../../../../plume3d/export/latest/plume3d.gui.d.ts" />

//
// Mock
// Will need to plug back FB API if needed
//
namespace vsauce {

    export let logger = plume.logger;

    export type Config = {
        publisher: string,
        gameId: string,
        gameVersion: string,
        fbm?: {
            interstitialPlacementId: string,
            rewardedVideoPlacementId: string,
        }
    }

    export type LoadingConfig = {
        optDelayBeforeCompleteFire?: number;
        optProgressAlgo?: number;
        optEstimateLoadingTime?: number;
    }

    export class VGame extends plume.Game {

        config: Config;
        loadingConfig: LoadingConfig;

        bootstrap: VBootstrap;
        dataManager: VDataManager;
        // scoreManager: VScoreManager;
        adsManager: VAdsManager;
        // socialManager: VSocialManager;

        // To be called by game
        protected initialize(config: Config) {
            this.config = config;

            // util.enableDomConsole();
            logger.debug("Starting " + config.gameId + " " + config.gameVersion);
            logger.debug("✰ html5 version by https://gamebuilt.com/ ✰")

            let gui = new plume.CanvasGui({ canvasId: "2d" });
            let engine = new plume.Engine({ canvasId: "3d" });
            this.initializeEngine(engine);
            this.initializeGui(gui);

            // let factory: VFactory;
            // if (config.publisher == "dummy") {
            //     factory = new vsauce["VDummyFactory"]();
            // } else if (config.publisher == "fbmessenger") {
            //     factory = new vsauce["VFBMessengerFactory"]();
            // }
            // this.bootstrap = factory.newVBootstrap();
            // this.dataManager = factory.newVDataManager();
            // this.scoreManager = factory.newVScoreManager();
            // this.adsManager = factory.newVAdsManager();
            // this.socialManager = factory.newVSocialManager();

            this.bootstrap = new VDummyBoostrap();
            this.dataManager = new VDummyDataManager();
            this.adsManager = new VDummyAdsManager();

            this.scaleManager.resizeHandler.add(this.onResize, this);
            this.onResize();
        }

        // To be called by game
        protected loadAssets(assets: Array<plume.AssetDef>, loadingConfig: LoadingConfig) {
            let self = this;
            this.loadingConfig = loadingConfig;
            if (this.loadingConfig.optDelayBeforeCompleteFire == null) this.loadingConfig.optDelayBeforeCompleteFire = 50;
            if (this.loadingConfig.optEstimateLoadingTime == null) this.loadingConfig.optEstimateLoadingTime = 5000;
            if (this.loadingConfig.optProgressAlgo == null) this.loadingConfig.optProgressAlgo = plume.PreloadingProgressAlgo.Assets;

            this.bootstrap.onReady(assets, function () {
                self.onAssetsLoaded();
                self.goToGame();
            });
        }

        // To be overrided by game (show menu). Called by lib when all assets/initialization steps are finished
        protected goToGame() {
        }

        // Optional
        protected onAssetsLoaded() {
        }
        protected onResize() {
        }

        // Helper
        public switchScene(scene: plume.GuiScene) {
            this.gui.switchScene(scene);
        }

        get gui(): plume.CanvasGui {
            return this._gui as plume.CanvasGui;
        }

    }

}