namespace vads {

    export interface AdConfig {
        inline: boolean,
    }

    export type EventType = "gameover" | "update" | "ready";

    export interface GameplayParameters {
    }

    export abstract class AdsHandler {

        private static INST: AdsHandler;

        public _config: AdConfig;
        public _handlerByType = new plume.HashMap<plume.Handler<any>>();
        public _gameplay: GameplayParameters;

        public assets: { [name: string]: string } = {};

        constructor() {
            if (AdsHandler.INST != null) {
                throw new Error("Already created");
            }

            AdsHandler.INST = this;

            this._gameplay = this.initGameplayParameters();

            // Retrieve private config from html
            this._config = window["__gb_adsconfig"];
            this.init();

            // Expose adshandler for voodoo
            window["__gb_adshandler"] = this;
        }

        static get(): AdsHandler {
            return this.INST;
        }

        //
        // Internal
        //

        protected abstract initGameplayParameters(): GameplayParameters;

        abstract gameplay: GameplayParameters;

        isPlayable(): boolean {
            return this._config != null;
        }

        setupLoader(loader: plume.Loader3d) {
            if (this.isPlayable() && this._config.inline) {
                let inlineLoadingManager = new plume.InlineLoadingManager();
                inlineLoadingManager.initialize(loader);
            }
        }

        fire(event: EventType, data: any) {
            let h = this._handlerByType.get(event);
            if (h == null) {
                // no handler registered
                return;
            }

            h.fire(data);
        }

        private ensureHandler(event: EventType): plume.Handler<any> {
            let h = this._handlerByType.get(event);
            if (h != null) return h;

            let handler = new plume.Handler();
            this._handlerByType.put(event, handler);
            return handler;
        }

        private init() {
            if (this._config == null) {
                logger.debug("No ads config found");
                return;
            }

            if (this._config.inline) {
                plume.LoaderProxy.set(new plume.InlineLoader());
            }
        }

        //
        //
        //

        //
        // VOODOO Api
        //

        // pause() : void;
        // resume() : void;
        // mute() : void;
        // unmute() : void;

        abstract start(): void;

        on(event: EventType, cb: () => void): void {
            let h = this.ensureHandler(event);
            h.add(cb);
        }

        //
        // END VOODOO Api
        //

    }

}