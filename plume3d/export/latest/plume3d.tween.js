"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var plume;
(function (plume) {
    var Tween = /** @class */ (function () {
        function Tween(object, tweenManager) {
            this.id = "DefaultId";
            this._repeatCount = 1;
            this._currentStepIndex = -1;
            this._steps = [];
            this._state = 0 /* PENDING */;
            this._savedValues = {};
            this._object = object;
            this._tweenManager = tweenManager;
        }
        Tween.prototype.getObject = function () {
            return this._object;
        };
        Tween.prototype.delay = function (delay) {
            var step = new plume.DelayTweenStep(delay);
            this._steps.push(step);
            return this;
        };
        Tween.prototype.repeat = function (repeat) {
            this._repeatCount = repeat;
        };
        Tween.prototype.to = function (properties, duration, ease) {
            if (duration === void 0) { duration = 1000; }
            if (ease === void 0) { ease = plume.easing.linear; }
            var step = new plume.PropertiesTweenStep(this._object, properties, duration, ease, false);
            this._steps.push(step);
            return this;
        };
        Tween.prototype.from = function (properties, duration, ease) {
            if (duration === void 0) { duration = 1000; }
            if (ease === void 0) { ease = plume.easing.linear; }
            var step = new plume.PropertiesTweenStep(this._object, properties, duration, ease, true);
            this._steps.push(step);
            return this;
        };
        Tween.prototype.start = function () {
            if (this._state == 1 /* RUNNING */) {
                plume.logger.warn("Tween already started");
                return;
            }
            if (this._steps.length == 0) {
                plume.logger.warn("No step defined in Tween");
                return;
            }
            this._state = 1 /* RUNNING */;
            this._nextStep();
            if (this.onStart) {
                this.onStart();
            }
        };
        Tween.prototype.stop = function () {
            if (this._state == 2 /* TERMINATED */) {
                // already stopped
                return;
            }
            this._state = 2 /* TERMINATED */;
            if (this.onComplete) {
                this.onComplete(this);
            }
            this._tweenManager.remove(this);
        };
        Tween.prototype.update = function (simulationTimestep) {
            if (this._state != 1 /* RUNNING */)
                return;
            if (this._currentStep == null) {
                this.stop();
                return;
            }
            this._currentStep.update(simulationTimestep);
            if (this._currentStep.isComplete()) {
                // complete animation
                this.render(1);
                // go to next step
                this._nextStep();
            }
        };
        Tween.prototype.render = function (interpolation) {
            if (this._state != 1 /* RUNNING */ || this._currentStep == null)
                return;
            this._currentStep.render(interpolation);
        };
        Tween.prototype._nextStep = function () {
            this._currentStepIndex++;
            ;
            if (this._currentStepIndex < this._steps.length) {
                this._currentStep = this._steps[this._currentStepIndex];
                this._currentStep.start();
            }
            else {
                // All steps done
                this._repeatCount -= 1;
                if (this._repeatCount == 0) {
                    this._currentStep = null;
                }
                else {
                    // Restart from 0 if repeat > 1 || repeat < -1
                    this._currentStepIndex = -1;
                    this._nextStep();
                }
            }
        };
        return Tween;
    }());
    plume.Tween = Tween;
})(plume || (plume = {}));
/// <reference path="../../build/latest/plume3d.core.d.ts" />
var plume;
(function (plume) {
    var TweenManager = /** @class */ (function () {
        function TweenManager() {
            this._tweens = [];
            TweenManager._instance = this;
            this._game = plume.Game.get();
            this._game.addUpdatable(this);
            this._game.addRenderable(this);
        }
        TweenManager.get = function () {
            if (TweenManager._instance == null) {
                TweenManager._instance = new TweenManager();
            }
            return TweenManager._instance;
        };
        TweenManager.prototype.create = function (object, id) {
            var tween = new plume.Tween(object, this);
            if (id != null)
                tween.id = id;
            this._tweens.push(tween);
            return tween;
        };
        TweenManager.prototype.remove = function (tween) {
            var index = this._tweens.indexOf(tween);
            if (index > -1) {
                this._tweens.splice(index, 1);
                tween.stop();
            }
        };
        TweenManager.prototype.removeAll = function () {
            this.cancelAll();
            this._tweens = [];
        };
        TweenManager.prototype.cancelAll = function () {
            for (var i = 0; i < this._tweens.length; i++) {
                this._tweens[i].stop();
            }
        };
        TweenManager.prototype.update = function (simulationTimestep) {
            for (var i = 0; i < this._tweens.length; i++) {
                this._tweens[i].update(simulationTimestep);
            }
        };
        TweenManager.prototype.render = function (interpolation) {
            for (var i = 0; i < this._tweens.length; i++) {
                this._tweens[i].render(interpolation);
            }
        };
        Object.defineProperty(TweenManager.prototype, "activeTweensCount", {
            get: function () {
                return this._tweens.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TweenManager.prototype, "activeTweens", {
            get: function () {
                return this._tweens.map(function (elem) {
                    return elem.id;
                }).join();
            },
            enumerable: true,
            configurable: true
        });
        return TweenManager;
    }());
    plume.TweenManager = TweenManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var TweenStep = /** @class */ (function () {
        function TweenStep(duration) {
            this.complete = false;
            this.elapsedTime = null;
            this.startTime = null;
            this.duration = duration;
        }
        TweenStep.prototype.start = function () {
            this.startTime = plume.Time.now();
            this.elapsedTime = 0;
            this.complete = false;
        };
        TweenStep.prototype.update = function (simulationTimestep) {
            var now = plume.Time.now();
            this.elapsedTime = now - this.startTime;
            if (this.elapsedTime >= this.duration) {
                this.complete = true;
            }
        };
        TweenStep.prototype.render = function (interpolation) {
            // Nothing by default
        };
        TweenStep.prototype.isComplete = function () { return this.complete; };
        return TweenStep;
    }());
    plume.TweenStep = TweenStep;
    var DelayTweenStep = /** @class */ (function (_super) {
        __extends(DelayTweenStep, _super);
        function DelayTweenStep(duration) {
            return _super.call(this, duration) || this;
        }
        return DelayTweenStep;
    }(TweenStep));
    plume.DelayTweenStep = DelayTweenStep;
    var PropertiesTweenStep = /** @class */ (function (_super) {
        __extends(PropertiesTweenStep, _super);
        function PropertiesTweenStep(object, props, duration, easing, isFrom) {
            var _this = _super.call(this, duration) || this;
            _this._startValues = {};
            _this._endValues = {};
            _this._previousValues = {};
            _this._computedValues = {};
            _this._properties = props;
            _this._easing = easing;
            _this._from = isFrom;
            _this._object = object;
            return _this;
        }
        PropertiesTweenStep.prototype.start = function () {
            _super.prototype.start.call(this);
            for (var property in this._properties) {
                if (this._from) {
                    this._startValues[property] = this._properties[property];
                    this._endValues[property] = this._object[property];
                }
                else {
                    this._startValues[property] = this._object[property];
                    this._endValues[property] = this._properties[property];
                }
                this._previousValues[property] = this._startValues[property];
                this._computedValues[property] = this._startValues[property];
            }
        };
        PropertiesTweenStep.prototype.update = function (simulationTimestep) {
            _super.prototype.update.call(this, simulationTimestep);
            var delta = Math.min(this.elapsedTime, this.duration);
            var percent = delta / this.duration;
            var easingValue = this._easing(percent);
            for (var property in this._properties) {
                // save previous
                this._previousValues[property] = this._computedValues[property];
                // compute new
                var startV = this._startValues[property];
                var endV = this._endValues[property];
                var computed = startV + ((endV - startV) * easingValue);
                this._computedValues[property] = computed;
            }
        };
        PropertiesTweenStep.prototype.render = function (interpolation) {
            _super.prototype.render.call(this, interpolation);
            for (var property in this._computedValues) {
                var prev = this._previousValues[property];
                var last = this._computedValues[property];
                this._object[property] = prev + ((last - prev) * interpolation);
            }
        };
        return PropertiesTweenStep;
    }(TweenStep));
    plume.PropertiesTweenStep = PropertiesTweenStep;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var TweenValueListener = /** @class */ (function () {
        function TweenValueListener(initialValue, roundValue) {
            if (roundValue === void 0) { roundValue = true; }
            this.roundValue = roundValue;
            if (roundValue) {
                this._value = Math.round(initialValue);
            }
            else {
                this._value = initialValue;
            }
        }
        TweenValueListener.prototype._notify = function (v) {
            var previous = this._value;
            if (previous == v)
                return;
            if (v > previous) {
                if (this.onValueUp != null)
                    this.onValueUp(v);
                if (this.onValueChange != null)
                    this.onValueChange(v);
            }
            else if (v < previous) {
                if (this.onValueDown != null)
                    this.onValueDown(v);
                if (this.onValueChange != null)
                    this.onValueChange(v);
            }
        };
        Object.defineProperty(TweenValueListener.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (v) {
                if (this.roundValue) {
                    v = Math.round(v);
                }
                this._notify(v);
                this._value = v;
            },
            enumerable: true,
            configurable: true
        });
        return TweenValueListener;
    }());
    plume.TweenValueListener = TweenValueListener;
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.tween.js.map