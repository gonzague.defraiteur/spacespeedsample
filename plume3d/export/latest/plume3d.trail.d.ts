/// <reference path="plume3d.core.d.ts" />
declare module plume {
    interface TrailMaterialParameters extends THREE.ShaderMaterialParameters {
    }
    class TrailMaterial extends THREE.RawShaderMaterial {
        constructor(parameters?: TrailMaterialParameters);
        minID: number;
        maxID: number;
        tailSharp: number;
        headColor: THREE.Vector4;
        tailColor: THREE.Vector4;
    }
}
declare module plume {
    class TrailRenderer {
        private static _PositionComponentCount;
        private static _IndicesPerFace;
        private static _FacesPerQuad;
        private static _sharedMaterial;
        private _scene;
        private _active;
        private _geometry;
        private _mesh;
        private _nodeCenters;
        private _currentNodeCenter;
        private _nodeIDs;
        private _currentLength;
        private _currentEnd;
        private _currentNodeID;
        private _verticesPerNode;
        private _faceIndicesPerNode;
        private _facesPerNode;
        private _length;
        private _targetObject;
        private _localHeadGeometry;
        private _vertexCount;
        private _faceCount;
        material: TrailMaterial;
        constructor(material?: TrailMaterial, scene?: THREE.Scene);
        static getOrCreateSharedMaterial(): TrailMaterial;
        initialize(length: number, tailSharp: boolean, localHeadWidth: number, targetObject: THREE.Object3D): void;
        private _initializeLocalHeadGeometry;
        private _initializeGeometry;
        private _ZeroVertices;
        private _ZeroIndices;
        private _FormInitialFaces;
        private _initializeMesh;
        private _destroyMesh;
        private _Reset;
        private _updateUniforms;
        update(): void;
        private _tempMatrix4;
        private _advance;
        private _advanceWithTransform;
        private _advanceGeometry;
        private _updateNodeID;
        private _updateNodeCenter;
        private _tempPosition;
        private _updateNodePositionsFromTransformMatrix;
        private _connectNodes;
        private _disconnectNodes;
        deactivate(): void;
        activate(): void;
    }
}
declare module plume.trail.shaders {
    let base_fragment: string;
    let base_vertex: string;
}
