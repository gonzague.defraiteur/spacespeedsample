"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="../../build/latest/plume3d.core.d.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf_1) {
        var GLTFLoader = /** @class */ (function () {
            function GLTFLoader(manager, textureLoaderManager) {
                this.manager = manager;
                this.textureLoaderManager = textureLoaderManager;
                this.crossOrigin = "anonymous";
                if (manager == null)
                    this.manager = THREE.DefaultLoadingManager;
                this.dracoLoader = null;
                var dracoloader = THREE["DRACOLoader"];
                if (dracoloader) {
                    dracoloader.setDecoderPath(plume.Loader3d.dracoLib);
                    this.dracoLoader = new dracoloader();
                }
            }
            GLTFLoader.prototype.load = function (url, onLoad, onProgress, onError) {
                var scope = this;
                var path = this.path !== undefined ? this.path : THREE.LoaderUtils.extractUrlBase(url);
                // Tells the LoadingManager to track an extra item, which resolves after
                // the model is fully loaded. This means the count of items loaded will
                // be incorrect, but ensures manager.onLoad() does not fire early.
                scope.manager.itemStart(url);
                var _onError = function (e) {
                    if (onError) {
                        onError(e);
                    }
                    else {
                        plume.logger.error(e);
                    }
                    scope.manager.itemEnd(url);
                    scope.manager.itemError(url);
                };
                var loader = new THREE.FileLoader(scope.manager);
                loader.setResponseType('arraybuffer');
                loader.load(url, function (data) {
                    try {
                        scope.parse(data, path, function (gltf) {
                            onLoad(gltf);
                            scope.manager.itemEnd(url);
                        }, _onError);
                    }
                    catch (e) {
                        _onError(e);
                    }
                }, onProgress, _onError);
            };
            GLTFLoader.prototype.setCrossOrigin = function (value) {
                this.crossOrigin = value;
                return this;
            };
            GLTFLoader.prototype.setPath = function (value) {
                this.path = value;
                return this;
            };
            GLTFLoader.prototype.setDRACOLoader = function (dracoLoader) {
                this.dracoLoader = dracoLoader;
                return this;
            };
            GLTFLoader.prototype.parse = function (data, path, onLoad, onError) {
                var content;
                var extensions = {};
                if (typeof data === 'string') {
                    content = data;
                }
                else {
                    var magic = THREE.LoaderUtils.decodeText(new Uint8Array(data, 0, 4));
                    if (magic === gltf_1.BINARY_EXTENSION_HEADER_MAGIC) {
                        try {
                            extensions.KHR_binary_glTF = new gltf_1.GLTFBinaryExtension(data);
                        }
                        catch (error) {
                            if (onError)
                                onError(error);
                            return;
                        }
                        content = extensions.KHR_binary_glTF.content;
                    }
                    else {
                        content = THREE.LoaderUtils.decodeText(new Uint8Array(data));
                    }
                }
                var json = JSON.parse(content);
                if (json.asset === undefined || parseInt(json.asset.version[0]) < 2) {
                    if (onError)
                        onError(new Error('Unsupported asset. glTF versions >=2.0 are supported. Use LegacyGLTFLoader instead.'));
                    return;
                }
                if (json.extensionsUsed) {
                    for (var i = 0; i < json.extensionsUsed.length; ++i) {
                        var extensionName = json.extensionsUsed[i];
                        var extensionsRequired = json.extensionsRequired || [];
                        switch (extensionName) {
                            case "KHR_lights_punctual":
                                extensions.KHR_lights_punctual = new gltf_1.GLTFLightsExtension(json);
                                break;
                            case "KHR_materials_unlit":
                                extensions.KHR_materials_unlit = new gltf_1.GLTFMaterialsUnlitExtension(json);
                                break;
                            case "KHR_materials_pbrSpecularGlossiness":
                                extensions.KHR_materials_pbrSpecularGlossiness = new gltf_1.GLTFMaterialsPbrSpecularGlossinessExtension();
                                break;
                            case "KHR_draco_mesh_compression":
                                extensions.KHR_draco_mesh_compression = new gltf_1.GLTFDracoMeshCompressionExtension(json, this.dracoLoader);
                                break;
                            case "MSFT_texture_dds":
                                extensions.MSFT_texture_dds = new gltf_1.GLTFTextureDDSExtension();
                                break;
                            case "GB_lightmaps":
                                extensions.GB_lightmaps = new gltf_1.GLTFLightMapsExtension(json);
                                break;
                            case "GB_sprites":
                                extensions.GB_sprites = new gltf_1.GLTFSpritesExtension(json);
                                break;
                            case "EXT_texture_transform":
                                extensions.EXT_texture_transform = new gltf_1.GLTFTextureTransformExtension();
                                break;
                            default:
                                if (extensionsRequired.indexOf(extensionName) >= 0) {
                                    console.warn('Unknown extension "' + extensionName + '".');
                                }
                        }
                    }
                }
                extensions.GB_Unity = new gltf_1.GLTFUnityExtension();
                var parser = new gltf_1.GLTFParser(json, extensions, {
                    path: path || this.path || '',
                    crossOrigin: this.crossOrigin,
                    manager: this.manager,
                    textureLoaderManager: this.textureLoaderManager,
                    factory: this.factory,
                });
                if (extensions.GB_lightmaps) {
                    extensions.GB_lightmaps.parser = parser;
                }
                if (extensions.GB_sprites) {
                    extensions.GB_sprites.parser = parser;
                }
                if (extensions.GB_Unity) {
                    extensions.GB_Unity.parser = parser;
                }
                parser.parse(function (glTF) {
                    // let glTF: GltfObject = {
                    //     scene: scene,
                    //     scenes: scenes,
                    //     cameras: cameras,
                    //     animations: animations,
                    //     asset: json.asset,
                    //     parser: parser,
                    //     userData: {}
                    // };
                    // addUnknownExtensionsToUserData(extensions, glTF, json);
                    onLoad(glTF);
                }, onError);
            };
            return GLTFLoader;
        }());
        gltf_1.GLTFLoader = GLTFLoader;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFParser = /** @class */ (function () {
            function GLTFParser(json, extensions, options) {
                this.json = json;
                this.extensions = extensions;
                this.options = options;
                this._externalResources = 0;
                this._loadedResources = 0;
                this._errorResources = 0;
                this._buffers = [];
                this._images = [];
                this._textures = [];
                this._accessors = [];
                this._bufferViews = [];
                this._materials = [];
                this._meshes = [];
                this._cameras = [];
                this._animations = [];
                this._animationsRelatives = [];
                this._nodes = [];
                this._scenes = [];
                this._skins = [];
                this._factory = (options.factory == null ? {} : options.factory);
                this._textureLoaderManager = options.textureLoaderManager;
                //this._textureLoaderManager.setCrossOrigin(this.options.crossOrigin);
                this._fileLoader = new THREE.FileLoader(this.options.manager);
                this._fileLoader.setResponseType('arraybuffer');
            }
            GLTFParser.prototype.parse = function (onLoad, onError) {
                var self = this;
                this._onCompleteCb = onLoad;
                //
                // Start by  loading all remote data
                //
                // Add fake assets
                this._externalResources++;
                //
                // Buffers
                var buffers = this.json.buffers;
                if (buffers != null && buffers.length > 0) {
                    var _loop_1 = function (i) {
                        var bufferDef = buffers[i];
                        bufferDef.index = i;
                        if (bufferDef.type && bufferDef.type !== 'arraybuffer') {
                            plume.logger.error("GLTFLoader: " + bufferDef.type + " buffer type is not supported.");
                            return "continue";
                        }
                        // If present, GLB container is required to be the first buffer.
                        if (bufferDef.uri == null && bufferDef.index == 0) {
                            self._buffers[bufferDef.index] = this_1.extensions.KHR_binary_glTF.body;
                            return "continue";
                        }
                        if (bufferDef.uri == null) {
                            plume.logger.warn("GLTFLoader: buffer without uri.");
                            return "continue";
                        }
                        var url = gltf.resolveURL(bufferDef.uri, this_1.options.path);
                        this_1._externalResources++;
                        this_1._fileLoader.load(url, function (response) {
                            self._loadedResources++;
                            // self._buffersByUri.put(bufferDef.uri, response as ArrayBuffer);
                            self._buffers[bufferDef.index] = response;
                            self._onCheckDataLoaded();
                        }, function () {
                            // progress
                        }, function (error) {
                            plume.logger.error("Failed to load " + bufferDef.uri);
                            self._errorResources++;
                            self._onCheckDataLoaded();
                        });
                    };
                    var this_1 = this;
                    for (var i = 0; i < buffers.length; i++) {
                        _loop_1(i);
                    }
                }
                // Images
                var images = this.json.images;
                if (images != null && images.length > 0) {
                    var _loop_2 = function (i) {
                        var imageDef = images[i];
                        imageDef.index = i;
                        if (imageDef.uri) {
                            var url = gltf.resolveURL(imageDef.uri, this_2.options.path);
                            this_2._externalResources++;
                            this_2._textureLoaderManager.load(url, function (response) {
                                self._loadedResources++;
                                // self._texturesByUri.put(imageDef.uri, response);
                                self._images[imageDef.index] = response;
                                self._onCheckDataLoaded();
                            }, function (error) {
                                plume.logger.error("Failed to load " + imageDef.uri);
                                self._errorResources++;
                                self._onCheckDataLoaded();
                            });
                        }
                        else {
                            // Embedded image loaded in GLTFEmbeddedImageImporter (once bufferview ready)
                        }
                    };
                    var this_2 = this;
                    for (var i = 0; i < images.length; i++) {
                        _loop_2(i);
                    }
                }
                if (this.extensions.KHR_draco_mesh_compression != null) {
                    // Using draco compression, load module
                    this._externalResources++;
                    this.extensions.KHR_draco_mesh_compression.initialize(function () {
                        self._loadedResources++;
                        self._onCheckDataLoaded();
                    });
                }
                // Remove fake assets
                this._externalResources--;
                this._onCheckDataLoaded();
            };
            GLTFParser.prototype._onCheckDataLoaded = function () {
                var totalLoaded = this._errorResources + this._loadedResources;
                if (totalLoaded >= this._externalResources) {
                    this._onExternalDataLoaded();
                }
            };
            GLTFParser.prototype._onExternalDataLoaded = function () {
                this._markDefs();
                // To build final gltf object we need
                // scenes
                // animations
                // cameras
                new gltf.GLTFBufferViewImporter(this).import();
                new gltf.GLTFEmbeddedImageImporter(this).import();
                new gltf.GLTFTextureImporter(this).import();
                new gltf.GLTFAccessorImporter(this).import();
                new gltf.GLTFMaterialImporter(this).import();
                new gltf.GLTFMeshImporter(this).import();
                new gltf.GLTFCameraImporter(this).import();
                new gltf.GLTFSkinImporter(this).import();
                new gltf.GLTFNodeImporter(this).import();
                new gltf.GLTFSceneImporter(this).import();
                new gltf.GLTFAnimationImporter(this).import();
                var gtfl = {
                    scene: this._scenes[0],
                    scenes: this._scenes,
                    cameras: this._cameras,
                    animations: this._animations,
                    animationsRelatives: this._animationsRelatives,
                    asset: this.json.asset,
                    parser: this,
                    userData: {}
                };
                this._onCompleteCb(gtfl);
            };
            /**
             * Marks the special nodes/meshes in json for efficient parse.
             */
            GLTFParser.prototype._markDefs = function () {
                var nodeDefs = this.json.nodes || [];
                var skinDefs = this.json.skins || [];
                var meshDefs = this.json.meshes || [];
                var meshReferences = {};
                var meshUses = {};
                // Nothing in the node definition indicates whether it is a Bone or an
                // Object3D. Use the skins' joint references to mark bones.
                for (var skinIndex = 0, skinLength = skinDefs.length; skinIndex < skinLength; skinIndex++) {
                    var joints = skinDefs[skinIndex].joints;
                    for (var i = 0, il = joints.length; i < il; i++) {
                        nodeDefs[joints[i]].isBone = true;
                    }
                }
                // Meshes can (and should) be reused by multiple nodes in a glTF asset. To
                // avoid having more than one THREE.Mesh with the same name, count
                // references and rename instances below.
                //
                // Example: CesiumMilkTruck sample model reuses "Wheel" meshes.
                for (var nodeIndex = 0, nodeLength = nodeDefs.length; nodeIndex < nodeLength; nodeIndex++) {
                    var nodeDef = nodeDefs[nodeIndex];
                    if (nodeDef.mesh !== undefined) {
                        if (meshReferences[nodeDef.mesh] === undefined) {
                            meshReferences[nodeDef.mesh] = meshUses[nodeDef.mesh] = 0;
                        }
                        meshReferences[nodeDef.mesh]++;
                        // Nothing in the mesh definition indicates whether it is
                        // a SkinnedMesh or Mesh. Use the node's mesh reference
                        // to mark SkinnedMesh if node has skin.
                        if (nodeDef.skin !== undefined) {
                            meshDefs[nodeDef.mesh].isSkinnedMesh = true;
                        }
                    }
                }
                this.json.meshReferences = meshReferences;
                this.json.meshUses = meshUses;
            };
            GLTFParser.prototype.assignTexture = function (textureInfo, materialParams, textureName) {
                var textureIndex = textureInfo.index;
                var texture = this._textures[textureIndex];
                if (textureInfo.extensions != null) {
                    if (textureInfo.extensions["EXT_texture_transform"] != null) {
                        texture = this.extensions.EXT_texture_transform.apply(texture, textureInfo.extensions["EXT_texture_transform"]);
                        //console.log("apply texture transform to " + textureName + " / " + JSON.stringify(textureInfo) + " / " + texture.image.src);
                    }
                }
                materialParams[textureName] = texture;
            };
            ;
            GLTFParser.prototype.getBufferView = function (index) {
                return this._bufferViews[index];
            };
            return GLTFParser;
        }());
        gltf.GLTFParser = GLTFParser;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        /* CONSTANTS */
        gltf.WEBGL_CONSTANTS = {
            FLOAT: 5126,
            //FLOAT_MAT2: 35674,
            FLOAT_MAT3: 35675,
            FLOAT_MAT4: 35676,
            FLOAT_VEC2: 35664,
            FLOAT_VEC3: 35665,
            FLOAT_VEC4: 35666,
            LINEAR: 9729,
            REPEAT: 10497,
            SAMPLER_2D: 35678,
            POINTS: 0,
            LINES: 1,
            LINE_LOOP: 2,
            LINE_STRIP: 3,
            TRIANGLES: 4,
            TRIANGLE_STRIP: 5,
            TRIANGLE_FAN: 6,
            UNSIGNED_BYTE: 5121,
            UNSIGNED_SHORT: 5123
        };
        gltf.WEBGL_TYPE = {
            5126: Number,
            //35674: THREE.Matrix2,
            35675: THREE.Matrix3,
            35676: THREE.Matrix4,
            35664: THREE.Vector2,
            35665: THREE.Vector3,
            35666: THREE.Vector4,
            35678: THREE.Texture
        };
        gltf.WEBGL_COMPONENT_TYPES = {
            5120: Int8Array,
            5121: Uint8Array,
            5122: Int16Array,
            5123: Uint16Array,
            5125: Uint32Array,
            5126: Float32Array
        };
        gltf.WEBGL_FILTERS = {
            9728: THREE.NearestFilter,
            9729: THREE.LinearFilter,
            9984: THREE.NearestMipMapNearestFilter,
            9985: THREE.LinearMipMapNearestFilter,
            9986: THREE.NearestMipMapLinearFilter,
            9987: THREE.LinearMipMapLinearFilter
        };
        gltf.WEBGL_WRAPPINGS = {
            33071: THREE.ClampToEdgeWrapping,
            33648: THREE.MirroredRepeatWrapping,
            10497: THREE.RepeatWrapping
        };
        gltf.WEBGL_SIDES = {
            1028: THREE.BackSide,
            1029: THREE.FrontSide // Culling back
            //1032: THREE.NoSide   // Culling front and back, what to do?
        };
        gltf.WEBGL_DEPTH_FUNCS = {
            512: THREE.NeverDepth,
            513: THREE.LessDepth,
            514: THREE.EqualDepth,
            515: THREE.LessEqualDepth,
            516: THREE.GreaterEqualDepth,
            517: THREE.NotEqualDepth,
            518: THREE.GreaterEqualDepth,
            519: THREE.AlwaysDepth
        };
        gltf.WEBGL_BLEND_EQUATIONS = {
            32774: THREE.AddEquation,
            32778: THREE.SubtractEquation,
            32779: THREE.ReverseSubtractEquation
        };
        gltf.WEBGL_BLEND_FUNCS = {
            0: THREE.ZeroFactor,
            1: THREE.OneFactor,
            768: THREE.SrcColorFactor,
            769: THREE.OneMinusSrcColorFactor,
            770: THREE.SrcAlphaFactor,
            771: THREE.OneMinusSrcAlphaFactor,
            772: THREE.DstAlphaFactor,
            773: THREE.OneMinusDstAlphaFactor,
            774: THREE.DstColorFactor,
            775: THREE.OneMinusDstColorFactor,
            776: THREE.SrcAlphaSaturateFactor
            // The followings are not supported by Three.js yet
            //32769: CONSTANT_COLOR,
            //32770: ONE_MINUS_CONSTANT_COLOR,
            //32771: CONSTANT_ALPHA,
            //32772: ONE_MINUS_CONSTANT_COLOR
        };
        gltf.WEBGL_TYPE_SIZES = {
            'SCALAR': 1,
            'VEC2': 2,
            'VEC3': 3,
            'VEC4': 4,
            'MAT2': 4,
            'MAT3': 9,
            'MAT4': 16
        };
        gltf.ATTRIBUTES = {
            POSITION: 'position',
            NORMAL: 'normal',
            TEXCOORD_0: 'uv',
            TEXCOORD_1: 'uv2',
            COLOR_0: 'color',
            WEIGHTS_0: 'skinWeight',
            JOINTS_0: 'skinIndex',
        };
        gltf.PATH_PROPERTIES = {
            scale: 'scale',
            translation: 'position',
            rotation: 'quaternion',
            weights: 'morphTargetInfluences'
        };
        gltf.INTERPOLATION = {
            CUBICSPLINE: THREE.InterpolateSmooth,
            // KeyframeTrack.optimize() can't handle glTF Cubic Spline output values layout,
            // using THREE.InterpolateSmooth for KeyframeTrack instantiation to prevent optimization.
            // See KeyframeTrack.optimize() for the detail.
            LINEAR: THREE.InterpolateLinear,
            STEP: THREE.InterpolateDiscrete
        };
        gltf.STATES_ENABLES = {
            2884: 'CULL_FACE',
            2929: 'DEPTH_TEST',
            3042: 'BLEND',
            3089: 'SCISSOR_TEST',
            32823: 'POLYGON_OFFSET_FILL',
            32926: 'SAMPLE_ALPHA_TO_COVERAGE'
        };
        gltf.ALPHA_MODES = {
            OPAQUE: 'OPAQUE',
            MASK: 'MASK',
            BLEND: 'BLEND'
        };
        gltf.MIME_TYPE_FORMATS = {
            'image/png': THREE.RGBAFormat,
            'image/jpeg': THREE.RGBFormat
        };
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        function resolveURL(url, path) {
            // Invalid URL
            if (typeof url !== 'string' || url === '')
                return '';
            // Absolute URL http://,https://,//
            if (/^(https?:)?\/\//i.test(url))
                return url;
            // Data URI
            if (/^data:.*,.*$/i.test(url))
                return url;
            // Blob URL
            if (/^blob:.*$/i.test(url))
                return url;
            // Relative URL
            return path + url;
        }
        gltf.resolveURL = resolveURL;
        /**
         * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#default-material
         */
        function createDefaultMaterial() {
            return new THREE.MeshStandardMaterial({
                color: 0xFFFFFF,
                emissive: 0x000000,
                metalness: 1,
                roughness: 1,
                transparent: false,
                depthTest: true,
                side: THREE.FrontSide
            });
        }
        gltf.createDefaultMaterial = createDefaultMaterial;
        function isPrimitiveEqual(a, b) {
            if (a.indices !== b.indices) {
                return false;
            }
            return isObjectEqual(a.attributes, b.attributes);
        }
        function isObjectEqual(a, b) {
            if (Object.keys(a).length !== Object.keys(b).length)
                return false;
            for (var key in a) {
                if (a[key] !== b[key])
                    return false;
            }
            return true;
        }
        function getCachedGeometry(cache, newPrimitive) {
            for (var i = 0; i < cache.length; i++) {
                var cached = cache[i];
                if (isPrimitiveEqual(cached.primitive, newPrimitive))
                    return cached;
            }
            return null;
        }
        gltf.getCachedGeometry = getCachedGeometry;
        function addPrimitiveAttributes(geometry, primitiveDef, accessors) {
            var attributes = primitiveDef.attributes;
            for (var gltfAttributeName in attributes) {
                var threeAttributeName = gltf.ATTRIBUTES[gltfAttributeName];
                var bufferAttribute = accessors[attributes[gltfAttributeName]];
                // Skip attributes already provided by e.g. Draco extension.
                if (!threeAttributeName)
                    continue;
                if (threeAttributeName in geometry.attributes)
                    continue;
                // if (threeAttributeName == ATTRIBUTES.NORMAL) continue;
                // if (threeAttributeName == ATTRIBUTES.TEXCOORD_1) continue;
                geometry.addAttribute(threeAttributeName, bufferAttribute);
            }
            if (primitiveDef.indices !== undefined && !geometry.index) {
                var index = accessors[primitiveDef.indices];
                if (index instanceof THREE.BufferAttribute) {
                    geometry.setIndex(index);
                }
                else {
                    plume.logger.error("TODO Interleaved buffer ??? ");
                }
            }
            if (primitiveDef.targets !== undefined) {
                addMorphTargets(geometry, primitiveDef.targets, accessors);
            }
            assignExtrasToUserData(geometry, primitiveDef);
        }
        gltf.addPrimitiveAttributes = addPrimitiveAttributes;
        function addMorphTargets(geometry, targets, accessors) {
            var hasMorphPosition = false;
            var hasMorphNormal = false;
            for (var i = 0; i < targets.length; i++) {
                var target = targets[i];
                if (target.POSITION !== undefined)
                    hasMorphPosition = true;
                if (target.NORMAL !== undefined)
                    hasMorphNormal = true;
                if (hasMorphPosition && hasMorphNormal)
                    break;
            }
            if (!hasMorphPosition && !hasMorphNormal)
                return;
            var morphPositions = [];
            var morphNormals = [];
            for (var i = 0; i < targets.length; i++) {
                var target = targets[i];
                var attributeName = 'morphTarget' + i;
                if (hasMorphPosition) {
                    // Three.js morph position is absolute value. The formula is
                    //   basePosition
                    //     + weight0 * ( morphPosition0 - basePosition )
                    //     + weight1 * ( morphPosition1 - basePosition )
                    //     ...
                    // while the glTF one is relative
                    //   basePosition
                    //     + weight0 * glTFmorphPosition0
                    //     + weight1 * glTFmorphPosition1
                    //     ...
                    // then we need to convert from relative to absolute here.
                    var positionAttribute = void 0;
                    if (target.POSITION !== undefined) {
                        // Cloning not to pollute original accessor
                        positionAttribute = cloneBufferAttribute(accessors[target.POSITION]);
                        positionAttribute["name"] = attributeName;
                        var position = geometry.attributes.position;
                        for (var j = 0, jl = positionAttribute.count; j < jl; j++) {
                            positionAttribute.setXYZ(j, positionAttribute.getX(j) + position.getX(j), positionAttribute.getY(j) + position.getY(j), positionAttribute.getZ(j) + position.getZ(j));
                        }
                    }
                    else {
                        positionAttribute = geometry.attributes.position;
                    }
                    morphPositions.push(positionAttribute);
                }
                if (hasMorphNormal) {
                    var normalAttribute = void 0;
                    if (target.NORMAL !== undefined) {
                        normalAttribute = cloneBufferAttribute(accessors[target.NORMAL]);
                        normalAttribute["name"] = attributeName;
                        var normal = geometry.attributes.normal;
                        for (var j = 0, jl = normalAttribute.count; j < jl; j++) {
                            normalAttribute.setXYZ(j, normalAttribute.getX(j) + normal.getX(j), normalAttribute.getY(j) + normal.getY(j), normalAttribute.getZ(j) + normal.getZ(j));
                        }
                    }
                    else {
                        normalAttribute = geometry.attributes.normal;
                    }
                    morphNormals.push(normalAttribute);
                }
            }
            if (hasMorphPosition)
                geometry.morphAttributes.position = morphPositions;
            if (hasMorphNormal)
                geometry.morphAttributes.normal = morphNormals;
        }
        function cloneBufferAttribute(attribute) {
            if (attribute instanceof THREE.InterleavedBufferAttribute) {
                var count = attribute.count;
                var itemSize = attribute.itemSize;
                var array = attribute.array.slice(0, count * itemSize);
                for (var i = 0; i < count; ++i) {
                    array[i] = attribute.getX(i);
                    if (itemSize >= 2)
                        array[i + 1] = attribute.getY(i);
                    if (itemSize >= 3)
                        array[i + 2] = attribute.getZ(i);
                    if (itemSize >= 4)
                        array[i + 3] = attribute.getW(i);
                }
                return new THREE.BufferAttribute(array, itemSize, attribute.normalized);
            }
            return attribute.clone();
        }
        function updateMorphTargets(mesh, meshDef) {
            mesh.updateMorphTargets();
            if (meshDef.weights !== undefined) {
                for (var i = 0; i < meshDef.weights.length; i++) {
                    mesh.morphTargetInfluences[i] = meshDef.weights[i];
                }
            }
            // .extras has user-defined data, so check that .extras.targetNames is an array.
            if (meshDef.extras && Array.isArray(meshDef.extras.targetNames)) {
                var targetNames = meshDef.extras.targetNames;
                if (mesh.morphTargetInfluences.length === targetNames.length) {
                    mesh.morphTargetDictionary = {};
                    for (var i = 0; i < targetNames.length; i++) {
                        mesh.morphTargetDictionary[targetNames[i]] = i;
                    }
                }
                else {
                    plume.logger.warn('GLTFLoader: Invalid extras.targetNames length. Ignoring names.');
                }
            }
        }
        gltf.updateMorphTargets = updateMorphTargets;
        function assignExtrasToUserData(object, gltfDef) {
            if (gltfDef.extras !== undefined) {
                if (typeof gltfDef.extras === 'object') {
                    object.userData = gltfDef.extras;
                }
                else {
                    plume.logger.warn('GLTFLoader: Ignoring primitive type .extras, ' + gltfDef.extras);
                }
            }
        }
        gltf.assignExtrasToUserData = assignExtrasToUserData;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        gltf.BINARY_EXTENSION_HEADER_MAGIC = 'glTF';
        var BINARY_EXTENSION_HEADER_LENGTH = 12;
        var BINARY_EXTENSION_CHUNK_TYPES = { JSON: 0x4E4F534A, BIN: 0x004E4942 };
        var GLTFBinaryExtension = /** @class */ (function () {
            function GLTFBinaryExtension(data) {
                this.name = "KHR_binary_glTF";
                this.content = null;
                this.body = null;
                var headerView = new DataView(data, 0, BINARY_EXTENSION_HEADER_LENGTH);
                this.header = {
                    magic: THREE.LoaderUtils.decodeText(new Uint8Array(data.slice(0, 4))),
                    version: headerView.getUint32(4, true),
                    length: headerView.getUint32(8, true)
                };
                if (this.header.magic !== gltf.BINARY_EXTENSION_HEADER_MAGIC) {
                    throw new Error('Unsupported glTF-Binary header.');
                }
                else if (this.header.version < 2.0) {
                    throw new Error('Legacy binary file detected. Use LegacyGLTFLoader instead.');
                }
                var chunkView = new DataView(data, BINARY_EXTENSION_HEADER_LENGTH);
                var chunkIndex = 0;
                while (chunkIndex < chunkView.byteLength) {
                    var chunkLength = chunkView.getUint32(chunkIndex, true);
                    chunkIndex += 4;
                    var chunkType = chunkView.getUint32(chunkIndex, true);
                    chunkIndex += 4;
                    if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.JSON) {
                        var contentArray = new Uint8Array(data, BINARY_EXTENSION_HEADER_LENGTH + chunkIndex, chunkLength);
                        this.content = THREE.LoaderUtils.decodeText(contentArray);
                    }
                    else if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.BIN) {
                        var byteOffset = BINARY_EXTENSION_HEADER_LENGTH + chunkIndex;
                        this.body = data.slice(byteOffset, byteOffset + chunkLength);
                    }
                    // Clients must ignore chunks with unknown types.
                    chunkIndex += chunkLength;
                }
                if (this.content === null) {
                    throw new Error('JSON content not found.');
                }
            }
            return GLTFBinaryExtension;
        }());
        gltf.GLTFBinaryExtension = GLTFBinaryExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFDracoMeshCompressionExtension = /** @class */ (function () {
            function GLTFDracoMeshCompressionExtension(json, dracoLoader) {
                this.json = json;
                this.dracoLoader = dracoLoader;
                if (!dracoLoader) {
                    throw new Error('No DRACOLoader instance provided.');
                }
                this.name = "KHR_draco_mesh_compression";
            }
            GLTFDracoMeshCompressionExtension.prototype.initialize = function (onComplete) {
                var self = this;
                THREE["DRACOLoader"].getDecoderModule().then(function (module) {
                    self.dracoDecoder = module.decoder;
                    onComplete();
                });
            };
            GLTFDracoMeshCompressionExtension.prototype.decodePrimitive = function (primitive, parser) {
                var json = this.json;
                var dracoLoader = this.dracoLoader;
                var bufferViewIndex = primitive.extensions[this.name].bufferView;
                var gltfAttributeMap = primitive.extensions[this.name].attributes;
                var threeAttributeMap = {};
                var attributeNormalizedMap = {};
                var attributeTypeMap = {};
                for (var attributeName in gltfAttributeMap) {
                    if (!(attributeName in gltf.ATTRIBUTES))
                        continue;
                    threeAttributeMap[gltf.ATTRIBUTES[attributeName]] = gltfAttributeMap[attributeName];
                }
                for (var attributeName in primitive.attributes) {
                    if (gltf.ATTRIBUTES[attributeName] !== undefined && gltfAttributeMap[attributeName] !== undefined) {
                        var accessorDef = json.accessors[primitive.attributes[attributeName]];
                        var componentType = gltf.WEBGL_COMPONENT_TYPES[accessorDef.componentType];
                        attributeTypeMap[gltf.ATTRIBUTES[attributeName]] = componentType;
                        attributeNormalizedMap[gltf.ATTRIBUTES[attributeName]] = accessorDef.normalized === true;
                    }
                }
                var bufferView = parser.getBufferView(bufferViewIndex);
                // let geometry0: THREE.BufferGeometry;
                // dracoLoader.decodeDracoFile(bufferView, function (geometry: any) {
                //     for (let attributeName in geometry.attributes) {
                //         let attribute = geometry.attributes[attributeName];
                //         let normalized = attributeNormalizedMap[attributeName];
                //         if (normalized !== undefined) attribute.normalized = normalized;
                //     }
                //     geometry0 = geometry;
                // }, threeAttributeMap, attributeTypeMap);
                var geometry0;
                dracoLoader.decodeDracoFileInternal(bufferView, this.dracoDecoder, function (geometry) {
                    for (var attributeName in geometry.attributes) {
                        var attribute = geometry.attributes[attributeName];
                        var normalized = attributeNormalizedMap[attributeName];
                        if (normalized !== undefined)
                            attribute.normalized = normalized;
                    }
                    geometry0 = geometry;
                }, threeAttributeMap || {}, attributeTypeMap || {});
                return geometry0;
            };
            ;
            return GLTFDracoMeshCompressionExtension;
        }());
        gltf.GLTFDracoMeshCompressionExtension = GLTFDracoMeshCompressionExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFLightMapsExtension = /** @class */ (function () {
            function GLTFLightMapsExtension(json) {
                this._initialized = false;
                this._processedGeometries = [];
                this._processedMaterial = new plume.HashMap();
                this.name = "GB_lightmaps";
                this.lightmaps = [];
                var extension = (json.extensions && json.extensions.GB_lightmaps) || {};
                var lightmapDefs = extension.lightmaps || [];
                for (var i = 0; i < lightmapDefs.length; i++) {
                    var lightmapDef = lightmapDefs[i];
                    this.lightmaps.push(lightmapDef);
                }
            }
            GLTFLightMapsExtension.prototype._initTextures = function () {
                if (this._initialized)
                    return;
                this._initialized = true;
                for (var i = 0; i < this.lightmaps.length; i++) {
                    var textureIdx = this.lightmaps[i].texture.index;
                    var texture = this.parser._textures[textureIdx];
                    // texture.encoding = THREE.GammaEncoding;
                }
            };
            GLTFLightMapsExtension.prototype.applyLightmap = function (node, lightmapDef) {
                var textureIdx = this._getTexture(lightmapDef);
                if (textureIdx == null) {
                    return;
                }
                if (!this._initialized) {
                    this._initTextures();
                }
                if (node instanceof THREE.Mesh) {
                    this._applyLightmapOnMesh(node, lightmapDef);
                }
                else if (node instanceof THREE.Group) {
                    // TODO check..
                    for (var c = 0; c < node.children.length; c++) {
                        var child = node.children[c];
                        if (child instanceof THREE.Mesh) {
                            this._applyLightmapOnMesh(child, lightmapDef);
                        }
                        else {
                            plume.logger.warn("Can not apply lightmap on node " + node.type + " (" + node.name + ")");
                        }
                    }
                }
                else {
                    plume.logger.warn("Can not apply lightmap on node " + node.type + " (" + node.name + ")");
                }
            };
            GLTFLightMapsExtension.prototype._applyLightmapOnMesh = function (node, lightmapDef) {
                var textureIdx = this._getTexture(lightmapDef);
                if (textureIdx == null) {
                    return;
                }
                var material = node.material;
                if (material instanceof THREE.MeshStandardMaterial || material instanceof THREE.MeshLambertMaterial || material instanceof THREE.MeshPhongMaterial || material instanceof THREE.ShaderMaterial || material instanceof THREE.RawShaderMaterial) {
                    // if (material instanceof THREE.MeshStandardMaterial || material instanceof THREE.MeshLambertMaterial || material instanceof THREE.MeshPhongMaterial) {
                    var texture = this.parser._textures[textureIdx];
                    var key = material.uuid + "-" + textureIdx;
                    var mat = this._processedMaterial.get(key);
                    if (mat == null) {
                        // copy material and assign texture
                        material = material.clone();
                        material.lightMap = texture;
                        node.material = material;
                        this._processedMaterial.put(key, mat);
                    }
                    else {
                        node.material = mat;
                    }
                    this._setUV2(node, lightmapDef.uvScale, lightmapDef.uvOffset);
                }
                else {
                    plume.logger.warn("Can not apply lightmap on material " + node.material);
                }
            };
            GLTFLightMapsExtension.prototype._getTexture = function (lightmapDef) {
                var idx = lightmapDef.lightmap;
                if (idx >= this.lightmaps.length) {
                    plume.logger.warn("Trying to get data from unregistered lightmap " + idx);
                    return null;
                }
                var lightmap = this.lightmaps[idx];
                return lightmap.texture.index;
            };
            GLTFLightMapsExtension.prototype._setUV2 = function (node, uvScale, uvOffset) {
                var geometry = node.geometry;
                var key = geometry.uuid + "#" + uvScale.join(",") + "#" + uvOffset.join(",");
                var idx = this._processedGeometries.indexOf(key);
                if (idx >= 0) {
                    // reuse uv2 already computed
                    return;
                }
                else {
                    // this._processedGeometries.push(key);
                }
                // else, we nned to clone geometry to set new uv2
                geometry = geometry.clone();
                node.geometry = geometry;
                var uv2 = geometry.getAttribute("uv2");
                if (uv2 != null) {
                    // Use uv2 from unity (fix to match atlas)
                    this._fixUv2(node, uvScale, uvOffset);
                }
                else {
                    // No uv2, use uv1 but fix to match atlas
                    this._copyUvToUv2(node, uvScale, uvOffset);
                }
            };
            GLTFLightMapsExtension.prototype._fixUv2 = function (node, uvScale, uvOffset) {
                var geometry = node.geometry;
                var uv = geometry.getAttribute("uv");
                var uv2 = geometry.getAttribute("uv2");
                for (var i = 0; i < uv.count; i++) {
                    // (1 - uv2.getY(i)) => flip to be in unity coordinate
                    // 1 - (unityy) => flip to be in three coordinate
                    var x = uv2.getX(i) * uvScale[0] + uvOffset[0];
                    var y = 1 - ((1 - uv2.getY(i)) * uvScale[1] + uvOffset[1]);
                    uv2.setXY(i, x, y);
                }
            };
            GLTFLightMapsExtension.prototype._copyUvToUv2 = function (node, uvScale, uvOffset) {
                var geometry = node.geometry;
                var uv = geometry.getAttribute("uv");
                var uv2 = new THREE.BufferAttribute(new Float32Array(uv.count * 2), 2);
                for (var i = 0; i < uv.count; i++) {
                    var x = uv.getX(i) * uvScale[0] + uvOffset[0];
                    var y = 1 - ((1 - uv.getY(i)) * uvScale[1] + uvOffset[1]);
                    uv2.setXY(i, x, y);
                }
                geometry.addAttribute("uv2", uv2);
            };
            return GLTFLightMapsExtension;
        }());
        gltf.GLTFLightMapsExtension = GLTFLightMapsExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFLightsExtension = /** @class */ (function () {
            function GLTFLightsExtension(json) {
                this.name = "KHR_lights_punctual";
                this.lights = [];
                var extension = (json.extensions && json.extensions.KHR_lights_punctual) || {};
                var lightDefs = extension.lights || [];
                for (var i = 0; i < lightDefs.length; i++) {
                    var lightDef = lightDefs[i];
                    var lightNode = void 0;
                    var color = new THREE.Color(0xffffff);
                    if (lightDef.color !== undefined)
                        color.fromArray(lightDef.color);
                    var range = lightDef.range !== undefined ? lightDef.range : 0;
                    switch (lightDef.type) {
                        case 'directional':
                            lightNode = new THREE.DirectionalLight(color);
                            lightNode.target.position.set(0, 0, 1);
                            lightNode.add(lightNode.target);
                            break;
                        case 'point':
                            lightNode = new THREE.PointLight(color);
                            lightNode.decay = 2;
                            lightNode.distance = range;
                            break;
                        case 'spot':
                            lightNode = new THREE.SpotLight(color);
                            lightNode.distance = range;
                            lightNode.decay = 2;
                            // Handle spotlight properties.
                            lightDef.spot = lightDef.spot || {};
                            lightDef.spot.innerConeAngle = lightDef.spot.innerConeAngle !== undefined ? lightDef.spot.innerConeAngle : 0;
                            lightDef.spot.outerConeAngle = lightDef.spot.outerConeAngle !== undefined ? lightDef.spot.outerConeAngle : Math.PI / 4.0;
                            lightNode.angle = lightDef.spot.outerConeAngle;
                            lightNode.penumbra = 1.0 - lightDef.spot.innerConeAngle / lightDef.spot.outerConeAngle;
                            lightNode.target.position.set(0, 0, 1);
                            lightNode.add(lightNode.target);
                            break;
                        default:
                            throw new Error('GLTFLoader: Unexpected light type, "' + lightDef.type + '".');
                    }
                    if (lightDef.intensity !== undefined)
                        lightNode.intensity = lightDef.intensity;
                    lightNode.name = lightDef.name || ('light_' + i);
                    this.lights.push(lightNode);
                }
            }
            return GLTFLightsExtension;
        }());
        gltf.GLTFLightsExtension = GLTFLightsExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFMaterialsPbrSpecularGlossinessExtension = /** @class */ (function () {
            function GLTFMaterialsPbrSpecularGlossinessExtension() {
                this.specularGlossinessParams = [
                    'color',
                    'map',
                    'lightMap',
                    'lightMapIntensity',
                    'aoMap',
                    'aoMapIntensity',
                    'emissive',
                    'emissiveIntensity',
                    'emissiveMap',
                    'bumpMap',
                    'bumpScale',
                    'normalMap',
                    'displacementMap',
                    'displacementScale',
                    'displacementBias',
                    'specularMap',
                    'specular',
                    'glossinessMap',
                    'glossiness',
                    'alphaMap',
                    'envMap',
                    'envMapIntensity',
                    'refractionRatio',
                ];
                this.name = "KHR_materials_pbrSpecularGlossiness";
            }
            GLTFMaterialsPbrSpecularGlossinessExtension.prototype.getMaterialType = function (materialDef) {
                return THREE.ShaderMaterial;
            };
            GLTFMaterialsPbrSpecularGlossinessExtension.prototype.extendParams = function (params, material, parser) {
                var pbrSpecularGlossiness = material.extensions[this.name];
                var shader = THREE.ShaderLib['standard'];
                var uniforms = THREE.UniformsUtils.clone(shader.uniforms);
                var specularMapParsFragmentChunk = [
                    '#ifdef USE_SPECULARMAP',
                    '	uniform sampler2D specularMap;',
                    '#endif'
                ].join('\n');
                var glossinessMapParsFragmentChunk = [
                    '#ifdef USE_GLOSSINESSMAP',
                    '	uniform sampler2D glossinessMap;',
                    '#endif'
                ].join('\n');
                var specularMapFragmentChunk = [
                    'vec3 specularFactor = specular;',
                    '#ifdef USE_SPECULARMAP',
                    '	vec4 texelSpecular = texture2D( specularMap, vUv );',
                    '	texelSpecular = sRGBToLinear( texelSpecular );',
                    '	// reads channel RGB, compatible with a glTF Specular-Glossiness (RGBA) texture',
                    '	specularFactor *= texelSpecular.rgb;',
                    '#endif'
                ].join('\n');
                var glossinessMapFragmentChunk = [
                    'float glossinessFactor = glossiness;',
                    '#ifdef USE_GLOSSINESSMAP',
                    '	vec4 texelGlossiness = texture2D( glossinessMap, vUv );',
                    '	// reads channel A, compatible with a glTF Specular-Glossiness (RGBA) texture',
                    '	glossinessFactor *= texelGlossiness.a;',
                    '#endif'
                ].join('\n');
                var lightPhysicalFragmentChunk = [
                    'PhysicalMaterial material;',
                    'material.diffuseColor = diffuseColor.rgb;',
                    'material.specularRoughness = clamp( 1.0 - glossinessFactor, 0.04, 1.0 );',
                    'material.specularColor = specularFactor.rgb;',
                ].join('\n');
                var fragmentShader = shader.fragmentShader
                    .replace('uniform float roughness;', 'uniform vec3 specular;')
                    .replace('uniform float metalness;', 'uniform float glossiness;')
                    .replace('#include <roughnessmap_pars_fragment>', specularMapParsFragmentChunk)
                    .replace('#include <metalnessmap_pars_fragment>', glossinessMapParsFragmentChunk)
                    .replace('#include <roughnessmap_fragment>', specularMapFragmentChunk)
                    .replace('#include <metalnessmap_fragment>', glossinessMapFragmentChunk)
                    .replace('#include <lights_physical_fragment>', lightPhysicalFragmentChunk);
                delete uniforms.roughness;
                delete uniforms.metalness;
                delete uniforms.roughnessMap;
                delete uniforms.metalnessMap;
                uniforms.specular = { value: new THREE.Color().setHex(0x111111) };
                uniforms.glossiness = { value: 0.5 };
                uniforms.specularMap = { value: null };
                uniforms.glossinessMap = { value: null };
                params.vertexShader = shader.vertexShader;
                params.fragmentShader = fragmentShader;
                params.uniforms = uniforms;
                params.defines = { 'STANDARD': '' };
                params.color = new THREE.Color(1.0, 1.0, 1.0);
                params.opacity = 1.0;
                if (Array.isArray(pbrSpecularGlossiness.diffuseFactor)) {
                    var array = pbrSpecularGlossiness.diffuseFactor;
                    params.color.fromArray(array);
                    params.opacity = array[3];
                }
                if (pbrSpecularGlossiness.diffuseTexture !== undefined) {
                    parser.assignTexture(params, 'map', pbrSpecularGlossiness.diffuseTexture.index);
                }
                params.emissive = new THREE.Color(0.0, 0.0, 0.0);
                params.glossiness = pbrSpecularGlossiness.glossinessFactor !== undefined ? pbrSpecularGlossiness.glossinessFactor : 1.0;
                params.specular = new THREE.Color(1.0, 1.0, 1.0);
                if (Array.isArray(pbrSpecularGlossiness.specularFactor)) {
                    params.specular.fromArray(pbrSpecularGlossiness.specularFactor);
                }
                if (pbrSpecularGlossiness.specularGlossinessTexture !== undefined) {
                    var specGlossIndex = pbrSpecularGlossiness.specularGlossinessTexture.index;
                    parser.assignTexture(params, 'glossinessMap', specGlossIndex);
                    parser.assignTexture(params, 'specularMap', specGlossIndex);
                }
            };
            GLTFMaterialsPbrSpecularGlossinessExtension.prototype.createMaterial = function (params) {
                // setup material properties based on MeshStandardMaterial for Specular-Glossiness
                var mat = new THREE.ShaderMaterial({
                    defines: params.defines,
                    vertexShader: params.vertexShader,
                    fragmentShader: params.fragmentShader,
                    uniforms: params.uniforms,
                    fog: true,
                    lights: true,
                    opacity: params.opacity,
                    transparent: params.transparent
                });
                var material = mat;
                material.isGLTFSpecularGlossinessMaterial = true;
                material.color = params.color;
                material.map = params.map === undefined ? null : params.map;
                material.lightMap = null;
                material.lightMapIntensity = 1.0;
                material.aoMap = params.aoMap === undefined ? null : params.aoMap;
                material.aoMapIntensity = 1.0;
                material.emissive = params.emissive;
                material.emissiveIntensity = 1.0;
                material.emissiveMap = params.emissiveMap === undefined ? null : params.emissiveMap;
                material.bumpMap = params.bumpMap === undefined ? null : params.bumpMap;
                material.bumpScale = 1;
                material.normalMap = params.normalMap === undefined ? null : params.normalMap;
                if (params.normalScale)
                    material.normalScale = params.normalScale;
                material.displacementMap = null;
                material.displacementScale = 1;
                material.displacementBias = 0;
                material.specularMap = params.specularMap === undefined ? null : params.specularMap;
                material.specular = params.specular;
                material.glossinessMap = params.glossinessMap === undefined ? null : params.glossinessMap;
                material.glossiness = params.glossiness;
                material.alphaMap = null;
                material.envMap = params.envMap === undefined ? null : params.envMap;
                material.envMapIntensity = 1.0;
                material.refractionRatio = 0.98;
                material.extensions.derivatives = true;
                return material;
            };
            /**
             * Clones a GLTFSpecularGlossinessMaterial instance. The ShaderMaterial.copy() method can
             * copy only properties it knows about or inherits, and misses many properties that would
             * normally be defined by MeshStandardMaterial.
             *
             * This method allows GLTFSpecularGlossinessMaterials to be cloned in the process of
             * loading a glTF model, but cloning later (e.g. by the user) would require these changes
             * AND also updating `.onBeforeRender` on the parent mesh.
             *
             * @param  {THREE.ShaderMaterial} source
             * @return {THREE.ShaderMaterial}
             */
            GLTFMaterialsPbrSpecularGlossinessExtension.prototype.cloneMaterial = function (source) {
                var target = source.clone();
                target["isGLTFSpecularGlossinessMaterial"] = true;
                var params = this.specularGlossinessParams;
                for (var i = 0, il = params.length; i < il; i++) {
                    target[params[i]] = source[params[i]];
                }
                return target;
            };
            // Here's based on refreshUniformsCommon() and refreshUniformsStandard() in WebGLRenderer.
            GLTFMaterialsPbrSpecularGlossinessExtension.prototype.refreshUniforms = function (renderer, scene, camera, geometry, material, group) {
                if (material["isGLTFSpecularGlossinessMaterial"] !== true) {
                    return;
                }
                var uniforms = material.uniforms;
                var defines = material.defines;
                uniforms.opacity.value = material.opacity;
                uniforms.diffuse.value.copy(material.color);
                uniforms.emissive.value.copy(material.emissive).multiplyScalar(material.emissiveIntensity);
                uniforms.map.value = material.map;
                uniforms.specularMap.value = material.specularMap;
                uniforms.alphaMap.value = material.alphaMap;
                uniforms.lightMap.value = material.lightMap;
                uniforms.lightMapIntensity.value = material.lightMapIntensity;
                uniforms.aoMap.value = material.aoMap;
                uniforms.aoMapIntensity.value = material.aoMapIntensity;
                // uv repeat and offset setting priorities
                // 1. color map
                // 2. specular map
                // 3. normal map
                // 4. bump map
                // 5. alpha map
                // 6. emissive map
                var uvScaleMap;
                if (material.map) {
                    uvScaleMap = material.map;
                }
                else if (material.specularMap) {
                    uvScaleMap = material.specularMap;
                }
                else if (material.displacementMap) {
                    uvScaleMap = material.displacementMap;
                }
                else if (material.normalMap) {
                    uvScaleMap = material.normalMap;
                }
                else if (material.bumpMap) {
                    uvScaleMap = material.bumpMap;
                }
                else if (material.glossinessMap) {
                    uvScaleMap = material.glossinessMap;
                }
                else if (material.alphaMap) {
                    uvScaleMap = material.alphaMap;
                }
                else if (material.emissiveMap) {
                    uvScaleMap = material.emissiveMap;
                }
                if (uvScaleMap !== undefined) {
                    // backwards compatibility
                    if (uvScaleMap.isWebGLRenderTarget) {
                        uvScaleMap = uvScaleMap.texture;
                    }
                    if (uvScaleMap.matrixAutoUpdate === true) {
                        uvScaleMap.updateMatrix();
                    }
                    uniforms.uvTransform.value.copy(uvScaleMap.matrix);
                }
                uniforms.envMap.value = material.envMap;
                uniforms.envMapIntensity.value = material.envMapIntensity;
                uniforms.flipEnvMap.value = (material.envMap && material.envMap.isCubeTexture) ? -1 : 1;
                uniforms.refractionRatio.value = material.refractionRatio;
                uniforms.specular.value.copy(material.specular);
                uniforms.glossiness.value = material.glossiness;
                uniforms.glossinessMap.value = material.glossinessMap;
                uniforms.emissiveMap.value = material.emissiveMap;
                uniforms.bumpMap.value = material.bumpMap;
                uniforms.normalMap.value = material.normalMap;
                uniforms.displacementMap.value = material.displacementMap;
                uniforms.displacementScale.value = material.displacementScale;
                uniforms.displacementBias.value = material.displacementBias;
                if (uniforms.glossinessMap.value !== null && defines.USE_GLOSSINESSMAP === undefined) {
                    defines.USE_GLOSSINESSMAP = '';
                    // set USE_ROUGHNESSMAP to enable vUv
                    defines.USE_ROUGHNESSMAP = '';
                }
                if (uniforms.glossinessMap.value === null && defines.USE_GLOSSINESSMAP !== undefined) {
                    delete defines.USE_GLOSSINESSMAP;
                    delete defines.USE_ROUGHNESSMAP;
                }
            };
            return GLTFMaterialsPbrSpecularGlossinessExtension;
        }());
        gltf.GLTFMaterialsPbrSpecularGlossinessExtension = GLTFMaterialsPbrSpecularGlossinessExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFMaterialsUnlitExtension = /** @class */ (function () {
            function GLTFMaterialsUnlitExtension(json) {
                this.json = json;
                this.name = "KHR_materials_unlit";
            }
            GLTFMaterialsUnlitExtension.prototype.getMaterialType = function (materialDef) {
                return THREE.MeshBasicMaterial;
            };
            ;
            GLTFMaterialsUnlitExtension.prototype.extendParams = function (params, material, parser) {
                params.color = new THREE.Color(1.0, 1.0, 1.0);
                params.opacity = 1.0;
                var metallicRoughness = material.pbrMetallicRoughness;
                if (metallicRoughness) {
                    if (Array.isArray(metallicRoughness.baseColorFactor)) {
                        var array = metallicRoughness.baseColorFactor;
                        params.color.fromArray(array);
                        params.opacity = array[3];
                    }
                    if (metallicRoughness.baseColorTexture !== undefined) {
                        parser.assignTexture(metallicRoughness.baseColorTexture, params, 'map');
                    }
                }
            };
            ;
            return GLTFMaterialsUnlitExtension;
        }());
        gltf.GLTFMaterialsUnlitExtension = GLTFMaterialsUnlitExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFSpritesExtension = /** @class */ (function () {
            function GLTFSpritesExtension(json) {
                // sprites: Array<THREE.Sprite> = [];
                this.sprites = [];
                this.name = "GB_sprites";
                this.spritesDef = [];
                var extension = (json.extensions && json.extensions.GB_sprites) || {};
                var spritesDefs = extension.sprites || [];
                for (var i = 0; i < spritesDefs.length; i++) {
                    var spriteDef = spritesDefs[i];
                    this.spritesDef.push(spriteDef);
                }
            }
            GLTFSpritesExtension.prototype.getSprite = function (idx) {
                var s = this.sprites[idx];
                if (s != null)
                    return s.clone();
                var def = this.spritesDef[idx];
                var texture = this.parser._textures[def.texture];
                var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
                s = new THREE.Mesh(new THREE.PlaneBufferGeometry(), material);
                this.sprites[idx] = s;
                return s.clone();
            };
            return GLTFSpritesExtension;
        }());
        gltf.GLTFSpritesExtension = GLTFSpritesExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFTextureDDSExtension = /** @class */ (function () {
            function GLTFTextureDDSExtension() {
                if (!THREE.DDSLoader) {
                    throw new Error('THREE.GLTFLoader: Attempting to load .dds texture without importing THREE.DDSLoader');
                }
                this.name = "MSFT_texture_dds";
                this.ddsLoader = new THREE.DDSLoader();
            }
            return GLTFTextureDDSExtension;
        }());
        gltf.GLTFTextureDDSExtension = GLTFTextureDDSExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFTextureTransformExtension = /** @class */ (function () {
            function GLTFTextureTransformExtension() {
                this.name = "EXT_texture_transform";
            }
            GLTFTextureTransformExtension.prototype.apply = function (texture, params) {
                texture = texture.clone();
                if (params.scale != null) {
                    // texture.repeat.set(1 / params.scale[0], 1 / params.scale[1]);
                    texture.repeat.set(params.scale[0], params.scale[1]);
                }
                if (params.offset != null) {
                    texture.offset.set(params.offset[0], params.offset[1]);
                }
                texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                texture.needsUpdate = true;
                return texture;
            };
            return GLTFTextureTransformExtension;
        }());
        gltf.GLTFTextureTransformExtension = GLTFTextureTransformExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        // Not a regular extension
        // Exporter write node data in "extra" field
        // Data read here
        var GLTFUnityExtension = /** @class */ (function () {
            function GLTFUnityExtension() {
            }
            GLTFUnityExtension.prototype.applyToNode = function (node, def) {
                if (def.static != null) {
                    if (def.static == true) {
                        node.matrixAutoUpdate = false;
                        node.updateMatrix();
                    }
                }
                if (def.activeSelf != null) {
                    if (def.activeSelf == false) {
                        node.visible = false;
                    }
                }
                if (def.sprite != null) {
                    var extension = this.parser.extensions.GB_sprites;
                    var sprite = extension.getSprite(def.sprite.id);
                    if (def.sprite.color != null) {
                        sprite.material.color = new THREE.Color().setRGB(def.sprite.color[0], def.sprite.color[1], def.sprite.color[2]);
                    }
                    sprite.material.side = THREE.DoubleSide;
                    node.add(sprite);
                    this.parser._textures[def.sprite.id];
                }
            };
            return GLTFUnityExtension;
        }());
        gltf.GLTFUnityExtension = GLTFUnityExtension;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFImporter = /** @class */ (function () {
            function GLTFImporter(parser) {
                this.parser = parser;
            }
            return GLTFImporter;
        }());
        gltf.GLTFImporter = GLTFImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFAccessorImporter = /** @class */ (function (_super) {
            __extends(GLTFAccessorImporter, _super);
            function GLTFAccessorImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFAccessorImporter.prototype.import = function () {
                // Create all accessors
                if (this.parser.json.accessors == null || this.parser.json.accessors.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.accessors.length; i++) {
                    var accessorsDef = this.parser.json.accessors[i];
                    this._loadAccessor(accessorsDef, i);
                }
            };
            // There is no "sparse" accessor in Unity exporter
            // There is no "byteStride" accessor in Unity exporter
            // We do not support it for now to simplify code
            GLTFAccessorImporter.prototype._loadAccessor = function (accessorDef, index) {
                if (accessorDef.bufferView === undefined) { //  && accessorDef.sparse === undefined
                    // Ignore empty accessors, which may be used to declare runtime
                    // information about attributes coming from another source (e.g. Draco compression extension).
                    this.parser._accessors[index] = null;
                    return;
                }
                var bufferView = this.parser._bufferViews[accessorDef.bufferView];
                var bufferViewDef = this.parser.json.bufferViews[accessorDef.bufferView];
                var itemSize = gltf.WEBGL_TYPE_SIZES[accessorDef.type];
                var arrayConstructor = gltf.WEBGL_COMPONENT_TYPES[accessorDef.componentType];
                var elementBytes = arrayConstructor.BYTES_PER_ELEMENT;
                var itemBytes = elementBytes * itemSize;
                var byteOffset = accessorDef.byteOffset || 0;
                var normalized = (accessorDef.normalized === true);
                // For VEC3: itemSize is 3, elementBytes is 4, itemBytes is 12.
                if (bufferViewDef.byteStride != null && bufferViewDef.byteStride != itemBytes) {
                    plume.logger.warn("Accessor with 'byteStride' (need to be implemented)");
                    this.parser._accessors[index] = null;
                    return;
                }
                var array;
                var bufferAttribute;
                if (bufferView === null) {
                    array = new arrayConstructor(accessorDef.count * itemSize);
                }
                else {
                    array = new arrayConstructor(bufferView, byteOffset, accessorDef.count * itemSize);
                }
                bufferAttribute = new THREE.BufferAttribute(array, itemSize, normalized);
                this.parser._accessors[index] = bufferAttribute;
            };
            return GLTFAccessorImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFAccessorImporter = GLTFAccessorImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        // THREE.QuaternionKeyframeTrack.prototype.InterpolantFactoryMethodSmooth = function (result) {
        //     return new THREE.QuaternionLinearInterpolant(this.times, this.values, this.getValueSize(), result);
        // }
        var GLTFAnimationImporter = /** @class */ (function (_super) {
            __extends(GLTFAnimationImporter, _super);
            function GLTFAnimationImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFAnimationImporter.prototype.import = function () {
                if (this.parser.json.animations == null || this.parser.json.animations.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.animations.length; i++) {
                    var animationDef = this.parser.json.animations[i];
                    var anim = this._loadAnimation(animationDef, i);
                    if (animationDef.extras != null && (animationDef.extras.root != null || animationDef.extras.othersRoot != null)) {
                        // GB custom
                        // Anim can be shared
                        var root = (animationDef.extras.root == null ? this.parser._scenes[0] : this.parser._nodes[animationDef.extras.root]);
                        var relative = gltf.GLTFAnimationsHelper.convertToRelative(root, anim);
                        this.parser._animationsRelatives.push({
                            root: root,
                            clip: relative,
                        });
                        if (animationDef.extras.othersRoot != null) {
                            for (var i_1 = 0; i_1 < animationDef.extras.othersRoot.length; i_1++) {
                                var otherId = animationDef.extras.othersRoot[i_1];
                                var other = this.parser._nodes[otherId];
                                this.parser._animationsRelatives.push({
                                    root: other,
                                    clip: relative,
                                });
                            }
                        }
                    }
                    else {
                        // Regular "absolute" animation
                        this.parser._animations.push(anim);
                    }
                }
            };
            GLTFAnimationImporter.prototype._loadAnimation = function (animationDef, index) {
                var tracks = [];
                var _loop_3 = function (i) {
                    var channel = animationDef.channels[i];
                    var sampler = animationDef.samplers[channel.sampler];
                    if (sampler == null) {
                        plume.logger.warn("No sampler " + channel.sampler + " in " + JSON.stringify(animationDef));
                        return "continue";
                    }
                    var target = channel.target;
                    var input = animationDef.parameters !== undefined ? animationDef.parameters[sampler.input] : sampler.input;
                    var output = animationDef.parameters !== undefined ? animationDef.parameters[sampler.output] : sampler.output;
                    var inputAccessor = this_3.parser._accessors[input];
                    var outputAccessor = this_3.parser._accessors[output];
                    var node = this_3.parser._nodes[target.node];
                    if (node == null) {
                        plume.logger.warn("Node " + target.node + " used in " + animationDef.name + " does not exists.");
                        return "continue";
                    }
                    node.updateMatrix();
                    node.matrixAutoUpdate = true;
                    var TypedKeyframeTrack = void 0;
                    switch (gltf.PATH_PROPERTIES[target.path]) {
                        case gltf.PATH_PROPERTIES.weights:
                            TypedKeyframeTrack = THREE.NumberKeyframeTrack;
                            break;
                        case gltf.PATH_PROPERTIES.rotation:
                            TypedKeyframeTrack = THREE.QuaternionKeyframeTrack;
                            break;
                        case gltf.PATH_PROPERTIES.translation:
                        case gltf.PATH_PROPERTIES.scale:
                        default:
                            TypedKeyframeTrack = THREE.VectorKeyframeTrack;
                            break;
                    }
                    var targetName = node.name ? node.name : node.uuid;
                    var interpolation = sampler.interpolation !== undefined ? gltf.INTERPOLATION[sampler.interpolation] : THREE.InterpolateLinear;
                    var targetNames = [];
                    if (gltf.PATH_PROPERTIES[target.path] === gltf.PATH_PROPERTIES.weights) {
                        // node can be THREE.Group here but
                        // PATH_PROPERTIES.weights(morphTargetInfluences) should be
                        // the property of a mesh object under group.
                        node.traverse(function (object) {
                            if (object["isMesh"] === true && object["morphTargetInfluences"]) {
                                targetNames.push(object.name ? object.name : object.uuid);
                            }
                        });
                    }
                    else {
                        targetNames.push(targetName);
                    }
                    // KeyframeTrack.optimize() will modify given 'times' and 'values'
                    // buffers before creating a truncated copy to keep. Because buffers may
                    // be reused by other tracks, make copies here.
                    for (var j = 0; j < targetNames.length; j++) {
                        var track = new TypedKeyframeTrack(targetNames[j] + '.' + gltf.PATH_PROPERTIES[target.path], THREE.AnimationUtils.arraySlice(inputAccessor.array, 0, undefined), THREE.AnimationUtils.arraySlice(outputAccessor.array, 0, undefined), interpolation);
                        // Here is the trick to enable custom interpolation.
                        // Overrides .createInterpolant in a factory method which creates custom interpolation.
                        if (sampler.interpolation === 'CUBICSPLINE') {
                            track.createInterpolant = function InterpolantFactoryMethodGLTFCubicSpline(result) {
                                // A CUBICSPLINE keyframe in glTF has three output values for each input value,
                                // representing inTangent, splineVertex, and outTangent. As a result, track.getValueSize()
                                // must be divided by three to get the interpolant's sampleSize argument.
                                return new GLTFCubicSplineInterpolant(this.times, this.values, this.getValueSize() / 3, result);
                            };
                            // Workaround, provide an alternate way to know if the interpolant type is cubis spline to track.
                            // track.getInterpolation() doesn't return valid value for custom interpolant.
                            // track.createInterpolant.isInterpolantFactoryMethodGLTFCubicSpline = true;
                        }
                        tracks.push(track);
                    }
                };
                var this_3 = this;
                for (var i = 0; i < animationDef.channels.length; i++) {
                    _loop_3(i);
                }
                var name = animationDef.name !== undefined ? animationDef.name : 'animation_' + index;
                return new THREE.AnimationClip(name, -1, tracks);
            };
            return GLTFAnimationImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFAnimationImporter = GLTFAnimationImporter;
        var GLTFCubicSplineInterpolant = /** @class */ (function (_super) {
            __extends(GLTFCubicSplineInterpolant, _super);
            function GLTFCubicSplineInterpolant() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFCubicSplineInterpolant.prototype.copySampleValue_ = function (index) {
                // Copies a sample value to the result buffer. See description of glTF
                // CUBICSPLINE values layout in interpolate_() function below.
                var result = this.resultBuffer;
                var values = this.sampleValues;
                var valueSize = this.valueSize;
                var offset = index * valueSize * 3 + valueSize;
                for (var i = 0; i !== valueSize; i++) {
                    result[i] = values[offset + i];
                }
                return result;
            };
            // GLTFCubicSplineInterpolant.prototype.beforeStart_ = GLTFCubicSplineInterpolant.prototype.copySampleValue_;
            // GLTFCubicSplineInterpolant.prototype.afterEnd_ = GLTFCubicSplineInterpolant.prototype.copySampleValue_;
            GLTFCubicSplineInterpolant.prototype.beforeStart_ = function (index) {
                this.copySampleValue_(index);
            };
            GLTFCubicSplineInterpolant.prototype.afterEnd_ = function (index) {
                this.copySampleValue_(index);
            };
            GLTFCubicSplineInterpolant.prototype.interpolate_ = function (i1, t0, t, t1) {
                var result = this.resultBuffer;
                var values = this.sampleValues;
                var stride = this.valueSize;
                var stride2 = stride * 2;
                var stride3 = stride * 3;
                var td = t1 - t0;
                var p = (t - t0) / td;
                var pp = p * p;
                var ppp = pp * p;
                var offset1 = i1 * stride3;
                var offset0 = offset1 - stride3;
                var s0 = 2 * ppp - 3 * pp + 1;
                var s1 = ppp - 2 * pp + p;
                var s2 = -2 * ppp + 3 * pp;
                var s3 = ppp - pp;
                // Layout of keyframe output values for CUBICSPLINE animations:
                //   [ inTangent_1, splineVertex_1, outTangent_1, inTangent_2, splineVertex_2, ... ]
                for (var i = 0; i !== stride; i++) {
                    var p0 = values[offset0 + i + stride]; // splineVertex_k
                    var m0 = values[offset0 + i + stride2] * td; // outTangent_k * (t_k+1 - t_k)
                    var p1 = values[offset1 + i + stride]; // splineVertex_k+1
                    var m1 = values[offset1 + i] * td; // inTangent_k+1 * (t_k+1 - t_k)
                    result[i] = s0 * p0 + s1 * m0 + s2 * p1 + s3 * m1;
                }
                return result;
            };
            return GLTFCubicSplineInterpolant;
        }(THREE.Interpolant));
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFBufferViewImporter = /** @class */ (function (_super) {
            __extends(GLTFBufferViewImporter, _super);
            function GLTFBufferViewImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFBufferViewImporter.prototype.import = function () {
                if (this.parser.json.bufferViews == null || this.parser.json.bufferViews.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.bufferViews.length; i++) {
                    var bufferViewDef = this.parser.json.bufferViews[i];
                    var buffer = this.parser._buffers[bufferViewDef.buffer];
                    var byteLength = bufferViewDef.byteLength || 0;
                    var byteOffset = bufferViewDef.byteOffset || 0;
                    var bufferView = buffer.slice(byteOffset, byteOffset + byteLength);
                    this.parser._bufferViews[i] = bufferView;
                }
            };
            return GLTFBufferViewImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFBufferViewImporter = GLTFBufferViewImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFCameraImporter = /** @class */ (function (_super) {
            __extends(GLTFCameraImporter, _super);
            function GLTFCameraImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFCameraImporter.prototype.import = function () {
                if (this.parser.json.cameras == null || this.parser.json.cameras.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.cameras.length; i++) {
                    var cameraDef = this.parser.json.cameras[i];
                    this._loadCamera(cameraDef, i);
                }
            };
            GLTFCameraImporter.prototype._loadCamera = function (cameraDef, index) {
                var camera;
                if (cameraDef.type === 'perspective') {
                    var param = cameraDef.perspective;
                    var perspectiveCamera = new THREE.PerspectiveCamera(THREE.Math.radToDeg(param.yfov), param.aspectRatio || 1, param.znear || 1, param.zfar || 2e6);
                    camera = perspectiveCamera;
                    // TODO option
                    var screen_1 = plume.Game.get().scaleManager.getCurrentScaling();
                    perspectiveCamera.aspect = screen_1.windowWidth / screen_1.windowHeight;
                    perspectiveCamera.updateProjectionMatrix();
                }
                else if (cameraDef.type === 'orthographic') {
                    var param = cameraDef.orthographic;
                    camera = new THREE.OrthographicCamera(param.xmag / -2, param.xmag / 2, param.ymag / 2, param.ymag / -2, param.znear, param.zfar);
                }
                if (cameraDef.name !== undefined)
                    camera.name = cameraDef.name;
                gltf.assignExtrasToUserData(camera, cameraDef);
                this.parser._cameras[index] = camera;
            };
            return GLTFCameraImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFCameraImporter = GLTFCameraImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFEmbeddedImageImporter = /** @class */ (function (_super) {
            __extends(GLTFEmbeddedImageImporter, _super);
            function GLTFEmbeddedImageImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFEmbeddedImageImporter.prototype.import = function () {
                if (this.parser.json.images == null || this.parser.json.images.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.images.length; i++) {
                    var imageDef = this.parser.json.images[i];
                    if (imageDef.bufferView != null) {
                        this._loadImage(imageDef, i);
                    }
                }
            };
            GLTFEmbeddedImageImporter.prototype._loadImage = function (imageDef, index) {
                var self = this;
                var bufferView = this.parser._bufferViews[imageDef.bufferView];
                var blob = new Blob([bufferView], { type: imageDef.mimeType });
                var sourceURI = URL.createObjectURL(blob);
                var texture = this.parser._textureLoaderManager.load(sourceURI, function (t) {
                    // Ok, free memory
                    URL.revokeObjectURL(sourceURI);
                }, function (e) {
                    plume.logger.error("Failed to load texture from bufferView " + imageDef.bufferView);
                });
                self.parser._images[index] = texture;
            };
            return GLTFEmbeddedImageImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFEmbeddedImageImporter = GLTFEmbeddedImageImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFMaterialImporter = /** @class */ (function (_super) {
            __extends(GLTFMaterialImporter, _super);
            function GLTFMaterialImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFMaterialImporter.prototype.import = function () {
                if (this.parser.json.materials == null || this.parser.json.materials.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.materials.length; i++) {
                    var materialDef = this.parser.json.materials[i];
                    this._loadMaterial(materialDef, i);
                }
            };
            GLTFMaterialImporter.prototype._loadMaterial = function (materialDef, index) {
                var materialType;
                var materialParams = {};
                var materialExtensions = materialDef.extensions || {};
                if (materialExtensions == "KHR_materials_pbrSpecularGlossiness") {
                    var sgExtension = this.parser.extensions.KHR_materials_pbrSpecularGlossiness;
                    materialType = sgExtension.getMaterialType(materialDef);
                    sgExtension.extendParams(materialParams, materialDef, this.parser);
                }
                else if (materialExtensions == "KHR_materials_unlit") {
                    var kmuExtension = this.parser.extensions.KHR_materials_unlit;
                    materialType = kmuExtension.getMaterialType(materialDef);
                    kmuExtension.extendParams(materialParams, materialDef, this.parser);
                }
                else {
                    // Specification:
                    // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#metallic-roughness-material
                    materialType = THREE.MeshStandardMaterial;
                    var metallicRoughness = materialDef.pbrMetallicRoughness || {};
                    materialParams.color = new THREE.Color(1.0, 1.0, 1.0);
                    materialParams.opacity = 1.0;
                    if (Array.isArray(metallicRoughness.baseColorFactor)) {
                        var array = metallicRoughness.baseColorFactor;
                        materialParams.color.fromArray(array);
                        materialParams.opacity = array[3];
                    }
                    if (metallicRoughness.baseColorTexture !== undefined) {
                        this.parser.assignTexture(metallicRoughness.baseColorTexture, materialParams, 'map');
                    }
                    materialParams.metalness = metallicRoughness.metallicFactor !== undefined ? metallicRoughness.metallicFactor : 1.0;
                    materialParams.roughness = metallicRoughness.roughnessFactor !== undefined ? metallicRoughness.roughnessFactor : 1.0;
                    if (metallicRoughness.metallicRoughnessTexture !== undefined) {
                        var textureIndex = metallicRoughness.metallicRoughnessTexture.index;
                        this.parser.assignTexture(metallicRoughness.metallicRoughnessTexture, materialParams, 'metalnessMap');
                        this.parser.assignTexture(metallicRoughness.metallicRoughnessTexture, materialParams, 'roughnessMap');
                    }
                }
                if (materialDef.doubleSided === true) {
                    materialParams.side = THREE.DoubleSide;
                }
                var alphaMode = materialDef.alphaMode || gltf.ALPHA_MODES.OPAQUE;
                if (alphaMode === gltf.ALPHA_MODES.BLEND) {
                    materialParams.transparent = true;
                }
                else {
                    materialParams.transparent = false;
                    if (alphaMode === gltf.ALPHA_MODES.MASK) {
                        materialParams.alphaTest = materialDef.alphaCutoff !== undefined ? materialDef.alphaCutoff : 0.5;
                    }
                }
                if (materialDef.normalTexture !== undefined && materialType !== THREE.MeshBasicMaterial) {
                    this.parser.assignTexture(materialDef.normalTexture, materialParams, 'normalMap');
                    materialParams.normalScale = new THREE.Vector2(1, 1);
                    if (materialDef.normalTexture.scale !== undefined) {
                        materialParams.normalScale.set(materialDef.normalTexture.scale, materialDef.normalTexture.scale);
                    }
                }
                if (materialDef.occlusionTexture !== undefined && materialType !== THREE.MeshBasicMaterial) {
                    this.parser.assignTexture(materialDef.occlusionTexture, materialParams, 'aoMap');
                    if (materialDef.occlusionTexture.strength !== undefined) {
                        materialParams.aoMapIntensity = materialDef.occlusionTexture.strength;
                    }
                }
                if (materialDef.emissiveFactor !== undefined && materialType !== THREE.MeshBasicMaterial) {
                    materialParams.emissive = new THREE.Color().fromArray(materialDef.emissiveFactor);
                }
                if (materialDef.emissiveTexture !== undefined && materialType !== THREE.MeshBasicMaterial) {
                    this.parser.assignTexture(materialDef.emissiveTexture, materialParams, 'emissiveMap');
                }
                var material; //THREE.ShaderMaterial | THREE.MeshStandardMaterial | THREE.MeshBasicMaterial;
                if (materialExtensions == "KHR_materials_pbrSpecularGlossiness") {
                    material = this.parser.extensions.KHR_materials_pbrSpecularGlossiness.createMaterial(materialParams);
                }
                else {
                    if (this.parser._factory.materialFactory != null) {
                        material = this.parser._factory.materialFactory(materialType, materialParams);
                    }
                    else {
                        material = new materialType(materialParams);
                    }
                }
                if (materialDef.name !== undefined)
                    material.name = materialDef.name;
                // Normal map textures use OpenGL conventions:
                // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#materialnormaltexture
                if (material.normalScale) {
                    material.normalScale.y = -material.normalScale.y;
                }
                // baseColorTexture, emissiveTexture, and specularGlossinessTexture use sRGB encoding.
                if (material.map)
                    material.map.encoding = THREE.sRGBEncoding;
                if (material.emissiveMap)
                    material.emissiveMap.encoding = THREE.sRGBEncoding;
                if (material.specularMap)
                    material.specularMap.encoding = THREE.sRGBEncoding;
                gltf.assignExtrasToUserData(material, materialDef);
                if (this.parser._factory.materialProcessor != null) {
                    material = this.parser._factory.materialProcessor(material);
                }
                // if (materialDef.extensions) addUnknownExtensionsToUserData(extensions, material, materialDef);
                this.parser._materials[index] = material;
            };
            return GLTFMaterialImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFMaterialImporter = GLTFMaterialImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFMeshImporter = /** @class */ (function (_super) {
            __extends(GLTFMeshImporter, _super);
            function GLTFMeshImporter() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this._primitiveCache = [];
                _this._cache = new plume.HashMap();
                return _this;
            }
            GLTFMeshImporter.prototype.import = function () {
                if (this.parser.json.meshes == null || this.parser.json.meshes.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.meshes.length; i++) {
                    var meshDef = this.parser.json.meshes[i];
                    this._loadMesh(meshDef, i);
                }
            };
            GLTFMeshImporter.prototype._loadMesh = function (meshDef, index) {
                var primitives = meshDef.primitives;
                var originalMaterials = [];
                for (var i = 0; i < primitives.length; i++) {
                    originalMaterials[i] = (primitives[i].material === undefined ? gltf.createDefaultMaterial() : this.parser._materials[primitives[i].material]);
                }
                var geometries = this._loadGeometries(primitives);
                var isMultiMaterial = (geometries.length == 1 && geometries[0].groups.length > 0);
                var meshes = [];
                for (var i = 0; i < geometries.length; i++) {
                    var geometry = geometries[i];
                    var primitive = primitives[i];
                    // 1. create Mesh
                    var mesh = void 0;
                    var material = isMultiMaterial ? originalMaterials : originalMaterials[i];
                    if (primitive.mode === gltf.WEBGL_CONSTANTS.TRIANGLES ||
                        primitive.mode === gltf.WEBGL_CONSTANTS.TRIANGLE_STRIP ||
                        primitive.mode === gltf.WEBGL_CONSTANTS.TRIANGLE_FAN ||
                        primitive.mode === undefined) {
                        // .isSkinnedMesh isn't in glTF spec. See .markDefs()
                        mesh = (meshDef.isSkinnedMesh === true ? new THREE.SkinnedMesh(geometry, material) : new THREE.Mesh(geometry, material));
                        if (primitive.mode === gltf.WEBGL_CONSTANTS.TRIANGLE_STRIP) {
                            mesh.drawMode = THREE.TriangleStripDrawMode;
                        }
                        else if (primitive.mode === gltf.WEBGL_CONSTANTS.TRIANGLE_FAN) {
                            mesh.drawMode = THREE.TriangleFanDrawMode;
                        }
                    }
                    else if (primitive.mode === gltf.WEBGL_CONSTANTS.LINES) {
                        mesh = new THREE.LineSegments(geometry, material);
                    }
                    else if (primitive.mode === gltf.WEBGL_CONSTANTS.LINE_STRIP) {
                        mesh = new THREE.Line(geometry, material);
                    }
                    else if (primitive.mode === gltf.WEBGL_CONSTANTS.LINE_LOOP) {
                        mesh = new THREE.LineLoop(geometry, material);
                    }
                    else if (primitive.mode === gltf.WEBGL_CONSTANTS.POINTS) {
                        mesh = new THREE.Points(geometry, material);
                    }
                    else {
                        throw new Error('THREE.GLTFLoader: Primitive mode unsupported: ' + primitive.mode);
                    }
                    if (Object.keys(mesh.geometry.morphAttributes).length > 0) {
                        gltf.updateMorphTargets(mesh, meshDef);
                    }
                    mesh.name = meshDef.name || ('mesh_' + index);
                    if (geometries.length > 1)
                        mesh.name += '_' + i;
                    gltf.assignExtrasToUserData(mesh, meshDef);
                    meshes.push(mesh);
                    // 2. update Material depending on Mesh and BufferGeometry
                    var materials = isMultiMaterial ? mesh.material : [mesh.material];
                    var useVertexColors = geometry.attributes.color !== undefined;
                    var useFlatShading = geometry.attributes.normal === undefined;
                    var useSkinning = mesh.isSkinnedMesh === true;
                    var useMorphTargets = Object.keys(geometry.morphAttributes).length > 0;
                    var useMorphNormals = useMorphTargets && geometry.morphAttributes.normal !== undefined;
                    for (var j = 0, jl = materials.length; j < jl; j++) {
                        var material_1 = materials[j];
                        if (mesh instanceof THREE.Points) {
                            var cacheKey = 'PointsMaterial:' + material_1["uuid"];
                            var pointsMaterial = this._cache.get(cacheKey);
                            if (!pointsMaterial) {
                                pointsMaterial = new THREE.PointsMaterial();
                                THREE.Material.prototype.copy.call(pointsMaterial, material_1);
                                pointsMaterial.color.copy(material_1["color"]);
                                pointsMaterial.map = material_1["map"];
                                pointsMaterial.lights = false; // PointsMaterial doesn't support lights yet
                                this._cache.put(cacheKey, pointsMaterial);
                            }
                            material_1 = pointsMaterial;
                        }
                        else if (mesh instanceof THREE.Line) {
                            var cacheKey = 'LineBasicMaterial:' + material_1["uuid"];
                            var lineMaterial = this._cache.get(cacheKey);
                            if (!lineMaterial) {
                                lineMaterial = new THREE.LineBasicMaterial();
                                THREE.Material.prototype.copy.call(lineMaterial, material_1);
                                lineMaterial.color.copy(material_1["color"]);
                                lineMaterial.lights = false; // LineBasicMaterial doesn't support lights yet
                                this._cache.put(cacheKey, lineMaterial);
                            }
                            material_1 = lineMaterial;
                        }
                        // Clone the material if it will be modified
                        if (useVertexColors || useFlatShading || useSkinning || useMorphTargets) {
                            var cacheKey = 'ClonedMaterial:' + material_1["uuid"] + ':';
                            if (material_1["isGLTFSpecularGlossinessMaterial"])
                                cacheKey += 'specular-glossiness:';
                            if (useSkinning)
                                cacheKey += 'skinning:';
                            if (useVertexColors)
                                cacheKey += 'vertex-colors:';
                            if (useFlatShading)
                                cacheKey += 'flat-shading:';
                            if (useMorphTargets)
                                cacheKey += 'morph-targets:';
                            if (useMorphNormals)
                                cacheKey += 'morph-normals:';
                            var cachedMaterial = this._cache.get(cacheKey);
                            if (!cachedMaterial) {
                                cachedMaterial = material_1["isGLTFSpecularGlossinessMaterial"]
                                    ? this.parser.extensions.KHR_materials_pbrSpecularGlossiness.cloneMaterial(material_1)
                                    : material_1.clone();
                                if (useSkinning)
                                    cachedMaterial.skinning = true;
                                if (useVertexColors)
                                    cachedMaterial.vertexColors = THREE.VertexColors;
                                if (useFlatShading)
                                    cachedMaterial.flatShading = true;
                                if (useMorphTargets)
                                    cachedMaterial.morphTargets = true;
                                if (useMorphNormals)
                                    cachedMaterial.morphNormals = true;
                                this._cache.put(cacheKey, cachedMaterial);
                            }
                            material_1 = cachedMaterial;
                        }
                        materials[j] = material_1;
                        // workarounds for mesh and geometry
                        if (material_1["aoMap"] && geometry.attributes.uv2 === undefined && geometry.attributes.uv !== undefined) {
                            console.log('THREE.GLTFLoader: Duplicating UVs to support aoMap.');
                            geometry.addAttribute('uv2', new THREE.BufferAttribute(geometry.attributes.uv.array, 2));
                        }
                        if (material_1["isGLTFSpecularGlossinessMaterial"]) {
                            // for GLTFSpecularGlossinessMaterial(ShaderMaterial) uniforms runtime update
                            mesh.onBeforeRender = this.parser.extensions.KHR_materials_pbrSpecularGlossiness.refreshUniforms;
                        }
                    }
                    mesh.material = isMultiMaterial ? materials : materials[0];
                }
                if (meshes.length === 1) {
                    this.parser._meshes[index] = meshes[0];
                }
                else {
                    var group = new THREE.Group();
                    for (var i = 0, il = meshes.length; i < il; i++) {
                        group.add(meshes[i]);
                    }
                    this.parser._meshes[index] = group;
                }
            };
            GLTFMeshImporter.prototype._loadGeometries = function (primitives) {
                var geometries = [];
                for (var i = 0; i < primitives.length; i++) {
                    var primitive = primitives[i];
                    // See if we've already created this geometry
                    var cached = gltf.getCachedGeometry(this._primitiveCache, primitive);
                    if (cached != null) {
                        // Use the cached geometry if it exists
                        geometries.push(cached.geometry);
                    }
                    else if (primitive.extensions && primitive.extensions["KHR_draco_mesh_compression"]) {
                        // Use DRACO geometry if available
                        var geometry = this.parser.extensions.KHR_draco_mesh_compression.decodePrimitive(primitive, this.parser);
                        gltf.addPrimitiveAttributes(geometry, primitive, this.parser._accessors); // TODO check
                        if (this.parser._factory.geometryProcessor != null) {
                            geometry = this.parser._factory.geometryProcessor(geometry);
                        }
                        // Cache this geometry
                        this._primitiveCache.push({ primitive: primitive, geometry: geometry });
                        geometries.push(geometry);
                    }
                    else {
                        // Otherwise create a new geometry
                        var geometry = new THREE.BufferGeometry();
                        gltf.addPrimitiveAttributes(geometry, primitive, this.parser._accessors);
                        if (this.parser._factory.geometryProcessor != null) {
                            geometry = this.parser._factory.geometryProcessor(geometry);
                        }
                        // Cache this geometry
                        this._primitiveCache.push({ primitive: primitive, geometry: geometry });
                        geometries.push(geometry);
                    }
                }
                return geometries;
            };
            return GLTFMeshImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFMeshImporter = GLTFMeshImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFNodeImporter = /** @class */ (function (_super) {
            __extends(GLTFNodeImporter, _super);
            function GLTFNodeImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFNodeImporter.prototype.import = function () {
                if (this.parser.json.nodes == null || this.parser.json.nodes.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.nodes.length; i++) {
                    var nodeDef = this.parser.json.nodes[i];
                    this._loadNode(nodeDef, i);
                }
            };
            GLTFNodeImporter.prototype._loadNode = function (nodeDef, index) {
                // let json = this.parser.json;
                // let extensions = this.parser.extensions;
                var meshReferences = this.parser.json.meshReferences;
                var meshUses = this.parser.json.meshUses;
                // let nodeDef = json.nodes[nodeIndex];
                var node;
                // .isBone isn't in glTF spec. See .markDefs
                if (nodeDef.isBone === true) {
                    node = new THREE.Bone();
                }
                else if (nodeDef.mesh !== undefined) {
                    var mesh = this.parser._meshes[nodeDef.mesh];
                    if (meshReferences[nodeDef.mesh] > 1) {
                        var instanceNum = meshUses[nodeDef.mesh]++;
                        // node = AnimUtils.clone(mesh);
                        node = mesh.clone();
                        node.name += '_instance_' + instanceNum;
                        // onBeforeRender copy for Specular-Glossiness
                        node.onBeforeRender = mesh.onBeforeRender;
                        for (var i = 0, il = node.children.length; i < il; i++) {
                            node.children[i].name += '_instance_' + instanceNum;
                            node.children[i].onBeforeRender = mesh.children[i].onBeforeRender;
                        }
                    }
                    else {
                        node = mesh;
                    }
                }
                else if (nodeDef.camera !== undefined) {
                    node = this.parser._cameras[nodeDef.camera];
                }
                else if (nodeDef.extensions) {
                    if (nodeDef.extensions.KHR_lights_punctual != null) {
                        if (nodeDef.extensions.KHR_lights_punctual.light != null) {
                            var lights = this.parser.extensions.KHR_lights_punctual.lights;
                            node = lights[nodeDef.extensions.KHR_lights_punctual.light];
                        }
                    }
                }
                else {
                    node = new THREE.Object3D();
                }
                if (node == null) {
                    plume.logger.warn("construct default object3d");
                    node = new THREE.Object3D();
                }
                if (nodeDef.name !== undefined) {
                    node.name = THREE.PropertyBinding.sanitizeNodeName(nodeDef.name);
                }
                if (nodeDef.extensions != null && nodeDef.extensions.GB_lightmaps != null) {
                    this.parser.extensions.GB_lightmaps.applyLightmap(node, nodeDef.extensions.GB_lightmaps);
                }
                gltf.assignExtrasToUserData(node, nodeDef);
                // if (nodeDef.extensions) addUnknownExtensionsToUserData(extensions, node, nodeDef);
                if (nodeDef.matrix !== undefined) {
                    var matrix = new THREE.Matrix4();
                    matrix.fromArray(nodeDef.matrix);
                    node.applyMatrix(matrix);
                }
                else {
                    if (nodeDef.translation !== undefined) {
                        node.position.fromArray(nodeDef.translation);
                    }
                    if (nodeDef.rotation !== undefined) {
                        node.quaternion.fromArray(nodeDef.rotation);
                    }
                    if (nodeDef.scale !== undefined) {
                        node.scale.fromArray(nodeDef.scale);
                    }
                }
                if (nodeDef.extras != null && nodeDef.extras.gbunity) {
                    this.parser.extensions.GB_Unity.applyToNode(node, nodeDef.extras.gbunity);
                }
                this.parser._nodes[index] = node;
            };
            return GLTFNodeImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFNodeImporter = GLTFNodeImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFSceneImporter = /** @class */ (function (_super) {
            __extends(GLTFSceneImporter, _super);
            function GLTFSceneImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFSceneImporter.prototype.import = function () {
                if (this.parser.json.scenes == null)
                    return;
                for (var i = 0; i < this.parser.json.scenes.length; i++) {
                    this._loadScene(this.parser.json.scenes[i], i);
                }
            };
            GLTFSceneImporter.prototype._loadScene = function (sceneDef, sceneIndex) {
                var scene = new THREE.Scene();
                if (sceneDef.name !== undefined)
                    scene.name = sceneDef.name;
                gltf.assignExtrasToUserData(scene, sceneDef);
                // if (sceneDef.extensions) addUnknownExtensionsToUserData(extensions, scene, sceneDef);
                var nodeIds = sceneDef.nodes || [];
                for (var i = 0, il = nodeIds.length; i < il; i++) {
                    this._buildNodeHierachy(nodeIds[i], scene);
                }
                this.parser._scenes[sceneIndex] = scene;
            };
            GLTFSceneImporter.prototype._buildNodeHierachy = function (nodeId, parentObject) {
                var node = this.parser._nodes[nodeId];
                var nodeDef = this.parser.json.nodes[nodeId];
                // build skeleton
                if (nodeDef.skin !== undefined) {
                    var meshes = (node["isGroup"] === true ? node.children : [node]);
                    for (var i = 0; i < meshes.length; i++) {
                        var mesh = meshes[i];
                        var skinEntry = this.parser._skins[nodeDef.skin];
                        var bones = [];
                        var boneInverses = [];
                        for (var j = 0; j < skinEntry.joints.length; j++) {
                            var jointId = skinEntry.joints[j];
                            var jointNode = this.parser._nodes[jointId];
                            if (jointNode) {
                                bones.push(jointNode);
                                var mat = new THREE.Matrix4();
                                if (skinEntry.inverseBindMatrices !== undefined) {
                                    mat.fromArray(skinEntry.inverseBindMatrices.array, j * 16);
                                }
                                boneInverses.push(mat);
                            }
                            else {
                                plume.logger.warn("Joint " + jointId + " could not be found.");
                            }
                        }
                        if (mesh["isSkinnedMesh"] == true) {
                            mesh.bind(new THREE.Skeleton(bones, boneInverses), mesh.matrixWorld);
                        }
                    }
                }
                // build node hierachy
                parentObject.add(node);
                if (nodeDef.children) {
                    var children = nodeDef.children;
                    for (var i = 0; i < children.length; i++) {
                        var child = children[i];
                        this._buildNodeHierachy(child, node);
                    }
                }
            };
            return GLTFSceneImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFSceneImporter = GLTFSceneImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFSkinImporter = /** @class */ (function (_super) {
            __extends(GLTFSkinImporter, _super);
            function GLTFSkinImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFSkinImporter.prototype.import = function () {
                if (this.parser.json.skins == null || this.parser.json.skins.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.skins.length; i++) {
                    var skinDef = this.parser.json.skins[i];
                    this._loadSkin(skinDef, i);
                }
            };
            GLTFSkinImporter.prototype._loadSkin = function (skinDef, index) {
                var skinEntry = { joints: skinDef.joints, inverseBindMatrices: null };
                if (skinDef.inverseBindMatrices != null) {
                    skinEntry.inverseBindMatrices = this.parser._accessors[skinDef.inverseBindMatrices];
                }
                this.parser._skins[index] = skinEntry;
            };
            return GLTFSkinImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFSkinImporter = GLTFSkinImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
/// <reference path="GLTFImporter.ts" />
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFTextureImporter = /** @class */ (function (_super) {
            __extends(GLTFTextureImporter, _super);
            function GLTFTextureImporter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GLTFTextureImporter.prototype.import = function () {
                if (this.parser.json.textures == null || this.parser.json.textures.length <= 0)
                    return;
                for (var i = 0; i < this.parser.json.textures.length; i++) {
                    var textureDef = this.parser.json.textures[i];
                    this._loadTexture(textureDef, i);
                }
            };
            GLTFTextureImporter.prototype._loadTexture = function (textureDef, index) {
                var source = this.parser.json.images[textureDef.source];
                var texture = this.parser._images[textureDef.source];
                texture.flipY = false;
                if (textureDef.name !== undefined)
                    texture.name = textureDef.name;
                // Ignore unknown mime types, like DDS files.
                if (source.mimeType in gltf.MIME_TYPE_FORMATS) {
                    texture.format = gltf.MIME_TYPE_FORMATS[source.mimeType];
                }
                var samplers = this.parser.json.samplers || {};
                var sampler = samplers[textureDef.sampler] || {};
                texture.magFilter = gltf.WEBGL_FILTERS[sampler.magFilter] || THREE.LinearFilter;
                texture.minFilter = gltf.WEBGL_FILTERS[sampler.minFilter] || THREE.LinearMipMapLinearFilter;
                texture.wrapS = gltf.WEBGL_WRAPPINGS[sampler.wrapS] || THREE.RepeatWrapping;
                texture.wrapT = gltf.WEBGL_WRAPPINGS[sampler.wrapT] || THREE.RepeatWrapping;
                this.parser._textures[index] = texture;
            };
            return GLTFTextureImporter;
        }(gltf.GLTFImporter));
        gltf.GLTFTextureImporter = GLTFTextureImporter;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var gltf;
    (function (gltf) {
        var GLTFAnimationsHelper = /** @class */ (function () {
            function GLTFAnimationsHelper() {
            }
            //  Convert an "absolute" animation clip to a node relative clip
            GLTFAnimationsHelper.convertToRelative = function (rootNode, source) {
                // console.log("Converting " + source.name);
                // for (let i = 0; i < source.tracks.length; i++) {
                //     let track = source.tracks[i];
                //     console.log("track " + i + " : " + track.name);
                // }
                // nothing to do ? :)
                return source;
            };
            return GLTFAnimationsHelper;
        }());
        gltf.GLTFAnimationsHelper = GLTFAnimationsHelper;
    })(gltf = plume.gltf || (plume.gltf = {}));
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.gltf.js.map