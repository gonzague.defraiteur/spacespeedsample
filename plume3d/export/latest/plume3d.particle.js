"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="../../build/latest/plume3d.core.d.ts" />
var plume;
(function (plume) {
    // ParticleSystem
    var Emitter = /** @class */ (function (_super) {
        __extends(Emitter, _super);
        function Emitter() {
            var _this = _super.call(this) || this;
            _this._isPlaying = false;
            _this._isPaused = false;
            _this._isStopped = true;
            _this._isEmitting = false;
            _this._time = 0; // in seconds
            _this._tmpVec3_0 = new THREE.Vector3();
            _this._emitRateTimeCounter = 0;
            _this._emitRateDistanceCounter = 0;
            _this.main = new plume.ParticleMainModule();
            _this.renderer = new plume.ParticleSystemRenderer();
            _this.shape = new plume.ParticleShapeModule();
            _this.emission = new plume.ParticleEmissionModule();
            return _this;
        }
        Emitter.prototype.initialize = function () {
            var _this = this;
            this._particles = new plume.RecyclePool(function () { return new plume.Particle(_this); }, this.main.maxParticles);
            this.main.initialize(this);
            this.renderer.initialize(this);
            this.shape.initialize(this);
            this.emission.initialize(this);
            plume.Game.get().addUpdatable(this);
        };
        Emitter.prototype.destroy = function () {
            plume.Game.get().removeUpdatable(this);
        };
        Emitter.prototype.play = function () {
            if (this._isPaused) {
                this._isPaused = false;
            }
            if (this._isStopped) {
                this._isStopped = false;
            }
            this._time = 0.0;
            this._emitRateTimeCounter = 0.0;
            this._emitRateDistanceCounter = 0.0;
            this._isPlaying = true;
            // // prewarm
            // if (this._prewarm) {
            //     this._prewarmSystem();
            // }
        };
        Emitter.prototype.pause = function () {
            if (this._isStopped) {
                plume.logger.warn('Particle system is already stopped.');
                return;
            }
            if (this._isPlaying) {
                this._isPlaying = false;
            }
            this._isPaused = true;
        };
        Emitter.prototype.stop = function () {
            if (this._isPlaying) {
                this._isPlaying = false;
            }
            if (this._isPaused) {
                this._isPaused = false;
            }
            this.clear();
            this._time = 0.0;
            this._emitRateTimeCounter = 0.0;
            this._emitRateDistanceCounter = 0.0;
            this._isStopped = true;
        };
        Emitter.prototype.clear = function () {
            this._particles.reset();
            this.renderer.model.clear();
        };
        Emitter.prototype.emit = function (count /*, emitParams = null*/) {
            // if (emitParams !== null) {
            //     // TODO:
            // }
            for (var i = 0; i < count; i++) {
                if (this._particles.length >= this.main.maxParticles) {
                    return;
                }
                var particle_1 = this._particles.add();
                // let rand = pseudoRandom(randomRangeInt(0, INT_MAX));
                var rand = Math.random();
                // if (this._shapeModule.enable) {
                //     this._shapeModule.emit(particle);
                // }
                // else {
                //     vec3.set(particle.position, 0, 0, 0);
                //     vec3.copy(particle.velocity, particleEmitZAxis);
                // }
                particle_1.velocity.multiplyScalar(this.main.startSpeed.evaluate(this._time / this.main.duration, rand));
                // switch (this._simulationSpace) {
                //     case 'local':
                //         break;
                //     case 'world': {
                //         this._entity.getWorldMatrix(_world_mat);
                //         vec3.transformMat4(particle.position, particle.position, _world_mat);
                //         let worldRot = quat.create();
                //         this._entity.getWorldRot(worldRot);
                //         vec3.transformQuat(particle.velocity, particle.velocity, worldRot);
                //     }
                //         break;
                //     case 'custom':
                //         // TODO:
                //         break;
                // }
                // apply startRotation. now 2D only.
                // vec3.set(particle.rotation, this._startRotation.evaluate(this._time / this._duration, rand), 0, 0);
                // // apply startSize. now 2D only.
                // vec3.set(particle.startSize, this._startSize.evaluate(this._time / this._duration, rand), 0, 0);
                // vec3.copy(particle.size, particle.startSize);
                // // apply startColor.
                // color4.copy(particle.startColor, this._startColor.evaluate(this._time / this._duration, rand));
                // color4.copy(particle.color, particle.startColor);
                // // apply startLifetime.
                // particle.startLifetime = this._startLifetime.evaluate(this._time / this._duration, rand);
                // particle.remainingLifetime = particle.startLifetime;
                // particle.randomSeed = pseudoRandom(randomRangeInt(0, INT_MAX));
            } // end of particles forLoop.
        };
        // simulation, update particles.
        Emitter.prototype._updateParticles = function (dt) {
            // this._entity.getWorldMatrix(_world_mat);
            // if (this._velocityOvertimeModule.enable) {
            //     this._velocityOvertimeModule.update(this._simulationSpace, _world_mat);
            // }
            // if (this._forceOvertimeModule.enable) {
            //     this._forceOvertimeModule.update(this._simulationSpace, _world_mat);
            // }
            for (var i = 0; i < this._particles.length; ++i) {
                var p = this._particles.data[i];
                p.remainingLifetime -= dt;
                p.animatedVelocity.set(0, 0, 0);
                if (p.remainingLifetime < 0.0) {
                    this._particles.remove(i);
                    --i;
                    continue;
                }
                // p.velocity.y -= this._gravityModifier.evaluate(1 - p.remainingLifetime / p.startLifetime, p.randomSeed) * 9.8 * dt; // apply gravity.
                // if (this._sizeOvertimeModule.enable) {
                //     this._sizeOvertimeModule.animate(p);
                // }
                // if (this._colorOverLifetimeModule.enable) {
                //     this._colorOverLifetimeModule.animate(p);
                // }
                // if (this._forceOvertimeModule.enable) {
                //     this._forceOvertimeModule.animate(p, dt);
                // }
                // if (this._velocityOvertimeModule.enable) {
                //     this._velocityOvertimeModule.animate(p);
                // }
                // else {
                //     vec3.copy(p.ultimateVelocity, p.velocity);
                // }
                // if (this._limitVelocityOvertimeModule.enable) {
                //     this._limitVelocityOvertimeModule.animate(p);
                // }
                // if (this._rotationOvertimeModule.enable) {
                //     this._rotationOvertimeModule.animate(p, dt);
                // }
                // if (this._textureAnimationModule.enable) {
                //     this._textureAnimationModule.animate(p);
                // }
                var ultimateVelocity = this._tmpVec3_0.copy(p.totalVelocity).multiplyScalar(dt);
                p.position.add(ultimateVelocity); // apply velocity.
            }
        };
        // initialize particle system as though it had already completed a full cycle.
        // private _prewarmSystem() {
        //     this._startDelay.mode = 'constant'; // clear startDelay.
        //     this._startDelay.constant = 0;
        //     let dt = 1.0; // should use varying value?
        //     let cnt = this._duration / dt;
        //     for (let i = 0; i < cnt; ++i) {
        //         this._time += dt;
        //         this._emit(dt);
        //         this._updateParticles(dt);
        //     }
        // }
        // internal function
        Emitter.prototype._emit = function (dt) {
            // emit particles.
            var startDelay = this.main.startDelay.evaluate();
            if (this._time > startDelay) {
                if (!this._isStopped) {
                    this._isEmitting = true;
                }
                if (this._time > (this.main.duration + startDelay)) {
                    if (!this.main.loop) {
                        this._isEmitting = false;
                        this._isStopped = true;
                    }
                }
                // emit by rateOverTime
                this._emitRateTimeCounter += this.emission.rateOverTime.evaluate(this._time / this.main.duration, 1) * dt;
                if (this._emitRateTimeCounter > 1 && this._isEmitting) {
                    var emitNum = Math.floor(this._emitRateTimeCounter);
                    this._emitRateTimeCounter -= emitNum;
                    this.emit(emitNum);
                }
                // emit by rateOverDistance
                // this._entity.getWorldPos(this._curWPos);
                // let distance = vec3.distance(this._curWPos, this._oldWPos);
                // vec3.copy(this._oldWPos, this._curWPos);
                // this._emitRateDistanceCounter += distance * this._rateOverDistance.evaluate(this._time / this._duration, 1);
                // if (this._emitRateDistanceCounter > 1 && this._isEmitting) {
                //     let emitNum = Math.floor(this._emitRateDistanceCounter);
                //     this._emitRateDistanceCounter -= emitNum;
                //     this.emit(emitNum);
                // }
                // bursts
                // for (let i = 0; i < this._bursts.length; ++i) {
                //     this._bursts[i].update(this, dt);
                // }
            }
        };
        Emitter.prototype.update = function (dt) {
            var dtS = dt / 1000;
            var scaledDeltaTime = dtS * this.main.simulationSpeed;
            if (this._isPlaying) {
                this._time += scaledDeltaTime;
                // excute emission
                this._emit(scaledDeltaTime);
                // simulation, update particles.
                this._updateParticles(scaledDeltaTime);
                // update render data
                this.renderer.updateRenderData();
            }
        };
        Emitter.prototype.getParticles = function () {
            return this._particles;
        };
        Emitter.prototype.getParticleCount = function () {
            return this._particles.length;
        };
        Object.defineProperty(Emitter.prototype, "isPlaying", {
            get: function () {
                return this._isPlaying;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Emitter.prototype, "isPaused", {
            get: function () {
                return this._isPaused;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Emitter.prototype, "isStopped", {
            get: function () {
                return this._isStopped;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Emitter.prototype, "isEmitting", {
            get: function () {
                return this._isEmitting;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Emitter.prototype, "time", {
            get: function () {
                return this._time;
            },
            enumerable: true,
            configurable: true
        });
        return Emitter;
    }(THREE.Object3D));
    plume.Emitter = Emitter;
    //
    // Properties
    // automaticCullingEnabled	Does this system support Automatic Culling?
    // collision	Access the particle system collision module.
    // colorBySpeed	Access the particle system color by lifetime module.
    // colorOverLifetime	Access the particle system color over lifetime module.
    // customData	Access the particle system Custom Data module.
    // emission	Access the particle system emission module.
    // externalForces	Access the particle system external forces module.
    // forceOverLifetime	Access the particle system force over lifetime module.
    // inheritVelocity	Access the particle system velocity inheritance module.
    // isEmitting	Is the Particle System currently emitting particles? A Particle System may stop emitting when its emission module has finished, it has been paused or if the system has been stopped using Stop with the StopEmitting flag. Resume emitting by calling Play.
    // isPaused	Is the Particle System paused right now?
    // isPlaying	Is the Particle System playing right now?
    // isStopped	Is the Particle System stopped right now?
    // lights	Access the particle system lights module.
    // limitVelocityOverLifetime	Access the particle system limit velocity over lifetime module.
    // main	Access the main particle system settings.
    // noise	Access the particle system noise module.
    // particleCount	The current number of particles (Read Only).
    // randomSeed	Override the random seed used for the particle system emission.
    // rotationBySpeed	Access the particle system rotation by speed module.
    // rotationOverLifetime	Access the particle system rotation over lifetime module.
    // shape	Access the particle system shape module.
    // sizeBySpeed	Access the particle system size by speed module.
    // sizeOverLifetime	Access the particle system size over lifetime module.
    // subEmitters	Access the particle system sub emitters module.
    // textureSheetAnimation	Access the particle system texture sheet animation module.
    // time	Playback position in seconds.
    // trails	Access the particle system trails module.
    // trigger	Access the particle system trigger module.
    // useAutoRandomSeed	Controls whether the Particle System uses an automatically-generated random number to seed the random number generator.
    // velocityOverLifetime	Access the particle system velocity over lifetime module.
    //
    // Public Methods
    // Clear	Remove all particles in the particle system.
    // Emit	Emit count particles immediately.
    // GetCustomParticleData	Get a stream of custom per-particle data.
    // GetParticles	Gets the particles of this particle system.
    // IsAlive	Does the system contain any live particles, or will it produce more?
    // Pause	Pauses the system so no new particles are emitted and the existing particles are not updated.
    // Play	Starts the particle system.
    // SetCustomParticleData	Set a stream of custom per-particle data.
    // SetParticles	Sets the particles of this particle system.
    // Simulate	Fastforwards the particle system by simulating particles over given period of time, then pauses it.
    // Stop	Stops playing the particle system using the supplied stop behaviour.
    // TriggerSubEmitter	Triggers the specified sub emitter on all particles of the Particle System.
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Particle = /** @class */ (function () {
        function Particle(particleSystem) {
            this.particleSystem = particleSystem;
            this.position = new THREE.Vector3();
            this.velocity = new THREE.Vector3();
            this.animatedVelocity = new THREE.Vector3();
            this.totalVelocity = new THREE.Vector3();
        }
        return Particle;
    }());
    plume.Particle = Particle;
    // this.particleSystem = particleSystem;
    // this.position = vec3.new(0, 0, 0);
    // this.velocity = vec3.new(0, 0, 0);
    // this.animatedVelocity = vec3.new(0, 0, 0);
    // this.ultimateVelocity = vec3.new(0, 0, 0);
    // this.angularVelocity = vec3.new(0, 0, 0);
    // this.axisOfRotation = vec3.new(0, 0, 0);
    // this.rotation = vec3.new(0, 0, 0);
    // this.startSize = vec3.new(0, 0, 0);
    // this.size = vec3.zero();
    // this.startColor = color4.new(1, 1, 1, 1);
    // this.color = color4.create();
    // this.randomSeed = 0; // uint
    // this.remainingLifetime = 0.0;
    // this.startLifetime = 0.0;
    // this.emitAccumulator0 = 0.0;
    // this.emitAccumulator1 = 0.0;
    // this.frameIndex = 0.0;
    // angularVelocity	The angular velocity of the particle.
    // angularVelocity3D	The 3D angular velocity of the particle.
    // animatedVelocity	The animated velocity of the particle.
    // axisOfRotation	Mesh particles will rotate around this axis.
    // position	The position of the particle.
    // randomSeed	The random seed of the particle.
    // remainingLifetime	The remaining lifetime of the particle.
    // rotation	The rotation of the particle.
    // rotation3D	The 3D rotation of the particle.
    // startColor	The initial color of the particle. The current color of the particle is calculated procedurally based on this value and the active color modules.
    // startLifetime	The starting lifetime of the particle.
    // startSize	The initial size of the particle. The current size of the particle is calculated procedurally based on this value and the active size modules.
    // startSize3D	The initial 3D size of the particle. The current size of the particle is calculated procedurally based on this value and the active size modules.
    // totalVelocity	The total velocity of the particle.
    // velocity	The velocity of the particle.
})(plume || (plume = {}));
var plume;
(function (plume) {
    var MinMaxCurve = /** @class */ (function () {
        function MinMaxCurve() {
        }
        MinMaxCurve.prototype.evaluate = function (time, rndRatio) {
            if (time === void 0) { time = 0; }
            if (rndRatio === void 0) { rndRatio = 0; }
            switch (this.mode) {
                case 0 /* Constant */:
                    return this.constant;
                case 3 /* TwoConstants */:
                    return plume.Mathf.lerp(this.constantMin, this.constantMax, rndRatio);
            }
            plume.logger.warn("MinMaxCurve mode not implemented " + this.mode);
            return null;
        };
        MinMaxCurve.constant = function (v) {
            var m = new MinMaxCurve();
            m.mode = 0 /* Constant */;
            m.constant = v;
            return m;
        };
        MinMaxCurve.randomBetweenTwoConstant = function (min, max) {
            var m = new MinMaxCurve();
            m.mode = 3 /* TwoConstants */;
            m.constantMin = min;
            m.constantMax = max;
            return m;
        };
        return MinMaxCurve;
    }());
    plume.MinMaxCurve = MinMaxCurve;
})(plume || (plume = {}));
// this file is auto-generated.
var plume;
(function (plume) {
    var particle;
    (function (particle) {
        var shaders;
        (function (shaders) {
            shaders.common_fragment = '#define USE_SOFT_PARTICLE 0\n#define USE_BILLBOARD 1\n#define USE_STRETCHED_BILLBOARD 0\n#define USE_HORIZONTAL_BILLBOARD 0\n#define USE_VERTICAL_BILLBOARD 0\n#define USE_WORLD_SPACE 0\nprecision highp float;\nuniform sampler2D mainTexture;\nuniform vec4 tintColor;\nvarying vec2 vUv;\nvarying vec4 vColor;\nvoid main () {\ngl_FragColor = vec4(1.0, 1.0, 0.0, 1.0);\n}';
            shaders.common_vertex = '#define USE_SOFT_PARTICLE 0\n#define USE_BILLBOARD 1\n#define USE_STRETCHED_BILLBOARD 0\n#define USE_HORIZONTAL_BILLBOARD 0\n#define USE_VERTICAL_BILLBOARD 0\n#define USE_WORLD_SPACE 0\nprecision highp float;\nuniform mat4 modelMatrix;\nuniform mat4 modelViewMatrix;\nuniform mat4 projectionMatrix;\nuniform mat4 viewMatrix;\nuniform mat3 normalMatrix;\nuniform vec3 cameraPosition;\nattribute vec3 position;\nattribute vec3 uv;\nattribute vec2 uv0;\nattribute vec4 color;\n#if USE_STRETCHED_BILLBOARD\nattribute vec3 color0;\n#endif\nuniform vec2 frameTile;\nuniform vec2 mainTiling;\nuniform vec2 mainOffset;\n#if USE_STRETCHED_BILLBOARD\nuniform vec3 eye;\nuniform float velocityScale;\nuniform float lengthScale;\n#endif\nvarying vec2 vUv;\nvarying vec4 vColor;\nvoid main () {\nvec4 pos = vec4(position, 1);\n#if USE_STRETCHED_BILLBOARD\nvec4 velocity = vec4(color0.xyz, 0);\n#endif\n#if USE_WORLD_SPACE\n#else\npos = modelMatrix * pos;\n#if USE_STRETCHED_BILLBOARD\nvelocity = modelMatrix * velocity;\n#endif\n#endif\nvec2 cornerOffset = vec2((uv.x - 0.5) * uv0.x, (uv.y - 0.5) * uv0.x);\n#if USE_STRETCHED_BILLBOARD\n#else\nvec2 rotatedOffset;\nrotatedOffset.x = cos(uv0.y) * cornerOffset.x - sin(uv0.y) * cornerOffset.y;\nrotatedOffset.y = sin(uv0.y) * cornerOffset.x + cos(uv0.y) * cornerOffset.y;\n#endif\n#if USE_BILLBOARD\nvec3 camRight = normalize(vec3(viewMatrix[0][0], viewMatrix[1][0], viewMatrix[2][0]));\nvec3 camUp = normalize(vec3(viewMatrix[0][1], viewMatrix[1][1], viewMatrix[2][1]));\npos.xyz += (camRight * rotatedOffset.x) + (camUp * rotatedOffset.y);\n#elif USE_STRETCHED_BILLBOARD\nvec3 camRight = normalize(cross(pos.xyz - eye, velocity.xyz));\nvec3 camUp = velocity.xyz * velocityScale + normalize(velocity.xyz) * lengthScale * uv0.x;\npos.xyz += (camRight * abs(cornerOffset.x) * sign(cornerOffset.y)) - camUp * uv.x;\n#elif USE_HORIZONTAL_BILLBOARD\nvec3 camRight = vec3(1, 0, 0);\nvec3 camUp = vec3(0, 0, -1);\npos.xyz += (camRight * rotatedOffset.x) + (camUp * rotatedOffset.y);\n#elif USE_VERTICAL_BILLBOARD\nvec3 camRight = normalize(vec3(viewMatrix[0][0], viewMatrix[1][0], viewMatrix[2][0]));\nvec3 camUp = vec3(0, 1, 0);\npos.xyz += (camRight * rotatedOffset.x) + (camUp * rotatedOffset.y);\n#else\npos.x += rotatedOffset.x;\npos.y += rotatedOffset.y;\n#endif\npos = projectionMatrix * pos;\nvUv = uv.xy;\nvColor = color;\ngl_Position = pos;\n}';
        })(shaders = particle.shaders || (particle.shaders = {}));
    })(particle = plume.particle || (plume.particle = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var ParticleModule = /** @class */ (function () {
        function ParticleModule() {
        }
        ParticleModule.prototype.initialize = function (particleSystem) {
            this.particleSystem = particleSystem;
        };
        return ParticleModule;
    }());
    plume.ParticleModule = ParticleModule;
})(plume || (plume = {}));
/// <reference path="ParticleModule.ts" />
var plume;
(function (plume) {
    var ParticleEmissionModule = /** @class */ (function (_super) {
        __extends(ParticleEmissionModule, _super);
        function ParticleEmissionModule() {
            var _this = _super.call(this) || this;
            _this.enabled = true;
            _this.rateOverTime = plume.MinMaxCurve.constant(10);
            return _this;
        }
        return ParticleEmissionModule;
    }(plume.ParticleModule));
    plume.ParticleEmissionModule = ParticleEmissionModule;
    //     Properties
    // burstCount	The current number of bursts.
    // enabled	Enable/disable the Emission module.
    // rateOverDistance	The rate at which new particles are spawned, over distance.
    // rateOverDistanceMultiplier	Change the rate over distance multiplier.
    // rateOverTime	The rate at which new particles are spawned, over time.
    // rateOverTimeMultiplier	Change the rate over time multiplier.
})(plume || (plume = {}));
/// <reference path="ParticleModule.ts" />
var plume;
(function (plume) {
    var ParticleMainModule = /** @class */ (function (_super) {
        __extends(ParticleMainModule, _super);
        function ParticleMainModule() {
            var _this = _super.call(this) || this;
            _this.maxParticles = 1000;
            _this.simulationSpeed = 1;
            _this.duration = 5; // in seconds
            _this.loop = true;
            _this.startSpeed = plume.MinMaxCurve.constant(1);
            _this.startDelay = plume.MinMaxCurve.constant(0);
            return _this;
        }
        return ParticleMainModule;
    }(plume.ParticleModule));
    plume.ParticleMainModule = ParticleMainModule;
    //     Properties
    // customSimulationSpace	Simulate particles relative to a custom transform component.
    // duration	The duration of the particle system in seconds.
    // emitterVelocityMode	Control how the Particle System calculates its velocity, when moving in the world.
    // flipRotation	Makes some particles spin in the opposite direction.
    // gravityModifier	Scale applied to the gravity, defined by Physics.gravity.
    // gravityModifierMultiplier	Change the gravity mulutiplier.
    // loop	Is the particle system looping?
    // maxParticles	The maximum number of particles to emit.
    // playOnAwake	If set to true, the particle system will automatically start playing on startup.
    // prewarm	When looping is enabled, this controls whether this particle system will look like it has already simulated for one loop when first becoming visible.
    // scalingMode	Control how the particle system's Transform Component is applied to the particle system.
    // simulationSpace	This selects the space in which to simulate particles. It can be either world or local space.
    // simulationSpeed	Override the default playback speed of the Particle System.
    // startColor	The initial color of particles when emitted.
    // startDelay	Start delay in seconds.
    // startDelayMultiplier	Start delay multiplier in seconds.
    // startLifetime	The total lifetime in seconds that each new particle will have.
    // startLifetimeMultiplier	Start lifetime multiplier.
    // startRotation	The initial rotation of particles when emitted.
    // startRotation3D	A flag to enable 3D particle rotation.
    // startRotationMultiplier	Start rotation multiplier.
    // startRotationX	The initial rotation of particles around the X axis when emitted.
    // startRotationXMultiplier	Start rotation multiplier around the X axis.
    // startRotationY	The initial rotation of particles around the Y axis when emitted.
    // startRotationYMultiplier	Start rotation multiplier around the Y axis.
    // startRotationZ	The initial rotation of particles around the Z axis when emitted.
    // startRotationZMultiplier	Start rotation multiplier around the Z axis.
    // startSize	The initial size of particles when emitted.
    // startSize3D	A flag to enable specifying particle size individually for each axis.
    // startSizeMultiplier	Start size multiplier.
    // startSizeX	The initial size of particles along the X axis when emitted.
    // startSizeXMultiplier	Start rotation multiplier along the X axis.
    // startSizeY	The initial size of particles along the Y axis when emitted.
    // startSizeYMultiplier	Start rotation multiplier along the Y axis.
    // startSizeZ	The initial size of particles along the Z axis when emitted.
    // startSizeZMultiplier	Start rotation multiplier along the Z axis.
    // startSpeed	The initial speed of particles when emitted.
    // startSpeedMultiplier	A multiplier of the initial speed of particles when emitted.
    // stopAction	Configure whether the GameObject will automatically disable or destroy itself, when the Particle System is stopped and all particles have died.
    // useUnscaledTime	When true, use the unscaled delta time to simulate the Particle System. Otherwise, use the scaled delta time.
})(plume || (plume = {}));
/// <reference path="ParticleModule.ts" />
var plume;
(function (plume) {
    var ParticleShapeModule = /** @class */ (function (_super) {
        __extends(ParticleShapeModule, _super);
        function ParticleShapeModule() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ParticleShapeModule;
    }(plume.ParticleModule));
    plume.ParticleShapeModule = ParticleShapeModule;
    //     Properties
    // alignToDirection	Align particles based on their initial direction of travel.
    // angle	Angle of the cone.
    // arc	Circle arc angle.
    // arcMode	The mode used for generating particles around the arc.
    // arcSpeed	When using one of the animated modes, how quickly to move the emission position around the arc.
    // arcSpeedMultiplier	A multiplier of the arc speed of the emission shape.
    // arcSpread	Control the gap between emission points around the arc.
    // boxThickness	Thickness of the box.
    // donutRadius	The radius of the Donut shape.
    // enabled	Enable/disable the Shape module.
    // length	Length of the cone.
    // mesh	Mesh to emit particles from.
    // meshMaterialIndex	Emit particles from a single material of a mesh.
    // meshRenderer	MeshRenderer to emit particles from.
    // meshShapeType	Where on the mesh to emit particles from.
    // normalOffset	Move particles away from the surface of the source mesh.
    // position	Apply an offset to the position from which particles are emitted.
    // radius	Radius of the shape.
    // radiusMode	The mode used for generating particles along the radius.
    // radiusSpeed	When using one of the animated modes, how quickly to move the emission position along the radius.
    // radiusSpeedMultiplier	A multiplier of the radius speed of the emission shape.
    // radiusSpread	Control the gap between emission points along the radius.
    // radiusThickness	Thickness of the radius.
    // randomDirectionAmount	Randomizes the starting direction of particles.
    // randomPositionAmount	Randomizes the starting position of particles.
    // rotation	Apply a rotation to the shape from which particles are emitted.
    // scale	Apply scale to the shape from which particles are emitted.
    // shapeType	Type of shape to emit particles from.
    // skinnedMeshRenderer	SkinnedMeshRenderer to emit particles from.
    // sphericalDirectionAmount	Spherizes the starting direction of particles.
    // sprite	Sprite to emit particles from.
    // spriteRenderer	SpriteRenderer to emit particles from.
    // texture	Selects a texture to be used for tinting particle start colors.
    // textureAlphaAffectsParticles	When enabled, the alpha channel of the texture is applied to the particle alpha when spawned.
    // textureBilinearFiltering	When enabled, 4 neighboring samples are taken from the texture, and combined to give the final particle value.
    // textureClipChannel	Selects which channel of the texture is used for discarding particles.
    // textureClipThreshold	Discards particles when they are spawned on an area of the texture with a value lower than this threshold.
    // textureColorAffectsParticles	When enabled, the RGB channels of the texture are applied to the particle color when spawned.
    // textureUVChannel	When using a Mesh as a source shape type, this option controls which UV channel on the Mesh is used for reading the source texture.
    // useMeshColors	Modulate the particle colors with the vertex colors, or the material color if no vertex colors exist.
    // useMeshMaterialIndex	Emit from a single material, or the whole mesh.
})(plume || (plume = {}));
var plume;
(function (plume) {
    var ParticleContainer = /** @class */ (function (_super) {
        __extends(ParticleContainer, _super);
        function ParticleContainer(maxParticles) {
            var _this = _super.call(this) || this;
            _this.geometry = new THREE.BufferGeometry();
            var vertex = maxParticles * 4; // 4 vertex by particles
            _this.positionAtt = new THREE.BufferAttribute(new Float32Array(vertex * 3), 3).setDynamic(true);
            _this.uvAtt = new THREE.BufferAttribute(new Float32Array(vertex * 2), 2).setDynamic(true);
            _this.geometry.addAttribute("position", _this.positionAtt);
            _this.geometry.addAttribute("uv", _this.uvAtt);
            _this.createMaterial();
            return _this;
        }
        ParticleContainer.prototype.createMaterial = function () {
            this.material = new THREE.RawShaderMaterial({
                transparent: true,
                depthWrite: false,
                uniforms: {
                    'mainTexture': {
                        value: this.mainTexture
                    },
                    'tintColor': {
                        value: this.tintColor
                    }
                },
                blending: THREE.AdditiveBlending,
                vertexShader: plume.particle.shaders.common_vertex,
                fragmentShader: plume.particle.shaders.common_fragment
            });
        };
        ParticleContainer.prototype.setVertexData = function (idx, data) {
            if (data.position != null) {
                this.positionAtt.setXYZ(idx, data.position.x, data.position.y, data.position.z);
            }
            if (data.uv != null) {
                this.uvAtt.setXY(idx, data.uv.x, data.uv.y);
            }
        };
        ParticleContainer.prototype.clear = function () {
            // TODO
        };
        return ParticleContainer;
    }(THREE.Mesh));
    plume.ParticleContainer = ParticleContainer;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var _uvs = [
        0, 0,
        1, 0,
        0, 1,
        1, 1 // top-right
    ];
    var ParticleSystemRenderer = /** @class */ (function () {
        function ParticleSystemRenderer() {
            this.renderMode = 0 /* Billboard */;
            this._vertexDataFlag = {
                position: true,
                uv: true,
            };
            this._vertexDataTmp = {
                position: new THREE.Vector3(),
                uv: new THREE.Vector2(),
            };
        }
        ParticleSystemRenderer.prototype.initialize = function (particleSystem) {
            this.particleSystem = particleSystem;
            var main = particleSystem.main;
            this.model = new plume.ParticleContainer(main.maxParticles);
            this.particleSystem.add(this.model);
        };
        ParticleSystemRenderer.prototype.updateRenderData = function () {
            var idx = 0;
            var particles = this.particleSystem.getParticles();
            for (var i = 0; i < particles.length; i++) {
                var p = particles.data[i];
                for (var j = 0; j < 4; j++) { // four verts per particle.
                    if (this._vertexDataFlag.position) {
                        this._vertexDataTmp.position.copy(p.position);
                    }
                    if (this._vertexDataFlag.uv) {
                        this._vertexDataTmp.uv.x = _uvs[2 * j];
                        this._vertexDataTmp.uv.y = _uvs[2 * j + 1];
                    }
                    this.model.setVertexData(idx, this._vertexDataTmp);
                    idx++;
                }
            }
        };
        ParticleSystemRenderer.prototype._initializeMaterial = function () {
        };
        return ParticleSystemRenderer;
    }());
    plume.ParticleSystemRenderer = ParticleSystemRenderer;
    //     Properties
    // activeVertexStreamsCount	The number of currently active custom vertex streams.
    // alignment	Control the direction that particles face.
    // cameraVelocityScale	How much are the particles stretched depending on the Camera's speed.
    // enableGPUInstancing	Enables GPU Instancing on platforms where it is supported.
    // lengthScale	How much are the particles stretched in their direction of motion.
    // maskInteraction	Specifies how the Particle System Renderer interacts with SpriteMask.
    // maxParticleSize	Clamp the maximum particle size.
    // mesh	Mesh used as particle instead of billboarded texture.
    // meshCount	The number of meshes being used for particle rendering.
    // minParticleSize	Clamp the minimum particle size.
    // normalDirection	How much are billboard particle normals oriented towards the camera.
    // pivot	Modify the pivot point used for rotating particles.
    // renderMode	How particles are drawn.
    // sortingFudge	Biases particle system sorting amongst other transparencies.
    // sortMode	Sort particles within a system.
    // trailMaterial	Set the material used by the Trail module for attaching trails to particles.
    // velocityScale	How much are the particles stretched depending on "how fast they move".
    //
    // Public Methods
    // BakeMesh	Creates a snapshot of ParticleSystemRenderer and stores it in mesh.
    // BakeTrailsMesh	Creates a snapshot of ParticleSystem Trails and stores them in mesh.
    // GetActiveVertexStreams	Query which vertex shader streams are enabled on the ParticleSystemRenderer.
    // GetMeshes	Get the array of meshes to be used as particles.
    // SetActiveVertexStreams	Enable a set of vertex shader streams on the ParticleSystemRenderer.
    // SetMeshes	Set an array of meshes to be used as particles when the ParticleSystemRenderer.renderMode is set to ParticleSystemRenderMode.Mesh.
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.particle.js.map