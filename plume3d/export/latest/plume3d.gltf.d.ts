/// <reference path="plume3d.core.d.ts" />
declare module plume.gltf {
    type MaterialType = (typeof THREE.ShaderMaterial) | (typeof THREE.MeshStandardMaterial) | (typeof THREE.MeshBasicMaterial);
    type GLTFElementFactory = {
        materialFactory?: (type: MaterialType, params: any) => THREE.Material;
        geometryProcessor?: (geometry: THREE.BufferGeometry) => THREE.BufferGeometry;
        materialProcessor?: (material: THREE.Material) => THREE.Material;
    };
    class GLTFLoader {
        manager: THREE.LoadingManager;
        textureLoaderManager: plume.TextureLoaderManager;
        dracoLoader: any;
        crossOrigin: string;
        path: string;
        factory: GLTFElementFactory;
        constructor(manager: THREE.LoadingManager, textureLoaderManager: plume.TextureLoaderManager);
        load(url: string, onLoad: (gltf: GltfObject) => void, onProgress?: (request: ProgressEvent) => void, onError?: (event: ErrorEvent) => void): void;
        setCrossOrigin(value: string): this;
        setPath(value: string): this;
        setDRACOLoader(dracoLoader: any): this;
        parse(data: ArrayBuffer | string, path: string, onLoad: (gltf: GltfObject) => void, onError?: (event: ErrorEvent | Error) => void): void;
    }
}
declare module plume.gltf {
    class GLTFParser {
        json: gltfspec.GlTf;
        extensions: Extensions;
        options: GltfParserOptions;
        _textureLoaderManager: plume.TextureLoaderManager;
        private _fileLoader;
        private _onCompleteCb;
        private _externalResources;
        private _loadedResources;
        private _errorResources;
        _factory: GLTFElementFactory;
        _buffers: Array<ArrayBuffer>;
        _images: Array<THREE.Texture>;
        _textures: Array<THREE.Texture>;
        _accessors: Array<THREE.BufferAttribute | THREE.InterleavedBufferAttribute>;
        _bufferViews: Array<ArrayBuffer>;
        _materials: Array<THREE.Material>;
        _meshes: Array<THREE.Mesh | THREE.SkinnedMesh | THREE.Group>;
        _cameras: Array<THREE.Camera>;
        _animations: Array<THREE.AnimationClip>;
        _animationsRelatives: Array<{
            root: THREE.Object3D;
            clip: THREE.AnimationClip;
        }>;
        _nodes: Array<THREE.Object3D>;
        _scenes: Array<THREE.Scene>;
        _skins: Array<SkinEntry>;
        constructor(json: gltfspec.GlTf, extensions: Extensions, options: GltfParserOptions);
        parse(onLoad: (glTF: GltfObject) => void, onError: (event: ErrorEvent | Error) => void): void;
        private _onCheckDataLoaded;
        private _onExternalDataLoaded;
        /**
         * Marks the special nodes/meshes in json for efficient parse.
         */
        private _markDefs;
        assignTexture(textureInfo: gltfspec.TextureInfo, materialParams: Object, textureName: string): void;
        getBufferView(index: number): ArrayBuffer;
    }
}
declare module plume.gltf {
    type GltfParserOptions = {
        path: string;
        crossOrigin: string;
        manager: THREE.LoadingManager;
        textureLoaderManager: plume.TextureLoaderManager;
        factory?: GLTFElementFactory;
    };
    type GltfObject = {
        animations: Array<THREE.AnimationClip>;
        animationsRelatives: Array<{
            root: THREE.Object3D;
            clip: THREE.AnimationClip;
        }>;
        scene: THREE.Scene;
        scenes: Array<THREE.Scene>;
        cameras: Array<THREE.Camera>;
        asset: Object;
        parser: GLTFParser;
        userData?: any;
    };
    type PrimitiveCacheData = {
        primitive: gltfspec.MeshPrimitive;
        geometry: THREE.BufferGeometry;
    };
    type SkinEntry = {
        joints: number[];
        inverseBindMatrices: THREE.BufferAttribute;
    };
    interface Extensions {
        "KHR_binary_glTF"?: GLTFBinaryExtension;
        "KHR_draco_mesh_compression"?: GLTFDracoMeshCompressionExtension;
        "KHR_lights_punctual"?: GLTFLightsExtension;
        "KHR_materials_pbrSpecularGlossiness"?: GLTFMaterialsPbrSpecularGlossinessExtension;
        "KHR_materials_unlit"?: GLTFMaterialsUnlitExtension;
        "MSFT_texture_dds"?: GLTFTextureDDSExtension;
        "EXT_texture_transform"?: GLTFTextureTransformExtension;
        "GB_lightmaps"?: GLTFLightMapsExtension;
        "GB_sprites"?: GLTFSpritesExtension;
        "GB_Unity"?: GLTFUnityExtension;
    }
    type ExtensionsType = keyof Extensions;
    const WEBGL_CONSTANTS: {
        FLOAT: number;
        FLOAT_MAT3: number;
        FLOAT_MAT4: number;
        FLOAT_VEC2: number;
        FLOAT_VEC3: number;
        FLOAT_VEC4: number;
        LINEAR: number;
        REPEAT: number;
        SAMPLER_2D: number;
        POINTS: number;
        LINES: number;
        LINE_LOOP: number;
        LINE_STRIP: number;
        TRIANGLES: number;
        TRIANGLE_STRIP: number;
        TRIANGLE_FAN: number;
        UNSIGNED_BYTE: number;
        UNSIGNED_SHORT: number;
    };
    const WEBGL_TYPE: {
        5126: NumberConstructor;
        35675: typeof THREE.Matrix3;
        35676: typeof THREE.Matrix4;
        35664: typeof THREE.Vector2;
        35665: typeof THREE.Vector3;
        35666: typeof THREE.Vector4;
        35678: typeof THREE.Texture;
    };
    const WEBGL_COMPONENT_TYPES: {
        5120: Int8ArrayConstructor;
        5121: Uint8ArrayConstructor;
        5122: Int16ArrayConstructor;
        5123: Uint16ArrayConstructor;
        5125: Uint32ArrayConstructor;
        5126: Float32ArrayConstructor;
    };
    type TypedArray = Int8Array | Uint8Array | Int16Array | Uint16Array | Uint32Array | Float32Array;
    type TypedArrayConstructor = (typeof Int8Array) | (typeof Uint8Array) | (typeof Int16Array) | (typeof Uint16Array) | (typeof Uint32Array) | (typeof Float32Array);
    const WEBGL_FILTERS: {
        9728: THREE.TextureFilter;
        9729: THREE.TextureFilter;
        9984: THREE.TextureFilter;
        9985: THREE.TextureFilter;
        9986: THREE.TextureFilter;
        9987: THREE.TextureFilter;
    };
    const WEBGL_WRAPPINGS: {
        33071: THREE.Wrapping;
        33648: THREE.Wrapping;
        10497: THREE.Wrapping;
    };
    const WEBGL_SIDES: {
        1028: THREE.Side;
        1029: THREE.Side;
    };
    const WEBGL_DEPTH_FUNCS: {
        512: THREE.DepthModes;
        513: THREE.DepthModes;
        514: THREE.DepthModes;
        515: THREE.DepthModes;
        516: THREE.DepthModes;
        517: THREE.DepthModes;
        518: THREE.DepthModes;
        519: THREE.DepthModes;
    };
    const WEBGL_BLEND_EQUATIONS: {
        32774: THREE.BlendingEquation;
        32778: THREE.BlendingEquation;
        32779: THREE.BlendingEquation;
    };
    const WEBGL_BLEND_FUNCS: {
        0: THREE.BlendingDstFactor;
        1: THREE.BlendingDstFactor;
        768: THREE.BlendingDstFactor;
        769: THREE.BlendingDstFactor;
        770: THREE.BlendingDstFactor;
        771: THREE.BlendingDstFactor;
        772: THREE.BlendingDstFactor;
        773: THREE.BlendingDstFactor;
        774: THREE.BlendingDstFactor;
        775: THREE.BlendingDstFactor;
        776: THREE.BlendingSrcFactor;
    };
    const WEBGL_TYPE_SIZES: {
        'SCALAR': number;
        'VEC2': number;
        'VEC3': number;
        'VEC4': number;
        'MAT2': number;
        'MAT3': number;
        'MAT4': number;
    };
    const ATTRIBUTES: {
        POSITION: string;
        NORMAL: string;
        TEXCOORD_0: string;
        TEXCOORD_1: string;
        COLOR_0: string;
        WEIGHTS_0: string;
        JOINTS_0: string;
    };
    const PATH_PROPERTIES: {
        scale: string;
        translation: string;
        rotation: string;
        weights: string;
    };
    const INTERPOLATION: {
        CUBICSPLINE: THREE.InterpolationModes;
        LINEAR: THREE.InterpolationModes;
        STEP: THREE.InterpolationModes;
    };
    const STATES_ENABLES: {
        2884: string;
        2929: string;
        3042: string;
        3089: string;
        32823: string;
        32926: string;
    };
    const ALPHA_MODES: {
        OPAQUE: string;
        MASK: string;
        BLEND: string;
    };
    const MIME_TYPE_FORMATS: {
        'image/png': THREE.PixelFormat;
        'image/jpeg': THREE.PixelFormat;
    };
}
declare module plume.gltf {
    function resolveURL(url: string, path: string): string;
    /**
     * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#default-material
     */
    function createDefaultMaterial(): THREE.MeshStandardMaterial;
    function getCachedGeometry(cache: Array<PrimitiveCacheData>, newPrimitive: gltfspec.MeshPrimitive): PrimitiveCacheData;
    function addPrimitiveAttributes(geometry: THREE.BufferGeometry, primitiveDef: gltfspec.MeshPrimitive, accessors: Array<THREE.BufferAttribute | THREE.InterleavedBufferAttribute>): void;
    function updateMorphTargets(mesh: THREE.Mesh, meshDef: any): void;
    function assignExtrasToUserData(object: any, gltfDef: any): void;
}
declare module plume.gltf {
    let BINARY_EXTENSION_HEADER_MAGIC: string;
    class GLTFBinaryExtension {
        name: ExtensionsType;
        content: string;
        header: {
            magic: string;
            version: number;
            length: number;
        };
        body: ArrayBuffer;
        constructor(data: ArrayBuffer);
    }
}
declare module plume.gltf {
    class GLTFDracoMeshCompressionExtension {
        json: gltfspec.GlTf;
        dracoLoader: any;
        name: ExtensionsType;
        dracoDecoder: any;
        constructor(json: gltfspec.GlTf, dracoLoader: any);
        initialize(onComplete: () => void): void;
        decodePrimitive(primitive: gltfspec.MeshPrimitive, parser: GLTFParser): THREE.BufferGeometry;
    }
}
declare module plume.gltf {
    type GLTFLightMapRootExtension = {
        texture: {
            index: number;
        };
    };
    type GLTFLightMapNodeExtension = {
        lightmap: number;
        uvScale: Array<number>;
        uvOffset: Array<number>;
    };
    class GLTFLightMapsExtension {
        parser: GLTFParser;
        name: ExtensionsType;
        lightmaps: Array<GLTFLightMapRootExtension>;
        private _initialized;
        private _processedGeometries;
        private _processedMaterial;
        constructor(json: gltfspec.GlTf);
        private _initTextures;
        applyLightmap(node: THREE.Object3D, lightmapDef: GLTFLightMapNodeExtension): void;
        private _applyLightmapOnMesh;
        private _getTexture;
        private _setUV2;
        private _fixUv2;
        private _copyUvToUv2;
    }
}
declare module plume.gltf {
    class GLTFLightsExtension {
        name: ExtensionsType;
        lights: Array<THREE.Light>;
        constructor(json: gltfspec.GlTf);
    }
}
declare module plume.gltf {
    class GLTFMaterialsPbrSpecularGlossinessExtension {
        name: ExtensionsType;
        specularGlossinessParams: string[];
        constructor();
        getMaterialType(materialDef: gltfspec.Material): typeof THREE.ShaderMaterial;
        extendParams(params: any, material: gltfspec.Material, parser: GLTFParser): void;
        createMaterial(params: any): any;
        /**
         * Clones a GLTFSpecularGlossinessMaterial instance. The ShaderMaterial.copy() method can
         * copy only properties it knows about or inherits, and misses many properties that would
         * normally be defined by MeshStandardMaterial.
         *
         * This method allows GLTFSpecularGlossinessMaterials to be cloned in the process of
         * loading a glTF model, but cloning later (e.g. by the user) would require these changes
         * AND also updating `.onBeforeRender` on the parent mesh.
         *
         * @param  {THREE.ShaderMaterial} source
         * @return {THREE.ShaderMaterial}
         */
        cloneMaterial(source: THREE.ShaderMaterial): THREE.ShaderMaterial;
        refreshUniforms(renderer: THREE.WebGLRenderer, scene: THREE.Scene, camera: THREE.Camera, geometry: THREE.Geometry, material: any, group: THREE.Group): void;
    }
}
declare module plume.gltf {
    class GLTFMaterialsUnlitExtension {
        json: gltfspec.GlTf;
        name: ExtensionsType;
        constructor(json: gltfspec.GlTf);
        getMaterialType(materialDef: gltfspec.Material): typeof THREE.MeshBasicMaterial;
        extendParams(params: any, material: gltfspec.Material, parser: GLTFParser): void;
    }
}
declare module plume.gltf {
    type GLTFSpriteRootExtension = {
        texture: number;
    };
    type GLTFSpriteNodeExtension = {
        id: number;
        color?: Array<number>;
    };
    class GLTFSpritesExtension {
        parser: GLTFParser;
        name: ExtensionsType;
        spritesDef: Array<GLTFSpriteRootExtension>;
        sprites: Array<THREE.Mesh>;
        constructor(json: gltfspec.GlTf);
        getSprite(idx: number): THREE.Mesh;
    }
}
declare module plume.gltf {
    class GLTFTextureDDSExtension {
        name: ExtensionsType;
        ddsLoader: THREE.DDSLoader;
        constructor();
    }
}
declare module plume.gltf {
    type GLTFExtTextureTransformNodeExtension = {
        scale?: Array<number>;
        offset?: Array<number>;
        texCoord?: number;
    };
    class GLTFTextureTransformExtension {
        name: ExtensionsType;
        constructor();
        apply(texture: THREE.Texture, params: GLTFExtTextureTransformNodeExtension): THREE.Texture;
    }
}
declare module plume.gltf {
    type GLTFUnityExtraNodeExtension = {
        static?: boolean;
        activeSelf?: boolean;
        sprite?: GLTFSpriteNodeExtension;
    };
    class GLTFUnityExtension {
        parser: GLTFParser;
        constructor();
        applyToNode(node: THREE.Object3D, def: GLTFUnityExtraNodeExtension): void;
    }
}
declare module plume.gltf {
    class GLTFImporter {
        parser: GLTFParser;
        constructor(parser: GLTFParser);
    }
}
declare module plume.gltf {
    class GLTFAccessorImporter extends GLTFImporter {
        import(): void;
        private _loadAccessor;
    }
}
declare module plume.gltf {
    class GLTFAnimationImporter extends GLTFImporter {
        import(): void;
        private _loadAnimation;
    }
}
declare module plume.gltf {
    class GLTFBufferViewImporter extends GLTFImporter {
        import(): void;
    }
}
declare module plume.gltf {
    class GLTFCameraImporter extends GLTFImporter {
        import(): void;
        private _loadCamera;
    }
}
declare module plume.gltf {
    class GLTFEmbeddedImageImporter extends GLTFImporter {
        import(): void;
        private _loadImage;
    }
}
declare module plume.gltf {
    class GLTFMaterialImporter extends GLTFImporter {
        import(): void;
        private _loadMaterial;
    }
}
declare module plume.gltf {
    class GLTFMeshImporter extends GLTFImporter {
        private _primitiveCache;
        private _cache;
        import(): void;
        private _loadMesh;
        private _loadGeometries;
    }
}
declare module plume.gltf {
    class GLTFNodeImporter extends GLTFImporter {
        import(): void;
        private _loadNode;
    }
}
declare module plume.gltf {
    class GLTFSceneImporter extends GLTFImporter {
        import(): void;
        private _loadScene;
        private _buildNodeHierachy;
    }
}
declare module plume.gltf {
    class GLTFSkinImporter extends GLTFImporter {
        import(): void;
        private _loadSkin;
    }
}
declare module plume.gltf {
    class GLTFTextureImporter extends GLTFImporter {
        import(): void;
        private _loadTexture;
    }
}
declare module plume.gltf {
    class GLTFAnimationsHelper {
        static convertToRelative(rootNode: THREE.Object3D, source: THREE.AnimationClip): THREE.AnimationClip;
    }
}
