/// <reference path="plume3d.core.d.ts" />
/// <reference path="../../vendor/cannon.d.ts" />
declare module plume {
    interface CannonBodyParameter {
        mass: number;
        type?: number;
    }
    class CannonBodyBuilder {
        static newBodyFromMesh(mesh: THREE.Mesh, params?: CannonBodyParameter): CANNON.Body;
        static newBoxShape(w: number, h: number, d: number): CANNON.Box;
        static newBox(w: number, h: number, d: number, params?: CannonBodyParameter): CANNON.Body;
        static newSphere(radius: number, params?: CannonBodyParameter): CANNON.Body;
        static newCylinder(radiusTop: number, radiusBottom: number, height: number, numSegments: number, params?: CannonBodyParameter, fixRotation?: boolean): CANNON.Body;
        static newPlane(rotation?: THREE.Quaternion): CANNON.Body;
        static newTriMeshShape(geometry: THREE.BufferGeometry | THREE.Geometry): CANNON.Trimesh;
        static newTriMesh(geometry: THREE.BufferGeometry | THREE.Geometry): CANNON.Body;
        static newRigidBody(collisionShape: CANNON.Shape, params?: CannonBodyParameter): CANNON.Body;
    }
}
declare module plume {
    class CannonSimulation extends SimulationSupport {
        optFixedTimeStep: number;
        optMaxSubSteps: number;
        readyHandlers: Handler<boolean>;
        private _ready;
        private _world;
        private _dynamicBodies;
        private _dynamicBodiesData;
        private _bodies;
        private _visuals;
        private _stats;
        constructor(init?: (world: CANNON.World) => void);
        update(dt: number): void;
        readonly stats: SimulationStats;
        setGravity(x: number, y: number, z: number): void;
        protected onStart(): void;
        protected onStop(): void;
        getWorld(): CANNON.World;
        updatePositionAndRotation(body: CANNON.Body, position?: THREE.Vector3, rotation?: THREE.Quaternion): void;
        addRigidBody(body: CANNON.Body, mesh: THREE.Object3D, dynamic: boolean, group?: number, mask?: number): void;
        removeRigidBody(body: CANNON.Body): void;
        setDynamicBodySyncProperty(body: CANNON.Body, syncPosition: boolean, syncRotation: boolean): void;
        getRigidBodies(): Array<CANNON.Body>;
        removeRigidBodies(): void;
        private _syncMesh;
        destroy(): void;
        addVisual(body: CANNON.Body, material: THREE.MeshMaterialType, scene: THREE.Scene): void;
        removeVisual(mesh: THREE.Object3D): void;
        removeVisuals(): void;
        createVisual(body: CANNON.Body, material: THREE.MeshMaterialType): THREE.Object3D;
    }
}
