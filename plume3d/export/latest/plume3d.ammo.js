"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var plume;
(function (plume) {
    var AmmoBodyBuilder = /** @class */ (function () {
        function AmmoBodyBuilder() {
        }
        AmmoBodyBuilder.newBodyFromMesh = function (mesh, params) {
            var geometry = mesh.geometry;
            if (geometry instanceof THREE.BoxBufferGeometry) {
                geometry.computeBoundingBox();
                var bbox = geometry.boundingBox;
                var size = new THREE.Vector3();
                bbox.getSize(size);
                return this.newBox(size.x, size.y, size.z, params);
            }
            else if (geometry instanceof THREE.SphereBufferGeometry) {
                geometry.computeBoundingSphere();
                var s = geometry.boundingSphere;
                return this.newSphere(s.radius, params);
            }
            plume.logger.warn("Unable to build physics body from mesh: " + mesh);
            return null;
        };
        AmmoBodyBuilder.newBoxShape = function (w, h, d) {
            var geometry = new Ammo.btBoxShape(new Ammo.btVector3(w * 0.5, h * 0.5, d * 0.5));
            return geometry;
        };
        AmmoBodyBuilder.newBox = function (w, h, d, params) {
            if (params === void 0) { params = { mass: 0 }; }
            var geometry = this.newBoxShape(w, h, d);
            return this.newRigidBody(geometry, params);
        };
        AmmoBodyBuilder.newSphere = function (radius, params) {
            if (params === void 0) { params = { mass: 0 }; }
            var geometry = new Ammo.btSphereShape(radius);
            return this.newRigidBody(geometry, params);
        };
        AmmoBodyBuilder.newPlane = function (normal, distance) {
            var shape = new Ammo.btStaticPlaneShape(new Ammo.btVector3(normal.x, normal.y, normal.z), distance);
            return this.newRigidBody(shape);
        };
        AmmoBodyBuilder.newTriMeshShape = function (geometry) {
            var triMesh = new Ammo.btTriangleMesh(true, false);
            var unindexed;
            if (geometry.index) {
                unindexed = geometry.toNonIndexed();
            }
            else {
                unindexed = geometry;
            }
            var vertices = unindexed.getAttribute("position").array;
            for (var i = 0; i < vertices.length; i += 9) {
                var v0 = new Ammo.btVector3(vertices[i + 0], vertices[i + 1], vertices[i + 2]);
                var v1 = new Ammo.btVector3(vertices[i + 3], vertices[i + 4], vertices[i + 5]);
                var v2 = new Ammo.btVector3(vertices[i + 6], vertices[i + 7], vertices[i + 8]);
                triMesh.addTriangle(v0, v1, v2, false);
            }
            var shape = new Ammo.btBvhTriangleMeshShape(triMesh, false);
            return shape;
        };
        AmmoBodyBuilder.newTriMesh = function (geometry) {
            var shape = this.newTriMeshShape(geometry);
            return this.newRigidBody(shape);
        };
        AmmoBodyBuilder.newRigidBody = function (collisionShape, params) {
            if (params === void 0) { params = { mass: 0 }; }
            var transform = new Ammo.btTransform();
            transform.setIdentity();
            //transform.setOrigin(new Ammo.btVector3(pos.x, pos.y, pos.z));
            //transform.setRotation(new Ammo.btQuaternion(quat.x, quat.y, quat.z, quat.w));
            var motionState = new Ammo.btDefaultMotionState(transform);
            var localInertia = new Ammo.btVector3(0, 0, 0);
            collisionShape.calculateLocalInertia(params.mass, localInertia);
            var rbInfo = new Ammo.btRigidBodyConstructionInfo(params.mass, motionState, collisionShape, localInertia);
            var body = new Ammo.btRigidBody(rbInfo);
            // body.setFriction(friction);
            return body;
        };
        return AmmoBodyBuilder;
    }());
    plume.AmmoBodyBuilder = AmmoBodyBuilder;
})(plume || (plume = {}));
var plume;
(function (plume) {
    ;
})(plume || (plume = {}));
/// <reference path="../../build/latest/plume3d.core.d.ts" />
/// <reference path="../../vendor/ammo.d.ts" />
var plume;
(function (plume) {
    //
    // Ammo module init
    //
    var INITDONE = false;
    function initAmmo(ready) {
        if (INITDONE) {
            ready();
            return;
        }
        // var Module = { TOTAL_MEMORY: 67108864 * 2 };
        var option = {};
        // If 'auto' try to load wabassembly version
        if (typeof window["WebAssembly"] !== 'object' || AmmoSimulation.ammoLibType === 'js') {
            // Load js version
            var jsUrl = AmmoSimulation.ammoLibPath + "ammo.js";
            plume.http.loadJs(jsUrl, function () {
                var ammoModule = window["Ammo"];
                ammoModule(option).then(function () {
                    plume.logger.debug("Ammo ready");
                    INITDONE = true;
                    ready();
                });
            });
        }
        else {
            // Load wasm version
            var jsUrl = AmmoSimulation.ammoLibPath + "ammo.wasm.js";
            var wasmUrl_1 = AmmoSimulation.ammoLibPath + "ammo.wasm.wasm";
            plume.http.loadJs(jsUrl, function () {
                plume.logger.debug("Loading Ammo wasm");
                plume.http.loadArrayBuffer(wasmUrl_1, function (data) {
                    var ammoModule = window["Ammo"];
                    option.wasmBinary = data;
                    ammoModule(option).then(function () {
                        plume.logger.debug("Ammo ready");
                        INITDONE = true;
                        ready();
                    });
                });
            });
        }
    }
    var AmmoSimulation = /** @class */ (function (_super) {
        __extends(AmmoSimulation, _super);
        function AmmoSimulation() {
            var _this = _super.call(this) || this;
            // to check http://www.bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_The_World
            _this.optFixedTimeStep = 1 / 60;
            _this.optMaxSubSteps = 10;
            _this.readyHandlers = new plume.Handler();
            _this._ready = false;
            _this._dynamicBodies = []; // only used to track idx
            _this._dynamicBodiesData = [];
            _this._bodies = [];
            _this._pointer = 0;
            _this._customDataByBody = new plume.HashMap();
            _this._internalTickCbRegistered = false;
            _this._contactAddedCallbackRegistered = false;
            _this._contactProcessedCallbackRegistered = false;
            _this._contactDestroyedCallbackRegistered = false;
            var self = _this;
            _this._stats = { avg: 0, _timeTotal: 0, _flush: 0 };
            initAmmo(function () {
                self._ready = true;
                self._initOnReady();
                self.readyHandlers.fire(true);
            });
            return _this;
        }
        AmmoSimulation.prototype._initOnReady = function () {
            // Physics configuration
            var collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
            var dispatcher = new Ammo.btCollisionDispatcher(collisionConfiguration);
            var broadphase = new Ammo.btDbvtBroadphase();
            var solver = new Ammo.btSequentialImpulseConstraintSolver();
            this._physicsWorld = new Ammo.btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
            this._physicsWorld.setGravity(new Ammo.btVector3(0, -10, 0));
            this._TRANSFORM_AUX = new Ammo.btTransform();
            this._stats._flush = performance.now();
            this._stats._timeTotal = 0;
        };
        AmmoSimulation.prototype._syncMesh = function () {
            for (var i = 0; i < this._dynamicBodiesData.length; i++) {
                var data = this._dynamicBodiesData[i];
                var body = data.rigidbody;
                var mesh = data.mesh;
                var ms = body.getMotionState();
                if (ms) {
                    ms.getWorldTransform(this._TRANSFORM_AUX);
                    if (data.syncPosition) {
                        var p = this._TRANSFORM_AUX.getOrigin();
                        mesh.position.set(p.x(), p.y(), p.z());
                    }
                    if (data.syncRotation) {
                        var q = this._TRANSFORM_AUX.getRotation();
                        mesh.quaternion.set(q.x(), q.y(), q.z(), q.w());
                    }
                }
            }
        };
        AmmoSimulation.prototype.isReady = function () {
            return this._ready;
        };
        AmmoSimulation.prototype.update = function (dt) {
            _super.prototype.update.call(this, dt);
            if (!this._ready)
                return;
            if (!this._running)
                return;
            var sEllapsed = dt / 1000; // delta need to be in seconds
            var now = performance.now();
            this._physicsWorld.stepSimulation(sEllapsed, this.optMaxSubSteps, this.optFixedTimeStep);
            this._syncMesh();
            var after = performance.now();
            var statsEllapsed = (now - this._stats._flush);
            if (statsEllapsed >= 1000) {
                this._stats.avg = Math.round((this._stats._timeTotal / statsEllapsed) * 100) / 100;
                this._stats._flush = now;
                this._stats._timeTotal = 0;
            }
            this._stats._timeTotal += (after - now);
        };
        Object.defineProperty(AmmoSimulation.prototype, "stats", {
            get: function () {
                return this._stats;
            },
            enumerable: true,
            configurable: true
        });
        AmmoSimulation.prototype.setGravity = function (x, y, z) {
            this._physicsWorld.setGravity(new Ammo.btVector3(x, y, z));
        };
        AmmoSimulation.prototype.onStart = function () {
        };
        AmmoSimulation.prototype.onStop = function () {
        };
        AmmoSimulation.prototype.getWorld = function () {
            return this._physicsWorld;
        };
        AmmoSimulation.prototype.updatePositionAndRotation = function (body, position, rotation, scaling) {
            var ms = body.getMotionState();
            var cs = body.getCollisionShape();
            if (cs != null && scaling != null) {
                var locals = cs.getLocalScaling();
                if (locals.x() != scaling.x || locals.y() != scaling.y || locals.z() != scaling.z) {
                    cs.setLocalScaling(new Ammo.btVector3(scaling.x, scaling.y, scaling.z));
                }
            }
            if (ms) {
                ms.getWorldTransform(this._TRANSFORM_AUX);
                var p = this._TRANSFORM_AUX.getOrigin();
                var q = this._TRANSFORM_AUX.getRotation();
                if (position != null) {
                    p.setX(position.x);
                    p.setY(position.y);
                    p.setZ(position.z);
                }
                if (rotation != null) {
                    q.setValue(rotation.x, rotation.y, rotation.z, rotation.w);
                }
                this._TRANSFORM_AUX.setOrigin(p);
                this._TRANSFORM_AUX.setRotation(q);
                ms.setWorldTransform(this._TRANSFORM_AUX);
                body.setMotionState(ms);
            }
        };
        AmmoSimulation.prototype.addRigidBody = function (body, mesh, dynamic, group, mask) {
            if (mesh != null) {
                // initialize position and rotation body from mesh
                this.updatePositionAndRotation(body, mesh.position, mesh.quaternion, mesh.scale);
            }
            if (dynamic) {
                // add body to sync list
                this._dynamicBodies.push(body);
                this._dynamicBodiesData.push({
                    rigidbody: body,
                    mesh: mesh,
                    syncPosition: true,
                    syncRotation: true,
                });
            }
            this._bodies.push(body);
            this._physicsWorld.addRigidBody(body, group, mask);
            // logger.debug("dynamic bodies: " + this._dynamicBodies.length);
        };
        AmmoSimulation.prototype.removeRigidBody = function (body) {
            var idx = this._dynamicBodies.indexOf(body);
            if (idx >= 0) {
                this._dynamicBodies.splice(idx, 1);
                this._dynamicBodiesData.splice(idx, 1);
            }
            idx = this._bodies.indexOf(body);
            if (idx >= 0) {
                this._bodies.splice(idx, 1);
            }
            else {
                // logger.warn("Body not registered");
            }
            this._physicsWorld.removeRigidBody(body);
        };
        AmmoSimulation.prototype.setDynamicBodySyncProperty = function (body, syncPosition, syncRotation) {
            var idx = this._dynamicBodies.indexOf(body);
            var data = this._dynamicBodiesData[idx];
            data.syncPosition = syncPosition;
            data.syncRotation = syncRotation;
        };
        AmmoSimulation.prototype.getRigidBodies = function () {
            return this._bodies;
        };
        AmmoSimulation.prototype.removeRigidBodies = function () {
            for (var i = this._bodies.length - 1; i >= 0; i++) {
                var b = this._bodies[i];
                this.removeRigidBody(b);
            }
        };
        AmmoSimulation.prototype.setCustomData = function (body, data) {
            var ptr = body.getUserIndex();
            if (ptr == 0 || ptr == null) {
                this._pointer++;
                ptr = this._pointer;
                body.setUserIndex(this._pointer);
            }
            this._customDataByBody.put(ptr + "", data);
        };
        AmmoSimulation.prototype.getCustomData = function (body) {
            var ptr = body.getUserIndex();
            if (ptr == 0 || ptr == null) {
                return null;
            }
            var data = this._customDataByBody.get(ptr + "");
            return data;
        };
        // getCollisions() {
        //     return this._collisions;
        // }
        // checkCollisions() {
        //     if (this._collisions.length > 0) {
        //         this._collisions = [];
        //     }
        //     let dispatcher = this._physicsWorld.getDispatcher();
        //     let numManifolds = dispatcher.getNumManifolds();
        //     for (let i = 0; i < numManifolds; i++) {
        //         let manifold = dispatcher.getManifoldByIndexInternal(i);
        //         let numContacts = manifold.getNumContacts();
        //         // if (numContacts <= 0) {
        //         //     continue;
        //         // }
        //         let body0 = manifold.getBody0() as Ammo.btRigidBody;
        //         let body1 = manifold.getBody1() as Ammo.btRigidBody;
        //         let uptr0 = body0.getUserIndex();
        //         let uptr1 = body1.getUserIndex();
        //         console.log("Contact: " + uptr0 + " vs " + uptr1);
        //         let bc: BodyContact = {
        //             body0: body0,
        //             body1: body1,
        //             data0: (uptr0 != 0 ? this.getCustomData(body0) : null),
        //             data1: (uptr1 != 0 ? this.getCustomData(body1) : null),
        //             contacts: []
        //         }
        //         for (let p = 0; p < numContacts; p++) {
        //             let cp = manifold.getContactPoint(p);
        //             bc.contacts.push({
        //                 distance: cp.getDistance(),
        //             });
        //             console.log("M" + i + ", CP" + p + ": distance: " + cp.getDistance());
        //         }
        //         this._collisions.push(bc);
        //     }
        // }
        AmmoSimulation.prototype.setInternalTickCallback = function (fn) {
            if (!this._internalTickCbRegistered) {
                // Register function pointer only once
                var tickCallbackPointer = Ammo["addFunction"](this._onInternalTickCallback.bind(this));
                this._physicsWorld.setInternalTickCallbackPtr(tickCallbackPointer, null, false);
                this._internalTickCbRegistered = true;
            }
            this._internalTickCallback = fn;
        };
        AmmoSimulation.prototype.setContactAddedCallback = function (fn) {
            if (!this._contactAddedCallbackRegistered) {
                var pointer = Ammo["addFunction"](this._onContactAdded.bind(this));
                this._physicsWorld.setContactAddedCallback(pointer);
                this._contactAddedCallbackRegistered = true;
            }
            this._contactAddedCallback = fn;
        };
        AmmoSimulation.prototype.setContactProcessedCallback = function (fn) {
            if (!this._contactProcessedCallbackRegistered) {
                var pointer = Ammo["addFunction"](this._onContactProcessed.bind(this));
                this._physicsWorld.setContactProcessedCallback(pointer);
                this._contactProcessedCallbackRegistered = true;
            }
            this._contactProcessedCallback = fn;
        };
        AmmoSimulation.prototype.setcontactDestroyedCallback = function (fn) {
            if (!this._contactDestroyedCallbackRegistered) {
                var pointer = Ammo["addFunction"](this._onContactDestroyed.bind(this));
                this._physicsWorld.setContactDestroyedCallback(pointer);
                this._contactDestroyedCallbackRegistered = true;
            }
            this._contactDestroyedCallback = fn;
        };
        AmmoSimulation.prototype.wrapPointer = function (ptr, clazz) {
            var v = Ammo["wrapPointer"](ptr, clazz);
            return v;
        };
        AmmoSimulation.prototype._onInternalTickCallback = function (arg0, arg1) {
            if (this._internalTickCallback != null)
                this._internalTickCallback();
        };
        AmmoSimulation.prototype._onContactAdded = function (cp, colObj0, partId0, index0, colObj1, partId1, index1) {
            if (this._contactAddedCallback != null) {
                var obj0 = this.wrapPointer(colObj0, Ammo.btRigidBody);
                var obj1 = this.wrapPointer(colObj1, Ammo.btRigidBody);
                var contactPoint = this.wrapPointer(cp, Ammo.btManifoldPoint);
                this._contactAddedCallback(obj0, obj1, contactPoint);
            }
        };
        AmmoSimulation.prototype._onContactProcessed = function (cp, body0, body1) {
            if (this._contactProcessedCallback != null) {
                var obj0 = this.wrapPointer(body0, Ammo.btRigidBody);
                var obj1 = this.wrapPointer(body1, Ammo.btRigidBody);
                var contactPoint = this.wrapPointer(cp, Ammo.btManifoldPoint);
                this._contactProcessedCallback(obj0, obj1, contactPoint);
            }
        };
        AmmoSimulation.prototype._onContactDestroyed = function (arg0) {
            if (this._contactDestroyedCallback != null)
                this._contactDestroyedCallback(arg0);
        };
        AmmoSimulation.ammoLibPath = 'build/lib/ammo/';
        AmmoSimulation.ammoLibType = 'auto';
        AmmoSimulation.DISABLE_DEACTIVATION = 4;
        return AmmoSimulation;
    }(plume.SimulationSupport));
    plume.AmmoSimulation = AmmoSimulation;
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.ammo.js.map