/// <reference path="plume3d.core.d.ts" />
/// <reference path="../../vendor/ammo.d.ts" />
declare module plume {
    class AmmoBodyBuilder {
        static newBodyFromMesh(mesh: THREE.Mesh, params?: BodyParameter): Ammo.btRigidBody;
        static newBoxShape(w: number, h: number, d: number): Ammo.btBoxShape;
        static newBox(w: number, h: number, d: number, params?: BodyParameter): Ammo.btRigidBody;
        static newSphere(radius: number, params?: BodyParameter): Ammo.btRigidBody;
        static newPlane(normal: THREE.Vector3, distance: number): Ammo.btRigidBody;
        static newTriMeshShape(geometry: THREE.BufferGeometry): Ammo.btBvhTriangleMeshShape;
        static newTriMesh(geometry: THREE.BufferGeometry): Ammo.btRigidBody;
        static newRigidBody(collisionShape: Ammo.btCollisionShape, params?: BodyParameter): Ammo.btRigidBody;
    }
}
declare module plume {
    type AmmoContactCallback = (body0: Ammo.btRigidBody, body1: Ammo.btRigidBody, contactPoint: Ammo.btManifoldPoint) => void;
    const enum AmmoCollisionFlags {
        CF_STATIC_OBJECT = 1,
        CF_KINEMATIC_OBJECT = 2,
        CF_NO_CONTACT_RESPONSE = 4,
        CF_CUSTOM_MATERIAL_CALLBACK = 8,
        CF_CHARACTER_OBJECT = 16,
        CF_DISABLE_VISUALIZE_OBJECT = 32,
        CF_DISABLE_SPU_COLLISION_PROCESSING = 64
    }
    const enum AmmoCollisionFilterGroups {
        DefaultFilter = 1,
        StaticFilter = 2,
        KinematicFilter = 4,
        DebrisFilter = 8,
        SensorTrigger = 16,
        CharacterFilter = 32,
        AllFilter = -1
    }
}
declare module plume {
    class AmmoSimulation extends SimulationSupport {
        static ammoLibPath: string;
        static ammoLibType: string;
        static readonly DISABLE_DEACTIVATION = 4;
        optFixedTimeStep: number;
        optMaxSubSteps: number;
        readyHandlers: Handler<boolean>;
        private _ready;
        private _physicsWorld;
        private _dynamicBodies;
        private _dynamicBodiesData;
        private _bodies;
        private _pointer;
        private _customDataByBody;
        private _internalTickCallback;
        private _internalTickCbRegistered;
        private _contactAddedCallback;
        private _contactAddedCallbackRegistered;
        private _contactProcessedCallback;
        private _contactProcessedCallbackRegistered;
        private _contactDestroyedCallback;
        private _contactDestroyedCallbackRegistered;
        private _stats;
        private _TRANSFORM_AUX;
        constructor();
        private _initOnReady;
        private _syncMesh;
        isReady(): boolean;
        update(dt: number): void;
        readonly stats: SimulationStats;
        setGravity(x: number, y: number, z: number): void;
        protected onStart(): void;
        protected onStop(): void;
        getWorld(): Ammo.btDiscreteDynamicsWorld;
        updatePositionAndRotation(body: Ammo.btRigidBody, position?: THREE.Vector3, rotation?: THREE.Quaternion, scaling?: THREE.Vector3): void;
        addRigidBody(body: Ammo.btRigidBody, mesh: THREE.Object3D, dynamic: boolean, group?: number, mask?: number): void;
        removeRigidBody(body: Ammo.btRigidBody): void;
        setDynamicBodySyncProperty(body: Ammo.btRigidBody, syncPosition: boolean, syncRotation: boolean): void;
        getRigidBodies(): Array<Ammo.btRigidBody>;
        removeRigidBodies(): void;
        setCustomData(body: Ammo.btRigidBody, data: any): void;
        getCustomData(body: Ammo.btRigidBody): any;
        setInternalTickCallback(fn: () => void): void;
        setContactAddedCallback(fn: AmmoContactCallback): void;
        setContactProcessedCallback(fn: AmmoContactCallback): void;
        setcontactDestroyedCallback(fn: (userdata: any) => void): void;
        wrapPointer<T>(ptr: number, clazz: {
            new (...args: any[]): T;
        }): T;
        private _onInternalTickCallback;
        private _onContactAdded;
        private _onContactProcessed;
        private _onContactDestroyed;
    }
}
