"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var plume;
(function (plume) {
    var TrailMaterial = /** @class */ (function (_super) {
        __extends(TrailMaterial, _super);
        function TrailMaterial(parameters) {
            var _this = _super.call(this) || this;
            _this.type = "TrailMaterial";
            _this.uniforms.minID = { value: null };
            _this.uniforms.maxID = { value: null };
            _this.uniforms.tailSharp = { value: null };
            _this.uniforms.headColor = { value: new THREE.Vector4() };
            _this.uniforms.tailColor = { value: new THREE.Vector4() };
            _this.vertexShader = plume.trail.shaders.base_vertex;
            _this.fragmentShader = plume.trail.shaders.base_fragment;
            _this.transparent = true;
            _this.alphaTest = 0.5;
            _this.blending = THREE.CustomBlending;
            _this.blendSrc = THREE.SrcAlphaFactor;
            _this.blendDst = THREE.OneMinusSrcAlphaFactor;
            _this.blendEquation = THREE.AddEquation;
            _this.depthTest = true;
            _this.depthWrite = false;
            _this.side = THREE.DoubleSide;
            _this.setValues(parameters);
            return _this;
        }
        Object.defineProperty(TrailMaterial.prototype, "minID", {
            get: function () {
                return this.uniforms.minID.value;
            },
            set: function (v) {
                this.uniforms.minID.value = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrailMaterial.prototype, "maxID", {
            get: function () {
                return this.uniforms.maxID.value;
            },
            set: function (v) {
                this.uniforms.maxID.value = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrailMaterial.prototype, "tailSharp", {
            get: function () {
                return this.uniforms.tailSharp.value;
            },
            set: function (v) {
                this.uniforms.tailSharp.value = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrailMaterial.prototype, "headColor", {
            get: function () {
                return this.uniforms.headColor.value;
            },
            set: function (v) {
                this.uniforms.headColor.value = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrailMaterial.prototype, "tailColor", {
            get: function () {
                return this.uniforms.tailColor.value;
            },
            set: function (v) {
                this.uniforms.tailColor.value = v;
            },
            enumerable: true,
            configurable: true
        });
        return TrailMaterial;
    }(THREE.RawShaderMaterial));
    plume.TrailMaterial = TrailMaterial;
})(plume || (plume = {}));
// /// <reference path="../../build/latest/plume3d.core.d.ts" />
// // from https://github.com/mkkellogg/TrailRendererJS
// //
// // TODO: test, cleanup, types, review api
// //
// module plume {
//     type NodeConnection = {
//         attribute: THREE.BufferAttribute,
//         offset: number,
//         count: number
//     }
//     export class TrailRenderer {
//         private static _PositionComponentCount = 3;
//         private static _IndicesPerFace = 3;
//         private static _FacesPerQuad = 2;
//         private _scene: THREE.Scene;
//         private _active = false;
//         private _geometry: THREE.BufferGeometry = null;
//         private _mesh: THREE.Mesh = null;
//         private _nodeCenters: Array<THREE.Vector3> = null;
//         private _currentNodeCenter: THREE.Vector3 = null;
//         private _nodeIDs: Array<number> = null;
//         private _currentLength = 0;
//         private _currentEnd = 0;
//         private _currentNodeID = 0;
//         //
//         private _verticesPerNode: number;
//         private _faceIndicesPerNode: number;
//         private _facesPerNode: number;
//         private _length: number;
//         private _targetObject: THREE.Object3D;
//         private _localHeadGeometry: Array<THREE.Vector3>;
//         private _vertexCount: number;
//         private _faceCount: number;
//         public material: THREE.ShaderMaterial;
//         private static _materialInst: THREE.ShaderMaterial;
//         constructor() {
//             this._scene = Game.get().engine.scene;
//             this._createBaseMaterial();
//         }
//         private _createBaseMaterial() {
//             return this._CreateMaterial();
//         }
//         public initialize(length: number, tailSharp: boolean, localHeadWidth: number, targetObject: THREE.Object3D) {
//             this.deactivate();
//             this._DestroyMesh();
//             this._length = (length > 0) ? length + 1 : 0;
//             this._targetObject = targetObject;
//             this._InitializeLocalHeadGeometry(localHeadWidth);
//             this._nodeIDs = [];
//             this._nodeCenters = [];
//             for (let i = 0; i < this._length; i++) {
//                 this._nodeIDs[i] = -1;
//                 this._nodeCenters[i] = new THREE.Vector3();
//             }
//             this._InitializeGeometry();
//             this._InitializeMesh();
//             this.material.uniforms.minID.value = 0;
//             this.material.uniforms.maxID.value = 0;
//             this.material.uniforms.tailSharp.value = (tailSharp ? 1 : 0);
//             this._Reset();
//         }
//         private _CreateMaterial() {
//             if (TrailRenderer._materialInst != null) {
//                 this.material = TrailRenderer._materialInst;
//                 return this.material;
//             }
//             // let customUniforms : any = {};
//             // customUniforms.minID = { type: "f", value: null };
//             // customUniforms.maxID = { type: "f", value: null };
//             // customUniforms.tailSharp = { type: "f", value: null };
//             // customUniforms.headColor = { type: "v4", value: new THREE.Vector4() };
//             // customUniforms.tailColor = { type: "v4", value: new THREE.Vector4() };
//             // let vertexShader = trail.shaders.base_vertex;
//             // let fragmentShader = trail.shaders.base_fragment;
//             // this.material = new THREE.RawShaderMaterial(
//             //     {
//             //         uniforms: customUniforms,
//             //         vertexShader: vertexShader,
//             //         fragmentShader: fragmentShader,
//             //         transparent: true,
//             //         alphaTest: 0.5,
//             //         blending: THREE.CustomBlending,
//             //         blendSrc: THREE.SrcAlphaFactor,
//             //         blendDst: THREE.OneMinusSrcAlphaFactor,
//             //         blendEquation: THREE.AddEquation,
//             //         depthTest: true,
//             //         depthWrite: false,
//             //         side: THREE.DoubleSide
//             //     });
//             // TrailRenderer._materialInst = this.material;
//             // return this.material;
//             this.material = new TrailMaterial();
//             TrailRenderer._materialInst = this.material;
//             return this.material;
//         }
//         private _InitializeLocalHeadGeometry(localHeadWidth: number) {
//             this._localHeadGeometry = [];
//             let halfWidth = localHeadWidth / 2;
//             this._localHeadGeometry.push(new THREE.Vector3(-halfWidth, 0, 0));
//             this._localHeadGeometry.push(new THREE.Vector3(halfWidth, 0, 0));
//             this._verticesPerNode = 2;
//             this._facesPerNode = (this._verticesPerNode - 1) * 2;
//             this._faceIndicesPerNode = this._facesPerNode * 3;
//         }
//         private _InitializeGeometry() {
//             this._vertexCount = this._length * this._verticesPerNode;
//             this._faceCount = this._length * this._facesPerNode;
//             let geometry = new THREE.BufferGeometry();
//             let nodeIDs = new Float32Array(this._vertexCount);
//             let nodeVertexIDs = new Float32Array(this._vertexCount * this._verticesPerNode);
//             let positions = new Float32Array(this._vertexCount * TrailRenderer._PositionComponentCount);
//             let nodeCenters = new Float32Array(this._vertexCount * TrailRenderer._PositionComponentCount);
//             let indices = new Uint32Array(this._faceCount * TrailRenderer._IndicesPerFace);
//             let nodeIDAttribute = new THREE.BufferAttribute(nodeIDs, 1);
//             nodeIDAttribute.setDynamic(true);
//             geometry.addAttribute('nodeID', nodeIDAttribute);
//             let nodeVertexIDAttribute = new THREE.BufferAttribute(nodeVertexIDs, 1);
//             nodeVertexIDAttribute.setDynamic(true);
//             geometry.addAttribute('nodeVertexID', nodeVertexIDAttribute);
//             let nodeCenterAttribute = new THREE.BufferAttribute(nodeCenters, TrailRenderer._PositionComponentCount);
//             nodeCenterAttribute.setDynamic(true);
//             geometry.addAttribute('nodeCenter', nodeCenterAttribute);
//             let positionAttribute = new THREE.BufferAttribute(positions, TrailRenderer._PositionComponentCount);
//             positionAttribute.setDynamic(true);
//             geometry.addAttribute('position', positionAttribute);
//             let indexAttribute = new THREE.BufferAttribute(indices, 1);
//             indexAttribute.setDynamic(true);
//             geometry.setIndex(indexAttribute);
//             this._geometry = geometry;
//         }
//         private _ZeroVertices() {
//             let positions = this._geometry.getAttribute('position') as THREE.BufferAttribute;
//             for (let i = 0; i < this._vertexCount; i++) {
//                 positions.setXYZ(i, 0, 0, 0);
//             }
//             positions.needsUpdate = true;
//             positions.updateRange.count = - 1;
//         }
//         private _ZeroIndices() {
//             let indices = this._geometry.getIndex() as THREE.BufferAttribute;
//             for (let i = 0; i < this._faceCount; i++) {
//                 indices.setXYZ(i, 0, 0, 0);
//             }
//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;
//         }
//         private _FormInitialFaces() {
//             this._ZeroIndices();
//             let indices = this._geometry.getIndex();
//             for (let i = 0; i < this._length - 1; i++) {
//                 this._connectNodes(i, i + 1);
//             }
//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;
//         }
//         private _InitializeMesh() {
//             this._mesh = new THREE.Mesh(this._geometry, this.material);
//             // this.mesh.matrixAutoUpdate = false;
//         }
//         private _DestroyMesh() {
//             if (this._mesh) {
//                 this._scene.remove(this._mesh);
//                 this._mesh = null;
//             }
//         }
//         private _Reset() {
//             this._currentLength = 0;
//             this._currentEnd = -1;
//             this._currentNodeCenter = null;
//             this._currentNodeID = 0;
//             this._FormInitialFaces();
//             this._ZeroVertices();
//             this._geometry.setDrawRange(0, 0);
//         }
//         private _updateUniforms() {
//             if (this._currentLength < this._length) {
//                 this.material.uniforms.minID.value = 0;
//             } else {
//                 this.material.uniforms.minID.value = this._currentNodeID - this._length;
//             }
//             this.material.uniforms.maxID.value = this._currentNodeID;
//         }
//         update() {
//             this._mesh.position.copy(this._targetObject.position);
//             this._mesh.quaternion.copy(this._targetObject.quaternion);
//             this._mesh.scale.copy(this._targetObject.scale);
//             this._advance();
//         }
//         private _tempMatrix4 = new THREE.Matrix4();
//         // updateHead() {
//         //     if (this.currentEnd < 0) return;
//         //     this.targetObject.updateMatrixWorld(false);
//         //     this.tempMatrix4.copy(this.targetObject.matrixWorld);
//         //     this.updateNodePositionsFromTransformMatrix(this.currentEnd, this.tempMatrix4);
//         // }
//         private _advance() {
//             this._tempMatrix4.copy(this._targetObject.matrixWorld);
//             this._advanceWithTransform(this._tempMatrix4);
//             this._updateUniforms();
//         }
//         private _advanceWithTransform(transformMatrix: THREE.Matrix4) {
//             this._advanceGeometry(transformMatrix);
//         }
//         private _advanceGeometry(transformMatrix: THREE.Matrix4) {
//             let nextIndex = this._currentEnd + 1 >= this._length ? 0 : this._currentEnd + 1;
//             this._updateNodePositionsFromTransformMatrix(nextIndex, transformMatrix);
//             if (this._currentLength >= 1) {
//                 this._connectNodes(this._currentEnd, nextIndex);
//                 if (this._currentLength >= this._length) {
//                     let disconnectIndex = this._currentEnd + 1 >= this._length ? 0 : this._currentEnd + 1;
//                     this._disconnectNodes(disconnectIndex);
//                 }
//             }
//             if (this._currentLength < this._length) {
//                 this._currentLength++;
//             }
//             this._currentEnd++;
//             if (this._currentEnd >= this._length) {
//                 this._currentEnd = 0;
//             }
//             if (this._currentLength >= 1) {
//                 if (this._currentLength < this._length) {
//                     this._geometry.setDrawRange(0, (this._currentLength - 1) * this._faceIndicesPerNode);
//                 } else {
//                     this._geometry.setDrawRange(0, this._currentLength * this._faceIndicesPerNode);
//                 }
//             }
//             this._updateNodeID(this._currentEnd, this._currentNodeID);
//             this._currentNodeID++;
//         }
//         private _updateNodeID(nodeIndex: number, id: number) {
//             this._nodeIDs[nodeIndex] = id;
//             let nodeIDs = this._geometry.getAttribute('nodeID') as THREE.BufferAttribute;
//             let nodeVertexIDs = this._geometry.getAttribute('nodeVertexID') as THREE.BufferAttribute;
//             for (let i = 0; i < this._verticesPerNode; i++) {
//                 let baseIndex = nodeIndex * this._verticesPerNode + i;
//                 nodeIDs.setX(baseIndex, id);
//                 nodeVertexIDs.setX(baseIndex, i);
//                 // nodeIDs.array[baseIndex] = id;
//                 // nodeVertexIDs.array[baseIndex] = i;
//             }
//             nodeIDs.needsUpdate = true;
//             nodeVertexIDs.needsUpdate = true;
//             nodeIDs.updateRange.offset = nodeIndex * this._verticesPerNode;
//             nodeIDs.updateRange.count = this._verticesPerNode;
//             nodeVertexIDs.updateRange.offset = nodeIndex * this._verticesPerNode;
//             nodeVertexIDs.updateRange.count = this._verticesPerNode;
//         }
//         private _updateNodeCenter(nodeIndex: number, nodeCenter: THREE.Vector3) {
//             this._currentNodeCenter = this._nodeCenters[nodeIndex];
//             this._currentNodeCenter.copy(nodeCenter);
//             let nodeCenters = this._geometry.getAttribute('nodeCenter') as THREE.BufferAttribute;
//             for (let i = 0; i < this._verticesPerNode; i++) {
//                 let baseIndex = (nodeIndex * this._verticesPerNode + i);
//                 nodeCenters.setXYZ(baseIndex, nodeCenter.x, nodeCenter.y, nodeCenter.z);
//             }
//             nodeCenters.needsUpdate = true;
//             nodeCenters.updateRange.offset = nodeIndex * this._verticesPerNode * TrailRenderer._PositionComponentCount;
//             nodeCenters.updateRange.count = this._verticesPerNode * TrailRenderer._PositionComponentCount;
//         }
//         private _tempPosition = new THREE.Vector3();
//         private _updateNodePositionsFromTransformMatrix(nodeIndex: number, transformMatrix: THREE.Matrix4) {
//             let positions = this._geometry.getAttribute('position') as THREE.BufferAttribute;
//             this._tempPosition.set(0, 0, 0);
//             this._tempPosition.applyMatrix4(transformMatrix);
//             this._updateNodeCenter(nodeIndex, this._tempPosition);
//             let vertex = this._tempPosition;
//             for (let i = 0; i < this._localHeadGeometry.length; i++) {
//                 vertex.copy(this._localHeadGeometry[i]);
//                 vertex.applyMatrix4(transformMatrix);
//                 let positionIndex = ((this._verticesPerNode * nodeIndex) + i);
//                 positions.setXYZ(positionIndex, vertex.x, vertex.y, vertex.z);
//             }
//             positions.needsUpdate = true;
//             positions.updateRange.offset = nodeIndex * this._verticesPerNode * TrailRenderer._PositionComponentCount;
//             positions.updateRange.count = this._verticesPerNode * TrailRenderer._PositionComponentCount;
//         }
//         private _connectNodes(srcNodeIndex: number, destNodeIndex: number) {
//             let returnObj: NodeConnection = {
//                 "attribute": null,
//                 "offset": 0,
//                 "count": - 1
//             };
//             let indices = this._geometry.getIndex() as any;
//             for (let i = 0; i < this._localHeadGeometry.length - 1; i++) {
//                 let srcVertexIndex = (this._verticesPerNode * srcNodeIndex) + i;
//                 let destVertexIndex = (this._verticesPerNode * destNodeIndex) + i;
//                 let faceIndex = ((srcNodeIndex * this._facesPerNode) + (i * TrailRenderer._FacesPerQuad)) * TrailRenderer._IndicesPerFace;
//                 indices.setXYZ(faceIndex, srcVertexIndex, destVertexIndex, (srcVertexIndex + 1));
//                 indices.setXYZ(faceIndex + 3, destVertexIndex, (destVertexIndex + 1), (srcVertexIndex + 1));
//             }
//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;
//             returnObj.attribute = indices;
//             returnObj.offset = srcNodeIndex * this._facesPerNode * TrailRenderer._IndicesPerFace;
//             returnObj.count = this._facesPerNode * TrailRenderer._IndicesPerFace;
//             return returnObj;
//         }
//         private _disconnectNodes(srcNodeIndex: number) {
//             let returnObj: NodeConnection = {
//                 "attribute": null,
//                 "offset": 0,
//                 "count": - 1
//             };
//             let indices = this._geometry.getIndex() as any;
//             for (let i = 0; i < this._localHeadGeometry.length - 1; i++) {
//                 let faceIndex = ((srcNodeIndex * this._facesPerNode) + (i * TrailRenderer._FacesPerQuad)) * TrailRenderer._IndicesPerFace;
//                 indices.setXYZ(faceIndex, 0, 0, 0);
//                 indices.setXYZ(faceIndex + 3, 0, 0, 0);
//             }
//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;
//             returnObj.attribute = indices;
//             returnObj.offset = srcNodeIndex * this._facesPerNode * TrailRenderer._IndicesPerFace;
//             returnObj.count = this._facesPerNode * TrailRenderer._IndicesPerFace;
//             return returnObj;
//         }
//         public deactivate() {
//             if (this._active) {
//                 this._scene.remove(this._mesh);
//                 this._active = false;
//             }
//         }
//         public activate() {
//             if (!this._active) {
//                 this._scene.add(this._mesh);
//                 this._active = true;
//             }
//         }
//     }
//     // let Shader: any = {};
//     // Shader.BaseVertexVars = [
//     //     "attribute float nodeID;",
//     //     "attribute float nodeVertexID;",
//     //     "attribute vec3 nodeCenter;",
//     //     "uniform float minID;",
//     //     "uniform float maxID;",
//     //     "uniform vec4 headColor;",
//     //     "uniform vec4 tailColor;",
//     //     "uniform float tailSharp;",
//     //     "varying vec4 vColor;",
//     // ].join("\n");
//     // Shader.BaseFragmentVars = [
//     //     "varying vec4 vColor;",
//     //     "uniform sampler2D texture;",
//     // ].join("\n");
//     // Shader.VertexShaderCore = [
//     //     "float fraction = ( maxID - nodeID ) / ( maxID - minID );",
//     //     "vColor = ( 1.0 - fraction ) * headColor + fraction * tailColor;",
//     //     "vec4 realPosition; ",
//     //     "if(tailSharp == 1.0) {",
//     //     "  realPosition = vec4( ( 1.0 - fraction ) * position.xyz + fraction * nodeCenter.xyz, 1.0 ); ",
//     //     "} else {",
//     //     "  realPosition = vec4( position.xyz, 1.0 ); ",
//     //     "}",
//     // ].join("\n");
//     // Shader.BaseVertexShader = [
//     //     Shader.BaseVertexVars,
//     //     "void main() { ",
//     //     Shader.VertexShaderCore,
//     //     "gl_Position = projectionMatrix * viewMatrix * realPosition;",
//     //     "}"
//     // ].join("\n");
//     // Shader.BaseFragmentShader = [
//     //     Shader.BaseFragmentVars,
//     //     "void main() { ",
//     //     "gl_FragColor = vColor;",
//     //     "}"
//     // ].join("\n");
// }
/// <reference path="../../build/latest/plume3d.core.d.ts" />
// from https://github.com/mkkellogg/TrailRendererJS
//
// TODO: test, cleanup, types, review api
//
var plume;
(function (plume) {
    var TrailRenderer = /** @class */ (function () {
        function TrailRenderer(material, scene) {
            this._active = false;
            this._geometry = null;
            this._mesh = null;
            this._nodeCenters = null;
            this._currentNodeCenter = null;
            this._nodeIDs = null;
            this._currentLength = 0;
            this._currentEnd = 0;
            this._currentNodeID = 0;
            this._tempMatrix4 = new THREE.Matrix4();
            this._tempPosition = new THREE.Vector3();
            this._scene = (scene != null ? scene : plume.Game.get().engine.scene);
            this.material = (material != null ? material : TrailRenderer.getOrCreateSharedMaterial());
        }
        TrailRenderer.getOrCreateSharedMaterial = function () {
            if (this._sharedMaterial != null) {
                return this._sharedMaterial;
            }
            this._sharedMaterial = new plume.TrailMaterial();
            return this._sharedMaterial;
        };
        TrailRenderer.prototype.initialize = function (length, tailSharp, localHeadWidth, targetObject) {
            this.deactivate();
            this._destroyMesh();
            this._length = (length > 0) ? length + 1 : 0;
            this._targetObject = targetObject;
            this._initializeLocalHeadGeometry(localHeadWidth);
            this._nodeIDs = [];
            this._nodeCenters = [];
            for (var i = 0; i < this._length; i++) {
                this._nodeIDs[i] = -1;
                this._nodeCenters[i] = new THREE.Vector3();
            }
            this._initializeGeometry();
            this._initializeMesh();
            this.material.minID = 0;
            this.material.maxID = 0;
            this.material.tailSharp = (tailSharp ? 1 : 0);
            this._Reset();
        };
        TrailRenderer.prototype._initializeLocalHeadGeometry = function (localHeadWidth) {
            var halfWidth = localHeadWidth / 2;
            this._localHeadGeometry = [];
            this._localHeadGeometry.push(new THREE.Vector3(-halfWidth, 0, 0));
            this._localHeadGeometry.push(new THREE.Vector3(halfWidth, 0, 0));
            this._verticesPerNode = 2;
            this._facesPerNode = (this._verticesPerNode - 1) * 2;
            this._faceIndicesPerNode = this._facesPerNode * 3;
        };
        TrailRenderer.prototype._initializeGeometry = function () {
            this._vertexCount = this._length * this._verticesPerNode;
            this._faceCount = this._length * this._facesPerNode;
            var geometry = new THREE.BufferGeometry();
            var nodeIDs = new Float32Array(this._vertexCount);
            var nodeVertexIDs = new Float32Array(this._vertexCount * this._verticesPerNode);
            var positions = new Float32Array(this._vertexCount * TrailRenderer._PositionComponentCount);
            var nodeCenters = new Float32Array(this._vertexCount * TrailRenderer._PositionComponentCount);
            var indices = new Uint32Array(this._faceCount * TrailRenderer._IndicesPerFace);
            var nodeIDAttribute = new THREE.BufferAttribute(nodeIDs, 1);
            nodeIDAttribute.setDynamic(true);
            geometry.addAttribute('nodeID', nodeIDAttribute);
            var nodeVertexIDAttribute = new THREE.BufferAttribute(nodeVertexIDs, 1);
            nodeVertexIDAttribute.setDynamic(true);
            geometry.addAttribute('nodeVertexID', nodeVertexIDAttribute);
            var nodeCenterAttribute = new THREE.BufferAttribute(nodeCenters, TrailRenderer._PositionComponentCount);
            nodeCenterAttribute.setDynamic(true);
            geometry.addAttribute('nodeCenter', nodeCenterAttribute);
            var positionAttribute = new THREE.BufferAttribute(positions, TrailRenderer._PositionComponentCount);
            positionAttribute.setDynamic(true);
            geometry.addAttribute('position', positionAttribute);
            var indexAttribute = new THREE.BufferAttribute(indices, 1);
            indexAttribute.setDynamic(true);
            geometry.setIndex(indexAttribute);
            this._geometry = geometry;
        };
        TrailRenderer.prototype._ZeroVertices = function () {
            var positions = this._geometry.getAttribute('position');
            for (var i = 0; i < this._vertexCount; i++) {
                positions.setXYZ(i, 0, 0, 0);
            }
            positions.needsUpdate = true;
            positions.updateRange.count = -1;
        };
        TrailRenderer.prototype._ZeroIndices = function () {
            var indices = this._geometry.getIndex();
            for (var i = 0; i < this._faceCount; i++) {
                indices.setXYZ(i, 0, 0, 0);
            }
            indices.needsUpdate = true;
            indices.updateRange.count = -1;
        };
        TrailRenderer.prototype._FormInitialFaces = function () {
            this._ZeroIndices();
            var indices = this._geometry.getIndex();
            for (var i = 0; i < this._length - 1; i++) {
                this._connectNodes(i, i + 1);
            }
            indices.needsUpdate = true;
            indices.updateRange.count = -1;
        };
        TrailRenderer.prototype._initializeMesh = function () {
            this._mesh = new THREE.Mesh(this._geometry, this.material);
            // this.mesh.matrixAutoUpdate = false;
        };
        TrailRenderer.prototype._destroyMesh = function () {
            if (this._mesh) {
                this._scene.remove(this._mesh);
                this._mesh = null;
            }
        };
        TrailRenderer.prototype._Reset = function () {
            this._currentLength = 0;
            this._currentEnd = -1;
            this._currentNodeCenter = null;
            this._currentNodeID = 0;
            this._FormInitialFaces();
            this._ZeroVertices();
            this._geometry.setDrawRange(0, 0);
        };
        TrailRenderer.prototype._updateUniforms = function () {
            if (this._currentLength < this._length) {
                this.material.uniforms.minID.value = 0;
            }
            else {
                this.material.uniforms.minID.value = this._currentNodeID - this._length;
            }
            this.material.uniforms.maxID.value = this._currentNodeID;
        };
        TrailRenderer.prototype.update = function () {
            this._mesh.position.copy(this._targetObject.position);
            this._mesh.quaternion.copy(this._targetObject.quaternion);
            this._mesh.scale.copy(this._targetObject.scale);
            this._advance();
        };
        // updateHead() {
        //     if (this.currentEnd < 0) return;
        //     this.targetObject.updateMatrixWorld(false);
        //     this.tempMatrix4.copy(this.targetObject.matrixWorld);
        //     this.updateNodePositionsFromTransformMatrix(this.currentEnd, this.tempMatrix4);
        // }
        TrailRenderer.prototype._advance = function () {
            this._tempMatrix4.copy(this._targetObject.matrixWorld);
            this._advanceWithTransform(this._tempMatrix4);
            this._updateUniforms();
        };
        TrailRenderer.prototype._advanceWithTransform = function (transformMatrix) {
            this._advanceGeometry(transformMatrix);
        };
        TrailRenderer.prototype._advanceGeometry = function (transformMatrix) {
            var nextIndex = this._currentEnd + 1 >= this._length ? 0 : this._currentEnd + 1;
            this._updateNodePositionsFromTransformMatrix(nextIndex, transformMatrix);
            if (this._currentLength >= 1) {
                this._connectNodes(this._currentEnd, nextIndex);
                if (this._currentLength >= this._length) {
                    var disconnectIndex = this._currentEnd + 1 >= this._length ? 0 : this._currentEnd + 1;
                    this._disconnectNodes(disconnectIndex);
                }
            }
            if (this._currentLength < this._length) {
                this._currentLength++;
            }
            this._currentEnd++;
            if (this._currentEnd >= this._length) {
                this._currentEnd = 0;
            }
            if (this._currentLength >= 1) {
                if (this._currentLength < this._length) {
                    this._geometry.setDrawRange(0, (this._currentLength - 1) * this._faceIndicesPerNode);
                }
                else {
                    this._geometry.setDrawRange(0, this._currentLength * this._faceIndicesPerNode);
                }
            }
            this._updateNodeID(this._currentEnd, this._currentNodeID);
            this._currentNodeID++;
        };
        TrailRenderer.prototype._updateNodeID = function (nodeIndex, id) {
            this._nodeIDs[nodeIndex] = id;
            var nodeIDs = this._geometry.getAttribute('nodeID');
            var nodeVertexIDs = this._geometry.getAttribute('nodeVertexID');
            for (var i = 0; i < this._verticesPerNode; i++) {
                var baseIndex = nodeIndex * this._verticesPerNode + i;
                nodeIDs.setX(baseIndex, id);
                nodeVertexIDs.setX(baseIndex, i);
                // nodeIDs.array[baseIndex] = id;
                // nodeVertexIDs.array[baseIndex] = i;
            }
            nodeIDs.needsUpdate = true;
            nodeVertexIDs.needsUpdate = true;
            nodeIDs.updateRange.offset = nodeIndex * this._verticesPerNode;
            nodeIDs.updateRange.count = this._verticesPerNode;
            nodeVertexIDs.updateRange.offset = nodeIndex * this._verticesPerNode;
            nodeVertexIDs.updateRange.count = this._verticesPerNode;
        };
        TrailRenderer.prototype._updateNodeCenter = function (nodeIndex, nodeCenter) {
            this._currentNodeCenter = this._nodeCenters[nodeIndex];
            this._currentNodeCenter.copy(nodeCenter);
            var nodeCenters = this._geometry.getAttribute('nodeCenter');
            for (var i = 0; i < this._verticesPerNode; i++) {
                var baseIndex = (nodeIndex * this._verticesPerNode + i);
                nodeCenters.setXYZ(baseIndex, nodeCenter.x, nodeCenter.y, nodeCenter.z);
            }
            nodeCenters.needsUpdate = true;
            nodeCenters.updateRange.offset = nodeIndex * this._verticesPerNode * TrailRenderer._PositionComponentCount;
            nodeCenters.updateRange.count = this._verticesPerNode * TrailRenderer._PositionComponentCount;
        };
        TrailRenderer.prototype._updateNodePositionsFromTransformMatrix = function (nodeIndex, transformMatrix) {
            var positions = this._geometry.getAttribute('position');
            this._tempPosition.set(0, 0, 0);
            this._tempPosition.applyMatrix4(transformMatrix);
            this._updateNodeCenter(nodeIndex, this._tempPosition);
            var vertex = this._tempPosition;
            for (var i = 0; i < this._localHeadGeometry.length; i++) {
                vertex.copy(this._localHeadGeometry[i]);
                vertex.applyMatrix4(transformMatrix);
                var positionIndex = ((this._verticesPerNode * nodeIndex) + i);
                positions.setXYZ(positionIndex, vertex.x, vertex.y, vertex.z);
            }
            positions.needsUpdate = true;
            positions.updateRange.offset = nodeIndex * this._verticesPerNode * TrailRenderer._PositionComponentCount;
            positions.updateRange.count = this._verticesPerNode * TrailRenderer._PositionComponentCount;
        };
        TrailRenderer.prototype._connectNodes = function (srcNodeIndex, destNodeIndex) {
            var returnObj = {
                "attribute": null,
                "offset": 0,
                "count": -1
            };
            var indices = this._geometry.getIndex();
            for (var i = 0; i < this._localHeadGeometry.length - 1; i++) {
                var srcVertexIndex = (this._verticesPerNode * srcNodeIndex) + i;
                var destVertexIndex = (this._verticesPerNode * destNodeIndex) + i;
                var faceIndex = ((srcNodeIndex * this._facesPerNode) + (i * TrailRenderer._FacesPerQuad)) * TrailRenderer._IndicesPerFace;
                indices.setXYZ(faceIndex, srcVertexIndex, destVertexIndex, (srcVertexIndex + 1));
                indices.setXYZ(faceIndex + 3, destVertexIndex, (destVertexIndex + 1), (srcVertexIndex + 1));
            }
            indices.needsUpdate = true;
            indices.updateRange.count = -1;
            returnObj.attribute = indices;
            returnObj.offset = srcNodeIndex * this._facesPerNode * TrailRenderer._IndicesPerFace;
            returnObj.count = this._facesPerNode * TrailRenderer._IndicesPerFace;
            return returnObj;
        };
        TrailRenderer.prototype._disconnectNodes = function (srcNodeIndex) {
            var returnObj = {
                "attribute": null,
                "offset": 0,
                "count": -1
            };
            var indices = this._geometry.getIndex();
            for (var i = 0; i < this._localHeadGeometry.length - 1; i++) {
                var faceIndex = ((srcNodeIndex * this._facesPerNode) + (i * TrailRenderer._FacesPerQuad)) * TrailRenderer._IndicesPerFace;
                indices.setXYZ(faceIndex, 0, 0, 0);
                indices.setXYZ(faceIndex + 3, 0, 0, 0);
            }
            indices.needsUpdate = true;
            indices.updateRange.count = -1;
            returnObj.attribute = indices;
            returnObj.offset = srcNodeIndex * this._facesPerNode * TrailRenderer._IndicesPerFace;
            returnObj.count = this._facesPerNode * TrailRenderer._IndicesPerFace;
            return returnObj;
        };
        TrailRenderer.prototype.deactivate = function () {
            if (this._active) {
                this._scene.remove(this._mesh);
                this._active = false;
            }
        };
        TrailRenderer.prototype.activate = function () {
            if (!this._active) {
                this._scene.add(this._mesh);
                this._active = true;
            }
        };
        TrailRenderer._PositionComponentCount = 3;
        TrailRenderer._IndicesPerFace = 3;
        TrailRenderer._FacesPerQuad = 2;
        return TrailRenderer;
    }());
    plume.TrailRenderer = TrailRenderer;
    // let Shader: any = {};
    // Shader.BaseVertexVars = [
    //     "attribute float nodeID;",
    //     "attribute float nodeVertexID;",
    //     "attribute vec3 nodeCenter;",
    //     "uniform float minID;",
    //     "uniform float maxID;",
    //     "uniform vec4 headColor;",
    //     "uniform vec4 tailColor;",
    //     "uniform float tailSharp;",
    //     "varying vec4 vColor;",
    // ].join("\n");
    // Shader.BaseFragmentVars = [
    //     "varying vec4 vColor;",
    //     "uniform sampler2D texture;",
    // ].join("\n");
    // Shader.VertexShaderCore = [
    //     "float fraction = ( maxID - nodeID ) / ( maxID - minID );",
    //     "vColor = ( 1.0 - fraction ) * headColor + fraction * tailColor;",
    //     "vec4 realPosition; ",
    //     "if(tailSharp == 1.0) {",
    //     "  realPosition = vec4( ( 1.0 - fraction ) * position.xyz + fraction * nodeCenter.xyz, 1.0 ); ",
    //     "} else {",
    //     "  realPosition = vec4( position.xyz, 1.0 ); ",
    //     "}",
    // ].join("\n");
    // Shader.BaseVertexShader = [
    //     Shader.BaseVertexVars,
    //     "void main() { ",
    //     Shader.VertexShaderCore,
    //     "gl_Position = projectionMatrix * viewMatrix * realPosition;",
    //     "}"
    // ].join("\n");
    // Shader.BaseFragmentShader = [
    //     Shader.BaseFragmentVars,
    //     "void main() { ",
    //     "gl_FragColor = vColor;",
    //     "}"
    // ].join("\n");
})(plume || (plume = {}));
// this file is auto-generated.
var plume;
(function (plume) {
    var trail;
    (function (trail) {
        var shaders;
        (function (shaders) {
            shaders.base_fragment = 'precision highp float;\nvarying vec4 vColor;\nvoid main() {\ngl_FragColor = vColor;\n}';
            shaders.base_vertex = 'precision highp float;\nuniform mat4 modelMatrix;\nuniform mat4 modelViewMatrix;\nuniform mat4 projectionMatrix;\nuniform mat4 viewMatrix;\nuniform mat3 normalMatrix;\nuniform vec3 cameraPosition;\nattribute vec3 position;\nattribute vec3 uv;\nattribute float nodeID;\nattribute float nodeVertexID;\nattribute vec3 nodeCenter;\nuniform float minID;\nuniform float maxID;\nuniform vec4 headColor;\nuniform vec4 tailColor;\nuniform float tailSharp;\nvarying vec4 vColor;\nvoid main() {\nfloat fraction = ( maxID - nodeID ) / ( maxID - minID );\nvColor = ( 1.0 - fraction ) * headColor + fraction * tailColor;\nvec4 realPosition;\nif(tailSharp == 1.0) {\nrealPosition = vec4( ( 1.0 - fraction ) * position.xyz + fraction * nodeCenter.xyz, 1.0 );\n} else {\nrealPosition = vec4( position.xyz, 1.0 );\n}\ngl_Position = projectionMatrix * viewMatrix * realPosition;\n}';
        })(shaders = trail.shaders || (trail.shaders = {}));
    })(trail = plume.trail || (plume.trail = {}));
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.trail.js.map