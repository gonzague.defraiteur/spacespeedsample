#define USE_SOFT_PARTICLE 0
#define USE_BILLBOARD 1
#define USE_STRETCHED_BILLBOARD 0
#define USE_HORIZONTAL_BILLBOARD 0
#define USE_VERTICAL_BILLBOARD 0
#define USE_WORLD_SPACE 0

precision highp float;

// standard threejs uniform
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 cameraPosition;

attribute vec3 position;
attribute vec3 uv;
attribute vec2 uv0;
attribute vec4 color;

#if USE_STRETCHED_BILLBOARD
    attribute vec3 color0;
#endif

uniform vec2 frameTile;
uniform vec2 mainTiling;
uniform vec2 mainOffset;

// uniform mat4 model;
// uniform mat4 viewProj;
// #if USE_BILLBOARD || USE_VERTICAL_BILLBOARD
//     uniform mat4 view;
// #endif

#if USE_STRETCHED_BILLBOARD
    uniform vec3 eye;
    uniform float velocityScale;
    uniform float lengthScale;
#endif

varying vec2 vUv;
varying vec4 vColor;

void main () {
    vec4 pos = vec4(position, 1);
    #if USE_STRETCHED_BILLBOARD
        vec4 velocity = vec4(color0.xyz, 0);
    #endif
    #if USE_WORLD_SPACE
        
    #else
        pos = modelMatrix * pos;
        #if USE_STRETCHED_BILLBOARD
            velocity = modelMatrix * velocity;
        #endif
    #endif

    vec2 cornerOffset = vec2((uv.x - 0.5) * uv0.x, (uv.y - 0.5) * uv0.x);
    
    #if USE_STRETCHED_BILLBOARD
        
    #else
        vec2 rotatedOffset;
        rotatedOffset.x = cos(uv0.y) * cornerOffset.x - sin(uv0.y) * cornerOffset.y;
        rotatedOffset.y = sin(uv0.y) * cornerOffset.x + cos(uv0.y) * cornerOffset.y;
    #endif

    #if USE_BILLBOARD
        vec3 camRight = normalize(vec3(viewMatrix[0][0], viewMatrix[1][0], viewMatrix[2][0]));
        vec3 camUp = normalize(vec3(viewMatrix[0][1], viewMatrix[1][1], viewMatrix[2][1]));
        pos.xyz += (camRight * rotatedOffset.x) + (camUp * rotatedOffset.y);
        #elif USE_STRETCHED_BILLBOARD
            vec3 camRight = normalize(cross(pos.xyz - eye, velocity.xyz));
            vec3 camUp = velocity.xyz * velocityScale + normalize(velocity.xyz) * lengthScale * uv0.x;
            pos.xyz += (camRight * abs(cornerOffset.x) * sign(cornerOffset.y)) - camUp * uv.x;
        #elif USE_HORIZONTAL_BILLBOARD
            vec3 camRight = vec3(1, 0, 0);
            vec3 camUp = vec3(0, 0, -1);
            pos.xyz += (camRight * rotatedOffset.x) + (camUp * rotatedOffset.y);
        #elif USE_VERTICAL_BILLBOARD
            vec3 camRight = normalize(vec3(viewMatrix[0][0], viewMatrix[1][0], viewMatrix[2][0]));
            vec3 camUp = vec3(0, 1, 0);
            pos.xyz += (camRight * rotatedOffset.x) + (camUp * rotatedOffset.y);
    #else
        pos.x += rotatedOffset.x;
        pos.y += rotatedOffset.y;
    #endif

    pos = projectionMatrix * pos;

    // vec2 aniUV = vec2(0, floor(uv.z * frameTile.y));
    // aniUV.x = floor(uv.z * frameTile.x * frameTile.y - aniUV.y * frameTile.x);
    // aniUV.y = frameTile.y - aniUV.y - 1.0;
    // aniUV = (aniUV.xy + uv.xy) / vec2(frameTile.x, frameTile.y);
    // uv = aniUV * mainTiling + mainOffset;
    vUv = uv.xy;

    vColor = color;
    gl_Position = pos;
}