#define USE_SOFT_PARTICLE 0
#define USE_BILLBOARD 1
#define USE_STRETCHED_BILLBOARD 0
#define USE_HORIZONTAL_BILLBOARD 0
#define USE_VERTICAL_BILLBOARD 0
#define USE_WORLD_SPACE 0

precision highp float;

uniform sampler2D mainTexture;
uniform vec4 tintColor;

varying vec2 vUv;
varying vec4 vColor;

void main () {
    //gl_FragColor = 2.0 * vColor * tintColor * texture2D(mainTexture, vUv);
    gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0);
}