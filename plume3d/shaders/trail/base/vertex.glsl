precision highp float;

// standard threejs uniform
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 cameraPosition;
attribute vec3 position;
attribute vec3 uv;


attribute float nodeID;
attribute float nodeVertexID;
attribute vec3 nodeCenter;
uniform float minID;
uniform float maxID;
uniform vec4 headColor;
uniform vec4 tailColor;
uniform float tailSharp;
varying vec4 vColor;

void main() { 
    float fraction = ( maxID - nodeID ) / ( maxID - minID );
    vColor = ( 1.0 - fraction ) * headColor + fraction * tailColor;
    vec4 realPosition; 
    if(tailSharp == 1.0) {
        realPosition = vec4( ( 1.0 - fraction ) * position.xyz + fraction * nodeCenter.xyz, 1.0 ); 
    } else {
        realPosition = vec4( position.xyz, 1.0 ); 
    }

    gl_Position = projectionMatrix * viewMatrix * realPosition;
}
