"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ts = require("typescript");
var Lint = require("tslint");
var Rule = /** @class */ (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NamingSchemeWalker(sourceFile, this.getOptions()));
    };
    Rule.PRIVATE_METHOD_FAIL = "Private method should start with _";
    Rule.METHOD_FAIL = "Method should be camelCase";
    Rule.PRIVATE_PROPERTY_FAIL = "Private property should start with _";
    Rule.READONLY_PROPERTY_FAIL = "Read only property should be UPPERCASE";
    Rule.PROPERTY_FAIL = "Property should be camelCase";
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
// Class, interface: PascaleCase (check in another rule)
// Method: camelCase (_ before private)
// Property: camelCase (_ before private), readonly are uppercase
// getter/setter: like property
var NamingSchemeWalker = /** @class */ (function (_super) {
    __extends(NamingSchemeWalker, _super);
    function NamingSchemeWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NamingSchemeWalker.prototype.visitMethodDeclaration = function (node) {
        var name = node.name.getText();
        if (Lint.hasModifier(node.modifiers, ts.SyntaxKind.PrivateKeyword)) {
            if (!hasLeadingUnderscore(name)) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PRIVATE_METHOD_FAIL));
            }
        }
        else {
            if (!isLowerCase(name[0])) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.METHOD_FAIL));
            }
        }
        _super.prototype.visitMethodDeclaration.call(this, node);
    };
    NamingSchemeWalker.prototype.visitPropertyDeclaration = function (node) {
        var propname = node.name.getText();
        if (Lint.hasModifier(node.modifiers, ts.SyntaxKind.PrivateKeyword)) {
            if (!hasLeadingUnderscore(propname)) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PRIVATE_PROPERTY_FAIL));
            }
        }
        else if (Lint.hasModifier(node.modifiers, ts.SyntaxKind.ReadonlyKeyword)) {
            if (!isUpperCase(propname)) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.READONLY_PROPERTY_FAIL));
            }
        }
        else {
            if (!isLowerCase(propname[0])) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PROPERTY_FAIL));
            }
        }
        _super.prototype.visitPropertyDeclaration.call(this, node);
    };
    NamingSchemeWalker.prototype.visitGetAccessor = function (node) {
        if (!this._checkGetter(node.name.getText())) {
            this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PROPERTY_FAIL));
        }
        _super.prototype.visitGetAccessor.call(this, node);
    };
    NamingSchemeWalker.prototype.visitSetAccessor = function (node) {
        if (!this._checkGetter(node.name.getText())) {
            this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PROPERTY_FAIL));
        }
        _super.prototype.visitSetAccessor.call(this, node);
    };
    NamingSchemeWalker.prototype._checkGetter = function (name) {
        return isLowerCase(name[0]);
    };
    return NamingSchemeWalker;
}(Lint.RuleWalker));
function isUpperCase(str) {
    return str === str.toUpperCase();
}
exports.isUpperCase = isUpperCase;
function isLowerCase(str) {
    return str === str.toLowerCase();
}
exports.isLowerCase = isLowerCase;
function hasLeadingUnderscore(name) {
    var firstCharacter = name[0];
    return firstCharacter === "_";
}
exports.hasLeadingUnderscore = hasLeadingUnderscore;
