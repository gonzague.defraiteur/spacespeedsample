module plume {

    export type ParticleContainerVertexDataType = {
        position?: THREE.Vector3,
        uv?: THREE.Vector2
    }
    export type ParticleContainerVertexDataFlag = {
        position: boolean,
        uv: boolean
    }

    export class ParticleContainer extends THREE.Mesh {

        // geometry: THREE.BufferGeometry;
        // material: THREE.RawShaderMaterial;

        positionAtt: THREE.BufferAttribute;
        uvAtt: THREE.BufferAttribute;

        tintColor: THREE.Color;
        mainTexture: THREE.Texture;

        constructor(maxParticles: number) {
            super();

            this.geometry = new THREE.BufferGeometry();

            let vertex = maxParticles * 4; // 4 vertex by particles

            this.positionAtt = new THREE.BufferAttribute(new Float32Array(vertex * 3), 3).setDynamic(true);
            this.uvAtt = new THREE.BufferAttribute(new Float32Array(vertex * 2), 2).setDynamic(true);

            this.geometry.addAttribute("position", this.positionAtt);
            this.geometry.addAttribute("uv", this.uvAtt);

            this.createMaterial();
        }

        createMaterial() {
            this.material = new THREE.RawShaderMaterial({
                transparent: true,
                depthWrite: false,
                uniforms: {
                    'mainTexture': {
                        value: this.mainTexture
                    },
                    'tintColor': {
                        value: this.tintColor
                    }
                },
                blending: THREE.AdditiveBlending,
                vertexShader: particle.shaders.common_vertex,
                fragmentShader: particle.shaders.common_fragment
            });
        }

        setVertexData(idx: number, data: ParticleContainerVertexDataType) {
            if (data.position != null) {
                this.positionAtt.setXYZ(idx, data.position.x, data.position.y, data.position.z);
            }
            if (data.uv != null) {
                this.uvAtt.setXY(idx, data.uv.x, data.uv.y);
            }
        }

        clear() {
            // TODO
        }
    }

}