/// <reference path="ParticleModule.ts" />


module plume {

    export class ParticleShapeModule extends ParticleModule {


    }

    //     Properties
    // alignToDirection	Align particles based on their initial direction of travel.
    // angle	Angle of the cone.
    // arc	Circle arc angle.
    // arcMode	The mode used for generating particles around the arc.
    // arcSpeed	When using one of the animated modes, how quickly to move the emission position around the arc.
    // arcSpeedMultiplier	A multiplier of the arc speed of the emission shape.
    // arcSpread	Control the gap between emission points around the arc.
    // boxThickness	Thickness of the box.
    // donutRadius	The radius of the Donut shape.
    // enabled	Enable/disable the Shape module.
    // length	Length of the cone.
    // mesh	Mesh to emit particles from.
    // meshMaterialIndex	Emit particles from a single material of a mesh.
    // meshRenderer	MeshRenderer to emit particles from.
    // meshShapeType	Where on the mesh to emit particles from.
    // normalOffset	Move particles away from the surface of the source mesh.
    // position	Apply an offset to the position from which particles are emitted.
    // radius	Radius of the shape.
    // radiusMode	The mode used for generating particles along the radius.
    // radiusSpeed	When using one of the animated modes, how quickly to move the emission position along the radius.
    // radiusSpeedMultiplier	A multiplier of the radius speed of the emission shape.
    // radiusSpread	Control the gap between emission points along the radius.
    // radiusThickness	Thickness of the radius.
    // randomDirectionAmount	Randomizes the starting direction of particles.
    // randomPositionAmount	Randomizes the starting position of particles.
    // rotation	Apply a rotation to the shape from which particles are emitted.
    // scale	Apply scale to the shape from which particles are emitted.
    // shapeType	Type of shape to emit particles from.
    // skinnedMeshRenderer	SkinnedMeshRenderer to emit particles from.
    // sphericalDirectionAmount	Spherizes the starting direction of particles.
    // sprite	Sprite to emit particles from.
    // spriteRenderer	SpriteRenderer to emit particles from.
    // texture	Selects a texture to be used for tinting particle start colors.
    // textureAlphaAffectsParticles	When enabled, the alpha channel of the texture is applied to the particle alpha when spawned.
    // textureBilinearFiltering	When enabled, 4 neighboring samples are taken from the texture, and combined to give the final particle value.
    // textureClipChannel	Selects which channel of the texture is used for discarding particles.
    // textureClipThreshold	Discards particles when they are spawned on an area of the texture with a value lower than this threshold.
    // textureColorAffectsParticles	When enabled, the RGB channels of the texture are applied to the particle color when spawned.
    // textureUVChannel	When using a Mesh as a source shape type, this option controls which UV channel on the Mesh is used for reading the source texture.
    // useMeshColors	Modulate the particle colors with the vertex colors, or the material color if no vertex colors exist.
    // useMeshMaterialIndex	Emit from a single material, or the whole mesh.
}