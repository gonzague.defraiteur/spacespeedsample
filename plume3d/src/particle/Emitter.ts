/// <reference path="../../build/latest/plume3d.core.d.ts" />

module plume {

    // ParticleSystem
    export class Emitter extends THREE.Object3D {

        private _isPlaying = false;
        private _isPaused = false;
        private _isStopped = true;
        private _isEmitting = false;

        private _time = 0; // in seconds
        private _particles: RecyclePool<Particle>;

        private _tmpVec3_0 = new THREE.Vector3();

        private _emitRateTimeCounter = 0;
        private _emitRateDistanceCounter = 0;

        renderer: ParticleSystemRenderer;
        main: ParticleMainModule;
        shape: ParticleShapeModule;
        emission: ParticleEmissionModule;

        constructor() {
            super();

            this.main = new ParticleMainModule();
            this.renderer = new ParticleSystemRenderer();
            this.shape = new ParticleShapeModule();
            this.emission = new ParticleEmissionModule();
        }

        initialize() {
            this._particles = new RecyclePool(() => new Particle(this), this.main.maxParticles);

            this.main.initialize(this);
            this.renderer.initialize(this);
            this.shape.initialize(this);
            this.emission.initialize(this);

            Game.get().addUpdatable(this);
        }

        destroy() {
            Game.get().removeUpdatable(this);
        }

        play() {
            if (this._isPaused) {
                this._isPaused = false;
            }
            if (this._isStopped) {
                this._isStopped = false;
            }

            this._time = 0.0;
            this._emitRateTimeCounter = 0.0;
            this._emitRateDistanceCounter = 0.0;

            this._isPlaying = true;

            // // prewarm
            // if (this._prewarm) {
            //     this._prewarmSystem();
            // }
        }

        pause() {
            if (this._isStopped) {
                logger.warn('Particle system is already stopped.');
                return;
            }
            if (this._isPlaying) {
                this._isPlaying = false;
            }
            this._isPaused = true;
        }

        stop() {
            if (this._isPlaying) {
                this._isPlaying = false;
            }
            if (this._isPaused) {
                this._isPaused = false;
            }

            this.clear();
            this._time = 0.0;
            this._emitRateTimeCounter = 0.0;
            this._emitRateDistanceCounter = 0.0;
            this._isStopped = true;
        }

        clear() {
            this._particles.reset();
            this.renderer.model.clear();
        }

        emit(count: number /*, emitParams = null*/) {
            // if (emitParams !== null) {
            //     // TODO:
            // }

            for (let i = 0; i < count; i++) {
                if (this._particles.length >= this.main.maxParticles) {
                    return;
                }

                let particle = this._particles.add();
                // let rand = pseudoRandom(randomRangeInt(0, INT_MAX));
                let rand = Math.random();

                // if (this._shapeModule.enable) {
                //     this._shapeModule.emit(particle);
                // }
                // else {
                //     vec3.set(particle.position, 0, 0, 0);
                //     vec3.copy(particle.velocity, particleEmitZAxis);
                // }

                particle.velocity.multiplyScalar(this.main.startSpeed.evaluate(this._time / this.main.duration, rand));

                // switch (this._simulationSpace) {
                //     case 'local':
                //         break;
                //     case 'world': {
                //         this._entity.getWorldMatrix(_world_mat);
                //         vec3.transformMat4(particle.position, particle.position, _world_mat);
                //         let worldRot = quat.create();
                //         this._entity.getWorldRot(worldRot);
                //         vec3.transformQuat(particle.velocity, particle.velocity, worldRot);
                //     }
                //         break;
                //     case 'custom':
                //         // TODO:
                //         break;
                // }

                // apply startRotation. now 2D only.
                // vec3.set(particle.rotation, this._startRotation.evaluate(this._time / this._duration, rand), 0, 0);

                // // apply startSize. now 2D only.
                // vec3.set(particle.startSize, this._startSize.evaluate(this._time / this._duration, rand), 0, 0);
                // vec3.copy(particle.size, particle.startSize);

                // // apply startColor.
                // color4.copy(particle.startColor, this._startColor.evaluate(this._time / this._duration, rand));
                // color4.copy(particle.color, particle.startColor);

                // // apply startLifetime.
                // particle.startLifetime = this._startLifetime.evaluate(this._time / this._duration, rand);
                // particle.remainingLifetime = particle.startLifetime;

                // particle.randomSeed = pseudoRandom(randomRangeInt(0, INT_MAX));

            } // end of particles forLoop.
        }


        // simulation, update particles.
        private _updateParticles(dt: number) {

            // this._entity.getWorldMatrix(_world_mat);
            // if (this._velocityOvertimeModule.enable) {
            //     this._velocityOvertimeModule.update(this._simulationSpace, _world_mat);
            // }
            // if (this._forceOvertimeModule.enable) {
            //     this._forceOvertimeModule.update(this._simulationSpace, _world_mat);
            // }

            for (let i = 0; i < this._particles.length; ++i) {
                let p = this._particles.data[i];
                p.remainingLifetime -= dt;
                p.animatedVelocity.set(0, 0, 0);

                if (p.remainingLifetime < 0.0) {
                    this._particles.remove(i);
                    --i;
                    continue;
                }

                // p.velocity.y -= this._gravityModifier.evaluate(1 - p.remainingLifetime / p.startLifetime, p.randomSeed) * 9.8 * dt; // apply gravity.
                // if (this._sizeOvertimeModule.enable) {
                //     this._sizeOvertimeModule.animate(p);
                // }
                // if (this._colorOverLifetimeModule.enable) {
                //     this._colorOverLifetimeModule.animate(p);
                // }
                // if (this._forceOvertimeModule.enable) {
                //     this._forceOvertimeModule.animate(p, dt);
                // }
                // if (this._velocityOvertimeModule.enable) {
                //     this._velocityOvertimeModule.animate(p);
                // }
                // else {
                //     vec3.copy(p.ultimateVelocity, p.velocity);
                // }
                // if (this._limitVelocityOvertimeModule.enable) {
                //     this._limitVelocityOvertimeModule.animate(p);
                // }
                // if (this._rotationOvertimeModule.enable) {
                //     this._rotationOvertimeModule.animate(p, dt);
                // }
                // if (this._textureAnimationModule.enable) {
                //     this._textureAnimationModule.animate(p);
                // }

                let ultimateVelocity = this._tmpVec3_0.copy(p.totalVelocity).multiplyScalar(dt);
                p.position.add(ultimateVelocity); // apply velocity.
            }
        }


        // initialize particle system as though it had already completed a full cycle.
        // private _prewarmSystem() {
        //     this._startDelay.mode = 'constant'; // clear startDelay.
        //     this._startDelay.constant = 0;
        //     let dt = 1.0; // should use varying value?
        //     let cnt = this._duration / dt;
        //     for (let i = 0; i < cnt; ++i) {
        //         this._time += dt;
        //         this._emit(dt);
        //         this._updateParticles(dt);
        //     }
        // }

        // internal function
        private _emit(dt: number) {
            // emit particles.
            let startDelay = this.main.startDelay.evaluate();
            if (this._time > startDelay) {
                if (!this._isStopped) {
                    this._isEmitting = true;
                }
                if (this._time > (this.main.duration + startDelay)) {
                    if (!this.main.loop) {
                        this._isEmitting = false;
                        this._isStopped = true;
                    }
                }

                // emit by rateOverTime
                this._emitRateTimeCounter += this.emission.rateOverTime.evaluate(this._time / this.main.duration, 1) * dt;
                if (this._emitRateTimeCounter > 1 && this._isEmitting) {
                    let emitNum = Math.floor(this._emitRateTimeCounter);
                    this._emitRateTimeCounter -= emitNum;
                    this.emit(emitNum);
                }
                // emit by rateOverDistance
                // this._entity.getWorldPos(this._curWPos);
                // let distance = vec3.distance(this._curWPos, this._oldWPos);
                // vec3.copy(this._oldWPos, this._curWPos);
                // this._emitRateDistanceCounter += distance * this._rateOverDistance.evaluate(this._time / this._duration, 1);
                // if (this._emitRateDistanceCounter > 1 && this._isEmitting) {
                //     let emitNum = Math.floor(this._emitRateDistanceCounter);
                //     this._emitRateDistanceCounter -= emitNum;
                //     this.emit(emitNum);
                // }

                // bursts
                // for (let i = 0; i < this._bursts.length; ++i) {
                //     this._bursts[i].update(this, dt);
                // }
            }
        }

        update(dt: number) {
            let dtS = dt / 1000;
            let scaledDeltaTime = dtS * this.main.simulationSpeed;
            if (this._isPlaying) {
                this._time += scaledDeltaTime;
                // excute emission
                this._emit(scaledDeltaTime);
                // simulation, update particles.
                this._updateParticles(scaledDeltaTime);
                // update render data
                this.renderer.updateRenderData();
            }
        }


        getParticles() {
            return this._particles;
        }
        getParticleCount() {
            return this._particles.length;
        }

        get isPlaying() {
            return this._isPlaying;
        }
        get isPaused() {
            return this._isPaused;
        }
        get isStopped() {
            return this._isStopped;
        }
        get isEmitting() {
            return this._isEmitting;
        }
        get time() {
            return this._time;
        }
    }


    //
    // Properties
    // automaticCullingEnabled	Does this system support Automatic Culling?
    // collision	Access the particle system collision module.
    // colorBySpeed	Access the particle system color by lifetime module.
    // colorOverLifetime	Access the particle system color over lifetime module.
    // customData	Access the particle system Custom Data module.
    // emission	Access the particle system emission module.
    // externalForces	Access the particle system external forces module.
    // forceOverLifetime	Access the particle system force over lifetime module.
    // inheritVelocity	Access the particle system velocity inheritance module.
    // isEmitting	Is the Particle System currently emitting particles? A Particle System may stop emitting when its emission module has finished, it has been paused or if the system has been stopped using Stop with the StopEmitting flag. Resume emitting by calling Play.
    // isPaused	Is the Particle System paused right now?
    // isPlaying	Is the Particle System playing right now?
    // isStopped	Is the Particle System stopped right now?
    // lights	Access the particle system lights module.
    // limitVelocityOverLifetime	Access the particle system limit velocity over lifetime module.
    // main	Access the main particle system settings.
    // noise	Access the particle system noise module.
    // particleCount	The current number of particles (Read Only).
    // randomSeed	Override the random seed used for the particle system emission.
    // rotationBySpeed	Access the particle system rotation by speed module.
    // rotationOverLifetime	Access the particle system rotation over lifetime module.
    // shape	Access the particle system shape module.
    // sizeBySpeed	Access the particle system size by speed module.
    // sizeOverLifetime	Access the particle system size over lifetime module.
    // subEmitters	Access the particle system sub emitters module.
    // textureSheetAnimation	Access the particle system texture sheet animation module.
    // time	Playback position in seconds.
    // trails	Access the particle system trails module.
    // trigger	Access the particle system trigger module.
    // useAutoRandomSeed	Controls whether the Particle System uses an automatically-generated random number to seed the random number generator.
    // velocityOverLifetime	Access the particle system velocity over lifetime module.
    //
    // Public Methods
    // Clear	Remove all particles in the particle system.
    // Emit	Emit count particles immediately.
    // GetCustomParticleData	Get a stream of custom per-particle data.
    // GetParticles	Gets the particles of this particle system.
    // IsAlive	Does the system contain any live particles, or will it produce more?
    // Pause	Pauses the system so no new particles are emitted and the existing particles are not updated.
    // Play	Starts the particle system.
    // SetCustomParticleData	Set a stream of custom per-particle data.
    // SetParticles	Sets the particles of this particle system.
    // Simulate	Fastforwards the particle system by simulating particles over given period of time, then pauses it.
    // Stop	Stops playing the particle system using the supplied stop behaviour.
    // TriggerSubEmitter	Triggers the specified sub emitter on all particles of the Particle System.
}