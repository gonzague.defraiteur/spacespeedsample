/// <reference path="../../build/latest/plume3d.core.d.ts" />
/// <reference path="../../vendor/cannon.d.ts" />

module plume {

    export class CannonSimulation extends SimulationSupport {

        optFixedTimeStep = 1 / 60;
        optMaxSubSteps = 10;

        readyHandlers = new Handler<boolean>();

        private _ready = false;
        private _world: CANNON.World;

        private _dynamicBodies: Array<CANNON.Body> = []; // only used to track idx
        private _dynamicBodiesData: Array<DynamicBodyDataHolder> = [];
        private _bodies: Array<CANNON.Body> = [];

        private _visuals: Array<THREE.Object3D> = []; // Use for DEBUG


        private _stats: SimulationStats & { _timeTotal: number; _flush: number };

        constructor(init?: (world: CANNON.World) => void) {
            super();

            this._stats = { avg: 0, _timeTotal: 0, _flush: 0 };

            this._world = new CANNON.World();
            this._world.gravity.set(0, -10, 0);

            // this._world.broadphase = new CANNON.NaiveBroadphase();
            // this._world.solver.iterations = 10;
            // this._world.defaultContactMaterial.contactEquationStiffness = 1e7;
            // this._world.defaultContactMaterial.contactEquationRelaxation = 4;

            this._stats._flush = performance.now();
            this._stats._timeTotal = 0;

            if (init != null) {
                // init overload
                init(this._world);
            }
        }

        update(dt: number) {
            super.update(dt);

            if (!this._running) return;

            let sEllapsed = dt / 1000;
            let now = performance.now();

            this._world.step(this.optFixedTimeStep, sEllapsed, this.optMaxSubSteps);
            this._syncMesh();

            let after = performance.now();

            let statsEllapsed = (now - this._stats._flush);
            if (statsEllapsed >= 1000) {
                this._stats.avg = Math.round((this._stats._timeTotal / statsEllapsed) * 100) / 100;
                this._stats._flush = now;
                this._stats._timeTotal = 0;
            }
            this._stats._timeTotal += (after - now);
        }

        get stats(): SimulationStats {
            return this._stats;
        }

        setGravity(x: number, y: number, z: number) {
            this._world.gravity.set(x, y, z);
        }

        protected onStart() {
        }

        protected onStop() {
        }

        getWorld(): CANNON.World {
            return this._world;
        }

        updatePositionAndRotation(body: CANNON.Body, position?: THREE.Vector3, rotation?: THREE.Quaternion) {
            if (position != null) {
                body.position.set(position.x, position.y, position.z);
            }
            if (rotation != null) {
                body.quaternion.set(rotation.x, rotation.y, rotation.z, rotation.w);
            }
            // no scaling in cannonjs!
        }

        addRigidBody(body: CANNON.Body, mesh: THREE.Object3D, dynamic: boolean, group?: number, mask?: number) {
            if (mesh != null) {
                // initialize position and rotation body from mesh
                this.updatePositionAndRotation(body, mesh.position, mesh.quaternion);
            }

            if (dynamic) {
                // add body to sync list
                this._dynamicBodies.push(body);
                this._dynamicBodiesData.push({
                    rigidbody: body,
                    mesh: mesh,
                    syncPosition: true,
                    syncRotation: true,
                });
            }

            if (group != null) {
                body.collisionFilterGroup = group;
            }
            if (mask != null) {
                body.collisionFilterMask = mask;
            }

            this._bodies.push(body);
            this._world.addBody(body);

            // logger.debug("dynamic bodies: " + this._dynamicBodies.length);
        }

        removeRigidBody(body: CANNON.Body) {
            let idx = this._dynamicBodies.indexOf(body);
            if (idx >= 0) {
                this._dynamicBodies.splice(idx, 1);
                this._dynamicBodiesData.splice(idx, 1);
            }

            idx = this._bodies.indexOf(body);
            if (idx >= 0) {
                this._bodies.splice(idx, 1);
            } else {
                // logger.warn("Body not registered");
            }

            this._world.removeBody(body);
        }

        setDynamicBodySyncProperty(body: CANNON.Body, syncPosition: boolean, syncRotation: boolean) {
            let idx = this._dynamicBodies.indexOf(body);
            let data = this._dynamicBodiesData[idx];
            data.syncPosition = syncPosition;
            data.syncRotation = syncRotation;
        }

        getRigidBodies(): Array<CANNON.Body> {
            return this._bodies;
        }

        removeRigidBodies() {
            for (let i = this._bodies.length - 1; i >= 0; i--) {
                let b = this._bodies[i];
                this.removeRigidBody(b);
            }
        }

        private _syncMesh() {
            for (let i = 0; i < this._dynamicBodiesData.length; i++) {
                let data = this._dynamicBodiesData[i];
                let body = data.rigidbody;
                let mesh = data.mesh;

                if (data.syncPosition) {
                    let p = body.position;
                    mesh.position.set(p.x, p.y, p.z);
                }
                if (data.syncRotation) {
                    let q = body.quaternion;
                    mesh.quaternion.set(q.x, q.y, q.z, q.w);
                }
            }

            for (let i = 0; i < this._visuals.length; i++) {
                let visual = this._visuals[i];
                let body = visual.userData as CANNON.Body;
                visual.position.set(body.position.x, body.position.y, body.position.z);
                if (body.quaternion) {
                    visual.quaternion.set(body.quaternion.x, body.quaternion.y, body.quaternion.z, body.quaternion.w);
                }
            }
        }

        destroy() {
            this.removeRigidBodies();
        }

        // FOR DEBUG ONLY!
        addVisual(body: CANNON.Body, material: THREE.MeshMaterialType, scene: THREE.Scene) {
            let mesh = this.createVisual(body, material);
            mesh.userData = body;
            this._visuals.push(mesh);
            scene.add(mesh);
        }

        removeVisual(mesh: THREE.Object3D) {
            let idx = this._visuals.indexOf(mesh);
            if (idx >= 0) {
                this._visuals.splice(idx, 1);
                mesh.parent.remove(mesh);
            }
        }

        removeVisuals() {
            for (let i = this._visuals.length - 1; i >= 0; i--) {
                let m = this._visuals[i];
                this.removeVisual(m);
            }
        }



        // FOR DEBUG ONLY!
        createVisual(body: CANNON.Body, material: THREE.MeshMaterialType) {
            let obj = new THREE.Object3D();
            for (let l = 0; l < body.shapes.length; l++) {
                let shape = body.shapes[l];
                let mesh: THREE.Object3D;

                switch (shape.type) {
                    case CANNON.Shape.types.SPHERE:
                        let sphereGeometry = new THREE.SphereGeometry((shape as CANNON.Sphere).radius, 8, 8);
                        mesh = new THREE.Mesh(sphereGeometry, material);
                        break;
                    case CANNON.Shape.types.PLANE:
                        let planeGeometry = new THREE.PlaneGeometry(10, 10, 4, 4);
                        mesh = new THREE.Object3D();
                        let submesh = new THREE.Object3D();
                        let ground = new THREE.Mesh(planeGeometry, material);
                        ground.scale.set(100, 100, 100);
                        submesh.add(ground);
                        mesh.add(submesh);
                        break;
                    case CANNON.Shape.types.BOX:
                        let boxShape = shape as CANNON.Box;
                        let boxGeometry = new THREE.BoxGeometry(boxShape.halfExtents.x * 2, boxShape.halfExtents.y * 2, boxShape.halfExtents.z * 2);
                        mesh = new THREE.Mesh(boxGeometry, material);
                        break;
                    case CANNON.Shape.types.CONVEXPOLYHEDRON:
                        let convexGeometry = new THREE.Geometry();
                        let convexShape = shape as CANNON.ConvexPolyhedron;
                        // Add vertices
                        for (let i = 0; i < convexShape.vertices.length; i++) {
                            let v = convexShape.vertices[i];
                            convexGeometry.vertices.push(new THREE.Vector3(v.x, v.y, v.z));
                        }
                        for (let i = 0; i < convexShape.faces.length; i++) {
                            let face = convexShape.faces[i];
                            // add triangles
                            let a = face[0];
                            for (let j = 1; j < face.length - 1; j++) {
                                let b = face[j];
                                let c = face[j + 1];
                                convexGeometry.faces.push(new THREE.Face3(a, b, c));
                            }
                        }
                        convexGeometry.computeBoundingSphere();
                        convexGeometry.computeFaceNormals();
                        mesh = new THREE.Mesh(convexGeometry, material);
                        break;
                    case CANNON.Shape.types.TRIMESH:
                        let geometry = new THREE.Geometry();
                        let trimeshShape = shape as CANNON.Trimesh;
                        let v0 = new CANNON.Vec3();
                        let v1 = new CANNON.Vec3();
                        let v2 = new CANNON.Vec3();
                        for (let i = 0; i < trimeshShape.indices.length / 3; i++) {
                            trimeshShape.getTriangleVertices(i, v0, v1, v2);
                            geometry.vertices.push(
                                new THREE.Vector3(v0.x, v0.y, v0.z),
                                new THREE.Vector3(v1.x, v1.y, v1.z),
                                new THREE.Vector3(v2.x, v2.y, v2.z)
                            );
                            let j = geometry.vertices.length - 3;
                            geometry.faces.push(new THREE.Face3(j, j + 1, j + 2));
                        }
                        geometry.computeBoundingSphere();
                        geometry.computeFaceNormals();
                        mesh = new THREE.Mesh(geometry, material);
                        break;
                    default:
                        throw "Visual type not recognized: " + shape.type;
                }

                let o = body.shapeOffsets[l];
                let q = body.shapeOrientations[l];
                mesh.position.set(o.x, o.y, o.z);
                mesh.quaternion.set(q.x, q.y, q.z, q.w);

                obj.add(mesh);
            }

            return obj;
        }
    }

    type DynamicBodyDataHolder = {
        rigidbody: CANNON.Body,
        mesh: THREE.Object3D,
        syncPosition: boolean,
        syncRotation: boolean
    }
}