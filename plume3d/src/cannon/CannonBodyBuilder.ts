module plume {

    export interface CannonBodyParameter {
        mass: number;
        type?: number;
    }

    export class CannonBodyBuilder {

        static newBodyFromMesh(mesh: THREE.Mesh, params?: CannonBodyParameter) {
            let geometry = mesh.geometry;
            if (geometry instanceof THREE.BoxBufferGeometry || geometry instanceof THREE.BoxGeometry) {
                geometry.computeBoundingBox();
                let bbox = geometry.boundingBox;
                let size = new THREE.Vector3();
                bbox.getSize(size).multiply(mesh.scale);
                return this.newBox(size.x, size.y, size.z, params)
            } else if (geometry instanceof THREE.SphereBufferGeometry || geometry instanceof THREE.SphereGeometry) {
                geometry.computeBoundingSphere();
                let s = geometry.boundingSphere;
                return this.newSphere(s.radius * mesh.scale.x, params);
            } else if (geometry instanceof THREE.BufferGeometry || geometry instanceof THREE.Geometry) {
                let s = this.newTriMeshShape(geometry);
                return this.newRigidBody(s, params);
            }

            logger.warn("Unable to build physics body from mesh: " + mesh);
            return null;
        }

        static newBoxShape(w: number, h: number, d: number) {
            let geometry = new CANNON.Box(new CANNON.Vec3(w * 0.5, h * 0.5, d * 0.5));
            return geometry;
        }
        static newBox(w: number, h: number, d: number, params: CannonBodyParameter = { mass: 0 }) {
            let geometry = this.newBoxShape(w, h, d);
            return this.newRigidBody(geometry, params);
        }

        static newSphere(radius: number, params: CannonBodyParameter = { mass: 0 }) {
            let geometry = new CANNON.Sphere(radius);
            return this.newRigidBody(geometry, params);
        }

        // !! Cylinder in CANNON is oriented along the z axis (the shape will be rotated if fixRotation == true )
        static newCylinder(radiusTop: number, radiusBottom: number, height: number, numSegments: number, params: CannonBodyParameter = { mass: 0 }, fixRotation = true) {
            let geometry = new CANNON.Cylinder(radiusTop, radiusBottom, height, numSegments);

            if (fixRotation) {
                let quat = new CANNON.Quaternion();
                quat.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2);
                let translation = new CANNON.Vec3(0, 0, 0);
                geometry.transformAllPoints(translation, quat);
            }

            return this.newRigidBody(geometry, params);
        }

        static newPlane(rotation?: THREE.Quaternion) {
            let shape = new CANNON.Plane();
            let body = this.newRigidBody(shape);
            if (rotation != null) {
                body.quaternion.set(rotation.x, rotation.y, rotation.z, rotation.w);
            }
            return body;
        }

        static newTriMeshShape(geometry: THREE.BufferGeometry | THREE.Geometry) {
            if (geometry instanceof THREE.BufferGeometry) {
                let vertices = geometry.getAttribute("position").array as Array<number>;
                let index = geometry.index.array as Array<number>;
                let shape = new CANNON.Trimesh(vertices, index);
                return shape;
            } else {
                let vertices: number[] = [];
                for (let i = 0; i < geometry.vertices.length; i++) {
                    vertices[(i * 3) + 0] = (geometry.vertices[i].x);
                    vertices[(i * 3) + 1] = (geometry.vertices[i].y);
                    vertices[(i * 3) + 2] = (geometry.vertices[i].z);
                }
                let indices: number[] = [];
                for (let i = 0; i < geometry.faces.length; i++) {
                    indices.push(geometry.faces[i].a);
                    indices.push(geometry.faces[i].b);
                    indices.push(geometry.faces[i].c);
                }
                let shape = new CANNON.Trimesh(vertices, indices);
                return shape;
            }
        }

        static newTriMesh(geometry: THREE.BufferGeometry | THREE.Geometry) {
            let shape = this.newTriMeshShape(geometry);
            return this.newRigidBody(shape);
        }

        static newRigidBody(collisionShape: CANNON.Shape, params: CannonBodyParameter = { mass: 0 }) {
            let body = new CANNON.Body({
                mass: params.mass,
                type: params.type
            });
            body.addShape(collisionShape);
            return body;
        }

    }

}