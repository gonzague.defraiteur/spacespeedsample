module plume {

    export class TweenValueListener {

        private _value: number;

        onValueUp: (nv: number) => any;
        onValueDown: (nv: number) => any;
        onValueChange: (nv: number) => any;

        constructor(initialValue: number, private roundValue: boolean = true) {
            if (roundValue) {
                this._value = Math.round(initialValue);
            } else {
                this._value = initialValue;
            }
        }

        private _notify(v: number) {
            let previous = this._value;
            if (previous == v) return;

            if (v > previous) {
                if (this.onValueUp != null) this.onValueUp(v);
                if (this.onValueChange != null) this.onValueChange(v);
            } else if (v < previous) {
                if (this.onValueDown != null) this.onValueDown(v);
                if (this.onValueChange != null) this.onValueChange(v);
            }
        }

        set value(v: number) {
            if (this.roundValue) {
                v = Math.round(v);
            }

            this._notify(v);
            this._value = v;
        }

        get value(): number {
            return this._value;
        }

    }
}