﻿
module plume {

    export class TweenStep {

        protected duration: number;
        protected complete: boolean = false;
        protected elapsedTime: number = null;
        protected startTime: number = null;

        constructor(duration: number) {
            this.duration = duration;
        }

        start() {
            this.startTime = Time.now();
            this.elapsedTime = 0;
            this.complete = false;
        }

        update(simulationTimestep: number) {
            let now = Time.now();
            this.elapsedTime = now - this.startTime;
            if (this.elapsedTime >= this.duration) {
                this.complete = true;
            }
        }

        render(interpolation: number) {
            // Nothing by default
        }

        isComplete(): boolean { return this.complete; }
    }

    export class DelayTweenStep extends TweenStep {

        constructor(duration: number) {
            super(duration);
        }

    }

    export class PropertiesTweenStep extends TweenStep {

        private _object: any;
        private _properties: { [key: string]: number };
        private _easing: EasingFunction;

        private _from: boolean;

        private _startValues: { [key: string]: number } = {};
        private _endValues: { [key: string]: number } = {};

        private _previousValues: { [key: string]: number } = {};
        private _computedValues: { [key: string]: number } = {};


        constructor(object: any, props: { [key: string]: number }, duration: number, easing: EasingFunction, isFrom: boolean) {
            super(duration);
            this._properties = props;
            this._easing = easing;
            this._from = isFrom;
            this._object = object;
        }

        start() {
            super.start();

            for (let property in this._properties) {
                if (this._from) {
                    this._startValues[property] = this._properties[property];
                    this._endValues[property] = this._object[property];
                } else {
                    this._startValues[property] = this._object[property];
                    this._endValues[property] = this._properties[property];
                }
                this._previousValues[property] = this._startValues[property];
                this._computedValues[property] = this._startValues[property];
            }
        }

        update(simulationTimestep: number) {
            super.update(simulationTimestep);

            let delta = Math.min(this.elapsedTime, this.duration);

            let percent = delta / this.duration;
            let easingValue = this._easing(percent);

            for (let property in this._properties) {
                // save previous
                this._previousValues[property] = this._computedValues[property];

                // compute new
                let startV = this._startValues[property];
                let endV = this._endValues[property];
                let computed = startV + ((endV - startV) * easingValue);
                this._computedValues[property] = computed;
            }
        }

        render(interpolation: number) {
            super.render(interpolation);

            for (let property in this._computedValues) {
                let prev = this._previousValues[property];
                let last = this._computedValues[property];
                this._object[property] = prev + ((last - prev) * interpolation);
            }
        }
    }
}

