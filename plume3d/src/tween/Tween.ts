﻿
module plume {

    const enum TweenState {
        PENDING, RUNNING, TERMINATED
    }

    export class Tween {

        id: string = "DefaultId";

        private _object: any;
        private _tweenManager: TweenManager;

        private _repeatCount: number = 1;

        private _currentStepIndex: number = -1;
        private _currentStep: TweenStep;
        private _steps: Array<TweenStep> = [];
        private _state: TweenState = TweenState.PENDING;

        _savedValues: { [key: string]: number } = {};

        onStart: Function;
        onComplete: Function;

        constructor(object: any, tweenManager: TweenManager) {
            this._object = object;
            this._tweenManager = tweenManager;
        }

        getObject(): any {
            return this._object;
        }

        delay(delay: number) {
            let step = new DelayTweenStep(delay);
            this._steps.push(step);
            return this;
        }

        repeat(repeat: number) {
            this._repeatCount = repeat;
        }

        to(properties: { [key: string]: number }, duration: number = 1000, ease: EasingFunction = easing.linear): Tween {
            let step = new PropertiesTweenStep(this._object, properties, duration, ease, false);
            this._steps.push(step);
            return this;
        }

        from(properties: { [key: string]: number }, duration: number = 1000, ease: EasingFunction = easing.linear): Tween {
            let step = new PropertiesTweenStep(this._object, properties, duration, ease, true);
            this._steps.push(step);
            return this;
        }

        start() {
            if (this._state == TweenState.RUNNING) {
                logger.warn("Tween already started");
                return;
            }

            if (this._steps.length == 0) {
                logger.warn("No step defined in Tween");
                return;
            }

            this._state = TweenState.RUNNING;
            this._nextStep();

            if (this.onStart) {
                this.onStart();
            }
        }

        stop() {
            if (this._state == TweenState.TERMINATED) {
                // already stopped
                return;
            }

            this._state = TweenState.TERMINATED;

            if (this.onComplete) {
                this.onComplete(this);
            }

            this._tweenManager.remove(this);
        }

        update(simulationTimestep: number) {
            if (this._state != TweenState.RUNNING)
                return;

            if (this._currentStep == null) {
                this.stop();
                return;
            }

            this._currentStep.update(simulationTimestep);

            if (this._currentStep.isComplete()) {
                // complete animation
                this.render(1);
                // go to next step
                this._nextStep();
            }
        }

        render(interpolation: number) {
            if (this._state != TweenState.RUNNING || this._currentStep == null)
                return;

            this._currentStep.render(interpolation);
        }

        private _nextStep() {
            this._currentStepIndex++;;
            if (this._currentStepIndex < this._steps.length) {
                this._currentStep = this._steps[this._currentStepIndex];
                this._currentStep.start();
            } else {
                // All steps done
                this._repeatCount -= 1;
                if (this._repeatCount == 0) {
                    this._currentStep = null;
                } else {
                    // Restart from 0 if repeat > 1 || repeat < -1
                    this._currentStepIndex = -1;
                    this._nextStep();
                }
            }
        }
    }

}

