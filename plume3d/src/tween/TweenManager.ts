﻿/// <reference path="../../build/latest/plume3d.core.d.ts" />

module plume {

    export class TweenManager implements Updatable, Renderable {

        private static _instance: TweenManager;

        private _game: Game;
        private _tweens: Array<Tween> = [];

        constructor() {
            TweenManager._instance = this;

            this._game = Game.get();
            this._game.addUpdatable(this);
            this._game.addRenderable(this);
        }

        static get() {
            if (TweenManager._instance == null) {
                TweenManager._instance = new TweenManager();
            }
            return TweenManager._instance;
        }

        create(object: any, id?: string) {
            let tween = new Tween(object, this);
            if (id != null) tween.id = id;

            this._tweens.push(tween);
            return tween;
        }

        remove(tween: Tween) {
            let index = this._tweens.indexOf(tween);
            if (index > -1) {
                this._tweens.splice(index, 1);
                tween.stop();
            }
        }

        removeAll() {
            this.cancelAll();
            this._tweens = [];
        }

        cancelAll() {
            for (let i = 0; i < this._tweens.length; i++) {
                this._tweens[i].stop();
            }
        }

        update(simulationTimestep: number) {
            for (let i = 0; i < this._tweens.length; i++) {
                this._tweens[i].update(simulationTimestep);
            }
        }

        render(interpolation: number) {
            for (let i = 0; i < this._tweens.length; i++) {
                this._tweens[i].render(interpolation);
            }
        }

        get activeTweensCount(): number {
            return this._tweens.length;
        }

        get activeTweens(): string {
            return this._tweens.map(function (elem) {
                return elem.id;
            }).join();
        }

    }

}

