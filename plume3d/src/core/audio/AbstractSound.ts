﻿module plume {

    export class AbstractSound {

        protected muted: boolean = false;

        constructor(public _howl: Howl) {
        }

        protected play0(sprite?: string, onEndCallback?: Function): number {
            if (this.muted) {
                if (onEndCallback) onEndCallback();
                return 0;
            }

            let soundId = this._howl.play(sprite);
            if (onEndCallback != null) {
                this._howl.once("end", function () {
                    onEndCallback();
                }, soundId);
            }
            return soundId;
        }

        pause(id?: number) {
            this._howl.pause(id);
        }

        stop(id?: number) {
            this._howl.stop(id);
        }

        mute(id?: number) {
            this.muted = true;
            this._howl.mute(true, id);
        }

        unmute(id?: number) {
            this.muted = false;
            this._howl.mute(false, id);
        }

        fade(from: number, to: number, duration: number, id?: number, callback?: Function) {
            if (this.muted) {
                if (callback) callback();
                return;
            }

            this._howl.fade(from, to, duration, id);

            if (callback) {
                this._howl.once("fade", function (sid) {
                    callback();
                }, id);
            }
        }

        loop(loop: boolean, id?: number) {
            this._howl.loop(loop, id);
        }

        seek(seek?: number, id?: number): Howl | number {
            return this._howl.seek(seek, id);
        }

        rate(rate: number, id?: number): Howl {
            return this._howl.rate(rate, id);
        }

        setVolume(volume: number, id?: number) {
            this._howl.volume(volume, id);
        }

        getVolume() {
            return this._howl.volume();
        }

    }
}