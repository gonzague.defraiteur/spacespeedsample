﻿module plume {

    export class Music extends AbstractSound {

        private _spriteName: string = null;

        constructor(howl: Howl, sprite: { [name: string]: Array<number> }) {
            super(howl);

            if (sprite != null) {
                // assumption: we have only one sprite with the music
                for (let k in sprite) {
                    this._spriteName = k;
                    break;
                }
            }
        }

        play(onEndCallback?: Function): number {
            return super.play0(this._spriteName, onEndCallback);
        }
    }

}