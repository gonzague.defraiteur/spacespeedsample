﻿
module plume {


    export class MouseHandler implements Updatable {

        private _enable: boolean = false;
        private _touchEndFired = false;
        private _element: HTMLElement | Window;

        game: Game;
        inputManager: InputManager;
        pointer: PointerInput; // active pointer (mouse or first touch)
        pointers: Array<PointerInput>;
        mouseWheelHandlers: Handler<{ delta: number, event: WheelEvent }>;
        propagateAllEvents: boolean = false;

        wheelDelta = 0;

        private _lastWheel = 0;

        constructor(game: Game, inputManager: InputManager) {
            this.game = game;
            this.inputManager = inputManager;
            this.pointer = new PointerInput(game, inputManager);
            this.pointers = new Array(inputManager.maxTouchPointer);
            this.pointers[0] = this.pointer;
            for (let i = 1; i < inputManager.maxTouchPointer; i++) {
                this.pointers[i] = new PointerInput(game, inputManager);
            }

            this.mouseWheelHandlers = new Handler();
        }

        update() {
            for (let i = 0; i < this.pointers.length; i++) {
                this.pointers[i].update();
            }

            this.wheelDelta = this._lastWheel;
            this._lastWheel = 0;
        }

        enableOnElement(element: HTMLElement | Window) {
            this.disable();

            this._element = element;
            this.enable();
        }

        enable() {
            if (this._enable) return;
            if (this._element == null) throw new Error("No element specified for mouse event handling");

            this._enable = true;

            // let element = document.body; => on ios => trigger pull to refresh

            this._element.addEventListener('mousedown', this._mousedown);
            this._element.addEventListener('mousemove', this._mousemove);
            this._element.addEventListener('mouseup', this._mouseup);
            this._element.addEventListener('mouseout', this._mouseout);
            this._element.addEventListener('wheel', this._mousewheel);
            this._element.addEventListener('contextmenu', this._contextmenu);

            this._element.addEventListener('touchstart', this._touchstart);
            this._element.addEventListener('touchmove', this._touchmove);
            this._element.addEventListener('touchend', this._touchend);
            this._element.addEventListener('touchcancel', this._touchcancel);
            this._element.addEventListener('touchleave', this._touchleave);
        }

        disable() {
            if (!this._enable) return;
            if (this._element == null) return; // not enable

            this._enable = false;

            // let element = document.body;
            this._element.removeEventListener('mousedown', this._mousedown);
            this._element.removeEventListener('mousemove', this._mousemove);
            this._element.removeEventListener('mouseup', this._mouseup);
            this._element.removeEventListener('mouseout', this._mouseout);
            this._element.removeEventListener('wheel', this._mousewheel);
            this._element.removeEventListener('contextmenu', this._contextmenu);

            this._element.removeEventListener('touchstart', this._touchstart);
            this._element.removeEventListener('touchmove', this._touchmove);
            this._element.removeEventListener('touchend', this._touchend);
            this._element.removeEventListener('touchcancel', this._touchcancel);
            this._element.removeEventListener('touchleave', this._touchleave);
        }

        get isDown(): boolean {
            return this.pointer.isDown;
        }

        get isUp(): boolean {
            return this.pointer.isUp;
        }

        get isJustDown(): boolean {
            return this.pointer.isJustDown;
        }

        get isJustUp(): boolean {
            return this.pointer.isJustUp;
        }

        get downDuration(): number {
            return this.pointer.downDuration;
        }

        get downPosition(): Point {
            return this.pointer.downPosition;
        }

        get position(): Point {
            return this.pointer.position;
        }

        get upPosition(): Point {
            return this.pointer.upPosition;
        }

        private _mousedown = (e: MouseEvent) => {
            this.pointer.mouseDown(e);
            this.inputManager._pointerDirty = true;
            this._stopEvent(e);
        }

        private _mousemove = (e: MouseEvent) => {
            this.pointer.mouseMove(e);
            this.inputManager._pointerDirty = true;
            this._stopEvent(e);
        }

        private _mouseup = (e: MouseEvent) => {
            this.pointer.mouseUp(e);
            this.inputManager._pointerDirty = true;
            this._stopEvent(e);
        }

        private _mouseout = (e: MouseEvent) => {
            // we do not want to trigger a fake mouseup when leaving
            // this.mouseup(e);
        }

        private _contextmenu = (e: MouseEvent) => {
            this._stopEvent(e);
        }

        private _mousewheel = (e: WheelEvent) => {
            if (e.deltaY) {
                let delta = e.deltaY;
                if (delta < -1) delta = -1;
                else if (delta > 1) delta = 1;

                if (delta != 0) {
                    this._lastWheel = delta;
                    this.wheelDelta = delta;
                    this.mouseWheelHandlers.fire({ delta: delta, event: e });
                }
            }

            this._stopEvent(e);
        }

        private _getPointerOrFirstFree(touch: Touch): PointerInput {
            let identifier = touch.identifier;
            let firstFree = null;
            let idFound = null;
            for (let i = 0; i < this.pointers.length; i++) {
                let p = this.pointers[i];
                if (p.identifier == null && firstFree == null) {
                    firstFree = p;
                } else if (p.identifier == identifier) {
                    //console.log("Found active pointer. " + i + "  -- identifier=" + p.identifier);
                    idFound = p;
                    break;
                }
            }

            if (idFound != null) {
                return idFound;
            }

            return firstFree;
        }

        private _touchstart = (e: TouchEvent) => {
            //this.debugTouch("touchstart", e);
            for (let i = 0; i < e.changedTouches.length; i++) {
                let touch = e.changedTouches[i];
                let pointer = this._getPointerOrFirstFree(touch);
                if (pointer != null) {
                    pointer.touchDown(touch);
                }
            }
            this.inputManager._pointerDirty = true;
            this._stopEvent(e);
        }

        private _touchmove = (e: TouchEvent) => {
            for (let i = 0; i < e.changedTouches.length; i++) {
                let touch = e.changedTouches[i];
                let pointer = this._getPointerOrFirstFree(touch);
                if (pointer != null) {
                    pointer.touchMove(touch);
                }
            }
            this.inputManager._pointerDirty = true;
            this._stopEvent(e);
        }

        private _touchend = (e: TouchEvent) => {
            //this.debugTouch("touchend", e);
            for (let i = 0; i < e.changedTouches.length; i++) {
                let touch = e.changedTouches[i];
                let pointer = this._getPointerOrFirstFree(touch);
                if (pointer != null) {
                    pointer.touchUp(touch);
                }
            }
            this.inputManager._pointerDirty = true;

            if (!this._touchEndFired) {
                // First event is fired up for howler to initiate sound on IOS
                this._touchEndFired = true;
                return;
            }

            this._stopEvent(e);
        }

        private _stopEvent(e: Event) {
            if (this.propagateAllEvents) return;

            e.preventDefault();
            e.stopPropagation();
        }

        private _debugTouch(event: string, e: TouchEvent) {
            let changes = "";
            for (let i = 0; i < e.changedTouches.length; i++) {
                changes += "" + e.changedTouches[i].identifier;
                if (i < e.changedTouches.length - 1) changes += ",";
            }
            console.log("#### MouseHandler: " + event + ", changes: " + changes);
        }

        private _touchcancel = (e: TouchEvent) => {
            this._touchend(e);
        }

        private _touchleave = (e: TouchEvent) => {
            this._touchend(e);
        }

    }


    export interface MouseEventInterpretor {
        onUpdate(): void;
        onImmediate(): void;
    }

}
