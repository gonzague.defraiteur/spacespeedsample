﻿
module plume {

    export class PointerInput extends AbstractInput {

        scaleManager: ScaleManager;
        inputManager: InputManager;

        identifier: number;
        downPosition = new Point(0, 0);
        position = new Point(0, 0);
        upPosition = new Point(0, 0);
        diffPosition = new Point(0, 0);

        mouseActive: boolean = false;

        // Used to track state change to fire over, out, etc events
        interactiveElement: any;
        _previousHit: any = null;
        _lastDownTarget: any = null;

        private _touchInProgress: boolean = false;
        private _touchEndTime: number = 0;
        private _touchEndDebounce: number = 4000;

        private _prevMousePosition = new Point(-1, -1);
        private _prevMouseBState: number = -1;

        private _lastMouseEvent: MouseEvent;
        private _lastX = 0;
        private _lastY = 0;

        constructor(game: Game, inputManager: InputManager) {
            super();
            this.inputManager = inputManager;
            this.scaleManager = game.scaleManager;
        }

        update() {
            super.update();

            if (this._touchEndTime != null) {
                if (this._touchEndTime < Time.now()) {
                    this._touchEndTime = null;
                    this._touchInProgress = false;
                }
            }

            this.diffPosition.x = this.position.x - this._lastX;
            this.diffPosition.y = this.position.y - this._lastY;
            this._lastX = this.position.x;
            this._lastY = this.position.y;

            if (this.isUp && this.identifier != null) {
                this._releasePointer();
            }
        }

        isActive(): boolean {
            return this.mouseActive || this.identifier != null;
        }

        mouseDown(e: MouseEvent) {
            if (this._touchInProgress) return;
            if (this.isDown) return;

            super.onDown();

            this.downPosition.x = this._getXValue(e.pageX);
            this.downPosition.y = this._getYValue(e.pageY);
            this.position.x = this.downPosition.x;
            this.position.y = this.downPosition.y;
            this.diffPosition.x = 0;
            this.diffPosition.y = 0;
            this._lastX = this.position.x;
            this._lastY = this.position.y;
            this._lastMouseEvent = e;
        }

        mouseMove(e: MouseEvent) {
            this.position.x = this._getXValue(e.pageX);
            this.position.y = this._getYValue(e.pageY);
            this._lastMouseEvent = e;
            this.mouseActive = true;
        }

        mouseUp(e: MouseEvent) {
            if (this._touchInProgress) return;
            if (this.isUp) return;

            super.onUp();

            this.upPosition.x = this._getXValue(e.pageX);
            this.upPosition.y = this._getYValue(e.pageY);
            this._lastMouseEvent = e;

            this.inputManager.triggerImmediateClick();
        }

        touchDown(e: Touch) {
            if (this.isDown) return;
            if (this.identifier != null) return; // Processing another touch

            super.onDown();

            this._touchInProgress = true;
            this._touchEndTime = null;

            this.identifier = e.identifier;
            this.downPosition.x = this._getXValue(e.pageX);
            this.downPosition.y = this._getYValue(e.pageY);
            this.position.x = this.downPosition.x;
            this.position.y = this.downPosition.y;

            this._acquirePointer();
        }

        touchMove(e: Touch) {
            if (!this.isDown) return;
            if (this.identifier != e.identifier) return; // Processing another touch

            this._touchInProgress = true;

            this.position.x = this._getXValue(e.pageX);
            this.position.y = this._getYValue(e.pageY);
        }

        touchUp(e: Touch) {
            if (this.isUp) return;
            if (this.identifier != e.identifier) return; // Processing another touch

            super.onUp();

            this._touchInProgress = true;
            this._touchEndTime = Time.now() + this._touchEndDebounce;

            this.upPosition.x = this._getXValue(e.pageX);
            this.upPosition.y = this._getYValue(e.pageY);

            this.inputManager.triggerImmediateClick();
        }

        private _getXValue(pagex: number): number {
            return Math.round((pagex - this.scaleManager.offsetX) / this.scaleManager.scale);
        }

        private _getYValue(pagey: number): number {
            return Math.round((pagey - this.scaleManager.offsetY) / this.scaleManager.scale);
        }

        private _acquirePointer() {
            this._prevMousePosition.x = -1;
            this._prevMousePosition.y = -1;
            this._prevMouseBState = -1;
        }

        private _releasePointer() {
            this.identifier = null;
        }

        //
        // private 
        //
        // use only by InputManager
        mouseChanged(): boolean {
            let changed = false;
            let bstate = this.isDown ? 0 : 1;
            let newx = this.position.x;
            let newy = this.position.y;

            if (bstate != this._prevMouseBState) {
                changed = true;
            }

            if (!changed && (this._prevMousePosition.x != newx || this._prevMousePosition.y != newy)) {
                changed = true;
            }

            this._prevMouseBState = bstate;
            this._prevMousePosition.x = newx;
            this._prevMousePosition.y = newy;

            return changed;
        }

        wasSwipe() {
            let dst = Point.distance(this.upPosition, this.downPosition);
            return dst >= 60;
        }

        getLastMouseEvent(): MouseEvent {
            return this._lastMouseEvent;
        }
    }

}
