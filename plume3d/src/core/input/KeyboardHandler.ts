﻿
module plume {


    export class KeyboardHandler implements Updatable {

        private _enable: boolean = false;
        keys: Array<KeyInput> = [];
        keysByCode: HashMap<KeyInput> = new HashMap<KeyInput>();
        keyUpHandlers: Handler<KeyInput>;
        keyDownHandlers: Handler<KeyInput>;

        propagatedKeys: Array<number> = [];
        propagateAllKeys: boolean = false;

        constructor(game: Game, inputManager: InputManager) {
            this.enable();

            // dev (refresh/ devtools)
            this.propagatedKeys.push(KeyCode.F5);
            this.propagatedKeys.push(KeyCode.F12);

            this.keyUpHandlers = new Handler<KeyInput>();
            this.keyDownHandlers = new Handler<KeyInput>();
        }

        update() {

            if (!this.enable) return;

            for (let i = 0; i < this.keys.length; i++) {
                let k = this.keys[i];
                if (k != null) {
                    k.update();
                }
            }
        }

        enable() {
            if (this._enable) return;

            this._enable = true;

            window.addEventListener("keydown", this._keydown, false);
            window.addEventListener("keyup", this._keyup, false);
        }

        disable() {
            if (!this._enable) return;

            this._enable = false;

            window.removeEventListener("keydown", this._keydown);
            window.removeEventListener("keyup", this._keyup);
        }

        key(code: number | string): KeyInput {
            return this._getInput(code);
        }

        isDown(code: number | string): boolean {
            let key = this._getInput(code);
            if (key == null) return false;
            return key.isDown;
        }

        isUp(code: number | string): boolean {
            let key = this._getInput(code);
            if (key == null) return true;
            return key.isUp;
        }

        isJustDown(code: number | string): boolean {
            let key = this._getInput(code);
            if (key == null) return false;
            return key.isJustDown;
        }

        isJustUp(code: number | string): boolean {
            let key = this._getInput(code);
            if (key == null) return false;
            return key.isJustUp;
        }

        private _keydown = (e: KeyboardEvent) => {
            let key = this._ensureKey(e.which, e.code);
            key.keyDown(e);
            this.keyDownHandlers.fire(key);
            this._stopEvent(e);
        }

        private _keyup = (e: KeyboardEvent) => {
            let key = this._ensureKey(e.which, e.code);
            key.keyUp(e);
            this.keyUpHandlers.fire(key);
            this._stopEvent(e);
        }

        private _stopEvent(e: KeyboardEvent) {
            if (this.propagateAllKeys) return;

            let keycode = e.which;
            if (keycode && Collections.contains(this.propagatedKeys, keycode)) {
                return;
            }

            e.preventDefault();
            e.stopPropagation();
        }

        private _ensureKey(keycode: number, code: string): KeyInput {
            let key = this.keys[keycode];
            if (key == null) {
                key = new KeyInput(keycode);
                this.keys[keycode] = key; // keyocde : 1, 2, 3, etc
                this.keysByCode.put(code, key); // code: KeyA, KeyB, Space, etc
            }
            return key;
        }

        private _getInput(k: number | string) {
            if (typeof k == "number") return this.keys[k];
            return this.keysByCode.get(k);
        }
    }

}
