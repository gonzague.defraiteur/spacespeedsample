﻿
module plume {

    export class VisibilityManager {

        private _isUnloading = false; // true when onbeforeunload event has been fired (game shutdown)
        private _isVisible = true; // internal flag, defaults to true
        private _isExtendedVisible = true;
        private _hiddenPropertyName: string;
        private _visibilityEventName: string;

        private _visibilityChangeHandler: Handler<boolean> = new Handler<boolean>();
        private _extendedVisibilityChangeHandler: Handler<{ isExtended: boolean, triggerProperty: string }> = new Handler();
        private _beforeUnloadHandler: Handler<Event> = new Handler<Event>();

        private _browserPrefixes = ['moz', 'ms', 'o', 'webkit'];

        constructor() {

            let self = this;

            // bind and handle events
            let browserPrefix = this._getBrowserPrefix();
            this._hiddenPropertyName = this._getHiddenPropertyName(browserPrefix),
                this._visibilityEventName = this._getVisibilityEvent(browserPrefix);


            document.addEventListener(this._visibilityEventName, this._handleVisibilityChange.bind(this), false);

            // extra event listeners for better behaviour
            document.addEventListener('focus', function () {
                self._handleExtendedVisibilityChange(true, 'document-focus');
            }, false);

            document.addEventListener('blur', function () {
                self._handleExtendedVisibilityChange(false, 'document-blur');
            }, false);

            window.addEventListener('focus', function () {
                self._handleExtendedVisibilityChange(true, 'window-focus');
            }, false);

            window.addEventListener('blur', function () {
                self._handleExtendedVisibilityChange(false, 'window-blur');
            }, false);



            //window.onbeforeunload = this.handleOnBeforeUnload.bind(this);;
            window.addEventListener("beforeunload", this._handleOnBeforeUnload.bind(this), false);
        }




        // get the correct attribute name
        private _getHiddenPropertyName(prefix: string) {
            return (prefix ? prefix + 'Hidden' : 'hidden');
        }

        // get the correct event name
        private _getVisibilityEvent(prefix: string) {
            return (prefix ? prefix : '') + 'visibilitychange';
        }

        // get current browser vendor prefix
        private _getBrowserPrefix() {
            for (let i = 0; i < this._browserPrefixes.length; i++) {
                if (this._getHiddenPropertyName(this._browserPrefixes[i]) in document) {
                    // return vendor prefix
                    return this._browserPrefixes[i];
                }
            }

            // no vendor prefix needed
            return null;
        }

        private _handleVisibilityChange() {

            if (document[this._hiddenPropertyName]) {
                this._isVisible = false;
                this._visibilityChangeHandler.fire(false);
                this._onExtendedHidden(this._hiddenPropertyName);
                return;
            } else {
                this._isVisible = true;
                this._visibilityChangeHandler.fire(true);
                this._onExtendedVisible(this._hiddenPropertyName);
            }

        }

        private _handleExtendedVisibilityChange(forcedFlag: boolean, triggerProperty: string) {
            // forcedFlag is a boolean when this event handler is triggered by a
            // focus or blur eventotherwise it's an Event object
            if (forcedFlag) {
                return this._onExtendedVisible(triggerProperty);
            }
            return this._onExtendedHidden(triggerProperty);

        }



        private _onExtendedVisible(triggerProperty: string) {
            // prevent double execution
            if (this._isExtendedVisible) {
                return;
            }
            // change flag value
            this._isExtendedVisible = true;

            this._extendedVisibilityChangeHandler.fire({ isExtended: this._isExtendedVisible, triggerProperty: triggerProperty });
        }


        private _onExtendedHidden(triggerProperty: string) {
            // prevent double execution
            if (!this._isExtendedVisible) {
                return;
            }
            // change flag value
            this._isExtendedVisible = false;
            this._extendedVisibilityChangeHandler.fire({ isExtended: this._isExtendedVisible, triggerProperty: triggerProperty });
        }

        private _handleOnBeforeUnload(evt: Event) {
            this._isUnloading = true;
            this._beforeUnloadHandler.fire(evt);
        }

        addVisibilityChangeHandler(handler: (visible: boolean) => any) {
            this._visibilityChangeHandler.add(handler);
        }
        removeVisibilityChangeHandler(handler: (visible: boolean) => any) {
            this._visibilityChangeHandler.remove(handler);
        }

        addExtendedVisibilityChangeHandler(handler: Listener<{ isExtended: boolean, triggerProperty: string }>) {
            this._extendedVisibilityChangeHandler.add(handler);
        }
        removeExtendedVisibilityChangeHandler(handler: Listener<{ isExtended: boolean, triggerProperty: string }>) {
            this._extendedVisibilityChangeHandler.remove(handler);
        }

        addBeforeUnloadHandler(handler: (evt: Event) => any) {
            this._beforeUnloadHandler.add(handler);
        }
        removeBeforeUnloadHandler(handler: (evt: Event) => any) {
            this._beforeUnloadHandler.remove(handler);
        }

        isVisible(): boolean {
            return this._isVisible;
        }

        isUnloading(): boolean {
            return this._isUnloading;
        }

        get hidden(): boolean {
            return !this._isVisible;
        }

        set hidden(v: boolean) {
            throw "Read only";
        }


    }


}
