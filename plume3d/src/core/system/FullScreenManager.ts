﻿
module plume {


    interface FullScreenElement {
        name?: string
    }

    export class FullScreenManager {

        private _element: HTMLElement;
        protected _isInFullScreen: boolean;
        protected _changeHandler: Handler<{}>;

        fullscreenEnabled: boolean;

        requestFunction: Function;

        constructor() {
            this.fullscreenEnabled = document.fullscreenEnabled || document['msFullscreenEnabled'] || document['mozFullScreenEnabled'] || document['webkitFullscreenEnabled'];

            this._element = document.documentElement;
            this._isInFullScreen = this._hasFullscreenElement();

            document.addEventListener("fullscreenchange", this._onFullscreenChange);
            document.addEventListener("msfullscreenchange", this._onFullscreenChange);
            document.addEventListener("mozfullscreenchange", this._onFullscreenChange);
            document.addEventListener("webkitfullscreenchange", this._onFullscreenChange);
        }

        get isInFullScreen(): boolean {
            return this._isInFullScreen;
        }

        get changeHandler(): Handler<{}> {
            if (this._changeHandler == null) this._changeHandler = new Handler();
            return this._changeHandler;
        }

        requestFullScreen() {
            if (!this.fullscreenEnabled) return;

            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement['msRequestFullscreen']) {
                document.documentElement['msRequestFullscreen']();
            } else if (document.documentElement['mozRequestFullScreen']) {
                document.documentElement['mozRequestFullScreen']();
            } else if (document.documentElement['webkitRequestFullscreen']) {
                let aa = (<any>Element).ALLOW_KEYBOARD_INPUT;
                document.documentElement['webkitRequestFullscreen'](); //Element.ALLOW_KEYBOARD_INPUT
            }
        }

        exitFullscreen() {
            if (!this.fullscreenEnabled) return;

            if (document['exitFullscreen']) {
                document['exitFullscreen']();
            } else if (document['msExitFullscreen']) {
                document['msExitFullscreen']();
            } else if (document['mozCancelFullScreen']) {
                document['mozCancelFullScreen']();
            } else if (document['webkitExitFullscreen']) {
                document['webkitExitFullscreen']();
            }
        }

        private _onFullscreenChange = () => {
            this._isInFullScreen = this._hasFullscreenElement();

            if (this._changeHandler != null) {
                this._changeHandler.fire({});
            }
        }

        private _hasFullscreenElement(): boolean {
            let fullscreenElement = document['fullscreenElement'] || document['msFullscreenElement'] || document['mozFullScreenElement'] || document['webkitFullscreenElement'];
            return fullscreenElement != null;
        }

        toggleFullScreen() {
            if (!this.fullscreenEnabled) {
                logger.info("FullScreen is not enabled");
            } else {
                if (this.isInFullScreen) {
                    this.exitFullscreen();
                } else {
                    this.requestFullScreen();
                }
            }
        }

    }


}