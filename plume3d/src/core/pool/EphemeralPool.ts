/// <reference path="../utils/memory/RecyclePool.ts" />

module plume {

    // Keep only one instance for the game
    // Acces via plume.ephemeralPool
    // Data are automatically released each tick!
    export class EphemeralPool {

        vectors3: RecyclePool<THREE.Vector3>;
        quaternions: RecyclePool<THREE.Quaternion>;

        constructor() {
            this.vectors3 = new RecyclePool(() => {
                return new THREE.Vector3();
            }, 10);
            this.quaternions = new RecyclePool(() => {
                return new THREE.Quaternion();
            }, 5);
        }

        clean() {
            this.vectors3.reset();
            this.quaternions.reset();
        }

        v3(x: number = 0, y: number = 0, z: number = 0): THREE.Vector3 {
            let v = this.vectors3.add();
            v.set(x, y, z);
            return v;
        }

        q(x: number = 0, y: number = 0, z: number = 0, w: number = 1): THREE.Quaternion {
            let v = this.quaternions.add();
            v.set(x, y, z, w);
            return v;
        }
    }

    export let ephemeralPool: EphemeralPool = new EphemeralPool();

}