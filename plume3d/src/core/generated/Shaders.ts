// this file is auto-generated.
module plume.core.shaders {

	export let test1_fragment = 'void main() {\ngl_FragColor = vec4(1, 0, 0.5, 1);\n}';
	export let test1_vertex = 'attribute vec2 a_position;\nvoid main() {\ngl_Position = vec4(a_position, 0, 1);\n}';
	
}
