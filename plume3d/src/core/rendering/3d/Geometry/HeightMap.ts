module plume {

    export class HeightMap {

        optFilterR = 0.3;
        optFilterG = 0.59;
        optFilterB = 0.11;

        private _geometry: THREE.PlaneBufferGeometry;
        private _loaded = false;
        private _loadedCb: () => void;

        constructor(private _width: number, private _depth: number, private _options: { minHeight?: number, maxHeight?: number }) {
        }

        fromImage(url: string) {
            let self = this;
            let resource = new Image();
            resource.src = url;
            resource.onload = function () {
                self._readImg(resource);
            }
            resource.onerror = function () {
                logger.error("Failed to load " + url);
            }
            if (resource.complete) {
                // already loaded
                self._readImg(resource);
            }
        }

        fromPoints(w: number, z: number, heights: Array<number>) {
            this._createGeometry(w, z, heights);
        }

        private _readImg(img: HTMLImageElement) {
            let canvas = document.createElement("canvas");
            let context = canvas.getContext("2d");
            let width = img.width;
            let height = img.height;
            canvas.width = width;
            canvas.height = height;

            context.drawImage(img, 0, 0);

            let values: Array<number> = [];
            let data = context.getImageData(0, 0, width, height).data;

            for (let k = 0; k < data.length; k = k + 4) {
                let r = data[k + 0] / 255;
                let g = data[k + 1] / 255;
                let b = data[k + 2] / 255;
                let value = r * this.optFilterR + g * this.optFilterG + b * this.optFilterB;
                // position.y = options.minHeight + (options.maxHeight - options.minHeight) * gradient;
                values.push(value);
            }

            this._createGeometry(width, height, values);
        }

        private _createGeometry(swidth: number, sheight: number, heights: Array<number>) {

            if (swidth * sheight != heights.length) {
                logger.warn("Invalid parameters (not enough or too much points)");
            }

            let minHeight = this._options.minHeight || 0;
            let maxHeight = this._options.maxHeight || 1;

            //swidth-1?
            this._geometry = new THREE.PlaneBufferGeometry(this._width, this._depth, swidth - 1, sheight - 1);
            this._geometry.rotateX(- Math.PI / 2);

            let vertices = this._geometry.getAttribute("position").array as Array<number>;
            for (let i = 0, j = 0, l = vertices.length; i < l; i++ , j += 3) {
                let v = minHeight + (maxHeight - minHeight) * heights[i];
                vertices[j + 1] = v;
            }

            this._notifyLodead();
        }

        private _notifyLodead() {
            this._loaded = true;
            if (this._loadedCb != null) {
                this._loadedCb();
            }
        }

        createMesh(material: THREE.MeshMaterialType) {
            let mesh = new THREE.Mesh(this._geometry, material);
            return mesh;
        }

        getGeometry() {
            return this._geometry;
        }

        isLoaded() {
            return this._loaded;
        }

        onLoaded(cb: () => void) {
            if (this._loaded) {
                cb();
                return;
            }

            this._loadedCb = cb;
        }

    }

}