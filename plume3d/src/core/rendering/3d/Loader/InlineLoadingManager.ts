
module plume {

    type Asset = {
        t: string;
        name: string;
        data: string;
    }

    declare var pako: any;

    export class InlineLoadingManager {

        private static _INST: InlineLoadingManager;

        assetByName: plume.HashMap<Asset>;

        constructor() {
            InlineLoadingManager._INST = this;
        }

        static get(): InlineLoadingManager {
            return this._INST;
        }

        initialize(loader: Loader3d) {
            loader.manager.setURLModifier(this._getLocalUrl.bind(this));
            loader.onLoaded(this._onLoaded.bind(this));


            this.assetByName = new plume.HashMap<Asset>();
            let assets = window["__gb_assets"] as Array<Asset>;
            if (assets != null) {
                for (let i = 0; i < assets.length; i++) {
                    let a = assets[i];
                    if (this.assetByName.get(a.name) != null) {

                    }
                    this.assetByName.put(a.name, a);
                }
            }

            // ! Ugly hook for ads to overload assets (see VAdsHandler) !
            let __gb_adshandler = window["__gb_adshandler"] as any;
            if (__gb_adshandler != null) {
                let override = __gb_adshandler.assets as { [name: string]: string };
                for (let k in override) {
                    let asset = this.assetByName.get(k);
                    if (asset == null) {
                        logger.warn("No asset def found for " + k);
                    } else {
                        logger.debug("Replacing asset " + asset.name);
                        asset.data = override[k];
                    }
                }
            }
        }


        private _getLocalUrl(url: string): string {
            let asset = this.assetByName.get(url);
            if (asset == null) {
                //TB avoid logging big stuff
                let urlLenght = url.length;
                let maxLenght = Math.min(urlLenght, 100);

                if (url && url.substring(0, 4) != "data") {
                    logger.warn("No inline ressource found for " + url.slice(0, maxLenght) + "...(truncated)");
                }
                return url;
            }

            if (asset.t == "png") {
                //TB test zlib decompression 
                //TODO try to do less data conversion
                // maybe this should be done elsewhere
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        let b64Str = asset.data
                        let strData = atob(b64Str);

                        //uncompress
                        let unzipData = pako.inflate(strData, { raw: false });

                        //convert back array to base64 str
                        asset.data = this._arrayBufferToBase64(unzipData);
                    } catch (err) {
                        logger.error("failed to decompress image", err);
                    }
                }


                return asset.data;
            } else if (asset.t == "bin") {

                //TB test zlib decompression 
                //TODO try to do less data conversion
                // maybe this should be done elsewhere
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        let b64Str = asset.data
                        let strData = atob(b64Str);

                        //uncompress
                        let unzipData = pako.inflate(strData, { raw: false });

                        //convert back array to base64 str
                        asset.data = this._arrayBufferToBase64(unzipData);
                    } catch (err) {
                        logger.error("failed to decompress bin", err);
                    }
                }

                return "data:arraybuffer;base64," + asset.data
            } else if (asset.t == "json") {

                //TB test zlib decompression 
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        let b64Str = asset.data
                        let strData = atob(b64Str);

                        //directly decompress to utf8 string
                        let unzipData = pako.inflate(strData, { raw: false, to: 'string' });
                        asset.data = unzipData;
                    } catch (err) {
                        logger.error("failed to decompress json", err);
                    }
                }


                return "data:json," + asset.data
            } else {
                logger.warn("unsupported type: " + asset.t + " for " + asset.name);
            }

            return url;
        }

        private _onLoaded() {
            // console.log("LOADING FINISHED!");
            // KO
        }

        private _arrayBufferToBase64(buffer: any) {
            let binary = '';
            let bytes = new Uint8Array(buffer);
            let len = bytes.byteLength;
            for (let i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        }

    }

}