module plume {

    //
    // Same as THREE.TextureLoader but can update an existing texture
    //
    export class TextureLoader {

        crossOrigin = "anonymous";

        constructor(public manager?: THREE.LoadingManager, public cache?: TextureCache) {

            if (manager == null) {
                this.manager = THREE.DefaultLoadingManager;
            }
            if (cache == null) {
                this.cache = Engine.get().textureCache;
            }
        }

        load(url: string, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture {
            let texture = new THREE.Texture();
            this._load0(url, texture, onLoad, onError);
            return texture;
        }

        swap(url: string, texture: THREE.Texture, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture {
            this._load0(url, texture, onLoad, onError);
            return texture;
        }

        private _load0(url: string, texture: THREE.Texture, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture {

            let loader = new THREE.ImageLoader(this.manager);
            loader.setCrossOrigin(this.crossOrigin);
            // loader.setPath(this.path);

            loader.load(url, function (image) {
                texture.image = image;
                // JPEGs can't have an alpha channel, so memory can be saved by storing them as RGB.
                let isJPEG = url.search(/\.jpe?g$/i) > 0 || url.search(/^data\:image\/jpeg/) === 0;
                texture.format = isJPEG ? THREE.RGBFormat : THREE.RGBAFormat;
                texture.needsUpdate = true;
                if (onLoad !== undefined) {
                    onLoad(texture);
                }
            }, null, onError);
            return texture;
        }

    }

}