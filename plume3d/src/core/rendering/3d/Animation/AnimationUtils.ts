module plume {

    function parallelTraverse(a: THREE.Object3D, b: THREE.Object3D, callback: (sn: THREE.Object3D, cn: THREE.Object3D) => void) {
        callback(a, b);
        for (let i = 0; i < a.children.length; i++) {
            parallelTraverse(a.children[i], b.children[i], callback);
        }
    }

    // Waiting: https://github.com/mrdoob/three.js/pull/14494
    export class AnimUtils {
        static clone(source: THREE.Object3D) {

            let cloneLookup = new Map();
            let clone = source.clone();

            parallelTraverse(source, clone, function (sourceNode, clonedNode) {
                cloneLookup.set(sourceNode, clonedNode);
            });

            source.traverse(function (sourceMesh: THREE.SkinnedMesh) {

                if (!sourceMesh.isSkinnedMesh) return;

                let sourceBones = sourceMesh.skeleton.bones;
                let clonedMesh = cloneLookup.get(sourceMesh);
                clonedMesh.skeleton = sourceMesh.skeleton.clone();
                clonedMesh.skeleton.bones = sourceBones.map(function (sourceBone) {
                    if (!cloneLookup.has(sourceBone)) {
                        throw new Error('THREE.AnimationUtils: Required bones are not descendants of the given object.');
                    }
                    return cloneLookup.get(sourceBone);
                });

                clonedMesh.bind(clonedMesh.skeleton, sourceMesh.bindMatrix);
                // console.log("Skeleton cloned! " + clonedMesh.name);
            });

            return clone;
        }
    }

}