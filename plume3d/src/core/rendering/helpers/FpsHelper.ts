module plume {
    export class FpsHelper implements Updatable {

        private _statsFlush: number;
        private _statsDiv: HTMLDivElement;
        private _engine: Engine;
        private _game: Game;

        optShowFps = true;
        optShowUpdateTime = false;
        optShowRenderTime = false;

        // rendering
        optShowCalls = false;
        optShowTriangles = false;

        // memory
        optShowGeometries = false;
        optShowTextures = false;

        // physics
        optShowPhysics = false;

        // debug
        optPrintRenderableOnClick = true;

        private _customStats = new HashMap<StatEntry>();

        constructor(showDiv = true) {
            this._statsFlush = performance.now();

            if (showDiv) {
                let div = document.createElement("div");
                div.id = "plume-fps";
                div.style.position = "absolute";
                div.style.color = "white";
                div.style.fontFamily = "Consolas, monaco, monospace";
                div.style.fontSize = "12px";
                div.style.backgroundColor = "#242526";
                div.style.zIndex = "11";
                div.onclick = this._OnClick.bind(this);
                document.body.appendChild(div);
                this._statsDiv = div;
            }

            this._engine = Engine.get();
            this._game = Game.get();

            this._game.trackStats = true;
            this._game.addUpdatable(this);
        }

        showAll() {
            this.optShowFps = true;
            this.optShowUpdateTime = true;
            this.optShowRenderTime = true;
            this.optShowCalls = true;
            this.optShowTriangles = true;
            this.optShowGeometries = true;
            this.optShowTextures = true;
            this.optShowPhysics = true;
        }

        update(dt: number) {

            let now = performance.now();
            let ellapsed = now - this._statsFlush;

            if (ellapsed >= 1000) {
                let info = this._engine.renderer.info;
                let text = "";

                // fps
                let fps = (this._game._renderStat.count / (ellapsed / 1000)).toFixed();
                this._statsFlush = now;
                text += fps + " fps\n";

                if (this.optShowUpdateTime) {
                    text += "Update: " + (this._game._updateStat.sum / this._game._updateStat.count).toFixed(2) + " ms\n";
                }
                if (this.optShowRenderTime) {
                    text += "Render: " + (this._game._renderStat.sum / this._game._updateStat.count).toFixed(2) + " ms\n";
                    text += "Render delta: " + (this._game._deltaStat.sum / this._game._deltaStat.count).toFixed(2) + " ms\n";
                }
                if (this._game.simulation && this._game.simulation.stats) {
                    if (this.optShowPhysics) {
                        text += "Physics: " + this._game.simulation.stats.avg + " ms\n";
                    }
                }

                this._game._updateStat.count = 0;
                this._game._updateStat.sum = 0;
                this._game._renderStat.count = 0;
                this._game._renderStat.sum = 0;
                this._game._deltaStat.count = 0;
                this._game._deltaStat.sum = 0;

                //
                // threejs stats 
                if (this.optShowCalls) {
                    text += "Calls: " + info.render.calls + "\n";
                }
                if (this.optShowTriangles) {
                    text += "Triangles: " + info.render.triangles + "\n";
                }
                if (this.optShowGeometries) {
                    text += "Geometries: " + info.memory.geometries + "\n";
                }
                if (this.optShowTextures) {
                    text += "Textures: " + info.memory.textures + "\n";
                }

                if (this._customStats.size() > 0) {
                    this._customStats.forEach(function (key, entry) {
                        let avg = entry.sum / entry.count;
                        text += key + ": " + avg.toFixed(2) + "\n";

                        entry.count = 0;
                        entry.sum = 0;
                    }, this);
                }

                let optData = this.onFlushStats();
                if (optData != null) {
                    text += optData;
                }

                if (this._statsDiv != null) {
                    this._statsDiv.innerText = text;
                }
            }
        }

        destroy() {
            this._game.removeUpdatable(this);
            if(this._statsDiv != null) {
                this._statsDiv.parentElement.removeChild(this._statsDiv);
            }
        }

        // Add any text for fps logger
        protected onFlushStats(): string {
            return null;
        }

        addCustom(name: string): StatEntry {
            let entry: StatEntry = {
                count: 0,
                start: 0,
                sum: 0
            }
            this._customStats.put(name, entry);
            return entry;
        }

        getCustom(name: string): StatEntry {
            return this._customStats.get(name);
        }

        private _OnClick() {
            if (!this.optPrintRenderableOnClick) return;

            let renderable = this._engine.renderer.renderLists.get(this._engine.scene, this._engine.camera);
            logger.debug("Opaque: " + renderable.opaque.length + ", Transparent: " + renderable.transparent.length);

            let datas = this._CollectMeshData(renderable.opaque, true);
            datas = this._CollectMeshData(renderable.transparent, true, datas);
            datas.sort(function (m0, m1) {
                return (m1.faces - m0.faces);
            });

            let total = 0;
            logger.debug("Triangles:");
            for (let i = 0; i < datas.length; i++) {
                let d = datas[i];
                logger.debug(d.name + ": " + d.faces + " triangles (with " + d.count + " instances)");
                total += d.faces;
            }
        }

        get div(): HTMLDivElement {
            return this._statsDiv;
        }

        // Call on demand only!
        private _CollectMeshData(input: Array<THREE.RenderItem>, sum: boolean = true, output?: Array<MeshData>): Array<MeshData> {
            if (output == null) {
                output = [];
            }

            let findByName = function (name: string) {
                for (let i = 0; i < output.length; i++) {
                    if (output[i].name == name) return output[i];
                }
                return null;
            }

            for (let i = 0; i < input.length; i++) {
                let renderitem = input[i];
                let object = renderitem.object;
                let name = (object.name == null ? "na" : object.name);
                let faces = -1;
                if (object instanceof THREE.Mesh) {
                    let geometry = object.geometry;
                    if (geometry instanceof THREE.Geometry) {
                        faces = geometry.faces.length;
                    } else if (geometry instanceof THREE.BufferGeometry) {
                        let g = geometry.toNonIndexed();
                        let vertices = g.getAttribute("position").array as Array<number>;
                        faces = vertices.length / 9;
                    }
                }

                if (sum) {
                    let found = findByName(name);
                    if (found == null) {
                        found = { name: name, faces: faces, count: 1 };
                        output.push(found);
                    } else {
                        found.count += 1;
                        found.faces += faces;
                    }
                } else {
                    output.push({ name: name, faces: faces, count: 1 });
                }
            }

            return output;
        }

    }


    type MeshData = {
        name: string,
        faces: number,
        count: number,
    }
}