module plume {

    //
    // TODO merge with 3js ?
    //
    export class Rectangle {

        private _left: number;
        private _top: number;
        private _width: number;
        private _height: number;


        constructor(left: number, top: number, width: number, height: number) {
            this._left = left;
            this._top = top;
            this._width = width;
            this._height = height;
        }

        getCenter(): Point {
            return new Point(this._left + Math.round(this._width / 2), this._top + Math.round(this._height / 2));
        }

        containsPoint(point: Point): boolean {
            return this.containsPoint2(point.x, point.y);
        }

        containsPoint2(px: number, py: number): boolean {
            if ((px >= this.left) && (px <= this.right)) {
                if ((py >= this.top) && (py <= this.bottom)) {
                    return true;
                }
                return false;
            }
            return false;
        }

        overlap(rectangle: Rectangle): boolean {
            return this.left < rectangle.right && this.right > rectangle.left && this.top < rectangle.bottom && this.bottom > rectangle.top;
        }

        toString() {
            return this.x + "," + this.y + " | " + this._width + "," + this._height
        }

        copy(): Rectangle {
            return new Rectangle(this._left, this._top, this._width, this._height);
        }

        // Accessor

        get x(): number {
            return this._left;
        }

        get left(): number {
            return this._left;
        }

        set x(x: number) {
            this._left = x;
        }

        set left(x: number) {
            this._left = x;
        }

        get right(): number {
            return this._left + this._width;
        }

        set right(x: number) {
            this._left = x - this._width;
        }

        get y(): number {
            return this._top;
        }

        get top(): number {
            return this._top;
        }

        set y(y: number) {
            this._top = y;
        }

        set top(y: number) {
            this._top = y;
        }

        get bottom(): number {
            return this._top + this._height;
        }

        set bottom(y: number) {
            this._top = y - this._height;
        }

        get width(): number {
            return this._width;
        }

        set width(width: number) {
            this._width = width;
        }

        get height(): number {
            return this._height;
        }

        set height(height: number) {
            this._height = height;
        }

    }
}