module plume {
    export class Mathf {

        static readonly PI2 = Math.PI;
        static readonly DEG2RAD: number = Math.PI / 180;
        static readonly RAD2DEG: number = 180 / Math.PI;

        static clamp(value: number, min: number, max: number): number {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        }

        static clamp01(value: number): number {
            if (value < 0)
                return 0;
            else if (value > 1)
                return 1;
            else
                return value;
        }

        // positive mod
        static mod(a: number, n: number): number {
            return (a % n + n) % n;
        }

        // t between 0 and 1
        static lerp(v0: number, v1: number, t: number): number {
            if (t <= 0) return v0;
            if (t >= 1) return v1;
            return (1 - t) * v0 + t * v1;
        }
        static inverseLerp(a: number, b: number, value: number): number {
            if (a == b) return 0;
            return (value - a) / (b - a);
        }

        static sign(v: number) {
            if (v >= 0) return 1;
            return -1;
        }

        static repeat(t: number, length: number) {
            return this.mod(t, length);
        }

        static pingPong(t: number, length: number) {
            if (t < 0) t = -t;
            let mod = t % length;
            // if mod is even
            if (Math.ceil(t / length) % 2 === 0) {
                return (mod === 0) ? 0 : length - (mod);
            }
            return (mod === 0) ? length : mod;
        }

        static map(num: number, inMin: number, inMax: number, outMin: number, outMax: number): number {
            if (num < inMin) num = inMin;
            else if (num > inMax) num = inMax;
            return (num - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
        }

        // Gradually changes a value towards a desired goal over time.
        static smoothDamp(current: number, target: number, currentVelocity: number, smoothTime: number, maxSpeed: number, deltaTime: number): { smoothValue: number, newVelocity: number } {
            smoothTime = Math.max(0.0001, smoothTime);
            deltaTime = deltaTime / 1000; // convert ms to s. tomatch unity algo
            let omega = 2 / smoothTime;
            let x = omega * deltaTime;
            let exp = 1 / (1 + x + 0.48 * x * x + 0.235 * x * x * x);
            let change = current - target;
            let originalTo = target;
            if (maxSpeed != null) {
                let maxChange = maxSpeed * smoothTime;
                change = Mathf.clamp(change, -maxChange, maxChange);
            }
            target = current - change;
            let temp = (currentVelocity + omega * change) * deltaTime;
            currentVelocity = (currentVelocity - omega * temp) * exp;
            let output = target + (change + temp) * exp;
            if (originalTo - current > 0 == output > originalTo) {
                output = originalTo;
                currentVelocity = (output - originalTo) / deltaTime;
            }
            return {
                smoothValue: output,
                newVelocity: currentVelocity,
            }
        }

        // Gradually changes an angle given in degrees towards a desired goal angle over time.
        static smoothDampAngle(current: number, target: number, currentVelocity: number, smoothTime: number, maxSpeed: number, deltaTime: number): { smoothValue: number, newVelocity: number } {
            target = current + this.deltaAngle(current, target);
            return this.smoothDamp(current, target, currentVelocity, smoothTime, maxSpeed, deltaTime);
        }

        // Calculates the shortest difference between two given angles.
        static deltaAngle(current: number, target: number): number {
            let delta = Mathf.repeat((target - current), this.PI2);
            if (delta > Math.PI)
                delta -= this.PI2;
            return delta;
        }
    }
}