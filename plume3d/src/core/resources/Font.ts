/// <reference path="AbstractLoadingResource.ts" />
/// <reference path="Texture.ts" />

module plume {

    export interface Font {
        size: number;
        lineHeight: number;
        lettersByCode: Array<Letter>;
        textureByCode: Array<Texture>;
        chars: Array<number>;
        resource: HTMLImageElement;
    }

    export interface Letter {
        id: number;
        x: number;
        y: number;
        height: number;
        width: number;
        xoffset: number;
        yoffset: number;
        xadvance: number;
    }

    export class HFont extends AbstractLoadingResource implements Font {

        private _atlas: FileResource;
        private _image: PImage;

        chars: Array<number>;
        lettersByCode: Array<Letter>;
        size: number;
        lineHeight: number;
        resource: HTMLImageElement;
        textureByCode: Array<Texture>;

        constructor(private _name: string, private _img: string, private _def: string) {
            super();

            // Wait for 2 the requests to be completed before marking resource as laded/failed
            let self = this;
            let q = new WaitQueue(2);
            q.onComplete = function () {
                self._onAllLoaded();
            };

            this.chars = [];
            this.lettersByCode = new Array(65000);
            this.textureByCode = new Array(65000);
            this._atlas = new FileResource(_def);
            this._image = Resources.addImage(_name, _img);
            this.resource = this._image.resource;

            this._atlas.onSuccess(function () {
                q.resolve(0, null);
            });
            this._atlas.onFailed(function () {
                q.resolve(0, null);
            });

            this._image.onSuccess(function () {
                q.resolve(1, null);
            });
            this._image.onFailed(function () {
                q.resolve(1, null);
            });
        }

        private _onAllLoaded() {
            if (!this._image.isTerminated() || !this._atlas.isTerminated()) {
                logger.error("Bad font loading. Image and atlas should be success/failed here");
                return;
            }

            if (this._image.isSuccess() && this._atlas.isSuccess()) {
                this._createFont();
                this.onSuccess0();
            } else {
                this.onFailed0();
            }
        }


        private _createFont() {
            let xmlDoc;
            if (window['DOMParser']) {
                let parser = new DOMParser();
                xmlDoc = parser.parseFromString(this._atlas.data, "text/xml");
            } else {
                xmlDoc = new window["ActiveXObject"]("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(this._atlas.data);
            }

            // <?xml version='1.0'?>
            // <font>
            //     <info aa='1' size='54' smooth='1' stretchH='100' bold='0' padding='0,0,0,0' spacing='0,0' charset='' italic='0' unicode='0' face='cantarell_white'/>
            //     <common scaleW='384' packed='0' pages='1' lineHeight='75' scaleH='384' base='61'/>
            //     <pages>
            //          <page id='0' file='cantarell_white.png'/>
            //     </pages>
            //      <chars count='95'>
            //          <char xadvance='15' x='48' chnl='0' yoffset='57' y='60' xoffset='0' id='32' page='0' height='0' width='0'/>

            let font = xmlDoc.getElementsByTagName('font')[0]
            let info = font.getElementsByTagName('info')[0];
            let common = font.getElementsByTagName('common')[0];

            this.size = parseInt(info.getAttribute("size"));
            this.lineHeight = parseInt(common.getAttribute("lineHeight"));

            let chars = font.getElementsByTagName('char');
            for (let i = 0; i < chars.length; i++) {
                let char = chars[i];
                let letter: Letter = {
                    id: parseInt(char.getAttribute('id')),
                    x: parseInt(char.getAttribute('x')),
                    y: parseInt(char.getAttribute('y')),
                    xoffset: parseInt(char.getAttribute('xoffset')),
                    yoffset: parseInt(char.getAttribute('yoffset')),
                    width: parseInt(char.getAttribute('width')),
                    height: parseInt(char.getAttribute('height')),
                    xadvance: parseInt(char.getAttribute('xadvance')),
                };
                this.lettersByCode[letter.id] = letter;

                let texture = new Texture(this._name + "/" + letter.id, letter.x, letter.y, letter.width, letter.height);
                texture.setImpl(this.resource);
                this.textureByCode[letter.id] = texture;
                this.chars.push(letter.id);
            }
        }

        getAssetDef(): AssetDef {
            return {
                type: "font",
                name: this._name,
                file: this._img,
                atlas: this._def
            }
        }

    }

}