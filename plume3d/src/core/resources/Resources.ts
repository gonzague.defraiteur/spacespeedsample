module plume {

    export class Resources {

        static images: { [name: string]: PImage } = {}
        static spritesheets: { [name: string]: Spritesheet } = {}
        static fonts: { [name: string]: Font } = {}

        static destroy() {
            delete Resources.images;
            delete Resources.spritesheets;
            delete Resources.fonts;
            return this;
        }

        static addImage(name: string, file: string): PImage {
            let image = new PImage(name, file);
            this.images[name] = image;
            return image;
        }

        static addFont(name: string, file: string, atlas: string): HFont {
            let font = new HFont(name, file, atlas);
            this.fonts[name] = font;
            return font;
        }

        static addLoadedFont(name: string, font: Font): Font {
            this.fonts[name] = font;
            return font;
        }

        static addSpriteSheet(name: string, file: string, atlas: string): Spritesheet {
            let spritesheet = new Spritesheet(name, file, atlas);
            this.spritesheets[name] = spritesheet;
            return spritesheet;
        }

        static addSoundsheet(name: string, file: string): Soundsheet {
            let spritesheet = new Soundsheet(name, file);
            return spritesheet;
        }

        static addFile(name: string, file: string): FileResource {
            let spritesheet = new FileResource(file);
            return spritesheet;
        }

    }


}