
module plume {

    export class PImage extends AbstractLoadingResource {

        private _texture: Texture;

        resource: HTMLImageElement;
        created: boolean = false;

        constructor(private _name: string, private _src: string) {
            super();

            let self = this;

            let proxy = LoaderProxy.get();
            this.resource = new Image();

            // Create 0x0 texture until loaded
            this._texture = new Texture(this._name, 0, 0, 0, 0);
            this._texture.setImpl(this.resource);
            Texture.addTexture(this._texture);

            proxy.loadImage(this.resource, _src);

            this.resource.onload = function () {
                self.onSuccess0();
            }
            this.resource.onerror = function () {
                self.onFailed0();
            }

            if (this.resource.complete) {
                // already loaded
                self.onSuccess0();
            }
        }

        protected onSuccess0() {
            // Image loaded, create the texture
            this._createTexture();

            // And fire complete
            super.onSuccess0();
        }

        private _createTexture() {
            if (this.created) return;

            this._texture.x = 0;
            this._texture.y = 0;
            this._texture.width = this.resource.width;
            this._texture.height = this.resource.height;

            this._texture.setLoaded(true);

            this.created = true;
        }

        getTexture(): Texture {
            return this._texture;
        }

        getAssetDef(): AssetDef {
            return {
                type: "img",
                name: this._name,
                file: this._src
            }
        }

    }

}