module plume {

    const enum LoaderState {
        PENDING, LOADING, TERMINATED
    }

    export type LoaderResources = {
        loading: number,
        loaded: number,
        failed: number
    }

    export class Loader2d {

        private _state: LoaderState = LoaderState.PENDING;

        private _completeCb: (success: boolean) => any;

        private _loadingResources: Array<AbstractLoadingResource>;
        private _failedResources: Array<AssetDef> = [];
        private _successResources: Array<AbstractLoadingResource> = [];

        private _terminated = false;
        private _attempt: number = 0;
        private _timer: plume.Timer;

        private _progressHandler: Handler<number> = new Handler();

        constructor(private _ressources: Array<AssetDef>, private _retryAttempt = 3) {
        }

        load(completeCb: (success: boolean) => any) {
            this._completeCb = completeCb;
            this._timer = plume.TimeManager.get().createTimer(100, this._checkLoaded, this);
            this._timer.setRepeat(-1);
            this._timer.start();

            this._load0();
        }

        getResourcesLoaded(): Array<AbstractLoadingResource> {
            return this._successResources;
        }

        private _checkLoaded() {
            if (this._state != LoaderState.LOADING) return;

            let self = this;
            let didProgress = false;
            for (let i = this._loadingResources.length - 1; i >= 0; i--) {
                // let resource = this._loadingResources[i] as (HFont | Spritesheet | HImage | Soundsheet);
                let resource = this._loadingResources[i] as (Soundsheet);
                if (resource.isTerminated()) {
                    this._loadingResources.splice(i, 1);
                    if (resource.isSuccess()) {
                        didProgress = true;
                        this._successResources.push(resource);
                    } else {
                        logger.error("Failed to load " + resource.getAssetDef().file);
                        this._failedResources.push(resource.getAssetDef());
                    }
                }
            }

            if (didProgress) {
                let total = this._loadingResources.length + this._successResources.length + this._failedResources.length;
                let progress = this._successResources.length / total;
                this._progressHandler.fire(progress);
            }

            if (this._loadingResources.length <= 0) {
                // All terminated
                if (this._failedResources.length > 0) {
                    this._state = LoaderState.PENDING;
                    // We got some failure
                    setTimeout(function () {
                        self._retry();
                    }, 250);
                } else {
                    this._state = LoaderState.TERMINATED;
                    this._terminate();
                }
            }
        }

        private _load0() {

            let self = this;

            this._attempt++;
            this._loadingResources = [];
            this._failedResources = [];

            this._ressources.forEach(function (asset) {
                if (asset.type == "img") {
                    let img = Resources.addImage(asset.name, asset.file);
                    self._loadingResources.push(img);
                } else if (asset.type == "font") {
                    let font = Resources.addFont(asset.name, asset.file, asset.atlas);
                    self._loadingResources.push(font);
                } else if (asset.type == "spritesheet") {
                    let spritesheet = Resources.addSpriteSheet(asset.name, asset.file, asset.atlas);
                    self._loadingResources.push(spritesheet);
                } else if (asset.type == "soundsheet") {
                    let soundsheet = Resources.addSoundsheet(asset.name, asset.file);
                    self._loadingResources.push(soundsheet);
                } else if (asset.type == "file") {
                    let soundsheet = Resources.addFile(asset.name, asset.file);
                    self._loadingResources.push(soundsheet);
                }
            });
            this._state = LoaderState.LOADING;
        }

        private _retry() {
            let self = this;
            if (this._attempt >= this._retryAttempt) {
                this._terminate();
            } else {
                this._ressources = [];
                this._failedResources.forEach(function (asset) {
                    self._ressources.push(asset);
                });
                this._load0();
            }
        }

        private _terminate() {
            this._timer.cancel();
            this._terminated = true;

            if (this._failedResources.length > 0) {
                this._completeCb(false);
            } else {
                this._completeCb(true);
            }
        }

        getStatus(): LoaderResources {
            return {
                loading: this._loadingResources.length,
                loaded: this._successResources.length,
                failed: this._failedResources.length,
            }
        }

        getProgress(): number {
            let total = this._loadingResources.length + this._successResources.length + this._failedResources.length;
            let progress = this._successResources.length / total;
            return progress;
        }

        get terminated(): boolean {
            return this._terminated;
        }

        // helper to load single file data
        static loadFile(file: string, onload: (data: string) => void) {
            let loader = new Loader2d([{ type: "file", name: file, file: file }], 5);
            loader.load(function (success) {
                if (!success) {
                    onload(null);
                    return;
                }

                let resources = loader.getResourcesLoaded();
                let fileResources = resources[0] as FileResource;
                onload(fileResources.data);
            });
        }
    }


}