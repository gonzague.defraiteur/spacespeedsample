module plume {

    declare var pako: any;


    type Asset = {
        t: string;
        name: string;
        data: string;
    }

    //
    // GB impl (here to simplify build for now)
    // Assets needs to be in html file in in
    // window["__gb_assets"] = [ {t: "png", name: "game.png", data: "data:image/png;base64,iVBORw0KGgoAAAANS"}, {t: "json", name: "game.json", data: "..", etc ]
    export class InlineLoader extends LoaderProxy {

        private _assetByName: HashMap<Asset>;

        assetExportName = "__gb_assets";

        // XHR
        loadXHR(url: string, onsucess: (response: string) => any, onerror: (error: string) => any): void {

            let asset = this._getAsset(url);
            if (asset == null) {
                logger.error("Asset not found: " + url);
                onerror("Not found " + url);
                return;
            }

            let response = "";
            if (asset.t == "json") {
                //TB test zlib decompression 
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        let b64Str = asset.data
                        let strData = atob(b64Str);

                        //directly decompress to utf8 string
                        let unzipData = pako.inflate(strData, { raw: false, to: 'string' });
                        asset.data = unzipData;
                    } catch (err) {
                        logger.error("failed to decompress json", err);
                    }
                }
                response = asset.data;
            } else {
                response = asset.data;
            }
            onsucess(response);
        }

        // Image
        loadImage(image: HTMLImageElement, src: string) {
            let asset = this._getAsset(src);
            if (asset == null) {
                let start = src.substr(0, 5);
                if (start != "data:") {
                    logger.error("Image not found: " + src);
                    return;
                } else {
                    // request a base64 image, we can use it
                    image.src = src;
                    return;
                }
            }

            image.src = asset.data;
        }

        private _getAsset(name: string): Asset {
            this._ensureInititalized();
            return this._assetByName.get(name);
        }

        private _ensureInititalized() {
            if (this._assetByName != null) return;

            this._assetByName = new HashMap<Asset>();
            let assets = window[this.assetExportName] as Array<Asset>;
            for (let i = 0; i < assets.length; i++) {
                let a = assets[i];
                this._assetByName.put(a.name, a);
            }
        }
    }

}