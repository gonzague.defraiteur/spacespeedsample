/// <reference path="AbstractLoadingResource.ts" />

module plume {

    export class FileResource extends AbstractLoadingResource {

        data: string;

        constructor(protected file: string) {
            super();

            let self = this;

            let onData = (data: string) => {
                self.data = data;
                self.onSuccess0();
            }

            let fail = (reason: string) => {
                logger.error("Can't load " + self.file + ", reason: " + reason);
                self.onFailed0();
            }

            let proxy = LoaderProxy.get();
            proxy.loadXHR(this.file, onData, fail);
        }

        getAssetDef(): AssetDef {
            return {
                type: "file",
                name: this.file,
                file: this.file
            }
        }

    }

}