/// <reference path="AbstractLoadingResource.ts" />

module plume {

    export class Spritesheet extends AbstractLoadingResource {

        private _atlas: FileResource;
        private _image: PImage;

        constructor(private _name: string, private _img: string, private _def: string) {
            super();

            // Wait for 2 the requests to be completed before marking resource as laded/failed
            let self = this;
            let q = new WaitQueue(2);
            q.onComplete = function () {
                self._onAllLoaded();
            };

            this._atlas = new FileResource(_def);
            this._image = Resources.addImage(_name, _img);

            this._atlas.onSuccess(function () {
                q.resolve(0, null);
            });
            this._atlas.onFailed(function () {
                q.resolve(0, null);
            });

            this._image.onSuccess(function () {
                q.resolve(1, null);
            });
            this._image.onFailed(function () {
                q.resolve(1, null);
            });
        }

        private _onAllLoaded() {
            if (!this._image.isTerminated() || !this._atlas.isTerminated()) {
                logger.error("Bad spritesheet loading. Image and atlas should be success/failed here");
                return;
            }

            if (this._image.isSuccess() && this._atlas.isSuccess()) {
                this._createSpritesheet();
                this.onSuccess0();
            } else {
                this.onFailed0();
            }
        }

        private _createSpritesheet() {
            let json = JSON.parse(this._atlas.data);
            for (let frameName in json.frames) {
                let frame = json.frames[frameName].frame;
                let texture: Texture = new Texture(frameName, frame.x, frame.y, frame.w, frame.h);

                if (json.frames[frameName].trimmed) {
                    let sourceSize = json.frames[frameName].sourceSize;
                    let spriteSourceSize = json.frames[frameName].spriteSourceSize;
                    texture.setTrim(spriteSourceSize.x, spriteSourceSize.y, sourceSize.w, sourceSize.h);
                }

                texture.setImpl(this._image.resource);
                texture.setLoaded(true);
                Texture.addTexture(texture);
            }
        }

        getAssetDef(): AssetDef {
            return {
                type: "spritesheet",
                name: this._name,
                file: this._img,
                atlas: this._def
            }
        }

    }

}