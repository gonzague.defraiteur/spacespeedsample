module plume {

    export class HttpRequest {

        private _xhrObject: XMLHttpRequest;

        get(url: string, connectionReceivedHandler: (response: string) => any, asyncSendFailResult: (error: string) => any) {

            let self = this;
            this._initHTTPRequest(asyncSendFailResult);

            if (this._xhrObject) {
                self._xhrObject.open("GET", url, true);

                self._xhrObject.onreadystatechange = function () {
                    if (self._xhrObject.readyState == 4) {
                        if (self._xhrObject.status == 200) {
                            connectionReceivedHandler(self._xhrObject.responseText);
                            self._xhrObject.onreadystatechange = null;
                        } else {
                            asyncSendFailResult("invalid status: " + self._xhrObject.status + " text=" + self._xhrObject.responseText + " statusText: " + self._xhrObject.statusText);
                            self._xhrObject.onreadystatechange = null;
                        }
                    }
                    if (self._xhrObject.readyState <= 0) {
                        asyncSendFailResult("invalid ready state: " + self._xhrObject.readyState);
                        self._xhrObject.onreadystatechange = null;
                    }
                }
                this._xhrObject.onerror = function (e) {
                    asyncSendFailResult("XHR onerror statusText: " + self._xhrObject.statusText);
                };
                this._xhrObject.send(null);
            }
        }

        private _initHTTPRequest(asyncSendFailResult: (error: string) => any) {
            if (window["XMLHttpRequest"]) // Firefox
                this._xhrObject = new XMLHttpRequest();
            else {
                asyncSendFailResult("Not Supported");
                return;
            }
        }


        post(url: string, data: string, connectionReceivedHandler: (data: string)=>any, asyncSendFailResult: (error: string)=>any) {
            let self = this;

            this._initHTTPRequest(asyncSendFailResult);
            if (this._xhrObject) {
                self._xhrObject.open("POST", url, true);

                self._xhrObject.onreadystatechange = function () {
                    if (self._xhrObject.readyState == 4) {
                        if (self._xhrObject.status == 200) {
                            connectionReceivedHandler(self._xhrObject.responseText);
                            self._xhrObject.onreadystatechange = null;
                        } else {
                            asyncSendFailResult("invalid status: " + self._xhrObject.status);
                        }
                    }
                    if (self._xhrObject.readyState <= 0) {
                        asyncSendFailResult("invalid ready state: " + self._xhrObject.readyState);
                        self._xhrObject.onreadystatechange = null;
                    }
                }
                this._xhrObject.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                this._xhrObject.send(data);
            }
        }

    }


}