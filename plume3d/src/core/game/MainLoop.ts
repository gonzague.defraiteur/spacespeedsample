module plume {

    export class RequestAnimationFrame {

        private _running = false;
        private _timeoutId: number;
        private _tick: number;
        private _setTimeoutSleep: number;


        private _onSetTimeoutCbBound: () => void;
        private _onRafCbBound: (time: number) => void;

        constructor(private callback: Function, private forceSetTimeOut: boolean, desiredFps = 60) {
            this._setTimeoutSleep = 1000 / desiredFps;
            this._onSetTimeoutCbBound = this._onSetTimeoutCb.bind(this);
            this._onRafCbBound = this._onRafCb.bind(this);
        }

        start() {
            if (this._running) return;

            this._running = true;
            this._tick = performance.now();

            this._timeoutId = (this.forceSetTimeOut) ? window.setTimeout(this._onSetTimeoutCbBound, 0) : window.requestAnimationFrame(this._onRafCbBound);
        }

        stop() {
            if (!this._running) return;

            this._running = false;

            if (this.forceSetTimeOut) {
                clearTimeout(this._timeoutId);
            }
            else {
                window.cancelAnimationFrame(this._timeoutId);
            }
        }

        private _onSetTimeoutCb() {
            this._tick = performance.now();
            this.callback(this._tick);

            let nextCall = Math.min(this._setTimeoutSleep, performance.now() - this._tick);
            this._timeoutId = window.setTimeout(this._onSetTimeoutCbBound, nextCall);
        }

        private _onRafCb(timestamp: number) {
            this._tick = timestamp;
            this.callback(timestamp);
            this._timeoutId = window.requestAnimationFrame(this._onRafCbBound);
        }

    }


    export class MainLoop {

        private _started = false;
        private _raf: RequestAnimationFrame;

        private _lastTime: number = 0;

        private _deltaHistoryIndex = 0;
        private _deltaHistoryCount = 10;
        private _deltaHistory: Array<number> = [];
        private _targetDeltaTime: number;
        private _timestep: number;

        private _frameDelta: number = 0;

        constructor(private updateCb: (time: number, timeEllapsed: number) => void, private renderCb: (time: number, timeEllapsed: number, interpolation: number) => void) {
        }

        start() {
            if (this._started) return;

            let loopOptions = Game.get().options.gameLoop;
            let desiredFps = (loopOptions != null && loopOptions.desiredFps != null) ? loopOptions.desiredFps : 60;
            let useSetimeout = (loopOptions != null && loopOptions.forceSetTimeout != null) ? loopOptions.forceSetTimeout : false;

            let self = this;
            this._started = true;
            this._targetDeltaTime = 1000 / desiredFps; // Estimated time between 2 loop
            this._timestep = 1000 / desiredFps;

            this._lastTime = performance.now();

            if (loopOptions.smoothDelta) {
                this._raf = new RequestAnimationFrame(this._ticksmooth.bind(this), useSetimeout, desiredFps);
                for (let i = 0; i < this._deltaHistoryCount; i++) {
                    this._deltaHistory[i] = this._targetDeltaTime;
                }
            } else {
                this._raf = new RequestAnimationFrame(this._tick.bind(this), useSetimeout, desiredFps);
            }

            this._raf.start();
        }

        private _tick(time: number) {
            time = performance.now();

            let delta = time - this._lastTime;
            delta = Math.min(delta, 100);

            let interpolation = Mathf.clamp(this._targetDeltaTime / delta, 0, 1);
            this._lastTime = time;
            this.updateCb(time, delta);
            this.renderCb(time, delta, interpolation);
        }

        // test, smooth delta time
        private _ticksmooth(time: number) {
            time = performance.now();

            let delta = time - this._lastTime;
            if (delta > 100) {
                delta = this._deltaHistory[this._deltaHistoryIndex];
            }

            this._deltaHistory[this._deltaHistoryIndex] = delta;
            this._deltaHistoryIndex++;
            if (this._deltaHistoryIndex >= this._deltaHistoryCount) this._deltaHistoryIndex = 0;

            let avg = 0;
            for (let i = 0; i < this._deltaHistoryCount; i++) {
                avg += this._deltaHistory[i];
            }
            avg /= this._deltaHistoryCount;

            let interpolation = avg / delta;
            this._lastTime = time;
            this.updateCb(time, avg);
            this.renderCb(time, delta, interpolation);
        }

        stop() {
            if (!this._started) return;

            this._started = false;
            this._raf.stop();
        }

    }


}