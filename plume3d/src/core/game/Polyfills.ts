namespace plume {

    (function (win: any) {

        // Date.now
        if (!(Date.now && Date.prototype.getTime)) {
            Date.now = function now() {
                return new Date().getTime();
            };
        }

        // performance.now
        if (!(win.performance && win.performance.now)) {
            let startTime = Date.now();
            if (!win.performance) {
                win.performance = {};
            }
            win.performance.now = function () {
                return Date.now() - startTime;
            };
        }

        // requestAnimationFrame
        let lastTime = Date.now();
        let prefix = ['ms', 'moz', 'webkit', 'o'];
        for (let x = 0; x < prefix.length && !win.requestAnimationFrame; ++x) {
            win.requestAnimationFrame = win[prefix[x] + 'RequestAnimationFrame'];
            win.cancelAnimationFrame = win[prefix[x] + 'CancelAnimationFrame'] || win[prefix[x] + 'CancelRequestAnimationFrame'];
        }

        if (!win.requestAnimationFrame) {
            win.requestAnimationFrame = function (callback: (time: number) => void) {
                if (typeof callback !== 'function') {
                    throw new TypeError(callback + 'is not a function');
                }

                let currentTime = Date.now()
                let delay = 16 + lastTime - currentTime;
                if (delay < 0) {
                    delay = 0;
                }
                lastTime = currentTime;

                return setTimeout(function () {
                    lastTime = Date.now();
                    callback(performance.now());
                }, delay);
            };
        }

        if (!win.cancelAnimationFrame) {
            win.cancelAnimationFrame = function (id: number) {
                clearTimeout(id);
            };
        }

    })(window);

}