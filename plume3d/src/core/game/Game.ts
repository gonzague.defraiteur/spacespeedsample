module plume {

    export class Game {

        protected static instance: Game;

        mainLoop: MainLoop;
        options: GameOption;

        scaleManager: ScaleManager
        audioManager: AudioManager
        deviceManager: DeviceManager;
        inputManager: InputManager;
        timeManager: TimeManager;

        _engine: Engine;
        _gui: Gui;
        _simulation: Simulation;

        _lastTick: number = 0;

        // Add generic object aware of the gameloop
        _updatables: Array<Updatable> = [];
        _renderables: Array<Renderable> = [];

        protected _bounds: Rectangle;

        private _doingUpdate = false;
        private _afterUpdate: Array<Function> = [];

        // TODO move from game to stat collector
        private _trackStats = false;
        _renderStat: StatEntry;
        _updateStat: StatEntry;
        _deltaStat: StatEntry;

        constructor(width: number, height: number, options: GameOption = {}) {

            Game.instance = this;

            // sanitize options
            options.gameLoop = options.gameLoop || {};
            options.gameLoop.desiredFps = options.gameLoop.desiredFps || 60;
            options.gameLoop.forceSetTimeout = options.gameLoop.forceSetTimeout || false;
            options.gameLoop.smoothDelta = options.gameLoop.smoothDelta || false;

            options.input = options.input || {};
            options.input.maxTouchPointer = options.input.maxTouchPointer || 1;

            this._lastTick = performance.now();
            this._bounds = new Rectangle(0, 0, width, height);
            this.options = options;
            this.timeManager = TimeManager.get();
            this.deviceManager = new DeviceManager();
            this.audioManager = this.newAudioManager();
        }

        start() {
            this.deviceManager.whenReady(() => {
                try {
                    this.start0();
                } catch (e) {
                    logger.error("Error occured while starting game", e);
                }
            });
        }

        protected newAudioManager() {
            return new AudioManager(this.deviceManager.visibilityManager);
        }

        protected newScaleManager() {
            return new ScaleManager(this);
        }

        static get(): Game {
            return Game.instance;
        }

        static options(): GameOption {
            return Game.instance.options;
        }

        protected start0() {

            let self = this;

            this.scaleManager = this.newScaleManager();
            this.mainLoop = new MainLoop(this.update.bind(this), this.render.bind(this));
            this.mainLoop.start();
            this.inputManager = new InputManager(this);

            this.onCreate();

            window.addEventListener('load', () => {
                self.scaleManager.resize();
            });

            // In iframe, take focus on click
            if (window.parent) {
                document.addEventListener('click', function () {
                    if (!document.hasFocus()) {
                        window.focus();
                    }
                }, false);
            }
        }

        initializeEngine(engine: Engine) {
            this._engine = engine;
            this.inputManager.mouse.enableOnElement(engine.canvas);
        }

        initializeGui(gui: Gui) {
            this._gui = gui;
        }

        initializeSimulation(simulation: Simulation) {
            this._simulation = simulation;
        }

        onCreate() {
        }

        update(time: number, timeEllapsed: number) {
            let lastTick = this._lastTick;
            this._lastTick = performance.now();

            if (this._trackStats) {
                let delta = this._lastTick - lastTick;
                this._deltaStat.sum += delta;
                this._deltaStat.count += 1;
            }

            Time._refresh(time);

            let t0 = performance.now();
            this._doingUpdate = true;
            try {
                this.inputManager.update(timeEllapsed);
                this.timeManager.update(timeEllapsed);

                for (let i = 0; i < this._updatables.length; i++) {
                    this._updatables[i].update(timeEllapsed);
                }

                if (this._simulation != null && this._simulation.running) this._simulation.update(timeEllapsed);
                if (this._gui) this._gui.update(timeEllapsed);

                this._doingUpdate = false;

                if (this._afterUpdate.length > 0) {
                    for (let i = 0; i < this._afterUpdate.length; i++) {
                        this._afterUpdate[i]();
                    }
                    this._afterUpdate = [];
                }

            } catch (e) {
                logger.error("Error occured in game update:" + e.message, e);
                this._doingUpdate = false;
            }
            let t1 = performance.now();

            if (this._trackStats) {
                let ellapsed = t1 - t0;
                this._updateStat.sum += ellapsed;
                this._updateStat.count += 1;
            }
        }

        render(time: number, timeEllapsed: number, interpolation: number) {
            let t1 = performance.now();
            try {
                for (let i = 0; i < this._renderables.length; i++) {
                    this._renderables[i].render(interpolation);
                }

                if (this._engine) this._engine.render(timeEllapsed, interpolation);
                if (this._gui) this._gui.render(timeEllapsed, interpolation);

            } catch (e) {
                logger.error("Error occured in game render: ", e);
            }
            let t2 = performance.now();

            if (this._trackStats) {
                let ellapsed = t2 - t1;
                this._renderStat.sum += ellapsed;
                this._renderStat.count += 1;
            }

            if (ephemeralPool != null) {
                ephemeralPool.clean();
            }
        }

        addRenderable(renderable: Renderable) {
            this._renderables.push(renderable);
        }

        removeRenderable(renderable: Renderable) {
            let i = this._renderables.indexOf(renderable);
            if (i >= 0) {
                this._renderables.splice(i, 1);
            }
            //else {
            // logger.warn("Cannot remove renderable " + i);
            //}
        }

        addUpdatable(updatable: Updatable) {
            if (this._doingUpdate) {
                let self = this;
                this._afterUpdate.push(function () {
                    self._updatables.push(updatable);
                });
            } else {
                this._updatables.push(updatable);
            }
        }

        removeUpdatable(updatable: Updatable) {
            if (this._doingUpdate) {
                let self = this;
                this._afterUpdate.push(function () {
                    self.removeUpdatable(updatable);
                });
            } else {
                let i = this._updatables.indexOf(updatable);
                if (i >= 0) {
                    this._updatables.splice(i, 1);
                }
                //else {
                // logger.warn("Cannot remove updatable " + i);
                //}
            }
        }

        getGui() {
            return this._gui;
        }

        get engine(): Engine {
            return this._engine;
        }
        set engine(v: Engine) {
            throw new Error("use initializeEngine()")
        }

        get simulation(): Simulation {
            return this._simulation;
        }
        set simulation(v: Simulation) {
            this._simulation = v;
        }

        get trackStats(): boolean {
            return this._trackStats;
        }
        set trackStats(v: boolean) {
            this._trackStats = true;
            this._updateStat = { count: 0, start: performance.now(), sum: 0 };
            this._renderStat = { count: 0, start: performance.now(), sum: 0 };
            this._deltaStat = { count: 0, start: performance.now(), sum: 0 };
        }


        get width(): number {
            return this._bounds.width;
        }
        get height(): number {
            return this._bounds.height;
        }
        get bounds(): Rectangle {
            return this._bounds;
        }
    }

}
