﻿module plume {

    export class ScaleManager {

        protected _game: Game;
        protected _pendingResize: boolean = false;

        enable: boolean = true;

        resizeHandler: Handler<{}>;
        canvasToResize: Array<HTMLCanvasElement> = [];

        protected _scalingStategy: ScalingStrategy;
        protected _currentScaling: ScalingStrategyResult;
        protected _windowSizeProvider: () => ({ width: number, height: number });

        constructor(game: Game) {
            this._game = game;
            this.init();
        }

        protected init() {
            // fit screen by default
            this._scalingStategy = new FitScalingStrategy();

            let self = this;
            window.addEventListener('resize', () => {
                if (self._pendingResize) return;
                self._pendingResize = true;

                window.setTimeout(() => {
                    self.resize();
                }, 50);
            });
            window.addEventListener('deviceOrientation', () => {
                if (self._pendingResize) return;
                self._pendingResize = true;

                window.setTimeout(() => {
                    self.resize();
                }, 50);
            });

            this.resizeHandler = new Handler();
            this.resize();
        }

        resize() {
            this._pendingResize = false;

            if (!this.enable) return;

            let width = document.documentElement.clientWidth;
            let height = document.documentElement.clientHeight;

            if (this._windowSizeProvider != null) {
                let size = this._windowSizeProvider();
                width = size.width;
                height = size.height;
            }

            let gameBounds = this._game.bounds;
            this._currentScaling = this._scalingStategy.getScaling(gameBounds.width, gameBounds.height, width, height);

            if (window.scrollTo) {
                window.scrollTo(0, 0);
            }

            // Fire resize event
            this.resizeHandler.fire({});
            // logger.debug("Scaling is => " + this.scale);
            // logger.debug("Game bounds is => " + gameBounds);
            // logger.debug("Absolute game size is:" + JSON.stringify(this.getAbsoluteGameSize()));
        }

        setWindowSizeProvider(provider: () => ({ width: number, height: number })) {
            this._windowSizeProvider = provider;
        }

        setScalingStategy(scalingStategy: ScalingStrategy) {
            this._scalingStategy = scalingStategy;
        }

        getAbsoluteGameSize() {
            let game = Game.get();
            let w = game.width * this._currentScaling.scale;
            let h = game.height * this._currentScaling.scale;
            return {
                width: w,
                height: h,
                marginX: this._currentScaling.x,
                marginY: this._currentScaling.y
            };
        }

        getWindowSize() {
            return {
                width: this._currentScaling.windowWidth,
                height: this._currentScaling.windowHeight
            }
        }

        getCurrentScaling(): ScalingStrategyResult {
            return this._currentScaling;
        }

        get offsetX(): number {
            return this._currentScaling.x;
        }
        get offsetY(): number {
            return this._currentScaling.y;
        }
        get scale(): number {
            return this._currentScaling.scale;
        }
        set offsetX(s: number) {
            this._currentScaling.x = s;
        }
        set offsetY(s: number) {
            this._currentScaling.y = s;
        }
        set scale(s: number) {
            this._currentScaling.scale = s;
        }

    }

    export interface ScalingStrategyResult {
        x: number;
        y: number;
        scale: number;
        windowWidth: number;
        windowHeight: number;
    }

    export interface ScalingStrategy {
        getScaling(sourceWidth: number, sourceHeight: number, targetWidth: number, targetHeight: number): ScalingStrategyResult;
    }

    export class FitScalingStrategy implements ScalingStrategy {
        getScaling(sourceWidth: number, sourceHeight: number, targetWidth: number, targetHeight: number): ScalingStrategyResult {
            let targetRatio = targetHeight / targetWidth;
            let sourceRatio = sourceHeight / sourceWidth;
            let scale = targetRatio > sourceRatio ? targetWidth / sourceWidth : targetHeight / sourceHeight;
            return {
                x: (targetWidth - sourceWidth * scale) / 2,
                y: (targetHeight - sourceHeight * scale) / 2,
                scale: scale,
                windowWidth: targetWidth,
                windowHeight: targetHeight,
            }
        }
    }

    export class FillScalingStrategy implements ScalingStrategy {
        getScaling(sourceWidth: number, sourceHeight: number, targetWidth: number, targetHeight: number): ScalingStrategyResult {
            let targetRatio = targetHeight / targetWidth;
            let sourceRatio = sourceHeight / sourceWidth;
            let scale = targetRatio < sourceRatio ? targetWidth / sourceWidth : targetHeight / sourceHeight;
            return {
                x: (targetWidth - sourceWidth * scale) / 2,
                y: (targetHeight - sourceHeight * scale) / 2,
                scale: scale,
                windowWidth: targetWidth,
                windowHeight: targetHeight,
            }
        }
    }

}
