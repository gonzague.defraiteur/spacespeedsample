
module plume {

    export interface GameOption {
        input?: InputOption;
        gameLoop?: LoopOptions;
    }

    export interface InputOption {
        maxTouchPointer?: number;
    }

    export interface LoopOptions {
        forceSetTimeout?: boolean;
        desiredFps?: number;
        smoothDelta?: boolean;
    }

}
