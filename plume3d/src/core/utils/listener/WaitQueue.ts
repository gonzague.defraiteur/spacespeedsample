module plume {

    // really basic wait n query before triggering onComplete
    export class WaitQueue {

        private _data: Array<any> = [];
        private _resolved: Array<boolean> = [];

        public onComplete: (data: Array<any>) => any;

        constructor(private size: number) {
            for (let i = 0; i < size; i++) {
                this._resolved[i] = false;
            }
        }

        resolve(index: number, data: any) {
            this._data[index] = data;
            this._resolved[index] = true;

            if (this._allDone()) {
                this.onComplete(this._data);
            }
        }

        private _allDone(): boolean {
            for (let i = 0; i < this._resolved.length; i++) {
                if (!this._resolved[i]) {
                    return false;
                }
            }
            return true;
        }

    }


}