module plume {

    export class Collections {

        static shuffle(array: Array<any>, useSeed = false) {
            let i = 0;
            let j = 0;
            let temp = null;
            for (i = array.length - 1; i > 0; i -= 1) {
                j = Math.floor(Random.get(useSeed) * (i + 1));
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }

        static rotate(array: Array<any>, step: number) {
            while (step--) {
                let temp = array.shift();
                array.push(temp);
            }
        }

        static concat(...arrays: Array<any>[]): Array<any> {
            let result = [];
            for (let i = 0; i < arrays.length; i++) {
                let sa = arrays[i];
                for (let j = 0; j < sa.length; j++) {
                    result.push(sa[j]);
                }
            }
            return result;
        }

        static addAll<T>(source: Array<T>, elements: Array<T>): void {
            if (elements == null) return;

            for (let i = 0; i < elements.length; i++) {
                source.push(elements[i]);
            }
        }

        static copy<T>(array: Array<T>): Array<T> {
            if (array == null) return null;
            return array.slice();
        }

        // a random element of the array
        static one<T>(array: Array<T>, useSeed = false): T {
            if (array == null || array.length < 1) return null;
            if (array.length == 1) return array[0];

            let rand = Random.intInRange(0, array.length - 1, useSeed);
            return array[rand];
        }

        // return n random elements from array
        static some<T>(array: Array<T>, count: number, useSeed = false): Array<T> {
            if (array == null || array.length < 1) return null;

            let copy = Collections.copy(array);
            Collections.shuffle(copy, useSeed);

            if (copy.length <= count) return copy;

            return copy.slice(0, count);
        }

        static remove<T>(array: Array<T>, obj: T): boolean {
            let idx = array.indexOf(obj);
            if (idx >= 0) {
                let removed = array.splice(idx, 1);
                if (removed != null && removed.length > 0) {
                    return true;
                }
            }
            return false;
        }

        static contains<T>(array: Array<T>, obj: T): boolean {
            return (array.indexOf(obj) >= 0);
        }

    }

}