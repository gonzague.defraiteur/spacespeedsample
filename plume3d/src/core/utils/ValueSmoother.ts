module plume {

    export abstract class ValueSmoother<T> {

        state = 0;
        data: any;

        startTime = 0;
        duration = 3000;

        startValue: T;
        endValue: T;
        currentValue: T;

        easing: EasingFunction;

        reset(startValue: T, targetValue: T, duration: number, now?: number, currentValue?: T) {
            this.duration = duration;
            this.startTime = (now == null ? performance.now() : now);
            this.startValue = startValue;
            this.endValue = targetValue;

            if (currentValue != null) {
                this.currentValue = currentValue;
            } else {
                if (this.currentValue == null) this.currentValue = this.startValue; // ensure we have a current value
            }
        }

        getInterpolation(now?: number): T {
            let now0 = (now == null ? performance.now() : now);
            let ellapsed = now0 - this.startTime;
            if (ellapsed >= this.duration) {
                return this.endValue;
            }

            let percent = ellapsed / this.duration;
            let easingvalue = percent; // linear by default
            if (this.easing != null) {
                easingvalue = this.easing(percent);
            }
            easingvalue = THREE.Math.clamp(easingvalue, 0, 1);

            this.currentValue = this.computeCurrentValue(easingvalue);

            return this.currentValue;
        }

        abstract computeCurrentValue(t: number): T;
    }

    export class ValueSmootherNumber extends ValueSmoother<number> {
        computeCurrentValue(t: number): number {
            return THREEEXT.Math.lerp(this.startValue, this.endValue, t);
        }
    }

    export class ValueSmootherVector3 extends ValueSmoother<THREE.Vector3> {
        constructor() {
            super();
            this.currentValue = new THREE.Vector3();
        }
        computeCurrentValue(t: number): THREE.Vector3 {
            return THREEEXT.Vector3.lerpVectors(this.startValue, this.endValue, t);
        }
    }

    export class ValueSmootherQuat extends ValueSmoother<THREE.Quaternion> {
        constructor() {
            super();
            this.currentValue = new THREE.Quaternion();
        }
        computeCurrentValue(t: number): THREE.Quaternion {
            THREE.Quaternion.slerp(this.startValue, this.endValue, this.currentValue, t);
            return this.currentValue;
        }
    }

}