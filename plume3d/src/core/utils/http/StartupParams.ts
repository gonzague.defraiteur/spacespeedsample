/// <reference path="../../utils/collection/HashMap.ts" />

module plume {

    export class StartupParams {

        private static _INSTANCE: StartupParams;

        private _params: HashMap<string> = StartupParams.urlParametersToHashMap(document.location.href);

        static getInstance(): StartupParams {
            if (StartupParams._INSTANCE == null) {
                StartupParams._INSTANCE = new StartupParams();
            }
            return StartupParams._INSTANCE;
        }

        static add(key: string, value: string) {
            StartupParams.getInstance()._params.put(key, value);
        }

        static remove(key: string): string {
            let v = StartupParams.getInstance()._params.remove(key);
            return v;
        }

        static get(key: string, valueIfNull?: string) {
            let value = StartupParams.getInstance()._params.get(key);
            if (value == null || value.length <= 0) return valueIfNull;
            return value;
        }

        static has(key: string): boolean {
            return StartupParams.getInstance()._params.containsKey(key);
        }

        static urlParametersToHashMap(url: string) {
            let map = new HashMap<string>();
            let pairs = url.substring(url.indexOf('?') + 1).split('&');
            for (let i = 0; i < pairs.length; i++) {
                if (!pairs[i])
                    continue;
                let pair = pairs[i].split('=');
                map.put(decodeURIComponent(pair[0]), decodeURIComponent(pair[1]));
            }
            return map;
        }

    }

}
