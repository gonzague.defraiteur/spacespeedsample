module plume.http {

    export function loadJs(url: string, onload: () => void) {
        let script = document.createElement("script");
        script.type = "text/javascript";
        if (script["readyState"]) {
            script["onreadystatechange"] = function () {
                if (script["readyState"] === "loaded"
                    || script["readyState"] === "complete") {
                    script["onreadystatechange"] = null;
                    onload();
                }
            };
        } else {
            script.onload = function () {
                onload();
            };
        }

        script.onerror = function (e) {
            logger.error("Failed to load script " + url + "! " + e);
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    export function loadArrayBuffer(url: string, onload: (data: ArrayBuffer) => void, onerror?: () => void) {
        let request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.addEventListener('load', function (event) {
            let response = this.response;
            if (this.status === 200 || this.status === 0) {
                // ok
                onload(response);
            } else {
                // on error
                if (onerror) onerror();
            }
        }, false);

        request.addEventListener('error', function (event) {
            // on error
            if (onerror) onerror();
        }, false);

        request.responseType = 'arraybuffer';

        request.send(null);
    }

}