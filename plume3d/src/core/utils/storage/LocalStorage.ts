namespace plume {

    export class LocalStorage {

        cacheData: Object;
        available: boolean = false;

        constructor(private _id: string) {
            this.available = this._isAvailable();

            if (this.available) {
                let data = localStorage.getItem(this._id);
                this.cacheData = JSONS.safeJson(data, {});
            } else {
                this.cacheData = {};
            }
        }

        private _isAvailable(): boolean {
            try {
                localStorage.setItem("test123456789", "test123456789");
                localStorage.removeItem("test123456789");
                return true;
            } catch (e) {
                return false;
            }
        }

        put(key: string, value: any) {
            this.cacheData[key] = value;
            if (this.available) {
                localStorage.setItem(this._id, JSON.stringify(this.cacheData));
            }
        }

        get<T>(key: string, defValue?: T): T {
            if (this.available) {
                let v = this.cacheData[key];
                if (v == null) {
                    return defValue;
                } else {
                    return v;
                }
            }
            return defValue;
        }

        remove(key: string) {
            if (this.available) {
                delete this.cacheData[key];

                let keys = Object.keys(this.cacheData);
                if (keys.length == 0) {
                    localStorage.setItem(this._id, "");
                    localStorage.removeItem(this._id);
                } else {
                    localStorage.setItem(this._id, JSON.stringify(this.cacheData));
                }
            }
        }

    }

}