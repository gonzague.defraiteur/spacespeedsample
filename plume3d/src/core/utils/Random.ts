﻿
module plume {
    export class Random {

        private static _seed = Date.now();

        static init(seed: number) {
            this._seed = seed;
            if (this._seed <= 0) this._seed += 2147483646;
        }

        static currentSeed(): number {
            return this._seed;
        }

        static get(useSeed = false): number {
            if (useSeed) return this._random0();
            return Math.random();
        }

        static inRange(min: number, max: number, useSeed = false): number {
            return this.get(useSeed) * (max - min + 1) + min;
        }

        static sign(useSeed = false): number {
            return (this.get(useSeed) < 0.5 ? 1 : -1);
        }

        // Random int beetween min,max inclusive both
        static intInRange(min: number, max: number, useSeed = false): number {
            return Math.floor(this.get(useSeed) * (max - min + 1)) + min;
        }

        // https://gist.github.com/blixt/f17b47c62508be59987b
        private static _getSeededInt(): number {
            return this._seed = this._seed * 16807 % 2147483647;
        }

        private static _random0(): number {
            return (this._getSeededInt() - 1) / 2147483646;
        }
    }
}
