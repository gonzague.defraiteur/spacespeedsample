﻿

module plume {

    export class JSONS {

        // deep merge source in dest
        static merge(source: {}, dest: {}) {
            for (let key in source) {
                let sValue = source[key];
                let typ: string = (typeof sValue);
                if (typ === "constructor" || typ == "function") {
                    continue;
                }

                if ((key in dest) && (typeof sValue === "object")) {
                    JSONS.merge(sValue, dest[key]);
                } else {
                    dest[key] = sValue;
                }
            }
        }

        static safeJson(data: string, defaultValue: any = null): any {
            if (data == null || data.length == 0) return defaultValue;
            try {
                let jso = JSON.parse(data);
                return jso;
            } catch (e) {
                logger.error("Failed to parse json data=" + data, e);
            }
            return defaultValue;
        }

    }

}