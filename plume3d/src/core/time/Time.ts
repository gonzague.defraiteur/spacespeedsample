﻿
module plume.Time {

    let _now: number = performance.now();
    let _ellapsed: number = performance.now();

    export function _refresh(time: number) {
        _ellapsed = time - _now;
        _now = time;
    }

    export function now() {
        return _now;
    }

    export function ellapsed() {
        return _ellapsed;
    }
}

