﻿
module plume {

    export class Timer {

        id: string = "DefaultId";
        _delay: number;
        _callback: Function;
        _thisArg: any;
        _argArray: any[];

        _timeManager: TimeManager;
        _tick: number;
        _repeatCount: number = 1;
        _running: boolean = false;
        _expired: boolean = false;
        _startTime: number;

        constructor(timeManager: TimeManager, delay: number, callback?: Function, thisArg?: any, argArray?: any[]) {
            this._timeManager = timeManager;
            this._delay = delay;
            this._callback = callback;
            this._thisArg = thisArg;
            this._argArray = argArray;
        }

        // -1 == infinite, default == 1
        setRepeat(repeat: number) {
            this._repeatCount = repeat;
        }

        update(time: number): boolean {
            if (!this._running) return true;

            let timeLeft = this._tick - time;
            if (timeLeft <= 0) {
                // Execute callback
                if (this._callback != null) {
                    this._callback.call(this._thisArg, this._argArray);
                }
                this._repeatCount--;

                if (this._repeatCount == 0) {
                    // Terminated
                    this._running = false;
                    this._expired = true;
                    return true;
                } else {
                    if (this._repeatCount < -1) this._repeatCount = -1;
                    // Update next tick
                    this._tick = time + this._delay;
                    this._startTime = time;
                    return false;
                }
            }

            return false;
        }

        start() {
            if (this._running) return;

            this._running = true;
            this._tick = Time.now() + this._delay;
            this._startTime = Time.now();
        }

        cancel() {
            this._running = false;
            this._timeManager.remove(this);
        }

        hasExpired(): boolean {
            return this._expired;
        }

        getProgress() {
            if (this._expired) return 1;
            if (!this._running) return 0;

            let now = Time.now();
            let elapsed = now - this._startTime;
            return elapsed / this._delay;
        }

        getDuration() {
            return this._delay;
        }

    }

}

