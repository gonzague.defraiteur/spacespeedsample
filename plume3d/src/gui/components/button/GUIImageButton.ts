﻿/// <reference path="GUIButton.ts" />

module plume {

    export class ImageButtonStyle {

        _stateUp: DisplayObject;
        _stateDown: DisplayObject;
        _stateOver: DisplayObject;

        constructor(stateUp: DisplayObject, stateDown?: DisplayObject, stateOver?: DisplayObject) {
            this._stateUp = stateUp;
            this._stateDown = stateDown;
            this._stateOver = stateOver;
        }

        static fromFrames(stateUp: string, stateDown?: string, stateOver?: string) {
            let up = Sprite.fromFrame(stateUp);
            let down = null;
            let over = null;

            if (stateDown != null) down = Sprite.fromFrame(stateDown);
            if (stateOver != null) over = Sprite.fromFrame(stateOver);

            let style = new ImageButtonStyle(up, down, over);
            return style;
        }

        static fromTextures(stateUp: Texture, stateDown?: Texture, stateOver?: Texture) {
            let up = Sprite.fromTexture(stateUp);
            let down = null;
            let over = null;

            if (stateDown != null) down = Sprite.fromTexture(stateDown);
            if (stateOver != null) over = Sprite.fromTexture(stateOver);

            let style = new ImageButtonStyle(up, down, over);
            return style;
        }
    }

    export class ImageButton extends Button {

        private _style: ImageButtonStyle;
        private _text: BitmapText = null;
        private _icon: Sprite = null;

        constructor(style: ImageButtonStyle) {
            super();
            this._style = style;
            this.addChild(this._style._stateDown);
            this.addChild(this._style._stateOver);
            this.addChild(this._style._stateUp);

            this.setSize(this._style._stateUp.width, this._style._stateUp.height);
        }

        static fromFramesArray(states: string[]) {
            let length = states.length;
            if (length < 1 || length > 3) {
                throw new Error("Invalid states array size");
            }

            let up = states[0];
            let down = null;
            let over = null;

            if (length > 1) down = states[1];
            if (length > 2) over = states[2];

            return ImageButton.fromFrames(up, down, over);
        }


        static fromFrames(stateUp: string, stateDown?: string, stateOver?: string) {
            let style = ImageButtonStyle.fromFrames(stateUp, stateDown, stateOver);
            return new ImageButton(style);
        }

        static fromTextures(stateUp: Texture, stateDown?: Texture, stateOver?: Texture) {
            let style = ImageButtonStyle.fromTextures(stateUp, stateDown, stateOver);
            return new ImageButton(style);
        }


        setFrames(stateUp: string, stateDown?: string, stateOver?: string) {
            if (this._style._stateDown) this._style._stateDown.removeSelf();
            if (this._style._stateOver) this._style._stateOver.removeSelf();
            if (this._style._stateUp) this._style._stateUp.removeSelf();

            this._style = ImageButtonStyle.fromFrames(stateUp, stateDown, stateOver);
            this.addChildAt(this._style._stateUp, 0);
            if (this._style._stateDown) this.addChildAt(this._style._stateDown, 0);
            if (this._style._stateOver) this.addChildAt(this._style._stateOver, 0);
        }

        setFramesArray(states: string[]) {
            let stateUp = states[0];
            let stateDown = null;
            let stateOver = null;
            if (states.length > 1) {
                stateDown = states[1];
            }
            if (states.length > 2) {
                stateOver = states[2];
            }
            this.setFrames(stateUp, stateDown, stateOver);


        }

        setContent(content: Container) {
            this.addChild(content);

            layout.centerOnX(content, this, false);
            layout.centerOnY(content, this, false);
        }

        setText(text: BitmapText) {
            if (this._text != null) this._text.removeSelf();
            this._text = text;

            // add text to group
            this.addChild(text);

            // center text
            layout.centerOnX(text, this, false);
            layout.centerOnY(text, this, false);
        }

        setIcon(frame: string, scale?: number) {
            if (this._icon != null) this._icon.removeSelf();

            this._icon = Sprite.fromFrame(frame);
            if (scale != null) this._icon.scaleXY = scale;

            layout.centerOnX(this._icon, this, false);
            layout.centerOnY(this._icon, this, false);

            this.addChild(this._icon);
        }

        protected setStateTexture() {

            if (this._style._stateDown) this._style._stateDown.visible = false;
            if (this._style._stateOver) this._style._stateDown.visible = false;
            this._style._stateUp.visible = false;

            if (this._down) {
                if (this._style._stateDown) {
                    this._style._stateDown.visible = true;
                }
                return;
            }

            if (this._over) {
                if (this._style._stateOver) {
                    this._style._stateOver.visible = true;
                }
                return;
            }

            if (this._style._stateUp) {
                this._style._stateUp.visible = true;
            }
        }

        onStateDown() {
            super.onStateDown();
            this.setStateTexture();
        }

        onStateUp() {
            super.onStateUp();
            this.setStateTexture();
        }

        onStateOver() {
            super.onStateOver();
            this.setStateTexture();
        }

        onStateOut() {
            super.onStateOut();
            this.setStateTexture();
        }

        get style(): ImageButtonStyle {
            return this._style;
        }

    }

}