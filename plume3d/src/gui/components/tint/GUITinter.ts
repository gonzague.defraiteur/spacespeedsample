module plume {

    export class Tinter {

        static rounding = 8;

        private static _multiplyDetected: boolean = null;

        private static _isMultiplySupported() {
            if (this._multiplyDetected === null) { //we haven't checked yet
                let canvas = document.createElement("canvas");
                let context = canvas.getContext("2d");
                canvas.width = 1;
                canvas.height = 1;
                context.clearRect(0, 0, 1, 1);
                context.globalCompositeOperation = 'source-over';
                context.fillStyle = "#000"; //black
                context.fillRect(0, 0, 1, 1);
                context.globalCompositeOperation = 'multiply';
                context.fillStyle = "#fff"; //white
                context.fillRect(0, 0, 1, 1);

                this._multiplyDetected = context.getImageData(0, 0, 1, 1).data[0] === 0;

                if (!this._multiplyDetected) {
                    logger.warn("Tinter : globalCompositeOperation multiply not supported, fallback to manual multiply")
                }
            }
            return this._multiplyDetected;
        }

        static tint(texture: Texture, color: number, useCache: boolean = true): Texture {
            let r = (color >> 16) & 0x0000FF;
            let g = (color >> 8) & 0x0000FF;
            let b = color & 0x0000FF;
            r = Math.min(255, Math.round(r / Tinter.rounding) * Tinter.rounding);
            g = Math.min(255, Math.round(g / Tinter.rounding) * Tinter.rounding);
            b = Math.min(255, Math.round(b / Tinter.rounding) * Tinter.rounding);

            let tintedTextureName = texture.name + "-tint-" + r + "," + g + "," + b;
            if (useCache) {
                let found = Texture.containsTexture(tintedTextureName);
                if (found != null) {
                    return found;
                }
            }

            let canvas = document.createElement("canvas");
            let context = canvas.getContext("2d");
            canvas.width = texture.width;
            canvas.height = texture.height;

            context.fillStyle = 'rgba(' + r + ',' + g + ',' + b + ',' + 1 + ')';
            context.fillRect(0, 0, texture.width, texture.height);

            //TB check multiply supported (ie does not support it)
            //https://caniuse.com/#feat=canvas-blending
            if (this._isMultiplySupported()) {
                context.globalCompositeOperation = 'multiply';
                context.drawImage(
                    texture.getImpl(),
                    texture.x,
                    texture.y,
                    texture.width,
                    texture.height,
                    0,
                    0,
                    texture.width,
                    texture.height
                );
            } else {
                //multiply not supported (internet explorer)
                //fallback to manual impl

                //1 - draw image
                context.drawImage(
                    texture.getImpl(),
                    texture.x,
                    texture.y,
                    texture.width,
                    texture.height,
                    0,
                    0,
                    texture.width,
                    texture.height
                );

                //2  - manual multiply
                let imgData = context.getImageData(0, 0, canvas.width, canvas.height);
                let data = imgData.data;
                for (let i = 0; i < data.length; i += 4) {
                    data[i] = r * data[i] / 255;
                    data[i + 1] = g * data[i + 1] / 255;
                    data[i + 2] = b * data[i + 2] / 255;
                }
                context.putImageData(imgData, 0, 0);
            }

            context.globalCompositeOperation = 'destination-atop';
            context.drawImage(
                texture.getImpl(),
                texture.x,
                texture.y,
                texture.width,
                texture.height,
                0,
                0,
                texture.width,
                texture.height
            );

            let tintedTexture = new Texture(tintedTextureName, 0, 0, texture.width, texture.height);
            tintedTexture.setImpl(canvas);
            if (useCache) {
                Texture.addTexture(tintedTexture);
            }
            return tintedTexture;
        }


    }


}