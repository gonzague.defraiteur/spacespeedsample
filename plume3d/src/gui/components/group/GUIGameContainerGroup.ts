/// <reference path="GUIContainerGroup.ts" />
module plume {

    // Basic layer for a game
    export class GameContainerGroup extends ContainerGroup {

        private _background: Container; // Background if any
        private _scene: Container; // Curent scene / gameplay
        private _gameplayHUD: Container; // Over the gameplay, does not move with camera. Use to draw virtual keys, buttons, etc.
        private _popup: Container; // Used by popup. Over current scene / gameplay
        private _top: Container; // Over the game. Use to draw custom black borders, etc.
        private _technical: Container; // Display over everything. Use for fps counter, etc.

        get background(): Container {
            if (this._background == null) {
                this._background = this.addChild("background", 1);
            }
            return this._background;
        }

        set background(container: Container) {
            throw new Error("background is read-only");
        }

        get scene(): Container {
            if (this._scene == null) {
                this._scene = this.addChild("scene", 100);
            }
            return this._scene;
        }

        set scene(container: Container) {
            throw new Error("scene is read-only");
        }




        get gameplayHUD(): Container {
            if (this._gameplayHUD == null) {
                this._gameplayHUD = this.addChild("gameplayHUD", 150);
            }
            return this._gameplayHUD;
        }

        set gameplayHUD(container: Container) {
            throw new Error("gameplayHUD is read-only");
        }

        get sceneHUD(): Container {
            if (this._gameplayHUD == null) {
                this._gameplayHUD = this.addChild("sceneHUD", 180);
            }
            return this._gameplayHUD;
        }

        set sceneHUD(container: Container) {
            throw new Error("sceneHUD is read-only");
        }

        get popup(): Container {
            if (this._popup == null) {
                this._popup = this.addChild("popup", 200);
            }
            return this._popup;
        }

        set popup(container: Container) {
            throw new Error("popup is read-only");
        }

        get top(): Container {
            if (this._top == null) {
                this._top = this.addChild("top", 300);
            }
            return this._top;
        }

        set top(container: Container) {
            throw new Error("top is read-only");
        }

        get technical(): Container {
            if (this._technical == null) {
                this._technical = this.addChild("technical", 400);
            }
            return this._technical;
        }

        set technical(container: Container) {
            throw new Error("technical is read-only");
        }
    }
}
