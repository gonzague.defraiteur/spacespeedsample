module plume.layout {

    export function centerOnX(source: HasWidth & HasX, container: HasWidth & HasX, absolute: boolean = true) {
        let dx = Math.round((container.width - source.width) / 2);
        source.x = absolute ? container.x + dx : dx;
    }

    export function centerOnY(source: HasHeight & HasY, container: HasHeight & HasY, absolute: boolean = true) {
        let dy = Math.round((container.height - source.height) / 2);
        source.y = absolute ? container.y + dy : dy;
    }

    export function centerOnXInParent(obj: DisplayObject) {
        centerOnX(obj, obj.parent, false);
    }
    export function centerOnYInParent(obj: DisplayObject) {
        centerOnY(obj, obj.parent, false);
    }
    export function centerOnXYInParent(obj: DisplayObject) {
        centerOnX(obj, obj.parent, false);
        centerOnY(obj, obj.parent, false);
    }

    export function computeBounds(container: Container, recursive: boolean = false): HasBounds {
        let left = Number.POSITIVE_INFINITY;
        let top = Number.POSITIVE_INFINITY;
        let bottom = Number.NEGATIVE_INFINITY;
        let right = Number.NEGATIVE_INFINITY;

        for (let i = 0; i < container.getChildrenCount(); i++) {
            let child = container.getChildAt(i);
            let bounds: HasBounds;
            if (recursive && child instanceof Container) {
                bounds = computeBounds(child, true);
            } else {
                bounds = child;
            }

            left = Math.min(left, bounds.x);
            top = Math.min(top, bounds.y);
            if (bounds.width > 0) right = Math.max(right, bounds.x + bounds.width);
            if (bounds.height > 0) bottom = Math.max(bottom, bounds.y + bounds.height);
        }

        return {
            x: left,
            y: top,
            width: (right - left),
            height: (bottom - top)
        }
    }

    export function setContainerSize(container: Container, recursive: boolean = false) {
        let bounds = computeBounds(container, recursive);
        container.setSize(bounds.width, bounds.height);
    }
}