// module plume {
//     export class ExpandedBounds {

//         private static INSTANCE: ExpandedBounds;
//         static getInstance(): ExpandedBounds {
//             return this.INSTANCE;
//         }
//         static get(): Rectangle {
//             return this.getInstance().bounds;
//         }

//         private game: Game;
//         private sm: ScaleManager;
//         public bounds = new h5ge.Rectangle(0, 0, 0, 0);

//         constructor(public maxWidth: number = null, public maxHeight: number = null) {
//             ExpandedBounds.INSTANCE = this;

//             this.game = Game.get();
//             this.sm = this.game.scaleManager;
//             this.sm.resizeHandler.add(this.onResize, this);

//             this.onResize();
//         }

//         private onResize() {

//             let scaling = this.sm.getCurrentScaling();
//             let marginX = scaling.x / scaling.scale;
//             let marginY = scaling.y / scaling.scale;

//             if (this.maxWidth != null) {
//                 let totalWidth = this.game.width + 2 * marginX;
//                 if (totalWidth > this.maxWidth) {
//                     marginX = marginX - (totalWidth - this.maxWidth) / 2;
//                 }
//             }

//             if (this.maxHeight != null) {
//                 let totalHeight = this.game.height + 2 * marginY;
//                 if (totalHeight > this.maxHeight) {
//                     marginY = marginY - (totalHeight - this.maxHeight) / 2;
//                 }
//             }

//             this.bounds = new h5ge.Rectangle(-marginX, -marginY, this.game.width + 2 * marginX, this.game.height + 2 * marginY);
//         }
//     }
// }