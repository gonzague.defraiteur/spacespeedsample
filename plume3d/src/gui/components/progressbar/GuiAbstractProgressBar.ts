module plume {

    export class AbstractProgressBar {

        onProgressExpired: () => void;

        protected _lastProgress = -1;

        // time limited
        private _startTime = 0;
        private _duration = 0;
        private _expired = false;
        private _started = false;

        constructor(protected _background: DisplayObject, protected _progress = 0) {
            this._autoUpdate();
        }

        startTimeLimited(duration: number, ellapsed = 0) {
            this._progress = ellapsed / duration;
            this._duration = duration;
            this._startTime = performance.now() - ellapsed;
            this._started = true;
        }

        // manual progress update (between 0 and 1)
        setProgress(progress: number) {

            if (progress < 0) progress = 0;
            else if (progress > 1) progress = 1;

            this._progress = progress;
        }

        // automatically called
        update(dt: number) {
            if (this._expired) return;
            if (!this._started) return;

            if (this._startTime == 0) return;

            let now = performance.now();
            this._progress = (now - this._startTime) / this._duration;
            this._progress = Math.min(this._progress, 1);

            if (this._progress == 1) {
                this._onExpired();
            }
        }

        // automatically called
        render(interpolation: number) {
        }

        getProgress() {
            return this._progress;
        }

        private _onExpired() {
            this._expired = true;
            this._started = false;

            if (this.onProgressExpired != null) {
                this.onProgressExpired();
            }
        }

        private _autoUpdate() {
            let self = this;
            let game = Game.get();
            game.addRenderable(this);
            game.addUpdatable(this);

            this._background.detachListener.once(function () {
                game.removeRenderable(self);
                game.removeUpdatable(self);
            });
        }

    }

}
