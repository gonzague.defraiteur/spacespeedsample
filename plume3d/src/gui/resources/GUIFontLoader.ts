module plume {

    export interface FontSpec {
        face: string;
        size: number;
        lineHeight: number;
        chars: Array<Letter>;
    }

    export class FontLoader {

        static loadAll(fonts: Array<FontSpec>) {
            for (let i = 0; i < fonts.length; i++) {
                FontLoader.load(fonts[i]);
            }
        }

        static load(font: FontSpec) {

            let sprite = Sprite.fromFrame(font.face + ".png");
            let chars = new Array<number>();
            let letters: Array<Letter> = new Array(65000);
            let textureByCode: Array<Texture> = new Array(65000);
            let f: Font = {
                size: font.size,
                lettersByCode: letters,
                lineHeight: font.lineHeight,
                textureByCode: textureByCode,
                chars: chars,
                resource: sprite.texture.getImpl() as HTMLImageElement,
            }

            let frameX = sprite.texture.x;
            let frameY = sprite.texture.y;
            if (sprite.texture.trimmed) {
                frameX -= sprite.texture.getTrim().x;
                frameY -= sprite.texture.getTrim().y;
            }

            for (let i = 0; i < font.chars.length; i++) {
                let char = font.chars[i];
                let letter = {
                    id: char.id,
                    x: char.x + frameX,
                    y: char.y + frameY,
                    xoffset: char.xoffset,
                    yoffset: char.yoffset,
                    width: char.width,
                    height: char.height,
                    xadvance: char.xadvance
                };

                let texture = new Texture(font.face + "/" + letter.id, letter.x, letter.y, letter.width, letter.height);
                texture.setImpl(sprite.texture.getImpl());
                //texture.setTrim();

                letters[char.id] = letter;
                textureByCode[char.id] = texture;
                chars.push(char.id);

                // if ("a".charCodeAt(0) == char.id || "p".charCodeAt(0) == char.id) {
                //     let aa = letters[letters.length - 1];
                //     console.log(char.id + ", It is 'a' => " + JSON.stringify(aa));
                // }
            }

            Resources.addLoadedFont(font.face, f);
        }

    }

}