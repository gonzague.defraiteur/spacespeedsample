module plume {

    export type CanvasGuiOptions = {
        canvasId?: string,
    }

    export class CanvasGui implements plume.Gui {

        private static _inst: CanvasGui;

        canvas: HTMLCanvasElement;

        scene: GuiScene;
        layers: GameContainerGroup;
        scaling: GuiScaling;
        expandedBounds: Rectangle;

        private _currentOrPendingScene: GuiScene;
        private _context2D: CanvasRenderingContext2D;

        private _mouseInterpretor: GuiMouseEventInterpretor;

        constructor(public options: CanvasGuiOptions) {
            CanvasGui._inst = this;

            this.expandedBounds = new Rectangle(0, 0, Game.get().width, Game.get().height);
            this.layers = new GameContainerGroup();
            this._initCanvas();

            this.scaling = new GuiScaling();
            this.scaling.init();

            this._mouseInterpretor = new GuiMouseEventInterpretor();
            Game.get().inputManager.mouseEventInterpretors.push(this._mouseInterpretor);
        }

        static get(): CanvasGui {
            return this._inst;
        }

        update(dt: number): void {
            this.layers.update();

            if (this.scene) {
                this.scene.update(dt);
            }
        }

        render(dt: number, interpolation: number): void {

            if (this.scene) {
                this.scene.render(dt, interpolation);
            }

            this._beginDraw();
            this.layers.root.draw(this._context2D);
            this._endDraw();
        }

        switchScene(newScene: GuiScene, animateOutCurrentScene: boolean = true, animateInNewScene: boolean = true) {
            if (this.scene) {
                this._currentOrPendingScene = newScene;
                this.scene.hide(() => {
                    this.layers.scene.removeChild(this.scene.container);
                    this._pushScene(newScene, animateInNewScene);
                }, animateOutCurrentScene);
            } else {
                this._currentOrPendingScene = newScene;
                this._pushScene(newScene, animateInNewScene);
            }
        }

        private _pushScene(newScene: GuiScene, animateIn: boolean) {
            logger.debug("Change scene");
            this.scene = newScene;
            if (this.scene) {
                this.layers.scene.addChild(this.scene.container);
                this.scene.show(animateIn);
            }
        }

        // get current scene (or scene we are switching to)
        getCurrentOrPendingScene(): GuiScene {
            return this._currentOrPendingScene;
        }

        //
        // Canvas draw
        //
        private _initCanvas() {
            if (this.options.canvasId != null) {
                let canvasId = this.options.canvasId;
                this.canvas = document.getElementById(canvasId) as HTMLCanvasElement;
                if (this.canvas == null) {
                    throw new Error("Unable to find canvas: " + canvasId);
                }
            } else {
                this.canvas = document.createElement("canvas");
                this.canvas.style.position = "absolute";
                document.body.appendChild(this.canvas);
            }

            let bounds = Game.get().bounds;
            this.canvas.width = bounds.width;
            this.canvas.height = bounds.height;
            this._context2D = this.canvas.getContext("2d");

            Renderer._applyStyle(this.canvas);

            this.canvas.style.pointerEvents = "none"; // gui canvas do not catch mouse event, forward to gl canvas
        }

        private _beginDraw() {
            this._context2D.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }

        private _endDraw() {
        }

        get context(): CanvasRenderingContext2D {
            return this._context2D;
        }
    }

}