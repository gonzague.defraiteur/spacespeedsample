﻿/// <reference path="GUIDisplayObject.ts" />

module plume {

    export class Container extends DisplayObject {

        private _children: Array<DisplayObject> = [];


        constructor(width: number = 0, height: number = 0) {
            super();
            this._debugId = "Container-" + this._id;

            this._width = width;
            this._height = height;
        }

        addChild(child: DisplayObject): DisplayObject {
            let object = this.addChildAt(child, this._children.length);
            return object;
        }

        addChildAt(child: DisplayObject, index: number): DisplayObject {
            // virtual update
            if (child._parent) {
                if (child._parent == this) {
                    console.error("Child already in H5Container. Use setChildIndex.");
                    return child;
                }
                child._parent.removeChild(child);
            }
            this._children.splice(index, 0, child);
            child._parent = this;
            return child;
        }

        removeChild(child: DisplayObject): DisplayObject | null {
            let index = this._children.indexOf(child);
            if (index === -1) return null;
            return this.removeChildAt(index);
        }

        removeChildAt(index: number): DisplayObject {
            // virtual update
            let child = this.getChildAt(index);
            child._parent = null;
            child.onDetach();
            this._children.splice(index, 1);
            return child;
        }

        removeChildren(beginIndex?: number, endIndex?: number): Array<DisplayObject> {
            // virtual update
            let start = (beginIndex == null ? 0 : beginIndex);
            let end = (endIndex == null ? this._children.length : endIndex);
            let range = end - start;
            if (range <= 0) {
                return [];
            }

            let removed = this._children.splice(start, range);
            for (let i = 0; i < removed.length; i++) {
                let child = removed[i];
                child._parent = null;
                child.onDetach();
            }
            return removed;
        }

        getChildIndex(child: DisplayObject): number {
            let index = this._children.indexOf(child);
            return index;
        }

        getChildById(id: string): DisplayObject {
            for (let i = 0; i < this._children.length; i++) {
                let c = this._children[i];
                if (c.getDebugId() == id) {
                    return c;
                }
            }
            return null;
        }

        setChildIndex(child: DisplayObject, index: number) {
            let currentIndex = this.getChildIndex(child);
            if (currentIndex == index) {
                console.error("Child " + child.getDebugId() + "already @ that index : " + index);
                return;
            }
            this._children.splice(currentIndex, 1); // Remove from old position
            this._children.splice(index, 0, child); // Add at new position
        }

        // onDetach called also on all children
        onDetach() {
            super.onDetach();

            let count = this._children.length;
            for (let i = 0; i < count; i++) {
                this._children[i].onDetach();
            }
        }

        getChildAt(index: number): DisplayObject {
            let child = this._children[index];
            return child;
        }

        getChildrenCount() {
            return this._children.length;
        }

        _outputToConsole(level: number, properties: Array<string> = []) {
            let spaces = '';
            for (let i = 0; i < level; i++) spaces += '  ';

            for (let i = 0; i < this._children.length; i++) {
                let child = this._children[i];
                console.log(spaces + this._getDebugPropertyString(child, properties));
                if (child instanceof Container) {
                    child._outputToConsole(level++, properties);
                }
            }
        }

        _countElements(visible: boolean = false): number {
            if (visible && !this.visible) return 0;

            let sum = 1;
            for (let i = 0; i < this._children.length; i++) {
                let child = this._children[i];
                if (child instanceof Container) {
                    sum = sum + child._countElements();
                } else {
                    sum = sum + 1;
                }
            }
            return sum;
        }

        private _getDebugPropertyString(o: DisplayObject, properties: Array<string>): string {
            let str = o.getDebugId() + " ";
            properties.forEach(function (p) {
                str += p + "=" + o[p] + " ";
            });
            return str;
        }

        protected draw0(context: CanvasRenderingContext2D) {
            super.draw0(context);

            for (let i = 0; i < this._children.length; i++) {
                let child = this._children[i];
                child.draw(context);
            }
        }

        setSize(w: number, h: number) {
            this._width = w;
            this._height = h;
        }

        setSizeFromChildren(recursive: boolean = false) {
            let bounds = layout.computeBounds(this, recursive);
            this.setSize(bounds.width, bounds.height);
        }

    }
}
