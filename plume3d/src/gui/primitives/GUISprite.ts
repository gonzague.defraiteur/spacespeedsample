/// <reference path="GUIDisplayObject.ts" />

module plume {

    export class Sprite extends DisplayObject {

        protected _texture: Texture;

        constructor(texture: Texture) {
            super();

            this._texture = texture;
            this._debugId = "Sprite-" + this._id;
            this.setSizeFromTexture();

            if (!this.texture.isLoaded()) {
                // Texture not yet loaded
                let self = this;
                this.texture.addLoadedHandler(function () {
                    self.setSizeFromTexture();
                });
            }
        }

        public setSizeFromTexture() {
            if (this.texture.trimmed) {
                // Get original sprite size
                let t = this.texture.getTrim();
                this._width = t.width;
                this._height = t.height;
            } else {
                this._width = this.texture.width;
                this._height = this.texture.height;
            }
        }

        static fromFrame(frameId: string): Sprite {
            let sprite: Sprite = new Sprite(Texture.getTexture(frameId));
            return sprite;
        }

        static fromImage(imageId: string): Sprite {
            let texture = Texture.fromImage(imageId);
            let sprite = new Sprite(texture);
            return sprite;
        }

        static fromTexture(texture: Texture): Sprite {
            return new Sprite(texture);
        }

        setTexture(texture: Texture) {
            this._texture = texture;
        }

        setFrame(frameId: string) {
            this._texture = Texture.getTexture(frameId);
        }

        addLoadedHandler(cb: () => any) {
            this.texture.addLoadedHandler(cb);
        }

        // Accessor

        get texture(): Texture {
            return this._texture;
        }

        // 40 fps with 1500 bunnies
        // 38 fps with 100 bunnies (nexus4)
        protected draw0(context: CanvasRenderingContext2D) {
            let texture = (this._tint == 0xFFFFFF ? this._texture : Tinter.tint(this._texture, this._tint));

            let dx = 0;
            let dy = 0;
            if (this.texture.trimmed) {
                dx = this.texture.getTrim().x;
                dy = this.texture.getTrim().y;
            }

            context.drawImage(texture.getImpl(), texture.x, texture.y, texture.width, texture.height, dx, dy, texture.width, texture.height);
        }

    }
}
