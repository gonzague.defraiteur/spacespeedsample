/// <reference path="GUISprite.ts" />
module plume {

    type LineDef = {
        letters: Array<Letter>,
        xposition: Array<number>,
        width: number
    }

    export class BitmapText extends Container {

        private _missingChar = false;
        private _text: string;
        private _font: Font;
        private _fontName: string;

        private _size: number;
        private _ratio = 1;

        private _align: string = "left";
        private _maxWidth: number = 10000;

        private _dirty: boolean = true;

        constructor(text: string, fontName: string, size?: number) {
            super();

            let font: Font = Resources.fonts[fontName];
            if (font == null) {
                throw new Error("Font not loaded: " + fontName);
            }

            this._fontName = fontName;
            this._font = font;
            this._text = text;
            this._size = size == null ? font.size : size;
            this._ratio = this._size / font.size;

            this._debugId = "Text-" + this._id;
        }

        // Accessor

        set text(text: string) {
            if (text == this._text) return;

            this._text = text;
            this._dirty = true;
        }
        get text(): string {
            return this._text;
        }

        set align(align: string) {
            this._align = align;
            this._dirty = true;
        }
        get align(): string {
            return this._align;
        }

        set maxWidth(maxWidth: number) {
            this._maxWidth = maxWidth;
            this._dirty = true;
        }
        get maxWidth(): number {
            return this._maxWidth;
        }

        get width(): number {
            if (this._dirty) this._create();
            return this._width * this._scaleX;
        }
        get height(): number {
            if (this._dirty) this._create();
            return this._height * this._scaleY;
        }

        set tint(tint: number) {
            if (tint == null) this._tint = 0xFFFFFF;
            else this._tint = tint;

            this._dirty = true;
        }

        hasMissingChar(): boolean {
            if (this._dirty) this._create();
            return this._missingChar;
        }

        draw0(context: CanvasRenderingContext2D) {
            if (this._dirty) {
                this._create();
                this._dirty = false;
            }

            super.draw0(context);
        }

        // Take in account: maxWidth, line break, alignment
        private _create() {
            this.removeChildren();

            if (this.text == null || this.text == "") {
                this.setSize(0, 0);
            }

            this._missingChar = false;

            let lines: Array<LineDef> = []; // Letters by line

            let xPos = 0;
            let currentLine: LineDef = { width: 0, letters: [], xposition: [] };
            lines.push(currentLine);

            let lastSpaceLetterAdded = -1;
            let lastSpaceLineWidth = -1;
            let maxHeight = 0;

            for (let i = 0; i < this._text.length; i++) {
                let c = this._text.charCodeAt(i);

                if (c == 10) {
                    // line break
                    currentLine = { width: 0, letters: [], xposition: [] };
                    lines.push(currentLine);
                    xPos = 0;
                    lastSpaceLetterAdded = -1;
                    lastSpaceLineWidth = -1;
                    continue;
                }

                let letter = this._font.lettersByCode[c];
                if (letter == null) {
                    // No mapping for char, ignore
                    this._missingChar = true;
                    continue;
                }

                if (c == 32) {
                    // space, reset flags
                    lastSpaceLetterAdded = 0;
                    lastSpaceLineWidth = xPos;
                }

                if (lastSpaceLetterAdded > -1) {
                    lastSpaceLetterAdded++;
                }
                currentLine.letters.push(letter);
                currentLine.xposition.push(xPos);
                xPos += this._getLetterWidthToDraw(letter);
                currentLine.width = xPos;
                let gliphHeight = letter.yoffset + letter.height;
                if (gliphHeight > maxHeight) {
                    maxHeight = gliphHeight;
                }

                if (this._maxWidth > 0 && xPos > this._maxWidth) {
                    // We go over maxWidth, need to go back from x letter, break line and continue
                    if (lastSpaceLetterAdded > 0) {
                        // Remove letter to add to new line
                        let from = currentLine.letters.length - lastSpaceLetterAdded;
                        currentLine.letters.splice(from, lastSpaceLetterAdded);
                        currentLine.xposition.splice(from, lastSpaceLetterAdded);
                        // Compute new line width
                        let newLineWidth = currentLine.xposition[from - 1] + this._getLetterWidthToDraw(currentLine.letters[from - 1]);
                        currentLine.width = newLineWidth;
                        // Reset index
                        i = i - lastSpaceLetterAdded + 1; // (+1 to ignore space replace by line break)
                        // Add new line
                        currentLine = { width: 0, letters: [], xposition: [] };
                        lines.push(currentLine);
                        xPos = 0;
                        lastSpaceLetterAdded = -1;
                        lastSpaceLineWidth = -1;
                    }
                }
            }

            let maxWidth = 0;
            for (let i = 0; i < lines.length; i++) {
                let line = lines[i];
                maxWidth = Math.max(line.width, maxWidth);
            }

            let width = Math.ceil(maxWidth);
            let lineHeight = (lines.length <= 1 ? this._normalizeSize(maxHeight) : this._normalizeSize(this._font.lineHeight)); // need to investigate good way to find line height
            let height = Math.ceil(lines.length * lineHeight);
            this.setSize(width, height);

            let yPos = 0;
            for (let i = 0; i < lines.length; i++) {
                let line = lines[i];
                let lineOffset = 0;
                if (this._align == "center") {
                    lineOffset = (maxWidth - line.width) / 2;
                } else if (this._align == "right") {
                    lineOffset = (maxWidth - line.width);
                }

                for (let c = 0; c < line.letters.length; c++) {
                    let letter = line.letters[c];
                    let x = line.xposition[c];
                    x += lineOffset;

                    if (letter.width > 0 && letter.height > 0) {
                        let texture = this._font.textureByCode[letter.id];
                        if (this._tint != 0xFFFFFF) {
                            let tinted = Tinter.tint(texture, this._tint, true);
                            texture = tinted;
                        }

                        let sprite = new Sprite(texture);
                        sprite.scaleXY = this._ratio;
                        sprite.x = x + letter.xoffset * this._ratio;
                        sprite.y = yPos + letter.yoffset * this._ratio;
                        this.addChild(sprite);
                    }
                }

                yPos += lineHeight;
            }

        }

        private _normalizeSize(s: number) {
            return s * this._ratio;
        }

        private _getLetterWidthToDraw(letter: Letter) {
            if (letter.id == 32) {
                return letter.xadvance * this._ratio;
            }
            return (letter.xoffset + letter.width) * this._ratio;
        }

    }
}

