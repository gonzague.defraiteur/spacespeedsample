module plume {

    export class Fonts {

        static clearYOffset(fontName: string) {
            let font: Font = Resources.fonts[fontName];
            if (font == null) {
                throw new Error("Font not loaded: " + fontName);
            }

            let offsetmin = Number.POSITIVE_INFINITY;
            for (let c = 0; c < font.chars.length; c++) {
                let letter = font.lettersByCode[font.chars[c]];
                if (letter.height <= 0) continue;
                if (letter.yoffset < offsetmin) offsetmin = letter.yoffset;
            }

            for (let c = 0; c < font.chars.length; c++) {
                let letter = font.lettersByCode[font.chars[c]];
                if (letter.height <= 0) continue;
                letter.yoffset = letter.yoffset - offsetmin;
            }
        }

    }

}