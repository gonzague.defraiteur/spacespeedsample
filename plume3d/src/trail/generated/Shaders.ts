// this file is auto-generated.
module plume.trail.shaders {

	export let base_fragment = 'precision highp float;\nvarying vec4 vColor;\nvoid main() {\ngl_FragColor = vColor;\n}';
	export let base_vertex = 'precision highp float;\nuniform mat4 modelMatrix;\nuniform mat4 modelViewMatrix;\nuniform mat4 projectionMatrix;\nuniform mat4 viewMatrix;\nuniform mat3 normalMatrix;\nuniform vec3 cameraPosition;\nattribute vec3 position;\nattribute vec3 uv;\nattribute float nodeID;\nattribute float nodeVertexID;\nattribute vec3 nodeCenter;\nuniform float minID;\nuniform float maxID;\nuniform vec4 headColor;\nuniform vec4 tailColor;\nuniform float tailSharp;\nvarying vec4 vColor;\nvoid main() {\nfloat fraction = ( maxID - nodeID ) / ( maxID - minID );\nvColor = ( 1.0 - fraction ) * headColor + fraction * tailColor;\nvec4 realPosition;\nif(tailSharp == 1.0) {\nrealPosition = vec4( ( 1.0 - fraction ) * position.xyz + fraction * nodeCenter.xyz, 1.0 );\n} else {\nrealPosition = vec4( position.xyz, 1.0 );\n}\ngl_Position = projectionMatrix * viewMatrix * realPosition;\n}';
	
}
