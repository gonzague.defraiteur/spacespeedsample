module plume {

    export interface TrailMaterialParameters extends THREE.ShaderMaterialParameters {

    }

    export class TrailMaterial extends THREE.RawShaderMaterial {


        constructor(parameters?: TrailMaterialParameters) {
            super();

            this.type = "TrailMaterial";

            this.uniforms.minID = { value: null };
            this.uniforms.maxID = { value: null };
            this.uniforms.tailSharp = { value: null };
            this.uniforms.headColor = { value: new THREE.Vector4() };
            this.uniforms.tailColor = { value: new THREE.Vector4() };

            this.vertexShader = trail.shaders.base_vertex;
            this.fragmentShader = trail.shaders.base_fragment;

            this.transparent = true;
            this.alphaTest = 0.5;
            this.blending = THREE.CustomBlending;
            this.blendSrc = THREE.SrcAlphaFactor;
            this.blendDst = THREE.OneMinusSrcAlphaFactor;
            this.blendEquation = THREE.AddEquation;
            this.depthTest = true;
            this.depthWrite = false;
            this.side = THREE.DoubleSide;

            this.setValues(parameters);
        }

        get minID(): number {
            return this.uniforms.minID.value;
        }
        set minID(v: number) {
            this.uniforms.minID.value = v;
        }
        get maxID(): number {
            return this.uniforms.maxID.value;
        }
        set maxID(v: number) {
            this.uniforms.maxID.value = v;
        }
        get tailSharp(): number {
            return this.uniforms.tailSharp.value;
        }
        set tailSharp(v: number) {
            this.uniforms.tailSharp.value = v;
        }
        get headColor(): THREE.Vector4 {
            return this.uniforms.headColor.value;
        }
        set headColor(v: THREE.Vector4) {
            this.uniforms.headColor.value = v;
        }
        get tailColor(): THREE.Vector4 {
            return this.uniforms.tailColor.value;
        }
        set tailColor(v: THREE.Vector4) {
            this.uniforms.tailColor.value = v;
        }

        // constructor(parameters?: TrailMaterialParameters) {
        //     super();

        //     this.uniforms = {
        //         map: { value: null },
        //         lightMap: { value: null },
        //     }

        //     this.name = "Z2LightmapMaterial";
        //     this.vertexShader = shaders.lightmap_vertex;
        //     this.fragmentShader = shaders.lightmap_fragment;

        //     if (parameters != null) this.setValues(parameters);

        //     this.refreshUniforms();
        // }

        // copy(source: this): this {
        //     super.copy(source);

        //     this._map = source._map;
        //     this._lightMap = source._lightMap;

        //     this.refreshUniforms();

        //     return this;
        // }

        // get map(): THREE.Texture {
        //     return this._map;
        // }
        // set map(v: THREE.Texture) {
        //     this._map = v;
        //     this.refreshUniforms();
        // }

        // get lightMap(): THREE.Texture {
        //     return this._lightMap;
        // }
        // set lightMap(v: THREE.Texture) {
        //     this._lightMap = v;
        //     this.refreshUniforms();
        // }

        // private refreshUniforms() {
        //     this.uniforms.map.value = this._map;
        //     this.uniforms.lightMap.value = this._lightMap;
        // }

    }

}



// /// <reference path="../../build/latest/plume3d.core.d.ts" />

// // from https://github.com/mkkellogg/TrailRendererJS

// //
// // TODO: test, cleanup, types, review api
// //
// module plume {

//     type NodeConnection = {
//         attribute: THREE.BufferAttribute,
//         offset: number,
//         count: number
//     }

//     export class TrailRenderer {

//         private static _PositionComponentCount = 3;
//         private static _IndicesPerFace = 3;
//         private static _FacesPerQuad = 2;

//         private _scene: THREE.Scene;
//         private _active = false;
//         private _geometry: THREE.BufferGeometry = null;
//         private _mesh: THREE.Mesh = null;
//         private _nodeCenters: Array<THREE.Vector3> = null;

//         private _currentNodeCenter: THREE.Vector3 = null;
//         private _nodeIDs: Array<number> = null;
//         private _currentLength = 0;
//         private _currentEnd = 0;
//         private _currentNodeID = 0;


//         //
//         private _verticesPerNode: number;
//         private _faceIndicesPerNode: number;
//         private _facesPerNode: number;
//         private _length: number;
//         private _targetObject: THREE.Object3D;
//         private _localHeadGeometry: Array<THREE.Vector3>;
//         private _vertexCount: number;
//         private _faceCount: number;

//         public material: THREE.ShaderMaterial;

//         private static _materialInst: THREE.ShaderMaterial;

//         constructor() {
//             this._scene = Game.get().engine.scene;
//             this._createBaseMaterial();
//         }

//         private _createBaseMaterial() {
//             return this._CreateMaterial();
//         }

//         public initialize(length: number, tailSharp: boolean, localHeadWidth: number, targetObject: THREE.Object3D) {

//             this.deactivate();
//             this._DestroyMesh();

//             this._length = (length > 0) ? length + 1 : 0;
//             this._targetObject = targetObject;

//             this._InitializeLocalHeadGeometry(localHeadWidth);

//             this._nodeIDs = [];
//             this._nodeCenters = [];

//             for (let i = 0; i < this._length; i++) {
//                 this._nodeIDs[i] = -1;
//                 this._nodeCenters[i] = new THREE.Vector3();
//             }

//             this._InitializeGeometry();
//             this._InitializeMesh();

//             this.material.uniforms.minID.value = 0;
//             this.material.uniforms.maxID.value = 0;
//             this.material.uniforms.tailSharp.value = (tailSharp ? 1 : 0);

//             this._Reset();
//         }

//         private _CreateMaterial() {

//             if (TrailRenderer._materialInst != null) {
//                 this.material = TrailRenderer._materialInst;
//                 return this.material;
//             }

//             // let customUniforms : any = {};
//             // customUniforms.minID = { type: "f", value: null };
//             // customUniforms.maxID = { type: "f", value: null };
//             // customUniforms.tailSharp = { type: "f", value: null };
//             // customUniforms.headColor = { type: "v4", value: new THREE.Vector4() };
//             // customUniforms.tailColor = { type: "v4", value: new THREE.Vector4() };

//             // let vertexShader = trail.shaders.base_vertex;
//             // let fragmentShader = trail.shaders.base_fragment;

//             // this.material = new THREE.RawShaderMaterial(
//             //     {
//             //         uniforms: customUniforms,
//             //         vertexShader: vertexShader,
//             //         fragmentShader: fragmentShader,

//             //         transparent: true,
//             //         alphaTest: 0.5,

//             //         blending: THREE.CustomBlending,
//             //         blendSrc: THREE.SrcAlphaFactor,
//             //         blendDst: THREE.OneMinusSrcAlphaFactor,
//             //         blendEquation: THREE.AddEquation,

//             //         depthTest: true,
//             //         depthWrite: false,

//             //         side: THREE.DoubleSide
//             //     });

//             // TrailRenderer._materialInst = this.material;
//             // return this.material;

//             this.material = new TrailMaterial();
//             TrailRenderer._materialInst = this.material;
//             return this.material;
//         }

//         private _InitializeLocalHeadGeometry(localHeadWidth: number) {

//             this._localHeadGeometry = [];

//             let halfWidth = localHeadWidth / 2;

//             this._localHeadGeometry.push(new THREE.Vector3(-halfWidth, 0, 0));
//             this._localHeadGeometry.push(new THREE.Vector3(halfWidth, 0, 0));
//             this._verticesPerNode = 2;

//             this._facesPerNode = (this._verticesPerNode - 1) * 2;
//             this._faceIndicesPerNode = this._facesPerNode * 3;
//         }

//         private _InitializeGeometry() {

//             this._vertexCount = this._length * this._verticesPerNode;
//             this._faceCount = this._length * this._facesPerNode;

//             let geometry = new THREE.BufferGeometry();
//             let nodeIDs = new Float32Array(this._vertexCount);
//             let nodeVertexIDs = new Float32Array(this._vertexCount * this._verticesPerNode);
//             let positions = new Float32Array(this._vertexCount * TrailRenderer._PositionComponentCount);
//             let nodeCenters = new Float32Array(this._vertexCount * TrailRenderer._PositionComponentCount);
//             let indices = new Uint32Array(this._faceCount * TrailRenderer._IndicesPerFace);

//             let nodeIDAttribute = new THREE.BufferAttribute(nodeIDs, 1);
//             nodeIDAttribute.setDynamic(true);
//             geometry.addAttribute('nodeID', nodeIDAttribute);

//             let nodeVertexIDAttribute = new THREE.BufferAttribute(nodeVertexIDs, 1);
//             nodeVertexIDAttribute.setDynamic(true);
//             geometry.addAttribute('nodeVertexID', nodeVertexIDAttribute);

//             let nodeCenterAttribute = new THREE.BufferAttribute(nodeCenters, TrailRenderer._PositionComponentCount);
//             nodeCenterAttribute.setDynamic(true);
//             geometry.addAttribute('nodeCenter', nodeCenterAttribute);

//             let positionAttribute = new THREE.BufferAttribute(positions, TrailRenderer._PositionComponentCount);
//             positionAttribute.setDynamic(true);
//             geometry.addAttribute('position', positionAttribute);

//             let indexAttribute = new THREE.BufferAttribute(indices, 1);
//             indexAttribute.setDynamic(true);
//             geometry.setIndex(indexAttribute);

//             this._geometry = geometry;
//         }

//         private _ZeroVertices() {
//             let positions = this._geometry.getAttribute('position') as THREE.BufferAttribute;
//             for (let i = 0; i < this._vertexCount; i++) {
//                 positions.setXYZ(i, 0, 0, 0);
//             }
//             positions.needsUpdate = true;
//             positions.updateRange.count = - 1;
//         }

//         private _ZeroIndices() {
//             let indices = this._geometry.getIndex() as THREE.BufferAttribute;
//             for (let i = 0; i < this._faceCount; i++) {
//                 indices.setXYZ(i, 0, 0, 0);
//             }
//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;
//         }

//         private _FormInitialFaces() {

//             this._ZeroIndices();

//             let indices = this._geometry.getIndex();
//             for (let i = 0; i < this._length - 1; i++) {
//                 this._connectNodes(i, i + 1);
//             }
//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;
//         }

//         private _InitializeMesh() {
//             this._mesh = new THREE.Mesh(this._geometry, this.material);
//             // this.mesh.matrixAutoUpdate = false;
//         }

//         private _DestroyMesh() {
//             if (this._mesh) {
//                 this._scene.remove(this._mesh);
//                 this._mesh = null;
//             }
//         }

//         private _Reset() {
//             this._currentLength = 0;
//             this._currentEnd = -1;

//             this._currentNodeCenter = null;
//             this._currentNodeID = 0;

//             this._FormInitialFaces();
//             this._ZeroVertices();

//             this._geometry.setDrawRange(0, 0);
//         }

//         private _updateUniforms() {
//             if (this._currentLength < this._length) {
//                 this.material.uniforms.minID.value = 0;
//             } else {
//                 this.material.uniforms.minID.value = this._currentNodeID - this._length;
//             }
//             this.material.uniforms.maxID.value = this._currentNodeID;
//         }


//         update() {
//             this._mesh.position.copy(this._targetObject.position);
//             this._mesh.quaternion.copy(this._targetObject.quaternion);
//             this._mesh.scale.copy(this._targetObject.scale);

//             this._advance();
//         }

//         private _tempMatrix4 = new THREE.Matrix4();

//         // updateHead() {
//         //     if (this.currentEnd < 0) return;

//         //     this.targetObject.updateMatrixWorld(false);
//         //     this.tempMatrix4.copy(this.targetObject.matrixWorld);

//         //     this.updateNodePositionsFromTransformMatrix(this.currentEnd, this.tempMatrix4);
//         // }

//         private _advance() {
//             this._tempMatrix4.copy(this._targetObject.matrixWorld);

//             this._advanceWithTransform(this._tempMatrix4);
//             this._updateUniforms();
//         }

//         private _advanceWithTransform(transformMatrix: THREE.Matrix4) {
//             this._advanceGeometry(transformMatrix);
//         }

//         private _advanceGeometry(transformMatrix: THREE.Matrix4) {

//             let nextIndex = this._currentEnd + 1 >= this._length ? 0 : this._currentEnd + 1;
//             this._updateNodePositionsFromTransformMatrix(nextIndex, transformMatrix);

//             if (this._currentLength >= 1) {
//                 this._connectNodes(this._currentEnd, nextIndex);
//                 if (this._currentLength >= this._length) {
//                     let disconnectIndex = this._currentEnd + 1 >= this._length ? 0 : this._currentEnd + 1;
//                     this._disconnectNodes(disconnectIndex);
//                 }
//             }

//             if (this._currentLength < this._length) {
//                 this._currentLength++;
//             }

//             this._currentEnd++;
//             if (this._currentEnd >= this._length) {
//                 this._currentEnd = 0;
//             }

//             if (this._currentLength >= 1) {
//                 if (this._currentLength < this._length) {
//                     this._geometry.setDrawRange(0, (this._currentLength - 1) * this._faceIndicesPerNode);
//                 } else {
//                     this._geometry.setDrawRange(0, this._currentLength * this._faceIndicesPerNode);
//                 }
//             }

//             this._updateNodeID(this._currentEnd, this._currentNodeID);
//             this._currentNodeID++;
//         }

//         private _updateNodeID(nodeIndex: number, id: number) {

//             this._nodeIDs[nodeIndex] = id;

//             let nodeIDs = this._geometry.getAttribute('nodeID') as THREE.BufferAttribute;
//             let nodeVertexIDs = this._geometry.getAttribute('nodeVertexID') as THREE.BufferAttribute;

//             for (let i = 0; i < this._verticesPerNode; i++) {
//                 let baseIndex = nodeIndex * this._verticesPerNode + i;
//                 nodeIDs.setX(baseIndex, id);
//                 nodeVertexIDs.setX(baseIndex, i);
//                 // nodeIDs.array[baseIndex] = id;
//                 // nodeVertexIDs.array[baseIndex] = i;
//             }

//             nodeIDs.needsUpdate = true;
//             nodeVertexIDs.needsUpdate = true;

//             nodeIDs.updateRange.offset = nodeIndex * this._verticesPerNode;
//             nodeIDs.updateRange.count = this._verticesPerNode;

//             nodeVertexIDs.updateRange.offset = nodeIndex * this._verticesPerNode;
//             nodeVertexIDs.updateRange.count = this._verticesPerNode;
//         }

//         private _updateNodeCenter(nodeIndex: number, nodeCenter: THREE.Vector3) {

//             this._currentNodeCenter = this._nodeCenters[nodeIndex];
//             this._currentNodeCenter.copy(nodeCenter);

//             let nodeCenters = this._geometry.getAttribute('nodeCenter') as THREE.BufferAttribute;
//             for (let i = 0; i < this._verticesPerNode; i++) {
//                 let baseIndex = (nodeIndex * this._verticesPerNode + i);
//                 nodeCenters.setXYZ(baseIndex, nodeCenter.x, nodeCenter.y, nodeCenter.z);
//             }

//             nodeCenters.needsUpdate = true;
//             nodeCenters.updateRange.offset = nodeIndex * this._verticesPerNode * TrailRenderer._PositionComponentCount;
//             nodeCenters.updateRange.count = this._verticesPerNode * TrailRenderer._PositionComponentCount;
//         }

//         private _tempPosition = new THREE.Vector3();

//         private _updateNodePositionsFromTransformMatrix(nodeIndex: number, transformMatrix: THREE.Matrix4) {

//             let positions = this._geometry.getAttribute('position') as THREE.BufferAttribute;

//             this._tempPosition.set(0, 0, 0);
//             this._tempPosition.applyMatrix4(transformMatrix);
//             this._updateNodeCenter(nodeIndex, this._tempPosition);

//             let vertex = this._tempPosition;
//             for (let i = 0; i < this._localHeadGeometry.length; i++) {
//                 vertex.copy(this._localHeadGeometry[i]);
//                 vertex.applyMatrix4(transformMatrix);

//                 let positionIndex = ((this._verticesPerNode * nodeIndex) + i);
//                 positions.setXYZ(positionIndex, vertex.x, vertex.y, vertex.z);
//             }

//             positions.needsUpdate = true;
//             positions.updateRange.offset = nodeIndex * this._verticesPerNode * TrailRenderer._PositionComponentCount;
//             positions.updateRange.count = this._verticesPerNode * TrailRenderer._PositionComponentCount;
//         }



//         private _connectNodes(srcNodeIndex: number, destNodeIndex: number) {

//             let returnObj: NodeConnection = {
//                 "attribute": null,
//                 "offset": 0,
//                 "count": - 1
//             };

//             let indices = this._geometry.getIndex() as any;

//             for (let i = 0; i < this._localHeadGeometry.length - 1; i++) {
//                 let srcVertexIndex = (this._verticesPerNode * srcNodeIndex) + i;
//                 let destVertexIndex = (this._verticesPerNode * destNodeIndex) + i;
//                 let faceIndex = ((srcNodeIndex * this._facesPerNode) + (i * TrailRenderer._FacesPerQuad)) * TrailRenderer._IndicesPerFace;
//                 indices.setXYZ(faceIndex, srcVertexIndex, destVertexIndex, (srcVertexIndex + 1));
//                 indices.setXYZ(faceIndex + 3, destVertexIndex, (destVertexIndex + 1), (srcVertexIndex + 1));
//             }

//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;

//             returnObj.attribute = indices;
//             returnObj.offset = srcNodeIndex * this._facesPerNode * TrailRenderer._IndicesPerFace;
//             returnObj.count = this._facesPerNode * TrailRenderer._IndicesPerFace;

//             return returnObj;
//         }

//         private _disconnectNodes(srcNodeIndex: number) {

//             let returnObj: NodeConnection = {
//                 "attribute": null,
//                 "offset": 0,
//                 "count": - 1
//             };

//             let indices = this._geometry.getIndex() as any;

//             for (let i = 0; i < this._localHeadGeometry.length - 1; i++) {
//                 let faceIndex = ((srcNodeIndex * this._facesPerNode) + (i * TrailRenderer._FacesPerQuad)) * TrailRenderer._IndicesPerFace;
//                 indices.setXYZ(faceIndex, 0, 0, 0);
//                 indices.setXYZ(faceIndex + 3, 0, 0, 0);
//             }

//             indices.needsUpdate = true;
//             indices.updateRange.count = - 1;

//             returnObj.attribute = indices;
//             returnObj.offset = srcNodeIndex * this._facesPerNode * TrailRenderer._IndicesPerFace;
//             returnObj.count = this._facesPerNode * TrailRenderer._IndicesPerFace;

//             return returnObj;
//         }

//         public deactivate() {
//             if (this._active) {
//                 this._scene.remove(this._mesh);
//                 this._active = false;
//             }
//         }

//         public activate() {
//             if (!this._active) {
//                 this._scene.add(this._mesh);
//                 this._active = true;
//             }
//         }
//     }






//     // let Shader: any = {};
//     // Shader.BaseVertexVars = [

//     //     "attribute float nodeID;",
//     //     "attribute float nodeVertexID;",
//     //     "attribute vec3 nodeCenter;",

//     //     "uniform float minID;",
//     //     "uniform float maxID;",

//     //     "uniform vec4 headColor;",
//     //     "uniform vec4 tailColor;",
//     //     "uniform float tailSharp;",

//     //     "varying vec4 vColor;",

//     // ].join("\n");

//     // Shader.BaseFragmentVars = [

//     //     "varying vec4 vColor;",
//     //     "uniform sampler2D texture;",

//     // ].join("\n");

//     // Shader.VertexShaderCore = [

//     //     "float fraction = ( maxID - nodeID ) / ( maxID - minID );",
//     //     "vColor = ( 1.0 - fraction ) * headColor + fraction * tailColor;",
//     //     "vec4 realPosition; ",
//     //     "if(tailSharp == 1.0) {",
//     //     "  realPosition = vec4( ( 1.0 - fraction ) * position.xyz + fraction * nodeCenter.xyz, 1.0 ); ",
//     //     "} else {",
//     //     "  realPosition = vec4( position.xyz, 1.0 ); ",
//     //     "}",

//     // ].join("\n");

//     // Shader.BaseVertexShader = [

//     //     Shader.BaseVertexVars,

//     //     "void main() { ",

//     //     Shader.VertexShaderCore,
//     //     "gl_Position = projectionMatrix * viewMatrix * realPosition;",

//     //     "}"

//     // ].join("\n");

//     // Shader.BaseFragmentShader = [

//     //     Shader.BaseFragmentVars,

//     //     "void main() { ",

//     //     "gl_FragColor = vColor;",

//     //     "}"

//     // ].join("\n");




// }
