module plume.gltf {

    export function resolveURL(url: string, path: string) {

        // Invalid URL
        if (typeof url !== 'string' || url === '') return '';

        // Absolute URL http://,https://,//
        if (/^(https?:)?\/\//i.test(url)) return url;

        // Data URI
        if (/^data:.*,.*$/i.test(url)) return url;

        // Blob URL
        if (/^blob:.*$/i.test(url)) return url;

        // Relative URL
        return path + url;

    }

	/**
	 * Specification: https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#default-material
	 */
    export function createDefaultMaterial() {
        return new THREE.MeshStandardMaterial({
            color: 0xFFFFFF,
            emissive: 0x000000,
            metalness: 1,
            roughness: 1,
            transparent: false,
            depthTest: true,
            side: THREE.FrontSide
        });
    }


    function isPrimitiveEqual(a: gltfspec.MeshPrimitive, b: gltfspec.MeshPrimitive) {
        if (a.indices !== b.indices) {
            return false;
        }
        return isObjectEqual(a.attributes, b.attributes);
    }

    function isObjectEqual<T>(a: T, b: T) {
        if (Object.keys(a).length !== Object.keys(b).length) return false;
        for (let key in a) {
            if (a[key] !== b[key]) return false;
        }
        return true;
    }

    export function getCachedGeometry(cache: Array<PrimitiveCacheData>, newPrimitive: gltfspec.MeshPrimitive) {
        for (let i = 0; i < cache.length; i++) {
            let cached = cache[i];
            if (isPrimitiveEqual(cached.primitive, newPrimitive)) return cached;
        }
        return null;
    }

    export function addPrimitiveAttributes(geometry: THREE.BufferGeometry, primitiveDef: gltfspec.MeshPrimitive, accessors: Array<THREE.BufferAttribute | THREE.InterleavedBufferAttribute>) {
        let attributes = primitiveDef.attributes;

        for (let gltfAttributeName in attributes) {

            let threeAttributeName = ATTRIBUTES[gltfAttributeName];
            let bufferAttribute = accessors[attributes[gltfAttributeName]];

            // Skip attributes already provided by e.g. Draco extension.
            if (!threeAttributeName) continue;
            if (threeAttributeName in geometry.attributes) continue;

            // if (threeAttributeName == ATTRIBUTES.NORMAL) continue;
            // if (threeAttributeName == ATTRIBUTES.TEXCOORD_1) continue;

            geometry.addAttribute(threeAttributeName, bufferAttribute);
        }

        if (primitiveDef.indices !== undefined && !geometry.index) {
            let index = accessors[primitiveDef.indices];
            if (index instanceof THREE.BufferAttribute) {
                geometry.setIndex(index);
            } else {
                logger.error("TODO Interleaved buffer ??? ");
            }
        }

        if (primitiveDef.targets !== undefined) {
            addMorphTargets(geometry, primitiveDef.targets, accessors);
        }

        assignExtrasToUserData(geometry, primitiveDef);
    }


    function addMorphTargets(geometry: THREE.BufferGeometry, targets: Array<any>, accessors: Array<THREE.BufferAttribute | THREE.InterleavedBufferAttribute>) {

        let hasMorphPosition = false;
        let hasMorphNormal = false;

        for (let i = 0; i < targets.length; i++) {
            let target = targets[i];

            if (target.POSITION !== undefined) hasMorphPosition = true;
            if (target.NORMAL !== undefined) hasMorphNormal = true;
            if (hasMorphPosition && hasMorphNormal) break;
        }

        if (!hasMorphPosition && !hasMorphNormal) return;

        let morphPositions = [];
        let morphNormals = [];

        for (let i = 0; i < targets.length; i++) {
            let target = targets[i];
            let attributeName = 'morphTarget' + i;

            if (hasMorphPosition) {
                // Three.js morph position is absolute value. The formula is
                //   basePosition
                //     + weight0 * ( morphPosition0 - basePosition )
                //     + weight1 * ( morphPosition1 - basePosition )
                //     ...
                // while the glTF one is relative
                //   basePosition
                //     + weight0 * glTFmorphPosition0
                //     + weight1 * glTFmorphPosition1
                //     ...
                // then we need to convert from relative to absolute here.

                let positionAttribute: THREE.BufferAttribute | THREE.InterleavedBufferAttribute;

                if (target.POSITION !== undefined) {
                    // Cloning not to pollute original accessor
                    positionAttribute = cloneBufferAttribute(accessors[target.POSITION]);
                    positionAttribute["name"] = attributeName;

                    let position = geometry.attributes.position;
                    for (let j = 0, jl = positionAttribute.count; j < jl; j++) {
                        positionAttribute.setXYZ(
                            j,
                            positionAttribute.getX(j) + position.getX(j),
                            positionAttribute.getY(j) + position.getY(j),
                            positionAttribute.getZ(j) + position.getZ(j)
                        );
                    }
                } else {
                    positionAttribute = geometry.attributes.position;
                }

                morphPositions.push(positionAttribute);
            }

            if (hasMorphNormal) {
                let normalAttribute: THREE.BufferAttribute | THREE.InterleavedBufferAttribute;

                if (target.NORMAL !== undefined) {
                    normalAttribute = cloneBufferAttribute(accessors[target.NORMAL]);
                    normalAttribute["name"] = attributeName;

                    let normal = geometry.attributes.normal;
                    for (let j = 0, jl = normalAttribute.count; j < jl; j++) {
                        normalAttribute.setXYZ(
                            j,
                            normalAttribute.getX(j) + normal.getX(j),
                            normalAttribute.getY(j) + normal.getY(j),
                            normalAttribute.getZ(j) + normal.getZ(j)
                        );
                    }
                } else {
                    normalAttribute = geometry.attributes.normal;
                }

                morphNormals.push(normalAttribute);
            }

        }

        if (hasMorphPosition) geometry.morphAttributes.position = morphPositions;
        if (hasMorphNormal) geometry.morphAttributes.normal = morphNormals;
    }

    function cloneBufferAttribute(attribute: THREE.BufferAttribute | THREE.InterleavedBufferAttribute) {
        if (attribute instanceof THREE.InterleavedBufferAttribute) {
            let count = attribute.count;
            let itemSize = attribute.itemSize;
            let array = attribute.array.slice(0, count * itemSize);

            for (let i = 0; i < count; ++i) {
                array[i] = attribute.getX(i);
                if (itemSize >= 2) array[i + 1] = attribute.getY(i);
                if (itemSize >= 3) array[i + 2] = attribute.getZ(i);
                if (itemSize >= 4) array[i + 3] = attribute.getW(i);
            }
            return new THREE.BufferAttribute(array, itemSize, attribute.normalized);
        }
        return attribute.clone();
    }

    export function updateMorphTargets(mesh: THREE.Mesh, meshDef: any) {
        mesh.updateMorphTargets();
        if (meshDef.weights !== undefined) {
            for (let i = 0; i < meshDef.weights.length; i++) {
                mesh.morphTargetInfluences[i] = meshDef.weights[i];
            }
        }

        // .extras has user-defined data, so check that .extras.targetNames is an array.
        if (meshDef.extras && Array.isArray(meshDef.extras.targetNames)) {
            let targetNames = meshDef.extras.targetNames;
            if (mesh.morphTargetInfluences.length === targetNames.length) {
                mesh.morphTargetDictionary = {};
                for (let i = 0; i < targetNames.length; i++) {
                    mesh.morphTargetDictionary[targetNames[i]] = i;
                }
            } else {
                logger.warn('GLTFLoader: Invalid extras.targetNames length. Ignoring names.');
            }
        }
    }

    export function assignExtrasToUserData(object: any, gltfDef: any) {
        if (gltfDef.extras !== undefined) {
            if (typeof gltfDef.extras === 'object') {
                object.userData = gltfDef.extras;
            } else {
                logger.warn('GLTFLoader: Ignoring primitive type .extras, ' + gltfDef.extras);
            }
        }
    }
}