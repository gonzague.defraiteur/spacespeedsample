/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFBufferViewImporter extends GLTFImporter {

        import() {
            if (this.parser.json.bufferViews == null || this.parser.json.bufferViews.length <= 0) return;

            for (let i = 0; i < this.parser.json.bufferViews.length; i++) {
                let bufferViewDef = this.parser.json.bufferViews[i];
                let buffer = this.parser._buffers[bufferViewDef.buffer];
                let byteLength = bufferViewDef.byteLength || 0;
                let byteOffset = bufferViewDef.byteOffset || 0;
                let bufferView = buffer.slice(byteOffset, byteOffset + byteLength);
                this.parser._bufferViews[i] = bufferView;
            }
        }
    }
}