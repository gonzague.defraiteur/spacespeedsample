/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFEmbeddedImageImporter extends GLTFImporter {

        import() {
            if (this.parser.json.images == null || this.parser.json.images.length <= 0) return;

            for (let i = 0; i < this.parser.json.images.length; i++) {
                let imageDef = this.parser.json.images[i];
                if (imageDef.bufferView != null) {
                    this._loadImage(imageDef, i);
                }
            }
        }

        private _loadImage(imageDef: gltfspec.Image, index: number) {
            let self = this;
            let bufferView = this.parser._bufferViews[imageDef.bufferView];
            let blob = new Blob([bufferView], { type: imageDef.mimeType });
            let sourceURI = URL.createObjectURL(blob);
            let texture = this.parser._textureLoaderManager.load(sourceURI, function (t) {
                // Ok, free memory
                URL.revokeObjectURL(sourceURI);
            }, function (e) {
                logger.error("Failed to load texture from bufferView " + imageDef.bufferView);
            });
            self.parser._images[index] = texture;
        }
    }
}