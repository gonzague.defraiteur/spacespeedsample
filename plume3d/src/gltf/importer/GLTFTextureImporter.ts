/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFTextureImporter extends GLTFImporter {

        import() {
            if (this.parser.json.textures == null || this.parser.json.textures.length <= 0) return;

            for (let i = 0; i < this.parser.json.textures.length; i++) {
                let textureDef = this.parser.json.textures[i];
                this._loadTexture(textureDef, i);
            }
        }

        private _loadTexture(textureDef: gltfspec.Texture, index: number) {
            let source = this.parser.json.images[textureDef.source];
            let texture = this.parser._images[textureDef.source];

            texture.flipY = false;

            if (textureDef.name !== undefined) texture.name = textureDef.name;

            // Ignore unknown mime types, like DDS files.
            if (source.mimeType in MIME_TYPE_FORMATS) {
                texture.format = MIME_TYPE_FORMATS[source.mimeType];
            }

            let samplers = this.parser.json.samplers || {};
            let sampler = samplers[textureDef.sampler] || {};
            texture.magFilter = WEBGL_FILTERS[sampler.magFilter] || THREE.LinearFilter;
            texture.minFilter = WEBGL_FILTERS[sampler.minFilter] || THREE.LinearMipMapLinearFilter;
            texture.wrapS = WEBGL_WRAPPINGS[sampler.wrapS] || THREE.RepeatWrapping;
            texture.wrapT = WEBGL_WRAPPINGS[sampler.wrapT] || THREE.RepeatWrapping;

            this.parser._textures[index] = texture;
        }
    }
}