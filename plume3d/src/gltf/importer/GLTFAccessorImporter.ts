/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFAccessorImporter extends GLTFImporter {

        import() {
            // Create all accessors
            if (this.parser.json.accessors == null || this.parser.json.accessors.length <= 0) return;

            for (let i = 0; i < this.parser.json.accessors.length; i++) {
                let accessorsDef = this.parser.json.accessors[i];
                this._loadAccessor(accessorsDef, i);
            }
        }

        // There is no "sparse" accessor in Unity exporter
        // There is no "byteStride" accessor in Unity exporter
        // We do not support it for now to simplify code
        private _loadAccessor(accessorDef: gltfspec.Accessor, index: number) {
            if (accessorDef.bufferView === undefined) { //  && accessorDef.sparse === undefined
                // Ignore empty accessors, which may be used to declare runtime
                // information about attributes coming from another source (e.g. Draco compression extension).
                this.parser._accessors[index] = null;
                return;
            }

            let bufferView = this.parser._bufferViews[accessorDef.bufferView];
            let bufferViewDef = this.parser.json.bufferViews[accessorDef.bufferView];

            let itemSize: number = WEBGL_TYPE_SIZES[accessorDef.type];
            let arrayConstructor = WEBGL_COMPONENT_TYPES[accessorDef.componentType];
            let elementBytes = arrayConstructor.BYTES_PER_ELEMENT;
            let itemBytes = elementBytes * itemSize;
            let byteOffset = accessorDef.byteOffset || 0;
            let normalized = (accessorDef.normalized === true);
            // For VEC3: itemSize is 3, elementBytes is 4, itemBytes is 12.

            if (bufferViewDef.byteStride != null && bufferViewDef.byteStride != itemBytes) {
                logger.warn("Accessor with 'byteStride' (need to be implemented)");
                this.parser._accessors[index] = null;
                return;
            }

            let array: Int8Array | Uint8Array | Int16Array | Uint16Array | Float32Array;
            let bufferAttribute: THREE.BufferAttribute | THREE.InterleavedBufferAttribute;

            if (bufferView === null) {
                array = new arrayConstructor(accessorDef.count * itemSize);
            } else {
                array = new arrayConstructor(bufferView, byteOffset, accessorDef.count * itemSize);
            }
            bufferAttribute = new THREE.BufferAttribute(array, itemSize, normalized);

            this.parser._accessors[index] = bufferAttribute;
        }



        // private _loadAccessor(accessorDef: gltfspec.Accessor, index: number) {
        //     if (accessorDef.bufferView === undefined && accessorDef.sparse === undefined) {
        //         // Ignore empty accessors, which may be used to declare runtime
        //         // information about attributes coming from another source (e.g. Draco
        //         // compression extension).
        //         this.parser._accessors[index] = null;
        //         return;
        //     }

        //     let bufferViews: Array<ArrayBuffer> = [];
        //     if (accessorDef.bufferView !== undefined) {
        //         bufferViews.push(this.parser._bufferViews[accessorDef.bufferView]);
        //     } else {
        //         bufferViews.push(null);
        //     }

        //     if (accessorDef.sparse !== undefined) {
        //         bufferViews.push(this.parser._bufferViews[accessorDef.sparse.indices.bufferView]);
        //         bufferViews.push(this.parser._bufferViews[accessorDef.sparse.values.bufferView]);
        //     }

        //     let bufferView = bufferViews[0];

        //     let itemSize: number = WEBGL_TYPE_SIZES[accessorDef.type];
        //     let TypedArray = WEBGL_COMPONENT_TYPES[accessorDef.componentType];

        //     // For VEC3: itemSize is 3, elementBytes is 4, itemBytes is 12.
        //     let elementBytes = TypedArray.BYTES_PER_ELEMENT;
        //     let itemBytes = elementBytes * itemSize;
        //     let byteOffset = accessorDef.byteOffset || 0;
        //     let byteStride = accessorDef.bufferView !== undefined ? this.parser.json.bufferViews[accessorDef.bufferView].byteStride : undefined;
        //     let normalized = accessorDef.normalized === true;
        //     let array: Int8Array | Uint8Array | Int16Array | Uint16Array | Float32Array;
        //     let bufferAttribute: THREE.BufferAttribute | THREE.InterleavedBufferAttribute;

        //     // The buffer is not interleaved if the stride is the item size in bytes.
        //     if (byteStride && byteStride !== itemBytes) {
        //         let ibCacheKey = 'InterleavedBuffer:' + accessorDef.bufferView + ':' + accessorDef.componentType;
        //         let ib = this.parser.cache.get(ibCacheKey);
        //         if (!ib) {
        //             // Use the full buffer if it's interleaved.
        //             array = new TypedArray(bufferView);
        //             // Integer parameters to IB/IBA are in array elements, not bytes.
        //             ib = new THREE.InterleavedBuffer(array, byteStride / elementBytes);
        //             this.parser.cache.put(ibCacheKey, ib);
        //         }
        //         bufferAttribute = new THREE.InterleavedBufferAttribute(ib, itemSize, byteOffset / elementBytes, normalized);

        //     } else {
        //         if (bufferView === null) {
        //             array = new TypedArray(accessorDef.count * itemSize);
        //         } else {
        //             array = new TypedArray(bufferView, byteOffset, accessorDef.count * itemSize);
        //         }
        //         bufferAttribute = new THREE.BufferAttribute(array, itemSize, normalized);
        //     }

        //     // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#sparse-accessors
        //     if (accessorDef.sparse !== undefined) {

        //         let itemSizeIndices = WEBGL_TYPE_SIZES.SCALAR;
        //         let TypedArrayIndices = WEBGL_COMPONENT_TYPES[accessorDef.sparse.indices.componentType];

        //         let byteOffsetIndices = accessorDef.sparse.indices.byteOffset || 0;
        //         let byteOffsetValues = accessorDef.sparse.values.byteOffset || 0;

        //         let sparseIndices = new TypedArrayIndices(bufferViews[1], byteOffsetIndices, accessorDef.sparse.count * itemSizeIndices);
        //         let sparseValues = new TypedArray(bufferViews[2], byteOffsetValues, accessorDef.sparse.count * itemSize);

        //         if (bufferView !== null) {
        //             // Avoid modifying the original ArrayBuffer, if the bufferView wasn't initialized with zeroes.
        //             (bufferAttribute as any).setArray((bufferAttribute as any).array.slice()); // TODO check
        //         }

        //         for (let i = 0, il = sparseIndices.length; i < il; i++) {
        //             let index = sparseIndices[i];

        //             bufferAttribute.setX(index, sparseValues[i * itemSize]);
        //             if (itemSize >= 2) bufferAttribute.setY(index, sparseValues[i * itemSize + 1]);
        //             if (itemSize >= 3) bufferAttribute.setZ(index, sparseValues[i * itemSize + 2]);
        //             if (itemSize >= 4) bufferAttribute.setW(index, sparseValues[i * itemSize + 3]);
        //             if (itemSize >= 5) throw new Error('THREE.GLTFLoader: Unsupported itemSize in sparse BufferAttribute.');
        //         }
        //     }

        //     this.parser._accessors[index] = bufferAttribute;
        // }
    }

}