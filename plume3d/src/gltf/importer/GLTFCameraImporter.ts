/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFCameraImporter extends GLTFImporter {

        import() {
            if (this.parser.json.cameras == null || this.parser.json.cameras.length <= 0) return;

            for (let i = 0; i < this.parser.json.cameras.length; i++) {
                let cameraDef = this.parser.json.cameras[i];
                this._loadCamera(cameraDef, i);
            }
        }

        private _loadCamera(cameraDef: gltfspec.Camera, index: number) {

            let camera: THREE.Camera;
            if (cameraDef.type === 'perspective') {
                let param = cameraDef.perspective;
                let perspectiveCamera = new THREE.PerspectiveCamera(THREE.Math.radToDeg(param.yfov), param.aspectRatio || 1, param.znear || 1, param.zfar || 2e6);
                camera = perspectiveCamera;

                // TODO option
                let screen = Game.get().scaleManager.getCurrentScaling();
                perspectiveCamera.aspect = screen.windowWidth / screen.windowHeight;
                perspectiveCamera.updateProjectionMatrix();


            } else if (cameraDef.type === 'orthographic') {
                let param = cameraDef.orthographic;
                camera = new THREE.OrthographicCamera(param.xmag / - 2, param.xmag / 2, param.ymag / 2, param.ymag / - 2, param.znear, param.zfar);
            }

            if (cameraDef.name !== undefined) camera.name = cameraDef.name;

            assignExtrasToUserData(camera, cameraDef);

            this.parser._cameras[index] = camera;
        }


    }
}