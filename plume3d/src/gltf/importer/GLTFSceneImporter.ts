/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFSceneImporter extends GLTFImporter {

        import() {
            if (this.parser.json.scenes == null) return;

            for (let i = 0; i < this.parser.json.scenes.length; i++) {
                this._loadScene(this.parser.json.scenes[i], i);
            }
        }

        private _loadScene(sceneDef: gltfspec.Scene, sceneIndex: number) {
            let scene = new THREE.Scene();
            if (sceneDef.name !== undefined) scene.name = sceneDef.name;

            assignExtrasToUserData(scene, sceneDef);

            // if (sceneDef.extensions) addUnknownExtensionsToUserData(extensions, scene, sceneDef);

            let nodeIds = sceneDef.nodes || [];
            for (let i = 0, il = nodeIds.length; i < il; i++) {
                this._buildNodeHierachy(nodeIds[i], scene);
            }

            this.parser._scenes[sceneIndex] = scene;
        }

        private _buildNodeHierachy(nodeId: number, parentObject: THREE.Object3D) {

            let node = this.parser._nodes[nodeId];
            let nodeDef = this.parser.json.nodes[nodeId];

            // build skeleton
            if (nodeDef.skin !== undefined) {
                let meshes = (node["isGroup"] === true ? node.children : [node]);
                for (let i = 0; i < meshes.length; i++) {
                    let mesh = meshes[i];
                    let skinEntry = this.parser._skins[nodeDef.skin];
                    let bones: Array<THREE.Bone> = [];
                    let boneInverses: Array<THREE.Matrix4> = [];
                    for (let j = 0; j < skinEntry.joints.length; j++) {
                        let jointId = skinEntry.joints[j];
                        let jointNode = this.parser._nodes[jointId];
                        if (jointNode) {
                            bones.push(jointNode as any);
                            let mat = new THREE.Matrix4();
                            if (skinEntry.inverseBindMatrices !== undefined) {
                                mat.fromArray(skinEntry.inverseBindMatrices.array as any, j * 16);
                            }
                            boneInverses.push(mat);
                        } else {
                            logger.warn("Joint " + jointId + " could not be found.");
                        }
                    }
                    if (mesh["isSkinnedMesh"] == true) {
                        (mesh as THREE.SkinnedMesh).bind(new THREE.Skeleton(bones, boneInverses), mesh.matrixWorld);
                    }
                }
            }

            // build node hierachy
            parentObject.add(node);

            if (nodeDef.children) {
                let children = nodeDef.children;
                for (let i = 0; i < children.length; i++) {
                    let child = children[i];
                    this._buildNodeHierachy(child, node);
                }
            }
        }

    }
}