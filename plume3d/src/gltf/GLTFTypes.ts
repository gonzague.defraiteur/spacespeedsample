module plume.gltf {

    export type GltfParserOptions = {
        path: string,
        crossOrigin: string,
        manager: THREE.LoadingManager,
        textureLoaderManager: plume.TextureLoaderManager,
        factory?: GLTFElementFactory,
    }

    export type GltfObject = {
        animations: Array<THREE.AnimationClip>;
        animationsRelatives: Array<{ root: THREE.Object3D, clip: THREE.AnimationClip }>;
        scene: THREE.Scene;
        scenes: Array<THREE.Scene>;
        cameras: Array<THREE.Camera>;
        asset: Object;
        parser: GLTFParser;
        userData?: any;
    };

    export type PrimitiveCacheData = {
        primitive: gltfspec.MeshPrimitive;
        geometry: THREE.BufferGeometry;
    }
    export type SkinEntry = {
        joints: number[];
        inverseBindMatrices: THREE.BufferAttribute;
    }

    export interface Extensions {
        "KHR_binary_glTF"?: GLTFBinaryExtension;
        "KHR_draco_mesh_compression"?: GLTFDracoMeshCompressionExtension;
        "KHR_lights_punctual"?: GLTFLightsExtension;
        "KHR_materials_pbrSpecularGlossiness"?: GLTFMaterialsPbrSpecularGlossinessExtension;
        "KHR_materials_unlit"?: GLTFMaterialsUnlitExtension;
        "MSFT_texture_dds"?: GLTFTextureDDSExtension;
        "EXT_texture_transform"?: GLTFTextureTransformExtension;
        "GB_lightmaps"?: GLTFLightMapsExtension;
        "GB_sprites"?: GLTFSpritesExtension;
        "GB_Unity"?: GLTFUnityExtension; // not regular extension, put in extra data
    }
    export type ExtensionsType = keyof Extensions;

    /* CONSTANTS */

    export const WEBGL_CONSTANTS = {
        FLOAT: 5126,
        //FLOAT_MAT2: 35674,
        FLOAT_MAT3: 35675,
        FLOAT_MAT4: 35676,
        FLOAT_VEC2: 35664,
        FLOAT_VEC3: 35665,
        FLOAT_VEC4: 35666,
        LINEAR: 9729,
        REPEAT: 10497,
        SAMPLER_2D: 35678,
        POINTS: 0,
        LINES: 1,
        LINE_LOOP: 2,
        LINE_STRIP: 3,
        TRIANGLES: 4,
        TRIANGLE_STRIP: 5,
        TRIANGLE_FAN: 6,
        UNSIGNED_BYTE: 5121,
        UNSIGNED_SHORT: 5123
    };

    export const WEBGL_TYPE = {
        5126: Number,
        //35674: THREE.Matrix2,
        35675: THREE.Matrix3,
        35676: THREE.Matrix4,
        35664: THREE.Vector2,
        35665: THREE.Vector3,
        35666: THREE.Vector4,
        35678: THREE.Texture
    };

    export const WEBGL_COMPONENT_TYPES = {
        5120: Int8Array,
        5121: Uint8Array,
        5122: Int16Array,
        5123: Uint16Array,
        5125: Uint32Array,
        5126: Float32Array
    };
    export type TypedArray = Int8Array | Uint8Array | Int16Array | Uint16Array | Uint32Array | Float32Array;
    export type TypedArrayConstructor = (typeof Int8Array) | (typeof Uint8Array) | (typeof Int16Array) | (typeof Uint16Array) | (typeof Uint32Array) | (typeof Float32Array);

    export const WEBGL_FILTERS = {
        9728: THREE.NearestFilter,
        9729: THREE.LinearFilter,
        9984: THREE.NearestMipMapNearestFilter,
        9985: THREE.LinearMipMapNearestFilter,
        9986: THREE.NearestMipMapLinearFilter,
        9987: THREE.LinearMipMapLinearFilter
    };

    export const WEBGL_WRAPPINGS = {
        33071: THREE.ClampToEdgeWrapping,
        33648: THREE.MirroredRepeatWrapping,
        10497: THREE.RepeatWrapping
    };

    export const WEBGL_SIDES = {
        1028: THREE.BackSide, // Culling front
        1029: THREE.FrontSide // Culling back
        //1032: THREE.NoSide   // Culling front and back, what to do?
    };

    export const WEBGL_DEPTH_FUNCS = {
        512: THREE.NeverDepth,
        513: THREE.LessDepth,
        514: THREE.EqualDepth,
        515: THREE.LessEqualDepth,
        516: THREE.GreaterEqualDepth,
        517: THREE.NotEqualDepth,
        518: THREE.GreaterEqualDepth,
        519: THREE.AlwaysDepth
    };

    export const WEBGL_BLEND_EQUATIONS = {
        32774: THREE.AddEquation,
        32778: THREE.SubtractEquation,
        32779: THREE.ReverseSubtractEquation
    };

    export const WEBGL_BLEND_FUNCS = {
        0: THREE.ZeroFactor,
        1: THREE.OneFactor,
        768: THREE.SrcColorFactor,
        769: THREE.OneMinusSrcColorFactor,
        770: THREE.SrcAlphaFactor,
        771: THREE.OneMinusSrcAlphaFactor,
        772: THREE.DstAlphaFactor,
        773: THREE.OneMinusDstAlphaFactor,
        774: THREE.DstColorFactor,
        775: THREE.OneMinusDstColorFactor,
        776: THREE.SrcAlphaSaturateFactor
        // The followings are not supported by Three.js yet
        //32769: CONSTANT_COLOR,
        //32770: ONE_MINUS_CONSTANT_COLOR,
        //32771: CONSTANT_ALPHA,
        //32772: ONE_MINUS_CONSTANT_COLOR
    };

    export const WEBGL_TYPE_SIZES = {
        'SCALAR': 1,
        'VEC2': 2,
        'VEC3': 3,
        'VEC4': 4,
        'MAT2': 4,
        'MAT3': 9,
        'MAT4': 16
    };

    export const ATTRIBUTES = {
        POSITION: 'position',
        NORMAL: 'normal',
        TEXCOORD_0: 'uv',
        TEXCOORD_1: 'uv2',
        COLOR_0: 'color',
        WEIGHTS_0: 'skinWeight',
        JOINTS_0: 'skinIndex',
    };

    export const PATH_PROPERTIES = {
        scale: 'scale',
        translation: 'position',
        rotation: 'quaternion',
        weights: 'morphTargetInfluences'
    };

    export const INTERPOLATION = {
        CUBICSPLINE: THREE.InterpolateSmooth, // We use custom interpolation GLTFCubicSplineInterpolation for CUBICSPLINE.
        // KeyframeTrack.optimize() can't handle glTF Cubic Spline output values layout,
        // using THREE.InterpolateSmooth for KeyframeTrack instantiation to prevent optimization.
        // See KeyframeTrack.optimize() for the detail.
        LINEAR: THREE.InterpolateLinear,
        STEP: THREE.InterpolateDiscrete
    };

    export const STATES_ENABLES = {
        2884: 'CULL_FACE',
        2929: 'DEPTH_TEST',
        3042: 'BLEND',
        3089: 'SCISSOR_TEST',
        32823: 'POLYGON_OFFSET_FILL',
        32926: 'SAMPLE_ALPHA_TO_COVERAGE'
    };

    export const ALPHA_MODES = {
        OPAQUE: 'OPAQUE',
        MASK: 'MASK',
        BLEND: 'BLEND'
    };

    export const MIME_TYPE_FORMATS = {
        'image/png': THREE.RGBAFormat,
        'image/jpeg': THREE.RGBFormat
    };
}