module plume.gltf {

    export class GLTFMaterialsUnlitExtension {

        name: ExtensionsType;

        constructor(public json: gltfspec.GlTf) {
            this.name = "KHR_materials_unlit";
        }

        getMaterialType(materialDef: gltfspec.Material) {
            return THREE.MeshBasicMaterial;
        };

        extendParams(params: any, material: gltfspec.Material, parser: GLTFParser) {

            params.color = new THREE.Color(1.0, 1.0, 1.0);
            params.opacity = 1.0;

            let metallicRoughness = material.pbrMetallicRoughness;
            if (metallicRoughness) {

                if (Array.isArray(metallicRoughness.baseColorFactor)) {
                    let array = metallicRoughness.baseColorFactor;
                    params.color.fromArray(array);
                    params.opacity = array[3];
                }

                if (metallicRoughness.baseColorTexture !== undefined) {
                    parser.assignTexture(metallicRoughness.baseColorTexture, params, 'map');
                }
            }
        };

    }
}