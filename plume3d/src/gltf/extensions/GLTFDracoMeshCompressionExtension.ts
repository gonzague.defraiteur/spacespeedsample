module plume.gltf {

    export class GLTFDracoMeshCompressionExtension {

        name: ExtensionsType;
        dracoDecoder: any;

        constructor(public json: gltfspec.GlTf, public dracoLoader: any) {
            if (!dracoLoader) {
                throw new Error('No DRACOLoader instance provided.');
            }
            this.name = "KHR_draco_mesh_compression";
        }

        initialize(onComplete: () => void) {
            let self = this;
            THREE["DRACOLoader"].getDecoderModule().then(function (module: any) {
                self.dracoDecoder =  module.decoder;
                onComplete();
            });
        }

        decodePrimitive(primitive: gltfspec.MeshPrimitive, parser: GLTFParser) {
            let json = this.json;
            let dracoLoader = this.dracoLoader;
            let bufferViewIndex = primitive.extensions[this.name].bufferView;
            let gltfAttributeMap = primitive.extensions[this.name].attributes;
            let threeAttributeMap = {};
            let attributeNormalizedMap = {};
            let attributeTypeMap = {};

            for (let attributeName in gltfAttributeMap) {
                if (!(attributeName in ATTRIBUTES)) continue;

                threeAttributeMap[ATTRIBUTES[attributeName]] = gltfAttributeMap[attributeName];
            }

            for (let attributeName in primitive.attributes) {
                if (ATTRIBUTES[attributeName] !== undefined && gltfAttributeMap[attributeName] !== undefined) {

                    let accessorDef = json.accessors[primitive.attributes[attributeName]];
                    let componentType = WEBGL_COMPONENT_TYPES[accessorDef.componentType];

                    attributeTypeMap[ATTRIBUTES[attributeName]] = componentType;
                    attributeNormalizedMap[ATTRIBUTES[attributeName]] = accessorDef.normalized === true;
                }
            }

            let bufferView = parser.getBufferView(bufferViewIndex);


            // let geometry0: THREE.BufferGeometry;
            // dracoLoader.decodeDracoFile(bufferView, function (geometry: any) {
            //     for (let attributeName in geometry.attributes) {
            //         let attribute = geometry.attributes[attributeName];
            //         let normalized = attributeNormalizedMap[attributeName];

            //         if (normalized !== undefined) attribute.normalized = normalized;
            //     }
            //     geometry0 = geometry;
            // }, threeAttributeMap, attributeTypeMap);

            let geometry0: THREE.BufferGeometry;
            dracoLoader.decodeDracoFileInternal(bufferView, this.dracoDecoder, function (geometry: any) {
                for (let attributeName in geometry.attributes) {
                    let attribute = geometry.attributes[attributeName];
                    let normalized = attributeNormalizedMap[attributeName];
                    if (normalized !== undefined) attribute.normalized = normalized;
                }
                geometry0 = geometry;
            }, threeAttributeMap || {}, attributeTypeMap || {});

            return geometry0;

        };
    }

}