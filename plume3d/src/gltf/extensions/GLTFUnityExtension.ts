module plume.gltf {

    export type GLTFUnityExtraNodeExtension = {
        static?: boolean;
        activeSelf?: boolean;
        sprite?: GLTFSpriteNodeExtension;
    }

    // Not a regular extension
    // Exporter write node data in "extra" field
    // Data read here
    export class GLTFUnityExtension {

        parser: GLTFParser;

        constructor() {
        }

        applyToNode(node: THREE.Object3D, def: GLTFUnityExtraNodeExtension) {
            if (def.static != null) {
                if (def.static == true) {
                    node.matrixAutoUpdate = false;
                    node.updateMatrix();
                }
            }
            if (def.activeSelf != null) {
                if (def.activeSelf == false) {
                    node.visible = false;
                }
            }
            if (def.sprite != null) {
                let extension = this.parser.extensions.GB_sprites;
                let sprite = extension.getSprite(def.sprite.id);

                if (def.sprite.color != null) {
                    (sprite.material as any).color = new THREE.Color().setRGB(def.sprite.color[0], def.sprite.color[1], def.sprite.color[2])
                }

                (sprite.material as any).side = THREE.DoubleSide;

                node.add(sprite);

                this.parser._textures[def.sprite.id];
            }
        }

    }
}