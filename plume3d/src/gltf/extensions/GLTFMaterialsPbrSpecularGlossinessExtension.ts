module plume.gltf {

    export class GLTFMaterialsPbrSpecularGlossinessExtension {

        name: ExtensionsType;
        specularGlossinessParams = [
            'color',
            'map',
            'lightMap',
            'lightMapIntensity',
            'aoMap',
            'aoMapIntensity',
            'emissive',
            'emissiveIntensity',
            'emissiveMap',
            'bumpMap',
            'bumpScale',
            'normalMap',
            'displacementMap',
            'displacementScale',
            'displacementBias',
            'specularMap',
            'specular',
            'glossinessMap',
            'glossiness',
            'alphaMap',
            'envMap',
            'envMapIntensity',
            'refractionRatio',
        ];

        constructor() {
            this.name = "KHR_materials_pbrSpecularGlossiness";
        }


        getMaterialType(materialDef: gltfspec.Material) {
            return THREE.ShaderMaterial;
        }

        extendParams(params: any, material: gltfspec.Material, parser: GLTFParser) {

            let pbrSpecularGlossiness = material.extensions[this.name];
            let shader = THREE.ShaderLib['standard'];
            let uniforms = THREE.UniformsUtils.clone(shader.uniforms);

            let specularMapParsFragmentChunk = [
                '#ifdef USE_SPECULARMAP',
                '	uniform sampler2D specularMap;',
                '#endif'
            ].join('\n');

            let glossinessMapParsFragmentChunk = [
                '#ifdef USE_GLOSSINESSMAP',
                '	uniform sampler2D glossinessMap;',
                '#endif'
            ].join('\n');

            let specularMapFragmentChunk = [
                'vec3 specularFactor = specular;',
                '#ifdef USE_SPECULARMAP',
                '	vec4 texelSpecular = texture2D( specularMap, vUv );',
                '	texelSpecular = sRGBToLinear( texelSpecular );',
                '	// reads channel RGB, compatible with a glTF Specular-Glossiness (RGBA) texture',
                '	specularFactor *= texelSpecular.rgb;',
                '#endif'
            ].join('\n');

            let glossinessMapFragmentChunk = [
                'float glossinessFactor = glossiness;',
                '#ifdef USE_GLOSSINESSMAP',
                '	vec4 texelGlossiness = texture2D( glossinessMap, vUv );',
                '	// reads channel A, compatible with a glTF Specular-Glossiness (RGBA) texture',
                '	glossinessFactor *= texelGlossiness.a;',
                '#endif'
            ].join('\n');

            let lightPhysicalFragmentChunk = [
                'PhysicalMaterial material;',
                'material.diffuseColor = diffuseColor.rgb;',
                'material.specularRoughness = clamp( 1.0 - glossinessFactor, 0.04, 1.0 );',
                'material.specularColor = specularFactor.rgb;',
            ].join('\n');

            let fragmentShader = shader.fragmentShader
                .replace('uniform float roughness;', 'uniform vec3 specular;')
                .replace('uniform float metalness;', 'uniform float glossiness;')
                .replace('#include <roughnessmap_pars_fragment>', specularMapParsFragmentChunk)
                .replace('#include <metalnessmap_pars_fragment>', glossinessMapParsFragmentChunk)
                .replace('#include <roughnessmap_fragment>', specularMapFragmentChunk)
                .replace('#include <metalnessmap_fragment>', glossinessMapFragmentChunk)
                .replace('#include <lights_physical_fragment>', lightPhysicalFragmentChunk);

            delete uniforms.roughness;
            delete uniforms.metalness;
            delete uniforms.roughnessMap;
            delete uniforms.metalnessMap;

            uniforms.specular = { value: new THREE.Color().setHex(0x111111) };
            uniforms.glossiness = { value: 0.5 };
            uniforms.specularMap = { value: null };
            uniforms.glossinessMap = { value: null };

            params.vertexShader = shader.vertexShader;
            params.fragmentShader = fragmentShader;
            params.uniforms = uniforms;
            params.defines = { 'STANDARD': '' };

            params.color = new THREE.Color(1.0, 1.0, 1.0);
            params.opacity = 1.0;

            if (Array.isArray(pbrSpecularGlossiness.diffuseFactor)) {
                let array = pbrSpecularGlossiness.diffuseFactor;
                params.color.fromArray(array);
                params.opacity = array[3];
            }

            if (pbrSpecularGlossiness.diffuseTexture !== undefined) {
                parser.assignTexture(params, 'map', pbrSpecularGlossiness.diffuseTexture.index);
            }

            params.emissive = new THREE.Color(0.0, 0.0, 0.0);
            params.glossiness = pbrSpecularGlossiness.glossinessFactor !== undefined ? pbrSpecularGlossiness.glossinessFactor : 1.0;
            params.specular = new THREE.Color(1.0, 1.0, 1.0);

            if (Array.isArray(pbrSpecularGlossiness.specularFactor)) {
                params.specular.fromArray(pbrSpecularGlossiness.specularFactor);
            }

            if (pbrSpecularGlossiness.specularGlossinessTexture !== undefined) {
                let specGlossIndex = pbrSpecularGlossiness.specularGlossinessTexture.index;
                parser.assignTexture(params, 'glossinessMap', specGlossIndex);
                parser.assignTexture(params, 'specularMap', specGlossIndex);
            }
        }

        createMaterial(params: any) {
            // setup material properties based on MeshStandardMaterial for Specular-Glossiness
            let mat = new THREE.ShaderMaterial({
                defines: params.defines,
                vertexShader: params.vertexShader,
                fragmentShader: params.fragmentShader,
                uniforms: params.uniforms,
                fog: true,
                lights: true,
                opacity: params.opacity,
                transparent: params.transparent
            });

            let material = mat as any;
            material.isGLTFSpecularGlossinessMaterial = true;

            material.color = params.color;

            material.map = params.map === undefined ? null : params.map;

            material.lightMap = null;
            material.lightMapIntensity = 1.0;

            material.aoMap = params.aoMap === undefined ? null : params.aoMap;
            material.aoMapIntensity = 1.0;

            material.emissive = params.emissive;
            material.emissiveIntensity = 1.0;
            material.emissiveMap = params.emissiveMap === undefined ? null : params.emissiveMap;

            material.bumpMap = params.bumpMap === undefined ? null : params.bumpMap;
            material.bumpScale = 1;

            material.normalMap = params.normalMap === undefined ? null : params.normalMap;
            if (params.normalScale) material.normalScale = params.normalScale;

            material.displacementMap = null;
            material.displacementScale = 1;
            material.displacementBias = 0;

            material.specularMap = params.specularMap === undefined ? null : params.specularMap;
            material.specular = params.specular;

            material.glossinessMap = params.glossinessMap === undefined ? null : params.glossinessMap;
            material.glossiness = params.glossiness;

            material.alphaMap = null;

            material.envMap = params.envMap === undefined ? null : params.envMap;
            material.envMapIntensity = 1.0;

            material.refractionRatio = 0.98;

            material.extensions.derivatives = true;

            return material;
        }

        /**
         * Clones a GLTFSpecularGlossinessMaterial instance. The ShaderMaterial.copy() method can
         * copy only properties it knows about or inherits, and misses many properties that would
         * normally be defined by MeshStandardMaterial.
         *
         * This method allows GLTFSpecularGlossinessMaterials to be cloned in the process of
         * loading a glTF model, but cloning later (e.g. by the user) would require these changes
         * AND also updating `.onBeforeRender` on the parent mesh.
         *
         * @param  {THREE.ShaderMaterial} source
         * @return {THREE.ShaderMaterial}
         */
        cloneMaterial(source: THREE.ShaderMaterial): THREE.ShaderMaterial {

            let target = source.clone();
            target["isGLTFSpecularGlossinessMaterial"] = true;

            let params = this.specularGlossinessParams;
            for (let i = 0, il = params.length; i < il; i++) {
                target[params[i]] = source[params[i]];
            }
            return target;
        }

        // Here's based on refreshUniformsCommon() and refreshUniformsStandard() in WebGLRenderer.
        refreshUniforms(renderer: THREE.WebGLRenderer, scene: THREE.Scene, camera: THREE.Camera, geometry: THREE.Geometry, material: any, group: THREE.Group) {

            if (material["isGLTFSpecularGlossinessMaterial"] !== true) {
                return;
            }

            let uniforms = material.uniforms;
            let defines = material.defines;

            uniforms.opacity.value = material.opacity;

            uniforms.diffuse.value.copy(material.color);
            uniforms.emissive.value.copy(material.emissive).multiplyScalar(material.emissiveIntensity);

            uniforms.map.value = material.map;
            uniforms.specularMap.value = material.specularMap;
            uniforms.alphaMap.value = material.alphaMap;

            uniforms.lightMap.value = material.lightMap;
            uniforms.lightMapIntensity.value = material.lightMapIntensity;

            uniforms.aoMap.value = material.aoMap;
            uniforms.aoMapIntensity.value = material.aoMapIntensity;

            // uv repeat and offset setting priorities
            // 1. color map
            // 2. specular map
            // 3. normal map
            // 4. bump map
            // 5. alpha map
            // 6. emissive map

            let uvScaleMap;
            if (material.map) {
                uvScaleMap = material.map;
            } else if (material.specularMap) {
                uvScaleMap = material.specularMap;
            } else if (material.displacementMap) {
                uvScaleMap = material.displacementMap;
            } else if (material.normalMap) {
                uvScaleMap = material.normalMap;
            } else if (material.bumpMap) {
                uvScaleMap = material.bumpMap;
            } else if (material.glossinessMap) {
                uvScaleMap = material.glossinessMap;
            } else if (material.alphaMap) {
                uvScaleMap = material.alphaMap;
            } else if (material.emissiveMap) {
                uvScaleMap = material.emissiveMap;
            }

            if (uvScaleMap !== undefined) {
                // backwards compatibility
                if (uvScaleMap.isWebGLRenderTarget) {
                    uvScaleMap = uvScaleMap.texture;
                }
                if (uvScaleMap.matrixAutoUpdate === true) {
                    uvScaleMap.updateMatrix();
                }
                uniforms.uvTransform.value.copy(uvScaleMap.matrix);
            }

            uniforms.envMap.value = material.envMap;
            uniforms.envMapIntensity.value = material.envMapIntensity;
            uniforms.flipEnvMap.value = (material.envMap && material.envMap.isCubeTexture) ? - 1 : 1;

            uniforms.refractionRatio.value = material.refractionRatio;

            uniforms.specular.value.copy(material.specular);
            uniforms.glossiness.value = material.glossiness;

            uniforms.glossinessMap.value = material.glossinessMap;

            uniforms.emissiveMap.value = material.emissiveMap;
            uniforms.bumpMap.value = material.bumpMap;
            uniforms.normalMap.value = material.normalMap;

            uniforms.displacementMap.value = material.displacementMap;
            uniforms.displacementScale.value = material.displacementScale;
            uniforms.displacementBias.value = material.displacementBias;

            if (uniforms.glossinessMap.value !== null && defines.USE_GLOSSINESSMAP === undefined) {
                defines.USE_GLOSSINESSMAP = '';
                // set USE_ROUGHNESSMAP to enable vUv
                defines.USE_ROUGHNESSMAP = '';
            }

            if (uniforms.glossinessMap.value === null && defines.USE_GLOSSINESSMAP !== undefined) {
                delete defines.USE_GLOSSINESSMAP;
                delete defines.USE_ROUGHNESSMAP;
            }
        }

    }
}