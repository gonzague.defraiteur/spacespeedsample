module plume.gltf {

    export type GLTFSpriteRootExtension = {
        texture: number;
    }
    export type GLTFSpriteNodeExtension = {
        id: number;
        color?: Array<number>;
    }

    export class GLTFSpritesExtension {

        parser: GLTFParser;

        name: ExtensionsType;
        spritesDef: Array<GLTFSpriteRootExtension>;

        // sprites: Array<THREE.Sprite> = [];
        sprites: Array<THREE.Mesh> = [];

        constructor(json: gltfspec.GlTf) {
            this.name = "GB_sprites";
            this.spritesDef = [];

            let extension = (json.extensions && json.extensions.GB_sprites) || {};
            let spritesDefs = extension.sprites || [];

            for (let i = 0; i < spritesDefs.length; i++) {
                let spriteDef: GLTFSpriteRootExtension = spritesDefs[i];
                this.spritesDef.push(spriteDef);
            }
        }

        getSprite(idx: number) {
            let s = this.sprites[idx];
            if (s != null) return s.clone();

            let def = this.spritesDef[idx];
            let texture = this.parser._textures[def.texture];
            let material = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
            s = new THREE.Mesh(new THREE.PlaneBufferGeometry(), material);
            this.sprites[idx] = s;

            return s.clone();
        }
    }
}