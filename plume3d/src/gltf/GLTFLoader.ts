/// <reference path="../../build/latest/plume3d.core.d.ts" />

module plume.gltf {

    type MaterialType = (typeof THREE.ShaderMaterial) | (typeof THREE.MeshStandardMaterial) | (typeof THREE.MeshBasicMaterial);

    export type GLTFElementFactory = {
        materialFactory?: (type: MaterialType, params: any) => THREE.Material;
        geometryProcessor?: (geometry: THREE.BufferGeometry) => THREE.BufferGeometry;
        materialProcessor?: (material: THREE.Material) => THREE.Material;
    }

    export class GLTFLoader {

        dracoLoader: any;
        crossOrigin = "anonymous";
        path: string;

        factory: GLTFElementFactory;

        constructor(public manager: THREE.LoadingManager, public textureLoaderManager: plume.TextureLoaderManager) {
            if (manager == null) this.manager = THREE.DefaultLoadingManager;
            this.dracoLoader = null;

            let dracoloader = THREE["DRACOLoader"];
            if (dracoloader) {
                dracoloader.setDecoderPath(Loader3d.dracoLib);
                this.dracoLoader = new dracoloader();
            }
        }


        load(url: string, onLoad: (gltf: GltfObject) => void, onProgress?: (request: ProgressEvent) => void, onError?: (event: ErrorEvent) => void) {

            let scope = this;
            let path = this.path !== undefined ? this.path : THREE.LoaderUtils.extractUrlBase(url);

            // Tells the LoadingManager to track an extra item, which resolves after
            // the model is fully loaded. This means the count of items loaded will
            // be incorrect, but ensures manager.onLoad() does not fire early.
            scope.manager.itemStart(url);

            let _onError = function (e: ErrorEvent) {
                if (onError) {
                    onError(e);
                } else {
                    logger.error(e);
                }
                scope.manager.itemEnd(url);
                scope.manager.itemError(url);
            };

            let loader = new THREE.FileLoader(scope.manager);
            loader.setResponseType('arraybuffer');
            loader.load(url, function (data) {
                try {
                    scope.parse(data, path, function (gltf: GltfObject) {
                        onLoad(gltf);
                        scope.manager.itemEnd(url);
                    }, _onError);
                } catch (e) {
                    _onError(e);
                }
            }, onProgress, _onError);
        }

        setCrossOrigin(value: string) {
            this.crossOrigin = value;
            return this;
        }

        setPath(value: string) {
            this.path = value;
            return this;
        }

        setDRACOLoader(dracoLoader: any) {
            this.dracoLoader = dracoLoader;
            return this;
        }

        parse(data: ArrayBuffer | string, path: string, onLoad: (gltf: GltfObject) => void, onError?: (event: ErrorEvent | Error) => void) {

            let content: string;
            let extensions: Extensions = {};

            if (typeof data === 'string') {
                content = data;
            } else {
                let magic = THREE.LoaderUtils.decodeText(new Uint8Array(data, 0, 4));
                if (magic === BINARY_EXTENSION_HEADER_MAGIC) {
                    try {
                        extensions.KHR_binary_glTF = new GLTFBinaryExtension(data);
                    } catch (error) {
                        if (onError) onError(error);
                        return;
                    }
                    content = extensions.KHR_binary_glTF.content;
                } else {
                    content = THREE.LoaderUtils.decodeText(new Uint8Array(data));
                }
            }

            let json: gltfspec.GlTf = JSON.parse(content);
            if (json.asset === undefined || parseInt(json.asset.version[0]) < 2) {
                if (onError) onError(new Error('Unsupported asset. glTF versions >=2.0 are supported. Use LegacyGLTFLoader instead.'));
                return;
            }

            if (json.extensionsUsed) {
                for (let i = 0; i < json.extensionsUsed.length; ++i) {
                    let extensionName = json.extensionsUsed[i];
                    let extensionsRequired = json.extensionsRequired || [];
                    switch (extensionName) {
                        case "KHR_lights_punctual":
                            extensions.KHR_lights_punctual = new GLTFLightsExtension(json);
                            break;
                        case "KHR_materials_unlit":
                            extensions.KHR_materials_unlit = new GLTFMaterialsUnlitExtension(json);
                            break;
                        case "KHR_materials_pbrSpecularGlossiness":
                            extensions.KHR_materials_pbrSpecularGlossiness = new GLTFMaterialsPbrSpecularGlossinessExtension();
                            break;
                        case "KHR_draco_mesh_compression":
                            extensions.KHR_draco_mesh_compression = new GLTFDracoMeshCompressionExtension(json, this.dracoLoader);
                            break;
                        case "MSFT_texture_dds":
                            extensions.MSFT_texture_dds = new GLTFTextureDDSExtension();
                            break;
                        case "GB_lightmaps":
                            extensions.GB_lightmaps = new GLTFLightMapsExtension(json);
                            break;
                        case "GB_sprites":
                            extensions.GB_sprites = new GLTFSpritesExtension(json);
                            break;
                        case "EXT_texture_transform":
                            extensions.EXT_texture_transform = new GLTFTextureTransformExtension();
                            break;
                        default:
                            if (extensionsRequired.indexOf(extensionName) >= 0) {
                                console.warn('Unknown extension "' + extensionName + '".');
                            }
                    }
                }
            }

            extensions.GB_Unity = new GLTFUnityExtension();

            let parser = new GLTFParser(json, extensions, {
                path: path || this.path || '',
                crossOrigin: this.crossOrigin,
                manager: this.manager,
                textureLoaderManager: this.textureLoaderManager,
                factory: this.factory,
            });

            if (extensions.GB_lightmaps) {
                extensions.GB_lightmaps.parser = parser;
            }
            if (extensions.GB_sprites) {
                extensions.GB_sprites.parser = parser;
            }
            if (extensions.GB_Unity) {
                extensions.GB_Unity.parser = parser;
            }

            parser.parse(function (glTF: GltfObject) {
                // let glTF: GltfObject = {
                //     scene: scene,
                //     scenes: scenes,
                //     cameras: cameras,
                //     animations: animations,
                //     asset: json.asset,
                //     parser: parser,
                //     userData: {}
                // };
                // addUnknownExtensionsToUserData(extensions, glTF, json);
                onLoad(glTF);
            }, onError);
        }
    }

}