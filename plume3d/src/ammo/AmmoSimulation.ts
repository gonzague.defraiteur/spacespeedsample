/// <reference path="../../build/latest/plume3d.core.d.ts" />
/// <reference path="../../vendor/ammo.d.ts" />

module plume {


    //
    // Ammo module init
    //
    let INITDONE = false;
    function initAmmo(ready: () => void) {
        if (INITDONE) {
            ready();
            return;
        }

        // var Module = { TOTAL_MEMORY: 67108864 * 2 };
        let option: any = {};

        // If 'auto' try to load wabassembly version
        if (typeof window["WebAssembly"] !== 'object' || AmmoSimulation.ammoLibType === 'js') {
            // Load js version
            let jsUrl = AmmoSimulation.ammoLibPath + "ammo.js";
            http.loadJs(jsUrl, function () {
                let ammoModule = window["Ammo"];
                ammoModule(option).then(function () {
                    logger.debug("Ammo ready");
                    INITDONE = true;
                    ready();
                });
            });
        } else {
            // Load wasm version
            let jsUrl = AmmoSimulation.ammoLibPath + "ammo.wasm.js";
            let wasmUrl = AmmoSimulation.ammoLibPath + "ammo.wasm.wasm";
            http.loadJs(jsUrl, function () {
                logger.debug("Loading Ammo wasm")
                http.loadArrayBuffer(wasmUrl, function (data) {
                    let ammoModule = window["Ammo"];
                    option.wasmBinary = data;
                    ammoModule(option).then(function () {
                        logger.debug("Ammo ready");
                        INITDONE = true;
                        ready();
                    });
                });
            });
        }
    }


    export class AmmoSimulation extends SimulationSupport {

        public static ammoLibPath = 'build/lib/ammo/';
        public static ammoLibType = 'auto';

        static readonly DISABLE_DEACTIVATION = 4;

        // to check http://www.bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_The_World
        optFixedTimeStep = 1 / 60;
        optMaxSubSteps = 10;

        readyHandlers = new Handler<boolean>();

        private _ready = false;
        private _physicsWorld: Ammo.btDiscreteDynamicsWorld;

        private _dynamicBodies: Array<Ammo.btRigidBody> = []; // only used to track idx
        private _dynamicBodiesData: Array<DynamicBodyDataHolder> = [];
        private _bodies: Array<Ammo.btRigidBody> = [];

        private _pointer = 0;
        private _customDataByBody = new HashMap<any>();

        private _internalTickCallback: () => void;
        private _internalTickCbRegistered = false;
        private _contactAddedCallback: AmmoContactCallback;
        private _contactAddedCallbackRegistered = false;
        private _contactProcessedCallback: AmmoContactCallback;
        private _contactProcessedCallbackRegistered = false;
        private _contactDestroyedCallback: (userdata: any) => void;
        private _contactDestroyedCallbackRegistered = false;

        private _stats: SimulationStats & { _timeTotal: number; _flush: number };

        private _TRANSFORM_AUX: Ammo.btTransform; // matrix re-used

        constructor() {
            super();

            let self = this;

            this._stats = { avg: 0, _timeTotal: 0, _flush: 0 };

            initAmmo(function () {
                self._ready = true;
                self._initOnReady();
                self.readyHandlers.fire(true);
            });
        }

        private _initOnReady() {
            // Physics configuration
            let collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
            let dispatcher = new Ammo.btCollisionDispatcher(collisionConfiguration);
            let broadphase = new Ammo.btDbvtBroadphase();
            let solver = new Ammo.btSequentialImpulseConstraintSolver();
            this._physicsWorld = new Ammo.btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
            this._physicsWorld.setGravity(new Ammo.btVector3(0, -10, 0));

            this._TRANSFORM_AUX = new Ammo.btTransform();

            this._stats._flush = performance.now();
            this._stats._timeTotal = 0;
        }

        private _syncMesh() {
            for (let i = 0; i < this._dynamicBodiesData.length; i++) {
                let data = this._dynamicBodiesData[i];
                let body = data.rigidbody;
                let mesh = data.mesh;
                let ms = body.getMotionState();
                if (ms) {
                    ms.getWorldTransform(this._TRANSFORM_AUX);
                    if (data.syncPosition) {
                        let p = this._TRANSFORM_AUX.getOrigin();
                        mesh.position.set(p.x(), p.y(), p.z());
                    }
                    if (data.syncRotation) {
                        let q = this._TRANSFORM_AUX.getRotation();
                        mesh.quaternion.set(q.x(), q.y(), q.z(), q.w());
                    }
                }
            }
        }

        isReady(): boolean {
            return this._ready;
        }

        update(dt: number) {
            super.update(dt);

            if (!this._ready) return;
            if (!this._running) return;

            let sEllapsed = dt / 1000; // delta need to be in seconds
            let now = performance.now();

            this._physicsWorld.stepSimulation(sEllapsed, this.optMaxSubSteps, this.optFixedTimeStep);
            this._syncMesh();

            let after = performance.now();

            let statsEllapsed = (now - this._stats._flush);
            if (statsEllapsed >= 1000) {
                this._stats.avg = Math.round((this._stats._timeTotal / statsEllapsed) * 100) / 100;
                this._stats._flush = now;
                this._stats._timeTotal = 0;
            }
            this._stats._timeTotal += (after - now);
        }

        get stats(): SimulationStats {
            return this._stats;
        }

        setGravity(x: number, y: number, z: number) {
            this._physicsWorld.setGravity(new Ammo.btVector3(x, y, z));
        }

        protected onStart() {
        }

        protected onStop() {
        }

        getWorld(): Ammo.btDiscreteDynamicsWorld {
            return this._physicsWorld;
		}
		
		static newTriMeshShape(geometry: THREE.BufferGeometry) {
            let triMesh = new Ammo.btTriangleMesh(true, false);

            let unindexed: THREE.BufferGeometry;
            if (geometry.index) {
                unindexed = geometry.toNonIndexed();
            } else {
                unindexed = geometry;
            }

            let vertices = unindexed.getAttribute("position").array as Array<number>;
            for (let i = 0; i < vertices.length; i += 9) {
                let v0 = new Ammo.btVector3(vertices[i + 0], vertices[i + 1], vertices[i + 2]);
                let v1 = new Ammo.btVector3(vertices[i + 3], vertices[i + 4], vertices[i + 5]);
                let v2 = new Ammo.btVector3(vertices[i + 6], vertices[i + 7], vertices[i + 8]);
                triMesh.addTriangle(v0, v1, v2, false);
            }

            let shape = new Ammo.btBvhTriangleMeshShape(triMesh, false);
            return shape;
        }

        updatePositionAndRotation(body: Ammo.btRigidBody, position?: THREE.Vector3, rotation?: THREE.Quaternion, scaling?: THREE.Vector3) {
            let ms = body.getMotionState();
            let cs = body.getCollisionShape();
            if (cs != null && scaling != null) {
                let locals = cs.getLocalScaling();
                if(locals.x() != scaling.x || locals.y() != scaling.y || locals.z() != scaling.z) {
                    cs.setLocalScaling(new Ammo.btVector3(scaling.x, scaling.y, scaling.z));
                }
            }
            if (ms) {
                ms.getWorldTransform(this._TRANSFORM_AUX);
                let p = this._TRANSFORM_AUX.getOrigin();
                let q = this._TRANSFORM_AUX.getRotation();

                if (position != null) {
                    p.setX(position.x);
                    p.setY(position.y);
                    p.setZ(position.z);
                }
                if (rotation != null) {
                    q.setValue(rotation.x, rotation.y, rotation.z, rotation.w);
                }
                this._TRANSFORM_AUX.setOrigin(p);
                this._TRANSFORM_AUX.setRotation(q);
                ms.setWorldTransform(this._TRANSFORM_AUX);
                body.setMotionState(ms);
            }
        }

        addRigidBody(body: Ammo.btRigidBody, mesh: THREE.Object3D, dynamic: boolean, group?: number, mask?: number) {
            if (mesh != null) {
                // initialize position and rotation body from mesh
                this.updatePositionAndRotation(body, mesh.position, mesh.quaternion, mesh.scale);
            }

            if (dynamic) {
                // add body to sync list
                this._dynamicBodies.push(body);
                this._dynamicBodiesData.push({
                    rigidbody: body,
                    mesh: mesh,
                    syncPosition: true,
                    syncRotation: true,
                });
            }

            this._bodies.push(body);

            this._physicsWorld.addRigidBody(body, group, mask);

            // logger.debug("dynamic bodies: " + this._dynamicBodies.length);
        }

        removeRigidBody(body: Ammo.btRigidBody) {
            let idx = this._dynamicBodies.indexOf(body);
            if (idx >= 0) {
                this._dynamicBodies.splice(idx, 1);
                this._dynamicBodiesData.splice(idx, 1);
            }

            idx = this._bodies.indexOf(body);
            if (idx >= 0) {
                this._bodies.splice(idx, 1);
            } else {
                // logger.warn("Body not registered");
            }

            this._physicsWorld.removeRigidBody(body);
        }

        setDynamicBodySyncProperty(body: Ammo.btRigidBody, syncPosition: boolean, syncRotation: boolean) {
            let idx = this._dynamicBodies.indexOf(body);
            let data = this._dynamicBodiesData[idx];
            data.syncPosition = syncPosition;
            data.syncRotation = syncRotation;
        }

        getRigidBodies(): Array<Ammo.btRigidBody> {
            return this._bodies;
        }

        removeRigidBodies() {
            for (let i = this._bodies.length - 1; i >= 0; i++) {
                let b = this._bodies[i];
                this.removeRigidBody(b);
            }
        }

        setCustomData(body: Ammo.btRigidBody, data: any) {
            let ptr: number = body.getUserIndex();
            if (ptr == 0 || ptr == null) {
                this._pointer++;
                ptr = this._pointer;
                body.setUserIndex(this._pointer);
            }
            this._customDataByBody.put(ptr + "", data);
        }
        getCustomData(body: Ammo.btRigidBody) {
            let ptr: number = body.getUserIndex();
            if (ptr == 0 || ptr == null) {
                return null;
            }
            let data = this._customDataByBody.get(ptr + "");
            return data;
        }

        // getCollisions() {
        //     return this._collisions;
        // }

        // checkCollisions() {

        //     if (this._collisions.length > 0) {
        //         this._collisions = [];
        //     }

        //     let dispatcher = this._physicsWorld.getDispatcher();
        //     let numManifolds = dispatcher.getNumManifolds();

        //     for (let i = 0; i < numManifolds; i++) {
        //         let manifold = dispatcher.getManifoldByIndexInternal(i);
        //         let numContacts = manifold.getNumContacts();
        //         // if (numContacts <= 0) {
        //         //     continue;
        //         // }

        //         let body0 = manifold.getBody0() as Ammo.btRigidBody;
        //         let body1 = manifold.getBody1() as Ammo.btRigidBody;
        //         let uptr0 = body0.getUserIndex();
        //         let uptr1 = body1.getUserIndex();

        //         console.log("Contact: " + uptr0 + " vs " + uptr1);

        //         let bc: BodyContact = {
        //             body0: body0,
        //             body1: body1,
        //             data0: (uptr0 != 0 ? this.getCustomData(body0) : null),
        //             data1: (uptr1 != 0 ? this.getCustomData(body1) : null),
        //             contacts: []
        //         }

        //         for (let p = 0; p < numContacts; p++) {
        //             let cp = manifold.getContactPoint(p);
        //             bc.contacts.push({
        //                 distance: cp.getDistance(),
        //             });
        //             console.log("M" + i + ", CP" + p + ": distance: " + cp.getDistance());
        //         }

        //         this._collisions.push(bc);
        //     }
        // }


        setInternalTickCallback(fn: () => void) {
            if (!this._internalTickCbRegistered) {
                // Register function pointer only once
                let tickCallbackPointer = Ammo["addFunction"](this._onInternalTickCallback.bind(this));
                this._physicsWorld.setInternalTickCallbackPtr(tickCallbackPointer, null, false);
                this._internalTickCbRegistered = true;
            }
            this._internalTickCallback = fn;
        }

        setContactAddedCallback(fn: AmmoContactCallback) {
            if (!this._contactAddedCallbackRegistered) {
                let pointer = Ammo["addFunction"](this._onContactAdded.bind(this));
                this._physicsWorld.setContactAddedCallback(pointer);
                this._contactAddedCallbackRegistered = true;
            }
            this._contactAddedCallback = fn;
        }
        setContactProcessedCallback(fn: AmmoContactCallback) {
            if (!this._contactProcessedCallbackRegistered) {
                let pointer = Ammo["addFunction"](this._onContactProcessed.bind(this));
                this._physicsWorld.setContactProcessedCallback(pointer);
                this._contactProcessedCallbackRegistered = true;
            }
            this._contactProcessedCallback = fn;
        }
        setcontactDestroyedCallback(fn: (userdata: any) => void) {
            if (!this._contactDestroyedCallbackRegistered) {
                let pointer = Ammo["addFunction"](this._onContactDestroyed.bind(this));
                this._physicsWorld.setContactDestroyedCallback(pointer);
                this._contactDestroyedCallbackRegistered = true;
            }
            this._contactDestroyedCallback = fn;
        }

        wrapPointer<T>(ptr: number, clazz: { new(...args: any[]): T }) {
            let v = Ammo["wrapPointer"](ptr, clazz);
            return v as T;
        }

        private _onInternalTickCallback(arg0: any, arg1: any) {
            if (this._internalTickCallback != null) this._internalTickCallback();
        }
        private _onContactAdded(cp: number, colObj0: number, partId0: any, index0: any, colObj1: number, partId1: any, index1: any) {
            if (this._contactAddedCallback != null) {
                let obj0 = this.wrapPointer(colObj0, Ammo.btRigidBody);
                let obj1 = this.wrapPointer(colObj1, Ammo.btRigidBody);
                let contactPoint = this.wrapPointer(cp, Ammo.btManifoldPoint);
                this._contactAddedCallback(obj0, obj1, contactPoint);
            }
        }
        private _onContactProcessed(cp: any, body0: any, body1: any) {
            if (this._contactProcessedCallback != null) {
                let obj0 = this.wrapPointer(body0, Ammo.btRigidBody);
                let obj1 = this.wrapPointer(body1, Ammo.btRigidBody);
                let contactPoint = this.wrapPointer(cp, Ammo.btManifoldPoint);
                this._contactProcessedCallback(obj0, obj1, contactPoint);
            }
        }
        private _onContactDestroyed(arg0: any) {
            if (this._contactDestroyedCallback != null) this._contactDestroyedCallback(arg0);
        }
    }

    // export type ContactPoint = {
    //     distance: number;
    // }

    // export type BodyContact = {
    //     body0: Ammo.btRigidBody;
    //     body1: Ammo.btRigidBody;
    //     data0: any;
    //     data1: any;
    //     contacts: Array<ContactPoint>;
    // }


    type DynamicBodyDataHolder = {
        rigidbody: Ammo.btRigidBody,
        mesh: THREE.Object3D,
        syncPosition: boolean,
        syncRotation: boolean
    }
}