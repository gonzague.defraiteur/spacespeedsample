module plume {

    export type AmmoContactCallback = (body0: Ammo.btRigidBody, body1: Ammo.btRigidBody, contactPoint: Ammo.btManifoldPoint) => void;

    export const enum AmmoCollisionFlags {
        CF_STATIC_OBJECT = 1,
        CF_KINEMATIC_OBJECT = 2,
        CF_NO_CONTACT_RESPONSE = 4,
        CF_CUSTOM_MATERIAL_CALLBACK = 8,//this allows per-triangle material (friction/restitution)
        CF_CHARACTER_OBJECT = 16,
        CF_DISABLE_VISUALIZE_OBJECT = 32, //disable debug drawing
        CF_DISABLE_SPU_COLLISION_PROCESSING = 64//disable parallel/SPU processing
    }

    export const enum AmmoCollisionFilterGroups {
        DefaultFilter = 1,
        StaticFilter = 2,
        KinematicFilter = 4,
        DebrisFilter = 8,
        SensorTrigger = 16,
        CharacterFilter = 32,
        AllFilter = -1 //all bits sets: DefaultFilter | StaticFilter | KinematicFilter | DebrisFilter | SensorTrigger
    };
}