module plume {

    export class AmmoBodyBuilder {

        static newBodyFromMesh(mesh: THREE.Mesh, params?: BodyParameter) {
            let geometry = mesh.geometry;
            if (geometry instanceof THREE.BoxBufferGeometry) {
                geometry.computeBoundingBox();
                let bbox = geometry.boundingBox;
                let size = new THREE.Vector3();
                bbox.getSize(size);
                return this.newBox(size.x, size.y, size.z, params)
            } else if (geometry instanceof THREE.SphereBufferGeometry) {
                geometry.computeBoundingSphere();
                let s = geometry.boundingSphere;
                return this.newSphere(s.radius, params);
            }

            logger.warn("Unable to build physics body from mesh: " + mesh);
            return null;
        }

        static newBoxShape(w: number, h: number, d: number) {
            let geometry = new Ammo.btBoxShape(new Ammo.btVector3(w * 0.5, h * 0.5, d * 0.5));
            return geometry;
        }
        static newBox(w: number, h: number, d: number, params: BodyParameter = { mass: 0 }) {
            let geometry = this.newBoxShape(w, h, d);
            return this.newRigidBody(geometry, params);
        }

        static newSphere(radius: number, params: BodyParameter = { mass: 0 }) {
            let geometry = new Ammo.btSphereShape(radius);
            return this.newRigidBody(geometry, params);
        }

        static newPlane(normal: THREE.Vector3, distance: number) {
            let shape = new Ammo.btStaticPlaneShape(new Ammo.btVector3(normal.x, normal.y, normal.z), distance);
            return this.newRigidBody(shape);
        }

        static newTriMeshShape(geometry: THREE.BufferGeometry) {
            let triMesh = new Ammo.btTriangleMesh(true, false);

            let unindexed: THREE.BufferGeometry;
            if (geometry.index) {
                unindexed = geometry.toNonIndexed();
            } else {
                unindexed = geometry;
            }

            let vertices = unindexed.getAttribute("position").array as Array<number>;
            for (let i = 0; i < vertices.length; i += 9) {
                let v0 = new Ammo.btVector3(vertices[i + 0], vertices[i + 1], vertices[i + 2]);
                let v1 = new Ammo.btVector3(vertices[i + 3], vertices[i + 4], vertices[i + 5]);
                let v2 = new Ammo.btVector3(vertices[i + 6], vertices[i + 7], vertices[i + 8]);
                triMesh.addTriangle(v0, v1, v2, false);
            }

            let shape = new Ammo.btBvhTriangleMeshShape(triMesh, false);
            return shape;
        }

        static newTriMesh(geometry: THREE.BufferGeometry) {
            let shape = this.newTriMeshShape(geometry);
            return this.newRigidBody(shape);
        }

        static newRigidBody(collisionShape: Ammo.btCollisionShape, params: BodyParameter = { mass: 0 }) {
            let transform = new Ammo.btTransform();
            transform.setIdentity();
            //transform.setOrigin(new Ammo.btVector3(pos.x, pos.y, pos.z));
            //transform.setRotation(new Ammo.btQuaternion(quat.x, quat.y, quat.z, quat.w));
            let motionState = new Ammo.btDefaultMotionState(transform);
            let localInertia = new Ammo.btVector3(0, 0, 0);
            collisionShape.calculateLocalInertia(params.mass, localInertia);
            let rbInfo = new Ammo.btRigidBodyConstructionInfo(params.mass, motionState, collisionShape, localInertia);
            let body = new Ammo.btRigidBody(rbInfo);
            // body.setFriction(friction);
            return body;
        }

    }

}