#include "stdafx.h"
#include "assimploader.h"

static std::string GetFolder(const std::string& filename)
{
	size_t pos = filename.find_last_of('/');
	if (pos == std::string::npos)
	{
		pos = filename.find_last_of('\\');
	}

	std::string path = filename.substr(0, pos + 1);

	return path;
}

Mesh* AssimpLoader::InitMesh(const aiMesh* mesh, float factor)
{
	Mesh* m = new Mesh();

	m->name = mesh->mName.data;
	if (m->name.length() == 0)
	{
		m->name = std::string("Mesh") + std::to_string(meshCounter++);
	}

	for (int i = 0; i < result->materials.size(); i++)
	{
		if (m->name == result->materials[i]->name)
		{
			meshCounter++;
			m->name = std::string(mesh->mName.data) + std::to_string(meshCounter);
			i = -1;
		}
	}

	m->materialID = mesh->mMaterialIndex;

	unsigned int numVertices = mesh->mNumVertices;

	aiVector3D zero;
	zero.x = 0.0f;
	zero.y = 0.0f;
	zero.z = 0.0f;

	float maxd[3] = { -999.9f, -999.9f, -999.9f };
	float mind[3] = { 999.9f, 999.9f, 999.9f };

	for (unsigned int i = 0; i < numVertices; i++)
	{
		aiVector3D* vertex = &(mesh->mVertices[i]);
		aiVector3D* normal = mesh->HasNormals() ? &(mesh->mNormals[i]) : &zero;
		aiVector3D* texCoord = &mesh->mTextureCoords[0][i];

		m->vertices.push_back(Vector3(vertex->x * 0.01f * factor, -vertex->y * 0.01f * factor, vertex->z * 0.01f * factor));
		m->normals.push_back(Vector3(normal->x, normal->y, normal->z));

		
		mind[0] = (mind[0] < m->vertices[m->vertices.size() - 1].x) ? mind[0] : m->vertices[m->vertices.size() - 1].x;
		mind[1] = (mind[1] < m->vertices[m->vertices.size() - 1].y) ? mind[1] : m->vertices[m->vertices.size() - 1].y;
		mind[2] = (mind[2] < m->vertices[m->vertices.size() - 1].z) ? mind[2] : m->vertices[m->vertices.size() - 1].z;

		maxd[0] = (maxd[0] > m->vertices[m->vertices.size() - 1].x) ? maxd[0] : m->vertices[m->vertices.size() - 1].x;
		maxd[1] = (maxd[1] > m->vertices[m->vertices.size() - 1].y) ? maxd[1] : m->vertices[m->vertices.size() - 1].y;
		maxd[2] = (maxd[2] > m->vertices[m->vertices.size() - 1].z) ? maxd[2] : m->vertices[m->vertices.size() - 1].z;

		if (mesh->HasTextureCoords(0))
		{
			m->uv.push_back(Vector2(texCoord->x, 1.0f - texCoord->y));
		}
		else
		{
			m->uv.push_back(Vector2(0.0f, 0.0f));
		}
	}

	printf("%f %f %f\n", mind[0], mind[1], mind[2]);
	printf("%f %f %f\n", maxd[0], maxd[1], maxd[2]);

	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		const aiFace* f = &(mesh->mFaces[i]);

		m->indices.push_back(Triangle(f->mIndices[0], f->mIndices[1], f->mIndices[2]));
	}

	return m;
}

Material* AssimpLoader::InitMaterial(const aiMaterial* mat)
{
	Material* m = new Material();

	aiString matName;
	mat->Get(AI_MATKEY_NAME, matName);
	m->name = matName.data;

	for (int i = 0; i < result->materials.size(); i++)
	{
		if (m->name == result->materials[i]->name)
		{
			materialCounter++;
			m->name = std::string(matName.data) + std::to_string(materialCounter);
			i = -1;
		}
	}

	aiColor3D diffuse(0.0f, 0.0f, 0.0f);
	mat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
	m->diffuse.x = diffuse.r;
	m->diffuse.y = diffuse.g;
	m->diffuse.z = diffuse.b;

	aiColor3D specular(0.0f, 0.0f, 0.0f);
	mat->Get(AI_MATKEY_COLOR_SPECULAR, specular);
	m->specular.x = specular.r;
	m->specular.y = specular.g;
	m->specular.z = specular.b;

	aiColor3D ambient(0.0f, 0.0f, 0.0f);
	mat->Get(AI_MATKEY_COLOR_AMBIENT, ambient);
	m->ambient.x = ambient.r;
	m->ambient.y = ambient.g;
	m->ambient.z = ambient.b;

	aiColor3D emissive(0.0f, 0.0f, 0.0f);
	mat->Get(AI_MATKEY_COLOR_EMISSIVE, emissive);
	m->emissive.x = emissive.r;
	m->emissive.y = emissive.g;
	m->emissive.z = emissive.b;

	float shininess = 0.0;
	mat->Get(AI_MATKEY_SHININESS, shininess);
	m->shininess = shininess;

	if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0)
	{
		aiString assimpPath;
		mat->GetTexture(aiTextureType_DIFFUSE, 0, &assimpPath);
		m->texture = assimpPath.data;
	}
	else
	{
		m->texture = "";
	}

	return m;
}

/*void ProcessNode(const aiNode* node, int depth)
{
	aiVector3D scaling, rotation, position;
	node->mTransformation.Decompose(scaling, rotation, position);
	for (int i = 0; i < depth; i++)
	{
		printf("\t");
	}
	printf("(%f %f %f) (%f %f %f) (%f %f %f)\n", scaling.x, scaling.y, scaling.z, rotation.x, rotation.y, rotation.z, position.x, position.y, position.z);

	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		ProcessNode(node->mChildren[i], depth + 1);
	}
}*/

void AssimpLoader::LoadFromScene(const std::string& path, const aiScene* scene, float factor)
{
	int materials = scene->mNumMaterials;
	
	for (int i = 0; i < materials; i++)
	{
		Material* m = InitMaterial(scene->mMaterials[i]);
		result->materials.push_back(m);
	}

	//ProcessNode(scene->mRootNode, 0);

	int meshes = scene->mNumMeshes;

	for (int i = 0; i < meshes; i++)
	{
		Mesh* m = InitMesh(scene->mMeshes[i], factor);
		result->meshes.push_back(m);
	}
}

void AssimpLoader::Load(const std::string& filename)
{
	if (result)
	{
		delete result;
	}
	result = new Model();

	Assimp::Importer importer;
	/*const aiScene* scene = importer.ReadFile(filename.c_str(),
		aiProcess_Triangulate |
		aiProcess_GenNormals |
		aiProcess_FlipUVs);*/
	const aiScene* scene = importer.ReadFile(filename.c_str(),
		aiProcess_Triangulate |
		aiProcess_GenNormals |
		aiProcess_JoinIdenticalVertices |
		aiProcess_CalcTangentSpace |
		aiProcess_LimitBoneWeights |
		aiProcess_ImproveCacheLocality |
		aiProcess_FlipUVs |
		aiProcess_PreTransformVertices);

	if (scene != NULL)
	{
		double factor, origFactor;
		scene->mMetaData->Get("UnitScaleFactor", factor);
		scene->mMetaData->Get("OriginalUnitScaleFactor", origFactor); 
		printf("UnitScaleFactor %lf\nOriginalUnitScaleFactor %lf\n", factor, origFactor);

		std::string path = GetFolder(filename);
		LoadFromScene(path, scene, (float)factor);
	}
}

void AssimpLoader::SaveObj(const std::string& filename, const std::string& filenameMat)
{
	FILE* f = fopen(filename.c_str(), "wt");

	fprintf(f, "mtllib %s\n", filenameMat.c_str());

	int baseIndex = 1;
	int maxIndex = 0;

	for (int i = 0; i < result->meshes.size(); i++)
	{
		const Mesh* m = result->meshes[i];

		fprintf(f, "o %s\n", m->name.c_str());

		for (int j = 0; j < m->vertices.size(); j++)
		{
			fprintf(f, "v %f %f %f\n", m->vertices[j].x, m->vertices[j].y, m->vertices[j].z);
		}

		for (int j = 0; j < m->uv.size(); j++)
		{
			fprintf(f, "vt %f %f\n", m->uv[j].u, m->uv[j].v);
		}

		for (int j = 0; j < m->normals.size(); j++)
		{
			fprintf(f, "vn %f %f %f\n", m->normals[j].x, m->normals[j].y, m->normals[j].z);
		}

		fprintf(f, "usemtl %s\n", result->materials[m->materialID]->name.c_str());
		
		fprintf(f, "s Off\n");

		for (int j = 0; j < m->indices.size(); j++)
		{
			int a = m->indices[j].a + baseIndex;
			int b = m->indices[j].b + baseIndex;
			int c = m->indices[j].c + baseIndex;

			maxIndex = maxIndex > a ? maxIndex : a;
			maxIndex = maxIndex > b ? maxIndex : b;
			maxIndex = maxIndex > c ? maxIndex : c;

			fprintf(f, "f %d/%d/%d %d/%d/%d %d/%d/%d\n", a, a, a, b, b, b, c, c, c);
		}

		baseIndex = maxIndex + 1;

		fprintf(f, "\n");
	}

	fclose(f);
}

void AssimpLoader::SaveMtl(const std::string& filename)
{
	FILE* f = fopen(filename.c_str(), "wt");

	for (int i = 0; i < result->materials.size(); i++)
	{
		const Material* m = result->materials[i];

		fprintf(f, "newmtl %s\n", m->name.c_str());
		fprintf(f, "Ns %f\n", m->shininess);
		fprintf(f, "Ka %f %f %f\n", m->ambient.x, m->ambient.y, m->ambient.z);
		fprintf(f, "Kd %f %f %f\n", m->diffuse.x, m->diffuse.y, m->diffuse.z);
		fprintf(f, "Ks %f %f %f\n", m->specular.x, m->specular.y, m->specular.z);
		fprintf(f, "Ke %f %f %f\n", m->emissive.x, m->emissive.y, m->emissive.z);
		fprintf(f, "Ni %f\n", 1.0f);
		fprintf(f, "d %f\n", 1.0f);
		fprintf(f, "illum %d\n", 2);
		if (m->texture.length() > 0)
		{
			fprintf(f, "map_Kd %s\n", m->texture.c_str());
		}
		fprintf(f, "\n");
	}

	fclose(f);
}

void AssimpLoader::Save(const std::string& filename)
{
	SaveObj(filename + ".obj", filename + ".mtl");
	SaveMtl(filename + ".mtl");
}