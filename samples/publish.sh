#!/bin/bash

#
# Parse args
#
while :
do
    case "$1" in
	  
    --game  )
	  GAME="$2"
	  shift 2
	  ;;
	  	  
      -*)
	  echo "Error: Unknown option: $1" >&2
	  exit 1
	  ;;
      *)  # No more options
	  break
	  ;;
    esac
done

if [ -z ${GAME+x} ]; then
	echo "No target defined: use --game GAME"  
	exit 1
fi

#
# Extract game version
#
cd $GAME
GAME_VERSION=$(grep -Po '"gameVersion[" ]*:[ "]*\K[^",]*' publisher.json)
if [ -z ${GAME_VERSION+x} ]; then
	echo "Enable to read game version from publisher.json"  
	exit 1
fi

PUBLISHER=dummy
SHORT_NAME=$GAME
IS_LATEST=true
ARCHIVE_NAME=$SHORT_NAME-$GAME_VERSION.zip

#
# Upload
#
echo "Uploading: $ARCHIVE_NAME"

GAMES_FOLDER=/data/containers/nginx/games
SSHKEY=C:/git/infinity/build/nugplay-inside/google_compute_engine

REMOTE_GAME_TMP_PATH=/tmp/_uploaded_games/$PUBLISHER/$SHORT_NAME
REMOTE_WEBGAME_VERSION_TMP_PATH=${REMOTE_GAME_TMP_PATH}/webgame/${GAME_VERSION}
REMOTE_GAME_PATH=$GAMES_FOLDER/$SHORT_NAME/$PUBLISHER
REMOTE_WEBGAME_VERSION_PATH=${REMOTE_GAME_PATH}/webgame/${GAME_VERSION}
REMOTE_LATEST_WEBGAME_PATH=${REMOTE_GAME_PATH}/webgame/latest

# create folders
ssh -i ${SSHKEY} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@dev-snapgame.com \
"rm -rf  ${REMOTE_WEBGAME_VERSION_TMP_PATH} ; \
mkdir -p ${REMOTE_WEBGAME_VERSION_TMP_PATH}/game" \
|| exit 1

# copy release
scp -r -i ${SSHKEY} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no release/$ARCHIVE_NAME root@dev-snapgame.com:${REMOTE_WEBGAME_VERSION_TMP_PATH} || exit 1

#update webgame
ssh -i ${SSHKEY} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@dev-snapgame.com \
"mkdir -p ${REMOTE_GAME_PATH}; \
rm -rf ${REMOTE_WEBGAME_VERSION_PATH} ;\
mkdir -p ${REMOTE_WEBGAME_VERSION_PATH} ;\
unzip ${REMOTE_WEBGAME_VERSION_TMP_PATH}/$ARCHIVE_NAME -d ${REMOTE_WEBGAME_VERSION_PATH};\
if [ \"$IS_LATEST\" = true ] ; then\
    rm -r ${REMOTE_LATEST_WEBGAME_PATH};\
    cp -r ${REMOTE_WEBGAME_VERSION_PATH} ${REMOTE_LATEST_WEBGAME_PATH};\
fi"
