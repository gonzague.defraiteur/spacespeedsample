
module GAME {

	export class GameplayScene extends plume.GuiScene implements EntityContainer {
		entities: IGameEntity[] = [];
		parent?: EntityContainer;
		paused: boolean = false;
		minSpeed: number;
		maxSpeed: number;
		minColor: THREE.Color;
		middleColor: THREE.Color;
		maxColor: THREE.Color;

		Root(): EntityContainer {
			throw new Error("Method not implemented.");
		}
		private static INSTANCE: GameplayScene = null;
		static firstSession = true;
		public static get(): GameplayScene {
			if (this.INSTANCE == null) {
				this.INSTANCE = new GameplayScene();
			}
			return this.INSTANCE;
		}

		private gameplay: Gameplay;
		hud: Hud;
		protected started: boolean = false;

		constructor() {
			super();

			GameplayScene.INSTANCE = this;

			this.hud = new Hud();
			this.addChild(this.hud);
			this.gameplay = new Gameplay(this);
		}

		start() {
			this.minSpeed = 0.75 * Math.pow(0 / 200 + 1.0, 0 * 0.030);
			this.maxSpeed = 0.75 * Math.pow(85 / 200 + 1.0, 85 * 0.030);
			this.minColor = new THREE.Color(this.hud.minSpeedColor);
			this.middleColor = new THREE.Color(this.hud.middleSpeedColor);
			this.maxColor = new THREE.Color(this.hud.maxSpeedColor);
			if (GameplayScene.firstSession) {
				AdsHandler.fireReady();
				GameplayScene.firstSession = false;
				//this.gameplay.Start();
				//this.restart();
			}
		

			if (!this.started) {
				this.started = true;
				this.gameplay.Start();
			} else {
				this.restart();
			}
		}

		restart() {
			let newScene = new GameplayScene();
			Game.get().switchScene(newScene);
			newScene.start();
		}


		update(ts: number) {

			if (!this.started) {
				return;
			}
			if (!(this.paused)) {
				var multiplier = 0.75;
				var score = Gameplay.Main.score;
				if (score > 85) {
					score = 85;
				}
				multiplier *= Math.pow(score / 200 + 1.0, score * 0.030);
				
				var interpolation = ((multiplier - this.minSpeed) / (this.maxSpeed - this.minSpeed));
				//console.log("INTERPOLATION:" + interpolation);
				this.hud.speedText.text =""+ Math.round(100 * (interpolation + 1.0)) + "%";
				var interpolation2 = MathHelper.Clamp01(interpolation * 8.0);
				if (interpolation > 0.5)
				{
					var interpolation2 = MathHelper.Clamp01((interpolation - 0.5) * 3.0);
					this.hud.speedText.tint = new THREE.Color().copy(this.middleColor).lerp(this.maxColor, interpolation2).getHex();
				}
				else
				{
					var interpolation2 = MathHelper.Clamp01(interpolation * 8.0);
					this.hud.speedText.tint = new THREE.Color().copy(this.minColor).lerp(this.middleColor, interpolation2).getHex();
				}
				//console.log("min color hex:" + this.minColor.getHex());
				


				ts *= multiplier;
				super.update(ts);
				this.gameplay.Update(ts / 1000.0);
				this.gameplay.Render(0);
			}
			else
			{
				//console.error("IM PAUSED.")
			}
			this.hud.update(ts);
		}

		hide(cb: Function) {
			this.gameplay.Destroy();
			super.hide(cb);
		}

		onHide() {
			super.onHide();
		}

	}

}