module GAME {

    export interface GameplayParameters extends vads.GameplayParameters {
		exampleVar: number,
    };

    export interface EndOfGameEvent {
        rank: number;
        score: number;
    }



    export class AdsHandler extends vads.AdsHandler {

        initGameplayParameters(): GameplayParameters {
            return {
				exampleVar : 12
            };
        }

        start() {
            GameplayScene.get().start();
            // Enable controls
            // Gameplay.Main.controlEnabled = true;
        }

        static fireReady() {
            this.get().fire("ready", {});
        }

        static fireEndOfGame(data: EndOfGameEvent) {
            this.get().fire("gameover", data);
        }

        get gameplay(): GameplayParameters {
            return this._gameplay as GameplayParameters;
        }
        set gameplay(v: GameplayParameters) {
            this._gameplay = v;
        }

        static getParam(): GameplayParameters {
            return (<AdsHandler>this.get()).gameplay;
        }

    }

}