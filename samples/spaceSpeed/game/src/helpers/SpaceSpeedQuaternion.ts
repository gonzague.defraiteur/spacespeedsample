module GAME {
	export class QuaternionHelper {
		public static add(v0: THREE.Vector3, v1: THREE.Vector3) {
			return (v0.clone().add(v1));
		}
		public static sub(v0: THREE.Vector3, v1: THREE.Vector3) {
			return (v0.clone().sub(v1));
		}
		public static get Identity(): THREE.Quaternion {
			return (new THREE.Quaternion(0, 0, 0, 1));
		}
		/*public static FromToRotation(from: THREE.Vector3, to: THREE.Vector3): THREE.Quaternion {
			var quat: THREE.Quaternion = new THREE.Quaternion();
			quat.setFromUnitVectors(from, to);
			return (quat);
		}*/
		public static LookAtQuaternion(sourcePoint: THREE.Vector3, destPoint: THREE.Vector3, up: THREE.Vector3): THREE.Quaternion {
			var forward: THREE.Vector3 = destPoint.clone().sub(sourcePoint);
			var upwards: THREE.Vector3 = up;

			forward = forward.clone().normalize();
			upwards = upwards.clone().normalize();
			// Don't allow zero vectors
			if (forward.lengthSq() < Number.EPSILON || upwards.lengthSq() < Number.EPSILON)// Vector3:: SqrMagnitude(upwards) < MathHelper<float>:: Epsilon())
			{
				return QuaternionHelper.Identity;
			}
			// Handle alignment with up direction
			if (1 - Math.abs(forward.dot(upwards)) < Number.EPSILON) {
				return QuaternionHelper.FromToRotation(Vector3.Forward, forward);
			}
			// Get orthogonal vectors
			var right: THREE.Vector3 = upwards.clone().cross(forward).normalize();

			var upwards: THREE.Vector3 = forward.clone().cross(right);
			// Calculate rotation
			//var quaternion : THREE.Quaternion = QuaternionHelper.Identity;
			var radicand: number = right.x + upwards.y + forward.z;
			var x: number = 0;
			var y: number = 0;
			var z: number = 0;
			var w: number = 0;
			if (radicand > 0) {
				w = Math.sqrt(1.0 + radicand) * 0.5;
				var recip: number = 1.0 / (4.0 * w);
				x = (upwards.z - forward.y) * recip;
				y = (forward.x - right.z) * recip;
				z = (right.y - upwards.x) * recip;
			}
			else if (right.x >= upwards.y && right.x >= forward.z) {
				x = Math.sqrt(1.0 + right.x - upwards.y - forward.z) * 0.5;
				var recip: number = 1.0 / (4.0 * x);
				w = (upwards.z - forward.y) * recip;
				z = (forward.x + right.z) * recip;
				y = (right.y + upwards.x) * recip;
			}
			else if (upwards.y > forward.z) {
				y = Math.sqrt(1.0 - right.x + upwards.y - forward.z) * 0.5;
				var recip: number = 1.0 / (4.0 * y);
				z = (upwards.z + forward.y) * recip;
				w = (forward.x - right.z) * recip;
				x = (right.y + upwards.x) * recip;
			}
			else {
				z = Math.sqrt(1.0 - right.x - upwards.y + forward.z) * 0.5;
				var recip: number = 1.0 / (4.0 * z);
				y = (upwards.z + forward.y) * recip;
				x = (forward.x + right.z) * recip;
				w = (right.y - upwards.x) * recip;
			}
			var res: THREE.Quaternion = new THREE.Quaternion(x, y, z, w);
			return res;
		}

		public static Orthogonal(vector: THREE.Vector3): THREE.Vector3 {
			var v: THREE.Vector3 = vector;
			return v.z < v.x ? new THREE.Vector3(v.y, -v.x, 0) : new THREE.Vector3(0, -v.z, v.y);
		}

		public static FromToRotation(fromVector: THREE.Vector3, toVector: THREE.Vector3): THREE.Quaternion {
			var dot: number = (fromVector.clone().dot(toVector));
			var k: number = Math.sqrt((fromVector.lengthSq()) *
				(toVector.lengthSq()));

			if (Math.abs(dot / k + 1.0) < 0.00001) {
				var ortho: THREE.Vector3 = QuaternionHelper.Orthogonal(fromVector);
				var axis: THREE.Vector3 = (ortho.clone().normalize());
				return new THREE.Quaternion(axis.x, axis.y, axis.z, 0);
			}
			var cross: THREE.Vector3 = (fromVector.clone().cross(toVector));
			return (new THREE.Quaternion(cross.x, cross.y, cross.z, dot + k).clone().normalize());
		}
	}
}