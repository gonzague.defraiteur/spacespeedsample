///<reference path="../SpaceSpeedInterfaces.ts"/>
///<reference path="../../utils/SpaceSpeedSoundManager.ts"/>
module GAME {
	export class Gameplay extends GameEntity {
		private static _Main: Gameplay = undefined;
		obstaclePool: Pool<number, CurveObstacle, CurveObstacleArgs> = undefined;
		engine: plume.Engine;
		camera: THREE.PerspectiveCamera;
		scene: THREE.Scene;
		orbit: THREE.EditorControls;
		transformControl: THREE.TransformControls;
		raycaster: THREE.Raycaster;
		mouse: THREE.Vector2;
		cube0: THREE.Mesh;
		cube1: THREE.Mesh;
		canChangeTarget: boolean = true;
		controlList: THREE.Object3D[];
		needsUpdate: boolean;
		lineGeometry: THREE.Geometry;
		curveGeometry: THREE.Geometry;
		normalsGeometry: THREE.Geometry;
		controlCube: THREE.Mesh;
		controlCube2: THREE.Mesh;
		bezierCurves: BezierCurve[] = [];
		SwitchFunc: () => void;
		activeTarget: THREE.Object3D;
		follower: CurveFollower;
		soundManager: SoundManager;
		xDirection: number;
		scene0: THREE.Scene;
		curveMaterial: any;
		firstCurve: BezierCurve;
		curveRadius: number = 1.3;
		sphere: THREE.Mesh;
		simulation: plume.CannonSimulation;
		lifeLeft: number;
		debug: boolean = false;
		//screenBloodMaterial: THREE.RawShaderMaterial;
		fpsHelper: plume.FpsHelper;
		score: number = 0;
		stream: CurveStream;
		startFov: number;
		endFov: number;

		public static get Main() {
			return (this._Main);
		}

		Initialize() {
			Gameplay._Main = this;
			this.scene = new THREE.Scene;
			this.engine = plume.Engine.get();
			this.engine.scene = this.scene;
			this.SetupCamera();
			this.SetupLights();
			this.SetupProceduralSeed();
			this.soundManager = new SoundManager();
			this.SetupCurveMaterial();
		}
		SetupCurveMaterial() {
			var shader: THREE.RawShaderMaterial = new THREE.RawShaderMaterial({
				uniforms: {
					time: { value: 1.0 },
					lineColor: new THREE.Uniform(new THREE.Vector4(0.55, 0.0, 0.0, 1.0)),
					useUvs: { value: 1 },
					depthTex: { type: "t", value: null }
				},
				vertexShader: shaders.tron_vertex,
				fragmentShader: shaders.tron_fragment,
				side: THREE.DoubleSide,
				transparent: true,
				depthFunc: THREE.NotEqualDepth,
				depthWrite: true,
				premultipliedAlpha: true,
			});
			this.curveMaterial = shader;
		}
		SetupLights() {
			var sunlight = new THREE.DirectionalLight(0xffffff, 0.6);
			sunlight.position.set(2, 2, -2);
			this.scene.add(sunlight);
			var ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
			this.scene.add(ambientLight);
		}
		SetupCamera() {
			this.camera = this.engine.camera as THREE.PerspectiveCamera;
			this.camera.near = 0.01;
			this.camera.far = 25;
			this.camera.fov = 25;
			// fov entre 25 et 50
			// aspect ratio 
			var ratio = window.innerHeight / window.innerWidth;
			if (ratio >= 1.0) {
				var v = 0.800738 / 40;
			}
			else {
				v = 0.800738 / 55;
			}

			this.camera.fov = ratio / v;
			this.startFov = this.camera.fov - 10;

			this.endFov = this.camera.fov;
			console.log("FOV:" + ratio / v);
			console.log("ratio:" + ratio);
			//ratio = 0.800738
			// fov = 25;

			// 
			//console.log("ratio / fov:" + (ratio / fov));
			//fov = 0.13581 
			// r / x = v
			// 1 / x = v / r
			//x = r / v
			//0.013581
			/*
			if(window.innerHeight > window.innerWidth){
				this.camera.fov = 50;
			}*/
			this.camera.updateProjectionMatrix();
			this.camera.position.set(0, 0, 0);
			this.camera.quaternion.copy(QuaternionHelper.Identity);
			var camAngle = -48.0;
			this.camera.setRotationFromAxisAngle(new THREE.Vector3(1, 0, 0), MathHelper.ToRad(camAngle));
			this.camera.translateZ(40);
			this.scene.add(this.camera);
		}
		SetupProceduralSeed(seed: number = undefined) {
			if (seed == undefined) {
				seed = MathHelper.fit01int(Math.random(), 0, 10000);
			}
			console.error("PROCEDURAL SEED (Use it in order to reproduce the same randomness) : " + seed);
			MathHelper.RandomSeed(seed);
		}
		InitializeDebug() {
			this.orbit = new THREE.EditorControls(this.camera, this.engine.canvas);
			this.transformControl = new THREE.TransformControls(this.camera, this.engine.renderer.domElement);
			this.transformControl.setMode("translate");
			this.orbit.enabled = false;
			var self = this;
			this.orbit.addEventListener("change", function () { self.render() });
			this.transformControl.addEventListener('change', function () { self.render() });

			this.scene.add(this.transformControl);
			this.transformControl.addEventListener('dragging-changed', function (event) {
				if (self.editMode) {
					console.log("DRAGGING CHANGED:" + (event as any).value);
					self.orbit.enabled = !((event as any).value);
					self.canChangeTarget = !((event as any).value);
					self.needsUpdate = ((event as any).value);
				}
			});
			this.mouse = new THREE.Vector2();
			window.addEventListener('mousemove', function (event) {
				self.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
				self.mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
			});
			window.requestAnimationFrame(function () {
				self.orbit.enabled = self.editMode;
			});
			this.orbit.enabled = this.editMode;
			this.controlList = [];
		}
		Start0(): void {

			this.Initialize();
			if (this.debug) {
				this.InitializeDebug();
			}




			var lastProcMesh: ProceduralMesh = undefined;
			var curvesInfo = CubicBezierUtil.GenerateRandomCurves(1);
			var curves = curvesInfo.res;
			var lastPts = curvesInfo.pts;
			this.firstCurve = undefined;
			for (var i = 0; i < curves.length; i++) {
				this.bezierCurves.push(new CubicBezierCurve(this, curves[i], 100, undefined, false));
				this.bezierCurves[this.bezierCurves.length - 1].lastGenerationPoints = lastPts;
				if (i == 0) {
					this.follower = new CurveFollower(this.bezierCurves[this.bezierCurves.length - 1]);
					this.bezierCurves[this.bezierCurves.length - 1].bezierCurveObjects.push(this.follower);
					this.firstCurve = this.bezierCurves[this.bezierCurves.length - 1];
				}

				/*	var procMesh = SpaceSpeedCylinderGenerator.GenerateCylinder(this.bezierCurves[this.bezierCurves.length - 1], lastProcMesh, 0.3, 10, this.curveRadius * 1.1);
	
					var geom = procMesh.GenerateTHREEGeometry();
					//var mesh = new THREE.Mesh(geom, shader);
					geom.computeBoundingBox();
					var center = geom.boundingBox.getCenter(new THREE.Vector3());
	
					var mesh = new THREE.Mesh();
					this.bezierCurves[this.bezierCurves.length - 1].procMesh = procMesh;
					this.bezierCurves[this.bezierCurves.length - 1].mesh = mesh;
					mesh.position.copy(center.clone());
					var mat = mesh.matrix;
					var inv = mat.getInverse(new THREE.Matrix4());
					for (var j = 0; j < geom.vertices.length; j++) {
						geom.vertices[j].sub(mesh.position);
					}
					mesh.renderOrder = 3000 - i;
					mesh.geometry = geom;
					mesh.material = this.curveMaterial;
					//this.scene.add(mesh);
					//this.controlList.push(mesh);
	
					lastProcMesh = procMesh;*/
			}
			var self = this;
			this.obstaclePool = new Pool<number, CurveObstacle, {}>(this, function (index: number, args: CurveObstacleArgs) {
				var type = <SpaceSpeedBlockType2>index;
				var generated = SpaceSpeedBlockGenerator.GeneratePattern2(type);
				//var generated = SpaceSpeedBlockGenerator.GeneratePattern2(randomPattern);
				var root: THREE.Object3D = new THREE.Object3D();
				for (var i = 0; i < generated.length; i++) {
					// rows
					for (var j = 0; j < generated[i].length; j++) {
						// columns
						for (var k = 0; k < generated[i][j].length; k++) {
							var mesh = new THREE.Mesh(generated[i][j][k], BezierCurve.ObstaclesMaterials);
							mesh.rotateX(MathHelper.ToRad(90));
							(mesh as any).obstacleInfo = <CurveObstacleInfo>{ columnIndex: j, rowIndex: i, actualAngle: 0 };
							var parent = new THREE.Object3D();
							parent.add(mesh);
							root.add(parent);

						}
					}
				}

				var newCurveObstalce = new CurveObstacle(root, args.position, args.angle, args.mode, type, args.parent);
				return (newCurveObstalce);
			});

			this.stream = new CurveStream(this.firstCurve, 40, this.curveRadius * 1.1);


			/*	window.setTimeout(function () {
					self.control.attach(cube1);
				}, 1000);*/


			/*			for (var i = 0; i < 100; i++)
						{
							var t = (i / 100);
							var pt = QuadraticBezierUtil.GetPoint(cube0.position, controlCube.position, cube1.position, t);
							geometry2.vertices.push(pt);
							geometry3.vertices.push(pt);
							var derivative = QuadraticBezierUtil.GetFirstDerivative(cube0.position, controlCube.position, cube1.position, t).multiplyScalar(1.0);
							var derivative2 = QuadraticBezierUtil.
							GetFirstDerivative(cube0.position, controlCube.position, 
								cube1.position, t + 0.02).multiplyScalar(1.0);			//geometry3.vertices.push(pt.clone().add(
							var cross = derivative.clone().cross(derivative2);
							geometry3.vertices.push(cross.multiplyScalar(0.25).add(pt));
			
						}*/




			var self = this;
			this.engine.addRenderable(function (dt: number) {
				self.PostRender(dt);
			}, true);

			this.scene0 = new THREE.Scene();

			var simulation = new plume.CannonSimulation();


			simulation.running = true;
			this.lifeLeft = 5;
			(self.parent as GameplayScene).hud.lifeLeftText.text = "" + this.lifeLeft;


			this.simulation = simulation;
			// un rendu dans frame buffer, le cylindre,


			this.lastObstaclePosition = 55.0;
			//this.bezierCurves[this.bezierCurves.length - 1].GenerateRandomObstacles();
			this.bezierCurves[this.bezierCurves.length - 1].ConstructNextCurve();
			//this.fpsHelper = new plume.FpsHelper();
			//this.fpsHelper.showAll();
		}
		PostRender(dt: number): any {
			if (!(GameplayScene.get().paused)) {
				this.follower.PreRender(dt / 1000.0);
			}
		}
		public lastObstaclePosition: number = 0;
		public editMode: boolean = false;
		public startedMusics: boolean = false;
		public materialSwitchDuration: number = 0.35;
		public timeSinceMaterialSwitch: number = 0.0;
		public musicIndex: number = 0;
		public musics: string[] = [Sounds.track1, Sounds.track0];
		public SwitchMusic() {
			var self = this;

			SoundManager.playMusic(this.musics[this.musicIndex], function () {
				self.SwitchMusic();
			});
			this.musicIndex++;
			this.musicIndex = this.musicIndex % this.musics.length;
		}
		Update0(deltaTime: number): void {

			this.simulation.update(deltaTime * 1000);


			this.timeSinceMaterialSwitch += deltaTime;
			while (this.timeSinceMaterialSwitch > this.materialSwitchDuration) {
				var mats = BezierCurve.ObstaclesMaterials;
				var colors: THREE.Color[] = [];
				for (var i = 0; i < mats.length; i++) {
					colors.push(mats[i].color);
				}
				for (var i = 0; i < mats.length; i++) {
					mats[i].color = colors[(i + 1) % mats.length];
				}
				this.timeSinceMaterialSwitch -= this.materialSwitchDuration;
			}


			if (SoundManager.ready && !this.startedMusics) {
				console.error("SOUND MANAGER READY.");
				this.SwitchMusic();

				this.startedMusics = true;
			}
			if (game.inputManager.keyboard.isJustDown(KeyCode.e) && game.inputManager.keyboard.isDown(KeyCode.shift)) {

				this.editMode = !(this.editMode);
				this.camera.far = this.editMode ? 1000 : 25;
				this.camera.updateProjectionMatrix();
				this.orbit.enabled = this.editMode;
				game.inputManager.mouse.propagateAllEvents = this.editMode;
			}
			if (this.editMode) {
				if (game.inputManager.keyboard.isJustDown(KeyCode.s)) {
					this.SwitchFunc();
				}
				if (game.inputManager.keyboard.isJustDown(KeyCode.f)) {

					this.orbit.focus(this.activeTarget, false);
				}
				if (this.canChangeTarget && game.inputManager.mouse.isJustDown) {
					//this.control.detach();
					//this.control.attach(this.cube1);
					console.log("MOUSE JUST DOWN.");
					if (this.raycaster == undefined) {
						this.raycaster = new THREE.Raycaster();
					}
					this.raycaster.setFromCamera(this.mouse, this.camera);

					// calculate objects intersecting the picking ray
					var intersects = this.raycaster.intersectObjects(this.scene.children, true);

					for (var i = 0; i < intersects.length; i++) {
						var current = intersects[i].object;
						var ok = true;
						if (this.controlList.indexOf(intersects[i].object) != -1) {

							console.log("CHANGED TARGET.");
							this.transformControl.detach();
							this.transformControl.attach(intersects[i].object);
							this.activeTarget = intersects[i].object;
						}
					}
				}
				if (this.needsUpdate) {
					for (var i = 0; i < this.bezierCurves.length; i++) {
						this.bezierCurves[i].needsUpdate = true;
					}
					this.Reconstruct();
					//	this.needsUpdate = false;
				}
				for (var i = 0; i < this.bezierCurves.length; i++) {
					for (var j = 0; j < this.bezierCurves[i].controlObjects.length; j++) {
						this.bezierCurves[i].controlObjects[j].updateMatrix();
					}

				}
			}
			else {

			}
		}
		Reconstruct(): any {
			var frames = VectorFrame.GetCubicRMF(this.cube0.position, this.cube1.position, this.controlCube.position, this.controlCube2.position);
			for (var i = 0; i < frames.length; i++) {
				this.curveGeometry.vertices[i] = (frames[i].origin.clone());
				this.normalsGeometry.vertices[(i * 2) + 0] = (frames[i].origin.clone());
				this.normalsGeometry.vertices[(i * 2) + 1] = (frames[i].normal.clone().add(frames[i].origin));
				//console.log("normal:" + frames[i].normal.toArray());
			}
			this.lineGeometry.vertices[0].copy(this.cube0.position);//.clone();
			this.lineGeometry.vertices[1].copy(this.cube1.position);
			this.lineGeometry.verticesNeedUpdate = true;
			this.normalsGeometry.verticesNeedUpdate = true;
			this.curveGeometry.verticesNeedUpdate = true;
			this.lineGeometry.elementsNeedUpdate = true;
			this.normalsGeometry.elementsNeedUpdate = true;
			this.curveGeometry.verticesNeedUpdate = true;

		}
		render() {
			this.engine.renderer.render(this.scene, this.camera);
		}
		Render0(i: number): void {

		}
		Destroy0(): void {

		}
	}

	export class SpaceSpeedCylinderPool {
		GetOne(p0: THREE.Vector3, p1: THREE.Vector3, control0: THREE.Vector3, control1: THREE.Vector3): THREE.CylinderGeometry {
			return (undefined);
		}
	}
}