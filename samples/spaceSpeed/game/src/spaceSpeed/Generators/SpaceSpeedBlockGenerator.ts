///<reference path="./SpaceSpeedObstacle.ts"/>
module GAME {
	export enum SpaceSpeedBlockType {
		EndLevel,
		NonSpecific,
		ThreeOneOnThree,
		ThreeTwoHalf,
		Cross,
		TwoSymettryTwo,
	}

	export enum SpaceSpeedBlockType2 {
		Circles,
		Cross,
		ThreeTwoHalf,
		Bars,
		TwoSymettryTwo,
	}
	export class SpaceSpeedBlockGenerator {
		public static Initialize() {
			this.GeneratePattern2(SpaceSpeedBlockType2.Circles);
			this.GeneratePattern2(SpaceSpeedBlockType2.Bars);
			//this.PatternByType[SpaceSpeedPatternType.NonSpecific] = this.GeneratePattern(SpaceSpeedPatternType.NonSpecific);
		}
		public static RenderSprite() {
			//var sprite = new THREE.Texture();
			//sprite.offset...

			///http://stemkoski.github.io/Three.js/Texture-Animation.html
		}



		private static _GenerateSpaceSpeedBlockOperation: ProceduralMeshOperation = undefined;
		private static _GenerateSpaceSpeedBlockOperation2: ProceduralMeshOperation = undefined;
		public static get GenerateSpaceSpeedBlockOperation2(): ProceduralMeshOperation {
			if (this._GenerateSpaceSpeedBlockOperation == undefined) {
				var operation: ProceduralMeshOperation = new ProceduralMeshOperation();

				operation.setparameter("partsAngles", [0, 0]);
				operation.setparameter("partsSizes", [150, 150]);
				operation.setparameter("divisionByLen", 4.0);
				operation.setparameter("radius", Gameplay.Main.curveRadius);

				// compute circle and holes.
				operation.addIterator(IteratorType.Detail, <DetailIterator>(data: ProceduralMesh) => {
					var partsAngles: number[] = data.ch("partsAngles");
					var partsSizes: number[] = data.ch("partsSizes");
					var divisionByLen: number = data.ch("divisionByLen");
					var radius: number = data.ch("radius");
					var lastEndAngle: number = 0;

					for (var i = 0; i < partsAngles.length; i++) {
						var groupPrims: number[] = [];
						var groupPoints: number[] = [];

						var centerPtnum: number = data.addpoint(Vector3.Zero);

						groupPoints.push(centerPtnum);
						//						data.setpointattrib(centerPtnum, "group", groupIndex);

						var startAngle = (partsAngles[i] + lastEndAngle);
						var endAngle = (startAngle + partsSizes[i]);

						var lastEndAngle = endAngle;

						var perimeter = (Math.abs(endAngle - startAngle) / 180 * Math.PI) * radius;

						var divisions: number = perimeter * divisionByLen;
						if (divisions < 1) {
							divisions = 1;
						}
						divisions = Math.ceil(divisions);
						var oldPtnum: number = -1;
						for (var j = 0; j < divisions; j++) {
							var angle = MathHelper.fit01(j / divisions, startAngle, endAngle);
							var nextAngle = MathHelper.fit01((j + 1) / divisions, startAngle, endAngle);
							var x = Math.cos(nextAngle / 180 * Math.PI) * radius;
							var y = Math.sin(nextAngle / 180 * Math.PI) * radius;
							var pEnd: THREE.Vector3 = new THREE.Vector3(x, 0, y);
							var startPtnum: number = oldPtnum;
							if (oldPtnum == -1) {
								var x = Math.cos(angle / 180 * Math.PI) * radius;
								var y = Math.sin(angle / 180 * Math.PI) * radius;

								var pStart: THREE.Vector3 = new THREE.Vector3(x, 0, y);
								startPtnum = data.addpoint(pStart);
								groupPoints.push(startPtnum);
							}


							var endPtnum: number = data.addpoint(pEnd);
							groupPoints.push(endPtnum);

							oldPtnum = endPtnum;
							//data.setpointattrib(startPtnum, "group", groupIndex);
							//data.setpointattrib(endPtnum, "group", groupIndex);
							var primNum = data.addprim([startPtnum, centerPtnum, endPtnum]);
							var accessor = new PrimitiveAccessor(data, primNum);
							accessor.uvs = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
							groupPrims.push(primNum);
							//data.setprimattrib(primNum, "group", groupIndex);
						}

						var groupIndex: number = data.addgroup("connectivity", groupPrims, groupPoints);
						var group = new GroupAccessor(data, groupIndex);

						group.setAttrib("startAngle", startAngle);
						group.setAttrib("endAngle", endAngle);
						group.setAttrib("radius", radius);
						group.setAttrib("divisions", divisions);
						group.setAttrib("divisionByLen", divisionByLen);


					}
				});
				operation = ProceduralMeshOperation.MergeOperations(operation, Operator.PolyExtrude);
				operation.setparameter("length", 0.5);//SpaceSpeedGame.SpaceSpeedBlockLength);
				this._GenerateSpaceSpeedBlockOperation = operation;
			}
			return (this._GenerateSpaceSpeedBlockOperation);
		}
		public static get GenerateSpaceSpeedBlockOperation3(): ProceduralMeshOperation {
			if (this._GenerateSpaceSpeedBlockOperation2 == undefined) {
				var operation: ProceduralMeshOperation = new ProceduralMeshOperation();

				operation.setparameter("partsAngles", [0, 0]);
				operation.setparameter("partsSizes", [150, 150]);
				operation.setparameter("divisionByLen", 4.0);
				operation.setparameter("radius", Gameplay.Main.curveRadius);
				operation.setparameter("nbBlocks", 2);
				operation.setparameter("blocksSeparation", 1.5);
				// si yen a pas assez on prends la derniere valeur.
				// ou bien si c'est undefined.
				operation.setparameter("blocksLength", [1.0]);
				//	operation.setparameter("blocksLengthArray", []);
				operation.setparameter("blocksStartOffset", []);

				// compute circle and holes.
				operation.addIterator(IteratorType.Detail, <DetailIterator>(data: ProceduralMesh) => {
					var partsAngles: number[] = data.ch("partsAngles");
					var partsSizes: number[] = data.ch("partsSizes");
					var divisionByLen: number = data.ch("divisionByLen");
					var radius: number = data.ch("radius");
					var nbBlocks: number = data.ch("nbBlocks");
					var blocksLength: number[] = data.ch("blocksLength");
					var blocksStartOffset: number[] = data.ch("blocksStartOffset");
					var lastEndAngle: number = 0;
					// nbBlocks: number;
					// blocksSeparation: number;
					// blocksLength: number;
					// blocksStartOffset: number; 

					for (var i = 0; i < partsAngles.length; i++) {
						var groupPrims: number[] = [];
						var groupPoints: number[] = [];

						var centerPtnum: number = data.addpoint(Vector3.Zero);

						groupPoints.push(centerPtnum);
						//						data.setpointattrib(centerPtnum, "group", groupIndex);

						var startAngle = (partsAngles[i] + lastEndAngle);
						var endAngle = (startAngle + partsSizes[i]);

						var lastEndAngle = endAngle;

						var perimeter = (Math.abs(endAngle - startAngle) / 180 * Math.PI) * radius;

						var divisions: number = perimeter * divisionByLen;
						if (divisions < 1) {
							divisions = 1;
						}
						divisions = Math.ceil(divisions);
						var oldPtnum: number = -1;
						var oldPtnum2: number = -1;
						var radius2 = radius * 0.5;
						for (var j = 0; j < divisions; j++) {
							var angle = MathHelper.fit01(j / divisions, startAngle, endAngle);
							var nextAngle = MathHelper.fit01((j + 1) / divisions, startAngle, endAngle);
							var x = Math.cos(nextAngle / 180 * Math.PI) * radius;
							var y = Math.sin(nextAngle / 180 * Math.PI) * radius;
							var pEnd: THREE.Vector3 = new THREE.Vector3(x, 0, y);
							var x2 = Math.cos(nextAngle / 180 * Math.PI) * radius2;
							var y2 = Math.sin(nextAngle / 180 * Math.PI) * radius2;
							var pEnd2 = new THREE.Vector3(x2, 0, y2);

							var startPtnum: number = oldPtnum;
							var startPtnum2: number = oldPtnum2;
							if (oldPtnum == -1) {
								var x = Math.cos(angle / 180 * Math.PI) * radius;
								var y = Math.sin(angle / 180 * Math.PI) * radius;
								var x2 = Math.cos(angle / 180 * Math.PI) * radius2;
								var y2 = Math.sin(angle / 180 * Math.PI) * radius2;

								var pStart: THREE.Vector3 = new THREE.Vector3(x, 0, y);
								var pStart2: THREE.Vector3 = new THREE.Vector3(x2, 0, y2);
								startPtnum = data.addpoint(pStart);
								groupPoints.push(startPtnum);
								startPtnum2 = data.addpoint(pStart2);
								groupPoints.push(startPtnum2);
							}


							var endPtnum: number = data.addpoint(pEnd);
							groupPoints.push(endPtnum);
							var endPtnum2: number = data.addpoint(pEnd2);
							groupPoints.push(endPtnum2);

							oldPtnum = endPtnum;
							oldPtnum2 = endPtnum2;
							//data.setpointattrib(startPtnum, "group", groupIndex);
							//data.setpointattrib(endPtnum, "group", groupIndex);

							var primNum = data.addprim([startPtnum, endPtnum, startPtnum2].reverse());
							var primNum2 = data.addprim([startPtnum2, endPtnum, endPtnum2].reverse());
							var accessor = new PrimitiveAccessor(data, primNum);
							accessor.uvs = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
							var accessor2 = new PrimitiveAccessor(data, primNum2);
							accessor2.uvs = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
							groupPrims.push(primNum2);
							groupPrims.push(primNum);
							//data.setprimattrib(primNum, "group", groupIndex);
						}

						var groupIndex: number = data.addgroup("connectivity", groupPrims, groupPoints);
						var group = new GroupAccessor(data, groupIndex);

						group.setAttrib("startAngle", startAngle);
						group.setAttrib("endAngle", endAngle);
						group.setAttrib("radius", radius);
						group.setAttrib("divisions", divisions);
						group.setAttrib("divisionByLen", divisionByLen);


					}
				});
				operation = ProceduralMeshOperation.MergeOperations(operation, Operator.PolyExtrude);
				operation.setparameter("length", 0.5);//SpaceSpeedGame.SpaceSpeedBlockLength);
				this._GenerateSpaceSpeedBlockOperation2 = operation;
			}
			return (this._GenerateSpaceSpeedBlockOperation2);
		}

		public static get GenerateSpaceSpeedBlockOperation(): ProceduralMeshOperation {
			if (this._GenerateSpaceSpeedBlockOperation2 == undefined) {
				var operation: ProceduralMeshOperation = new ProceduralMeshOperation();

				operation.setparameter("partsAngles", [0, 0]);
				operation.setparameter("partsSizes", [150, 150]);
				operation.setparameter("divisionByLen", 4.0);
				operation.setparameter("radius", Gameplay.Main.curveRadius);
				operation.setparameter("nbBlocks", 3);
				operation.setparameter("blocksSeparation", 0.4);
				// si yen a pas assez on prends la derniere valeur.
				// ou bien si c'est undefined.
				operation.setparameter("blocksLength", [0.2, 0.2, 0.1]);
				//	operation.setparameter("blocksLengthArray", []);
				operation.setparameter("blocksStartOffset", [0.0, -0.1, -0.1]);

				// compute circle and holes.
				operation.addIterator(IteratorType.Detail, <DetailIterator>(data: ProceduralMesh) => {
					var partsAngles: number[] = data.ch("partsAngles");
					var partsSizes: number[] = data.ch("partsSizes");
					var divisionByLen: number = data.ch("divisionByLen");
					var radius: number = data.ch("radius");
					var blocksSeparation: number = data.ch("blocksSeparation");
					var nbBlocks: number = data.ch("nbBlocks");
					var blocksLength: number[] = data.ch("blocksLength");
					var blocksStartOffset: number[] = data.ch("blocksStartOffset");
					var lastEndAngle: number = 0;
					// nbBlocks: number;
					// blocksSeparation: number;
					// blocksLength: number;
					// blocksStartOffset: number; 
					var radi0 = radius;
					for (var k = 0; k < nbBlocks; k++) {
						radi0 -= blocksStartOffset.length <= k ? blocksStartOffset[blocksStartOffset.length - 1] : blocksStartOffset[k];
						if (k > 0) {
							radi0 -= blocksSeparation;
						}
						var radi1 = blocksLength.length <= k ? blocksLength[blocksLength.length - 1] : blocksLength[k];
						radi1 = radi0 - radi1;
						var radius = radi0;
						var radius2 = radi1;
						var lastEndAngle = 0;

						for (var i = 0; i < partsAngles.length; i++) {
							var groupPrims: number[] = [];
							var groupPoints: number[] = [];

							var centerPtnum: number = data.addpoint(Vector3.Zero);

							groupPoints.push(centerPtnum);
							//						data.setpointattrib(centerPtnum, "group", groupIndex);

							var startAngle = (partsAngles[i] + lastEndAngle);
							var endAngle = (startAngle + partsSizes[i]);

							var lastEndAngle = endAngle;

							var perimeter = (Math.abs(endAngle - startAngle) / 180 * Math.PI) * radius;

							var divisions: number = perimeter * divisionByLen;
							if (divisions < 1) {
								divisions = 1;
							}
							divisions = Math.ceil(divisions);
							var oldPtnum: number = -1;
							var oldPtnum2: number = -1;

							for (var j = 0; j < divisions; j++) {
								var angle = MathHelper.fit01(j / divisions, startAngle, endAngle);
								var nextAngle = MathHelper.fit01((j + 1) / divisions, startAngle, endAngle);
								var x = Math.cos(nextAngle / 180 * Math.PI) * radius;
								var y = Math.sin(nextAngle / 180 * Math.PI) * radius;
								var pEnd: THREE.Vector3 = new THREE.Vector3(x, 0, y);
								var x2 = Math.cos(nextAngle / 180 * Math.PI) * radius2;
								var y2 = Math.sin(nextAngle / 180 * Math.PI) * radius2;
								var pEnd2 = new THREE.Vector3(x2, 0, y2);

								var startPtnum: number = oldPtnum;
								var startPtnum2: number = oldPtnum2;
								if (oldPtnum == -1) {
									var x = Math.cos(angle / 180 * Math.PI) * radius;
									var y = Math.sin(angle / 180 * Math.PI) * radius;
									var x2 = Math.cos(angle / 180 * Math.PI) * radius2;
									var y2 = Math.sin(angle / 180 * Math.PI) * radius2;

									var pStart: THREE.Vector3 = new THREE.Vector3(x, 0, y);
									var pStart2: THREE.Vector3 = new THREE.Vector3(x2, 0, y2);
									startPtnum = data.addpoint(pStart);
									groupPoints.push(startPtnum);
									startPtnum2 = data.addpoint(pStart2);
									groupPoints.push(startPtnum2);
								}


								var endPtnum: number = data.addpoint(pEnd);
								groupPoints.push(endPtnum);
								var endPtnum2: number = data.addpoint(pEnd2);
								groupPoints.push(endPtnum2);

								oldPtnum = endPtnum;
								oldPtnum2 = endPtnum2;
								//data.setpointattrib(startPtnum, "group", groupIndex);
								//data.setpointattrib(endPtnum, "group", groupIndex);

								var primNum = data.addprim([startPtnum, endPtnum, startPtnum2].reverse());
								var primNum2 = data.addprim([startPtnum2, endPtnum, endPtnum2].reverse());
								var accessor = new PrimitiveAccessor(data, primNum);
								accessor.uvs = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
								var accessor2 = new PrimitiveAccessor(data, primNum2);
								accessor2.uvs = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
								accessor.materialIndex = k;
								accessor2.materialIndex = k;
								groupPrims.push(primNum2);
								groupPrims.push(primNum);
								//data.setprimattrib(primNum, "group", groupIndex);
							}

							var groupIndex: number = data.addgroup("connectivity", groupPrims, groupPoints);
							var group = new GroupAccessor(data, groupIndex);
							group.setAttrib("columnIndex", i);
							group.setAttrib("rowIndex", k);
							group.setAttrib("startAngle", startAngle);
							group.setAttrib("endAngle", endAngle);
							group.setAttrib("radius", radius);
							group.setAttrib("divisions", divisions);
							group.setAttrib("divisionByLen", divisionByLen);


						}
					}
				});
				operation = ProceduralMeshOperation.MergeOperations(operation, Operator.PolyExtrude);
				operation.setparameter("length", 0.5);//SpaceSpeedGame.SpaceSpeedBlockLength);
				this._GenerateSpaceSpeedBlockOperation2 = operation;
			}
			return (this._GenerateSpaceSpeedBlockOperation2);
		}
		private static _GenerateObstacleOperation: ProceduralMeshOperation = undefined;
		public static get GenerateObstacleOperation(): ProceduralMeshOperation {
			if (this._GenerateObstacleOperation == undefined) {
				this._GenerateObstacleOperation = new ProceduralMeshOperation();
				this._GenerateObstacleOperation.setparameter("radius", 4.2 * 1.0);
				this._GenerateObstacleOperation.setparameter("outerCircleDistance", 1.0);

				this._GenerateObstacleOperation.setparameter("angle", 15);
				this._GenerateObstacleOperation.setparameter("divisions", 10);
				this._GenerateObstacleOperation.addIterator(IteratorType.Detail, (data: ProceduralMesh) => {
					var center: number = data.addpoint(Vector3.Zero);
					var radius = data.ch("radius") as number;
					var angle = data.ch("angle") as number;
					var outerCircleDistance = data.ch("outerCircleDistance") as number;

					var startAngle: number = - angle / 2.0;
					var endAngle: number = angle / 2.0;
					var x: number = Math.cos(startAngle / 180 * Math.PI);
					var y: number = Math.sin(startAngle / 180 * Math.PI);

					var startPos: THREE.Vector3 = new THREE.Vector3(x, 0, y).multiplyScalar(radius);
					x = Math.cos(endAngle / 180 * Math.PI);
					y = Math.sin(endAngle / 180 * Math.PI);
					var endPos: THREE.Vector3 = new THREE.Vector3(x, 0, y).multiplyScalar(radius);

					x = Math.cos(0);
					y = Math.sin(0);
					var outerCircleCenter: THREE.Vector3 = new THREE.Vector3(x, 0, y).multiplyScalar(radius - outerCircleDistance);
					var outerCircleCenterPtnum: number = data.addpoint(outerCircleCenter);

					var outerCircleRadius: number = outerCircleCenter.clone().sub(startPos).length();
					var startPosRelative: THREE.Vector3 = startPos.clone().sub(outerCircleCenter).normalize();
					var endPosRelative: THREE.Vector3 = endPos.clone().sub(outerCircleCenter).normalize();

					var divisions = 10;
					var outerCircleStartAngle: number = Math.atan2(startPosRelative.z, startPosRelative.x);
					var outerCircleEndAngle: number = Math.atan2(endPosRelative.z, endPosRelative.x);
					var lastEnd: number = -1;

					for (var i = 0; i < divisions; i++) {
						var outerptnum0 = 0;
						if (lastEnd == -1) {
							var angle0 = (outerCircleEndAngle - outerCircleStartAngle) * (i / divisions) + outerCircleStartAngle;
							x = Math.cos(angle0);
							y = Math.sin(angle0);
							var outerp0: THREE.Vector3 = new THREE.Vector3(x, 0, y).multiplyScalar(outerCircleRadius).add(outerCircleCenter);
							outerptnum0 = data.addpoint(outerp0);

						}
						else {
							outerptnum0 = lastEnd;
						}


						var angle1 = (outerCircleEndAngle - outerCircleStartAngle) * ((i + 1) / divisions) + outerCircleStartAngle;
						x = Math.cos(angle1);
						y = Math.sin(angle1);
						var outerp1: THREE.Vector3 = new THREE.Vector3(x, 0, y).multiplyScalar(outerCircleRadius).add(outerCircleCenter);
						var outerptnum1 = data.addpoint(outerp1);

						lastEnd = outerptnum1;
						data.addprim([outerptnum0, outerCircleCenterPtnum, outerptnum1]);
					}

					var p0 = data.addpoint(startPos);
					var p1 = data.addpoint(endPos);
					var prim = data.addprim([p0, center, p1]);

					data.IteratePointsRaw((data, accessor) => {
						accessor.P.y += ((data.ch("length") as number) - (data.ch("standardBlockLength") as number)) * 0.5;
					})
					data.IteratePrimitivesRaw((data, accessor) => {
						accessor.uvs = [new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2()];
					});
				});
				this._GenerateObstacleOperation = ProceduralMeshOperation.MergeOperations(this._GenerateObstacleOperation, Operator.PolyExtrude);
				this._GenerateObstacleOperation.setparameter("length", 1.05 * 2.0);//SpaceSpeedGame.SpaceSpeedBlockLength);
				this._GenerateObstacleOperation.setparameter("standardBlockLength", 2.0);//SpaceSpeedGame.SpaceSpeedBlockLength);
			}
			return (this._GenerateObstacleOperation);
		}


		public static ObstacleSizeByType: { [index: number]: number } = {
			[<number>SpaceSpeedObstacleType.Medium]:
				25,
			[<number>SpaceSpeedObstacleType.Small]:
				15,
		}
		public static ObstacleGeometryByType: { [index: number]: THREE.Geometry } = {};
		public static ObstacleByType: { [index: number]: SpaceSpeedObstacle } = {};
		public static PatternByType: { [index: number]: THREE.Geometry[] } = {};

		private static ObstacleBySize: { [index: number]: SpaceSpeedObstacle } = {};
		public static GetObstacleBySize(size: number) {
			if (this.ObstacleBySize[size] == undefined) {
				var generateObstacleOperation = this.GenerateObstacleOperation;
				generateObstacleOperation.in = new ProceduralMesh();
				var obstacleDef: SpaceSpeedObstacleDefinition = new SpaceSpeedObstacleDefinition(SpaceSpeedObstacleType.FullPart);
				obstacleDef.sizeAngle = size;
				generateObstacleOperation.setparameter("angle", obstacleDef.sizeAngle);
				generateObstacleOperation.setparameter("radius", Gameplay.Main.curveRadius);
				generateObstacleOperation.setparameter("outerCircleDistance", 2.5);

				generateObstacleOperation.execute();
				var geom = generateObstacleOperation.out.GenerateTHREEGeometry();
				this.ObstacleBySize[obstacleDef.sizeAngle] = new SpaceSpeedObstacle(obstacleDef, geom);
			}
			return (this.ObstacleBySize[size]);

		}
		public static PatterGeometryByType: { [index: number]: THREE.Geometry[][][] } = {};


		public static GeneratePattern2(patternType: SpaceSpeedBlockType2): THREE.Geometry[][][] {
			switch (patternType) {
				case SpaceSpeedBlockType2.Bars:
					{
						if (this.PatterGeometryByType[patternType] == undefined) {
							var parts: THREE.Geometry[][][] = [];


							var partsAngles = [0, 180 - 50];//, 90 - 50,  180 - 50];
							var partsSizes = [50, 50];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 2);
							operation.setparameter("blocksSeparation", 0.4);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.2, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 0.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);



							var lastColumnIndex: number = 0;

							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail("columnIndex");
								lastColumnIndex = columnIndex > lastColumnIndex ? columnIndex : lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							lastColumnIndex++;


							var partsAngles = [90, 180 - 50];
							var partsSizes = [50, 50];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 3);
							operation.setparameter("blocksSeparation", 0.2);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.1, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 0.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);





							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail<number>("columnIndex") + lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							this.PatterGeometryByType[patternType] = parts;
						}

						return (this.PatterGeometryByType[patternType]);
					}
				case SpaceSpeedBlockType2.Circles:
					{
						if (this.PatterGeometryByType[patternType] == undefined) {
							var parts: THREE.Geometry[][][] = [];


							var partsAngles = [0, 180 - 50];//, 90 - 50,  180 - 50];
							var partsSizes = [50, 50];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 2);
							operation.setparameter("blocksSeparation", 0.4);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.2, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 4.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);



							var lastColumnIndex: number = 0;

							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail("columnIndex");
								lastColumnIndex = columnIndex > lastColumnIndex ? columnIndex : lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							lastColumnIndex++;


							var partsAngles = [90, 180 - 50];
							var partsSizes = [50, 50];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 3);
							operation.setparameter("blocksSeparation", 0.2);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.1, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 4.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);





							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail<number>("columnIndex") + lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							this.PatterGeometryByType[patternType] = parts;
						}
						return (this.PatterGeometryByType[patternType]);
					}

					case SpaceSpeedBlockType2.ThreeTwoHalf:
					{
						if (this.PatterGeometryByType[patternType] == undefined) {
							var parts: THREE.Geometry[][][] = [];


							var partsAngles = [0, 180 - 50];//, 90 - 50,  180 - 50];
							var partsSizes = [20, 20];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 2);
							operation.setparameter("blocksSeparation", 0.4);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.4, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 4.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);



							var lastColumnIndex: number = 0;

							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail("columnIndex");
								lastColumnIndex = columnIndex > lastColumnIndex ? columnIndex : lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							lastColumnIndex++;


							var partsAngles = [90, 180 - 50];
							var partsSizes = [20, 20];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 3);
							operation.setparameter("blocksSeparation", 0.2);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.2, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 4.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);





							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail<number>("columnIndex") + lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							this.PatterGeometryByType[patternType] = parts;
						}
						return (this.PatterGeometryByType[patternType]);
					}

					case SpaceSpeedBlockType2.Cross:
					{
						if (this.PatterGeometryByType[patternType] == undefined) {
							var parts: THREE.Geometry[][][] = [];


							var partsAngles = [0, 90 - 60];//, 90 - 50,  180 - 50];
							var partsSizes = [60, 60];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 2);
							operation.setparameter("blocksSeparation", 0.4);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.4, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 4.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);



							var lastColumnIndex: number = 0;

							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail("columnIndex");
								lastColumnIndex = columnIndex > lastColumnIndex ? columnIndex : lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							lastColumnIndex++;


							var partsAngles = [180 - 20, 90 - 20];
							var partsSizes = [20, 20];//, 50, 50];

							var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;

							operation.setparameter("nbBlocks", 3);
							operation.setparameter("blocksSeparation", 0.2);
							// si yen a pas assez on prends la derniere valeur.
							// ou bien si c'est undefined.
							operation.setparameter("blocksLength", [0.2, 0.1]);
							//	operation.setparameter("blocksLengthArray", []);
							operation.setparameter("blocksStartOffset", [0.0, 0.2]);



							operation.setparameter("partsAngles", partsAngles);
							operation.setparameter("partsSizes", partsSizes);
							operation.setparameter("divisionByLen", 4.0);
							operation.in = new ProceduralMesh();
							operation.execute();
							var out: ProceduralMesh = operation.out;
							var outs = ProceduralMesh.SeparateParts(out, false);





							var threeMeshes: THREE.Geometry[] = [];

							for (var i = 0; i < outs.length; i++) {
								// also add random obstacles matching detail values
								var startAngle: number = outs[i].detail("startAngle");
								var endAngle: number = outs[i].detail("endAngle");
								var radius: number = outs[i].detail("radius");
								var divisions: number = outs[i].detail("divisions");
								var divisionByLen: number = outs[i].detail("divisionByLen");
								var rowIndex: number = outs[i].detail("rowIndex");
								while (parts.length <= rowIndex) {
									parts.push([]);
								}
								var columnIndex: number = outs[i].detail<number>("columnIndex") + lastColumnIndex;
								while (parts[rowIndex].length <= columnIndex) {
									parts[rowIndex].push([]);
								}
								parts[rowIndex][columnIndex].push(outs[i].GenerateTHREEGeometry());
							}
							this.PatterGeometryByType[patternType] = parts;
						}
						return (this.PatterGeometryByType[patternType]);
					}


			}
			return (undefined);
		}
		public static GeneratePattern(patternType: SpaceSpeedBlockType): THREE.Geometry[] {
			switch (patternType) {
				case SpaceSpeedBlockType.EndLevel:
					{
						var partsAngles = [0, 0];
						var partsSizes = [180, 180];
						var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;
						operation.setparameter("partsAngles", partsAngles);
						operation.setparameter("partsSizes", partsSizes);
						operation.in = new ProceduralMesh();
						operation.execute();
						var out: ProceduralMesh = operation.out;
						var outs = ProceduralMesh.SeparateParts(out, false);





						var threeMeshes: THREE.Geometry[] = [];
						var parts: THREE.Geometry[] = [];
						for (var i = 0; i < outs.length; i++) {
							// also add random obstacles matching detail values
							var startAngle: number = outs[i].detail("startAngle");
							var endAngle: number = outs[i].detail("endAngle");
							var radius: number = outs[i].detail("radius");
							var divisions: number = outs[i].detail("divisions");
							var divisionByLen: number = outs[i].detail("divisionByLen");

							parts.push(outs[i].GenerateTHREEGeometry());
						}
						return (parts);
					}
				case SpaceSpeedBlockType.NonSpecific:
					{
						var partsAngles = [45, 0];
						var partsSizes = [145, 145];
						var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;
						operation.setparameter("partsAngles", partsAngles);
						operation.setparameter("partsSizes", partsSizes);
						operation.in = new ProceduralMesh();
						operation.execute();
						var out: ProceduralMesh = operation.out;
						var outs = ProceduralMesh.SeparateParts(out, false);







						var threeMeshes: THREE.Geometry[] = [];
						var parts: THREE.Geometry[] = [];
						for (var i = 0; i < outs.length; i++) {
							// also add random obstacles matching detail values
							var startAngle: number = outs[i].detail("startAngle");
							var endAngle: number = outs[i].detail("endAngle");
							var radius: number = outs[i].detail("radius");
							var divisions: number = outs[i].detail("divisions");
							var divisionByLen: number = outs[i].detail("divisionByLen");

							parts.push(outs[i].GenerateTHREEGeometry());
						}
						return (parts);
						//return (threeMeshes);
					}
				case SpaceSpeedBlockType.ThreeOneOnThree:
					{
						var partSize: number = 360 / 6.0;// - (holeSize * 2.0);

						var partsAngles = [partSize, partSize, partSize];
						var partsSizes = [partSize, partSize, partSize];

						var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;
						operation.setparameter("partsAngles", partsAngles);
						operation.setparameter("partsSizes", partsSizes);
						operation.in = new ProceduralMesh();
						operation.execute();
						var out: ProceduralMesh = operation.out;
						var outs = ProceduralMesh.SeparateParts(out, false);







						var parts: THREE.Geometry[] = [];
						for (var i = 0; i < outs.length; i++) {
							// also add random obstacles matching detail values
							var startAngle: number = outs[i].detail("startAngle");
							var endAngle: number = outs[i].detail("endAngle");
							var radius: number = outs[i].detail("radius");
							var divisions: number = outs[i].detail("divisions");
							var divisionByLen: number = outs[i].detail("divisionByLen");
							parts.push(outs[i].GenerateTHREEGeometry());
						}
						return (parts);
					}
				case SpaceSpeedBlockType.ThreeTwoHalf:
					{

						var partSize: number = 360 / 6.0;// - (holeSize * 2.0);

						var partsAngles = [partSize, partSize, partSize];
						var partsSizes = [partSize, partSize, partSize];

						var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;
						operation.setparameter("partsAngles", partsAngles);
						operation.setparameter("partsSizes", partsSizes);
						operation.in = new ProceduralMesh();
						operation.execute();
						var out: ProceduralMesh = operation.out;
						var outs = ProceduralMesh.SeparateParts(out, false);







						var parts: THREE.Geometry[] = [];
						for (var i = 0; i < outs.length; i++) {
							// also add random obstacles matching detail values
							var startAngle: number = outs[i].detail("startAngle");
							var endAngle: number = outs[i].detail("endAngle");
							var radius: number = outs[i].detail("radius");
							var divisions: number = outs[i].detail("divisions");
							var divisionByLen: number = outs[i].detail("divisionByLen");
							parts.push(outs[i].GenerateTHREEGeometry());
						}
						return (parts);
					}
				case SpaceSpeedBlockType.Cross:
					{
						var holeSize: number = 65;
						var partSize: number = (180) - (holeSize);
						//console.error("partSize:" + partSize);
						var partsAngles = [holeSize, holeSize];
						var partsSizes = [partSize, partSize];

						var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;
						operation.setparameter("partsAngles", partsAngles);
						operation.setparameter("partsSizes", partsSizes);
						operation.in = new ProceduralMesh();
						operation.execute();
						var out: ProceduralMesh = operation.out;
						var outs = ProceduralMesh.SeparateParts(out, false);







						var parts: THREE.Geometry[] = [];
						for (var i = 0; i < outs.length; i++) {
							// also add random obstacles matching detail values
							var startAngle: number = outs[i].detail("startAngle");
							var endAngle: number = outs[i].detail("endAngle");
							var radius: number = outs[i].detail("radius");
							var divisions: number = outs[i].detail("divisions");
							var divisionByLen: number = outs[i].detail("divisionByLen");
							parts.push(outs[i].GenerateTHREEGeometry());
						}
						return (parts);
					}
				case SpaceSpeedBlockType.TwoSymettryTwo:
					{
						var holeSize: number = 55;
						var partSize: number = 180 - (holeSize);

						var partsAngles = [holeSize, holeSize];
						var partsSizes = [partSize, partSize];

						var operation = SpaceSpeedBlockGenerator.GenerateSpaceSpeedBlockOperation;
						operation.setparameter("partsAngles", partsAngles);
						operation.setparameter("partsSizes", partsSizes);
						operation.in = new ProceduralMesh();
						operation.execute();
						var out: ProceduralMesh = operation.out;
						var outs = ProceduralMesh.SeparateParts(out, false);







						var parts: THREE.Geometry[] = [];
						for (var i = 0; i < outs.length; i++) {
							// also add random obstacles matching detail values
							var startAngle: number = outs[i].detail("startAngle");
							var endAngle: number = outs[i].detail("endAngle");
							var radius: number = outs[i].detail("radius");
							var divisions: number = outs[i].detail("divisions");
							var divisionByLen: number = outs[i].detail("divisionByLen");
							parts.push(outs[i].GenerateTHREEGeometry());
						}
						return (parts);
					}

			}
		}
	}



}