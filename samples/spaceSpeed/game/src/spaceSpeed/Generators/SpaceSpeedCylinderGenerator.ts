module GAME {
	export class SpaceSpeedCylinderGenerator {
		public static GenerateCylinder(curve: BezierCurve,
			previous: ProceduralMesh = undefined,
			divisionsByLen: number = 0.30,
			segments: number = 10,
			radius: number = 1.3): ProceduralMesh {
			var frames = curve.activeData;
			var verts: THREE.Vector3[] = [];
			var normals: THREE.Vector3[] = [];
			var tangents: THREE.Vector3[] = [];

			for (var i = 0; i < frames.length; i++) {
				verts.push(frames[i].origin.clone());
				normals.push(frames[i].normal.clone());
				tangents.push(frames[i].tangent.clone());
			}

			var count: number = frames.length;
			var op = this.GenerateCylinderOperation;
			op.setparameter("vertices", verts);
			op.setparameter("normals", normals);
			op.setparameter("tangents", tangents);
			op.setparameter("count", count);
			op.setparameter("divisionsByLen", divisionsByLen);
			op.setparameter("segments", segments);
			op.setparameter("radius", radius);
			op.in = previous == undefined ? new ProceduralMesh() : previous;
			op.execute();
			return (op.out);
		}


		private static _GenerateCylinderOperation: ProceduralMeshOperation = undefined;
		public static get GenerateCylinderOperation(): ProceduralMeshOperation {
			if (this._GenerateCylinderOperation == undefined) {
				var res = new ProceduralMeshOperation();
				res.setparameter<number>("count", 1);
				res.setparameter<THREE.Vector3[]>("vertices", [Vector3.Zero]);
				res.setparameter<THREE.Vector3[]>("normals", [Vector3.One]);
				res.setparameter<THREE.Vector3[]>("tangents", [Vector3.Forward]);
				res.setparameter<number>("divisionsByLen", 5.0);
				res.setparameter<number>("segments", 24);
				res.setparameter<number>("radius", 1.0);
				res.addIterator(IteratorType.Detail, (data: ProceduralMesh) => {
					//etc..
					var verts = data.ch("vertices") as THREE.Vector3[];
					var normals = data.ch("normals") as THREE.Vector3[];
					var tangents = data.ch("tangents") as THREE.Vector3[];
					var divisionsByLen = data.ch("divisionsByLen") as number;
					var radius = data.ch("radius") as number;
					var count = data.ch("count") as number;
					var segments = data.ch("segments") as number;
					var iiuv = [new THREE.Vector2(0, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, 1), new THREE.Vector2(0, 1)];
					var startIndex = 0;
					var endIndex = 0;
					var dist = 0;
					var lastDist = 0;
					var diff = 0;
					var startInterpolation = 0;
					var endInterpolation = 0;
					divisionsByLen = 1 / divisionsByLen;
					var first = false;
					var previousStart = undefined;
					var elemIndex = 0;
					if (data.hasDetailAttrib("startInterpolation")) {
						first = true;
						var vertsLeft = data.detail("verticesLeft") as THREE.Vector3[];
						var normalsLeft = data.detail("normalsLeft") as THREE.Vector3[];
						var tangentsLeft = data.detail("tangentsLeft") as THREE.Vector3[];
						count += vertsLeft.length;
						verts = vertsLeft.concat(verts);
						normals = normalsLeft.concat(normals);
						tangents = tangentsLeft.concat(tangents);
						previousStart = data.detail("startInterpolation") as number;
						diff = data.detail("startDist");
						elemIndex = data.detail("elemIndex");
						data.clearData();
					}
					var iiuv = [new THREE.Vector2(0, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, 1), new THREE.Vector2(0, 1)];
					while (endIndex + 1 < count) {
						endIndex = startIndex;
						dist = diff;
						var startDist = dist;
						startInterpolation = diff / lastDist;
						if (first) {
							startInterpolation = previousStart;
						}
						var oldDist = dist;
						var firstDist = true;
						var invalid = false;
						while ((dist - startDist) <= (divisionsByLen) && endIndex + 1 < count) {
							lastDist = verts[endIndex + 1].distanceTo(verts[endIndex]);
							dist += lastDist;
							endIndex++;
							firstDist = false;
							if (!(endIndex + 1 < count)) {
								invalid = true;
							}
						}


						var diff = lastDist - ((dist - startDist) - (divisionsByLen));
						endInterpolation = diff / lastDist;

						var posStart = verts[startIndex].clone().lerp(verts[startIndex + 1], startInterpolation);
						var posEnd = verts[endIndex - 1].clone().lerp(verts[endIndex], endInterpolation);

						var normalStart = normals[startIndex].clone().lerp(normals[startIndex + 1], startInterpolation);
						var tangentStart = tangents[startIndex].clone().lerp(tangents[startIndex + 1], startInterpolation);

						var normalEnd = normals[endIndex - 1].clone().lerp(normals[endIndex], endInterpolation);
						var tangentEnd = tangents[endIndex - 1].clone().lerp(tangents[endIndex], endInterpolation);

						startIndex = endIndex - 1;
						var tangent = tangentStart;
						var normal = normalStart;
						var pos = posStart;
						var vertArray = [];


						var lookatQuat = QuaternionHelper.LookAtQuaternion(Vector3.Zero,
							tangent, normal);
						for (var i = 0; i < segments; i++) {
							var angle = (i / segments) * 360.0;
							var rad = MathHelper.ToRad(angle);
							var x = Math.cos(rad);
							var y = Math.sin(rad);
							var vec = new THREE.Vector3(x, y, 0.0).multiplyScalar(radius);
							var worldDirection = vec.clone().applyQuaternion(lookatQuat);
							var worldPosition = worldDirection.clone().add(pos);
							vertArray.push(worldPosition);
						}

						var startArray = vertArray;

						tangent = tangentEnd;
						normal = normalEnd;
						pos = posEnd;
						vertArray = [];

						lookatQuat = QuaternionHelper.LookAtQuaternion(Vector3.Zero,
							tangent, normal);
						for (var i = 0; i < segments; i++) {
							var angle = (i / segments) * 360.0;
							var rad = MathHelper.ToRad(angle);
							var x = Math.cos(rad);
							var y = Math.sin(rad);

							var vec = new THREE.Vector3(x, y, 0.0).multiplyScalar(radius);

							var worldDirection = vec.clone().applyQuaternion(lookatQuat);
							var worldPosition = worldDirection.clone().add(pos);
							vertArray.push(worldPosition);
						}


						var endArray = vertArray;
						var indexOffset = 0;
					/*	if (first) {

							var startVert = startArray[0];
							var minDist: number = Infinity;
							for (var l = 0; l < endArray.length; l++) {
								var totalDist = 0;

								for (var i = 0; i < endArray.length; i++) {
									var dist2 = endArray[(i + l) % endArray.length].distanceTo(startArray[i]);
									totalDist += dist2;
									
								}
								if (totalDist < minDist) {
									minDist = totalDist;
									indexOffset = l;
								}
							}
							console.log("NORMAL START:" + normalStart.toArray());
							console.log("NORMAL END:" + normalEnd.toArray());
							//indexOffset += endArray.length - 1;
						}*/
						indexOffset = 0;
						//
						var startIndices: number[] = [];
						var endIndices: number[] = [];

						for (var i = 0; i < startArray.length; i++) {
							var ptnum = data.addpoint(startArray[i]);
							startIndices.push(ptnum);

							ptnum = data.addpoint(endArray[i]);
							endIndices.push(ptnum);
						}
						// console.log("INDEX OFFSET:" + indexOffset);
						for (var i = 0; i < startIndices.length; i++) {

							var i0 = startIndices[i];
							var ii0 = 0;

							var i1 = endIndices[(i + 1 + indexOffset) % endIndices.length];
							var ii1 = 2;

							var i2 = endIndices[(i + indexOffset) % endIndices.length];
							var ii2 = 1;
							var primnum = data.addprim([i0, i1, i2].reverse());
							var accessor = data.PrimitiveAccessor(primnum);
							accessor.RecalculateNormal();
							accessor.uvs = [iiuv[ii0], iiuv[ii1], iiuv[ii2]].reverse();
							var color = new THREE.Color(0, 1, 0).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
							if (i % 2 == 1) {
								var color = new THREE.Color(0, 1, 0).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
								if (elemIndex % 2 == 1) {
									color = new THREE.Color(0.75, 0, 0.25);
								}

							}
							else {
								color = new THREE.Color(1, 0, 1).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
							}

							accessor.colors = [color, color, color];
							i0 = startIndices[i];
							ii0 = 0;
							i1 = startIndices[(i + 1) % startIndices.length];
							ii1 = 3;
							i2 = endIndices[(i + 1 + indexOffset) % endIndices.length];
							ii2 = 2;
							primnum = data.addprim([i0, i1, i2].reverse());
							accessor = data.PrimitiveAccessor(primnum);
							accessor.RecalculateNormal();
							accessor.uvs = [iiuv[ii0], iiuv[ii1], iiuv[ii2]].reverse();

							accessor.colors = [color, color, color];
						}
						elemIndex++;
						first = false;
					}

					data.setdetailattrib("verticesLeft", verts.slice(startIndex));
					data.setdetailattrib("normalsLeft", normals.slice(startIndex));
					data.setdetailattrib("tangentsLeft", tangents.slice(startIndex));
					data.setdetailattrib("startInterpolation", endInterpolation);
					data.setdetailattrib("startDist", diff);
					data.setdetailattrib("elemIndex", elemIndex);

				});
				this._GenerateCylinderOperation = res;
			}
			return (this._GenerateCylinderOperation);
		}
	}
}