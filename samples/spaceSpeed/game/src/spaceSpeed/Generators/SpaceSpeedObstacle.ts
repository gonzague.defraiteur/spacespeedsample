///<reference path="../../utils/SpaceSpeedConstants.ts"/>
module GAME
{
	export enum SpaceSpeedObstacleType {
		Small = 1,
		Medium = 2,
		Big = 3,
		FullPart = 4,
		HalfPart = 5
		
	}
	export class SpaceSpeedObstacleDefinition {
		public constructor(obstacleType: SpaceSpeedObstacleType) {
			this.obstacleType = obstacleType;
			this.sizeAngle = SpaceSpeedBlockGenerator.ObstacleSizeByType[this.obstacleType];

		}
		public sizeAngle: number = 0;
		public obstacleType: SpaceSpeedObstacleType = SpaceSpeedObstacleType.Medium;
	}

	export class SpaceSpeedObstacle {
		public positionAngle: number = 0;
		public definition: SpaceSpeedObstacleDefinition;
		public mesh: THREE.Mesh;
		public geometry: THREE.Geometry;
		public visible: boolean = true;
		public instances: SpaceSpeedObstacle[] = [];
		public static ObstacleMaterial: THREE.MeshMaterialType = new THREE.MeshPhongMaterial({ flatShading: false, color: Colors.EnemyRed });
		public static Initialize()
		{
			this.ObstacleMaterial = new THREE.MeshPhongMaterial({ flatShading: false, color: Colors.EnemyRed });
		}
		public constructor(definition: SpaceSpeedObstacleDefinition, geometry: THREE.Geometry) {
			this.definition = definition;
			this.geometry = geometry;
			this.mesh = new THREE.Mesh(geometry, SpaceSpeedObstacle.ObstacleMaterial.clone());
		}
		public GetOne(positionAngle: number) {
			var instance: SpaceSpeedObstacle = undefined;
			for (var i = 0; i < this.instances.length; i++) {
				if (!(this.instances[i].visible)) {
					instance = this.instances[i];
					instance.visible = true;
					instance.mesh.visible = true;
					break;
				}
			}
			if (instance == undefined) {
				instance = this.clone();
				this.instances.push(instance);
			}
			instance.visible = true;
			instance.SetRotation(positionAngle);
			return (instance);
		}
		public SetRotation(angle: number) {
			this.positionAngle = angle;
			var angle = angle;
			this.mesh.setRotationFromEuler(new THREE.Euler(0, (-angle) / 180 * Math.PI, 0));
			this.mesh.matrixWorldNeedsUpdate = true;
		}
		public clone(): SpaceSpeedObstacle {
			var res = new SpaceSpeedObstacle(this.definition, this.geometry);
			res.positionAngle = this.positionAngle;
			res.definition = this.definition;
			res.visible = this.visible;
			return (res);
		}
	}
}