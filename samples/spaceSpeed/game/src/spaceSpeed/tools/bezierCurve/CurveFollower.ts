module GAME {
	export class CurveFollower extends BezierCurveObject {
		customRenderTarget: CustomRenderTarget;
		customScene: THREE.Scene;
		farTest: THREE.Mesh;
		farTest2: THREE.Mesh;
		testPlane: THREE.Mesh;
		sphere: THREE.Mesh;
		sphereBody: CANNON.Body;
		simulation: plume.CannonSimulation;
		mesh: THREE.Mesh;
		
		SetParent(parent: BezierCurve) {

			super.SetParent(parent);
			if (parent.next == undefined) {
				parent.ConstructNextCurve();
			}
		}
		constructor(parent: BezierCurve) {
			super(parent);

			this.customScene = new THREE.Scene();
			this.customRenderTarget = new CustomRenderTarget(this.customScene, Gameplay.Main.camera);
			this.customRenderTarget.scene = this.customScene;


			this.sphere = new THREE.Mesh(new THREE.SphereBufferGeometry(0.15));
			var sphereBody = plume.CannonBodyBuilder.newBodyFromMesh(this.sphere, { mass: 500 });


		}
		speed: number = 12.0;
		activeIndex: number = 0;
		angle: number = 90.0;
		radius: number = 1.0;
		object3D: THREE.Object3D = undefined;

		public OnCurveUpdate(curve: BezierCurve): void {
			//throw new Error("Method not implemented.");
		}
		Start0(): void {




			this.sphere = new THREE.Mesh(new THREE.SphereBufferGeometry(0.15));
			var sphereBody = plume.CannonBodyBuilder.newBodyFromMesh(this.sphere, { mass: 500 });


			Gameplay.Main.simulation.addRigidBody(sphereBody, this.sphere, true);
			(sphereBody.collisionResponse as any) = 0;
			var gameplay = Gameplay.Main;
			var screenTakeHitEffect = new ScreenTakeHitEffect(gameplay);
			sphereBody.addEventListener("collide", function (e: any) {
				var collidedYet = e.body.obstacle.collidedYet;
				if (!(collidedYet)) {
					if (!((gameplay.parent as GameplayScene).hud.isGameOver)) {
						gameplay.lifeLeft--;
						if (gameplay.lifeLeft == 0) {
							(gameplay.parent as GameplayScene).hud.CallGameOver(gameplay.score);
							(gameplay).score = 0;
							(gameplay.parent as GameplayScene).hud.ScoreText.text = "" + gameplay.score;
							gameplay.lifeLeft = 5;
							screenTakeHitEffect.TakeBlackHit();
							(gameplay.parent as GameplayScene).hud.ScoreText.text = "" + gameplay.score;
							
						}

						(gameplay.parent as GameplayScene).hud.lifeLeftText.text = "" + gameplay.lifeLeft;


						e.body.obstacle.collidedYet = true;
						screenTakeHitEffect.TakeHit();
					}
				}
			});

			this.sphere = this.sphere;
			this.sphereBody = sphereBody;

			this.simulation = gameplay.simulation;

			var mesh = new THREE.Mesh(new THREE.CubeGeometry(0.25, 0.25, 0.25), new THREE.MeshBasicMaterial({ depthWrite: true, visible: false }));
			mesh.renderOrder = 50;
			mesh.visible = true;
			mesh.position.setY(-Gameplay.Main.curveRadius * 0.80);
			this.mesh = mesh;
			this.object3D = new THREE.Object3D();
			this.object3D.add(mesh);
			Gameplay.Main.scene.add(this.object3D);

			var shader = new THREE.RawShaderMaterial({ vertexShader: shaders.depth_vertex, fragmentShader: shaders.depth_fragment, transparent: true });
			var farTest = new THREE.Mesh(new THREE.CubeGeometry(1, 1, 0.5), shader);
			var testPlane = new THREE.Mesh(new THREE.PlaneGeometry(2, 2), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, depthTest: false }));
			testPlane.renderOrder = 5000;
			testPlane.translateZ(10);
			testPlane.translateX(2);
			testPlane.translateY(2);
			testPlane.rotation.y = Math.PI;
			/*
			
			new THREE.MeshBasicMaterial({color: 0xFFFF00, transparent:true, depthFunc: THREE.LessDepth, depthWrite:true})
			*/
			farTest.renderOrder = 0;
			this.farTest = farTest;

			this.farTest2 = this.farTest.clone();
			this.farTest.material = new THREE.MeshBasicMaterial({ color: new THREE.Color(1, 0, 0) });
			this.farTest2.scale.set(1.0, 1.0, 1.0);
			this.customScene.add(this.farTest2);
			this.testPlane = testPlane;

			farTest.translateZ(8.0);

			//this.object3D.add(farTest);
			//farTest.visible = false;
			//this.object3D.add(testPlane);
			//testPlane.visible = false;

			// sur un depth buffer on rends les objets,
			// mais coupés par le tube.
			// enfin, on rends deux fois les objets,
			// la premiere fois, on les rends avec un shader qui vient mettre en transparent les parties a l'intérieur du cylindre.
			// ce rendu a un render Order a 0
			// la seconde fois, on les rends avec render order a 5000,
			// le setup du material ne change pas,
			/*
			new THREE.MeshBasicMaterial({color: 0xFFFF00, transparent:true, depthFunc: THREE.LessDepth, depthWrite:true}));
			
			*/
			// ou bien, on rends les objets dans un frame buffer,
			// le fragment shader du tube regarde la depth d'une texture qu'on lui passe qui a le rendu des objets,
			// donc on rends les objets avec ça

			// float originalZ = gl_FragCoord.z / gl_FragCoord.w;
			// gl_FragColor = vec4(originalZ, 0, 0, 1.0); 
			// ensuite dans le fragment du tube

			// float originalZ = gl_FragCoord.z / gl_FragCoord.w;
			// if (originalZ > getDepthOnTexture(gl_FragCoord) )
			// render transparent.
			// enfin dans un pre render, 
			//throw new Error("Method not implemented.");
		}

		updateAngle: boolean = false;
		PreRender(deltaTime: number) {

			Gameplay.Main.xDirection = 0;
			var minPointerIndex: number = -1;
			var minPointerDuration: number = Infinity;
			for (var i = 0; i < game.inputManager.mouse.pointers.length; i++) {
				if (game.inputManager.mouse.pointers[i] != null
					&& game.inputManager.mouse.pointers[i].isDown
					&& minPointerDuration > game.inputManager.mouse.pointers[i].downDuration
					&& game.inputManager.mouse.pointers[i].interactiveElement == undefined) {
					minPointerDuration = game.inputManager.mouse.pointers[i].downDuration;
					minPointerIndex = i;
				}
			}
			if (minPointerIndex != -1) {

				if (game.inputManager.mouse.pointers[minPointerIndex].isDown && game.inputManager.mouse.pointers[minPointerIndex].downPosition.x >= game.width / 2) {
					Gameplay.Main.xDirection = -1;
				}
				if (game.inputManager.mouse.pointers[minPointerIndex].isDown && game.inputManager.mouse.pointers[minPointerIndex].downPosition.x < game.width / 2) {
					Gameplay.Main.xDirection = 1;
				}
			}

			if (game.inputManager.keyboard.isDown(KeyCode.leftarrow)) {
				Gameplay.Main.xDirection += 1;


			}
			if (game.inputManager.keyboard.isDown(KeyCode.rightarrow)) {
				Gameplay.Main.xDirection += -1;
			}
			var dir: number = 1.0;
			if (game.inputManager.keyboard.isDown(KeyCode.downarrow)) {
				dir = -0.0;
			}


			Gameplay.Main.follower.angle += (Gameplay.Main.xDirection * deltaTime * 180);

			//this.farTest.parent.updateMatrixWorld(true);
			//this.farTest.updateMatrixWorld(true);

			//this.angle += deltaTime * this.speed * 50;
			this.position += deltaTime * this.speed * dir;
			var curve = this.parent as BezierCurve;
			var frames = curve.activeData;
			var i = 0;
			for (i = this.activeIndex; i < frames.length && this.position >= frames[i].distance; i++) {

			}

			//console.log("FRAMES LEN:" + frames.length);
			//console.log("frame 0 dist:" + frames[0].distance);
			var nextIndex = i;
			var lastIndex = i - 1;
			if (nextIndex < frames.length) {
				//console.log("LAST INDEX:" + lastIndex);
				//console.log("NEXT INDEX:" + nextIndex);
				var diff = this.position - frames[lastIndex].distance;
				var interpolation = diff / (frames[nextIndex].distance - frames[lastIndex].distance);
				var pos3D = frames[nextIndex].origin.clone().sub(frames[lastIndex].origin).multiplyScalar(interpolation).add(frames[lastIndex].origin);
				var normal = frames[nextIndex].normal.clone().sub(frames[lastIndex].normal).multiplyScalar(interpolation).add(frames[lastIndex].normal);
				var tangent = frames[nextIndex].tangent.clone().sub(frames[lastIndex].tangent).multiplyScalar(interpolation).add(frames[lastIndex].tangent);
				var quat = QuaternionHelper.LookAtQuaternion(Vector3.Zero, tangent, normal);
				var angleRad = MathHelper.ToRad(this.angle);

				if (this.updateAngle) {

					var inverse = quat.clone().inverse();
					var upDir = new THREE.Vector3(0, 1.0, 0.0).applyQuaternion(this.object3D.quaternion);

					var pos2D = upDir.applyQuaternion(inverse);
					pos2D.z = 0;
					pos2D.normalize();
					var angleRad = Math.atan2(pos2D.y, pos2D.x);
					this.angle = MathHelper.ToDeg(angleRad);
					//console.log("UPDATED ANGLE.");
					this.updateAngle = false;
				}
				var angleRad = MathHelper.ToRad(this.angle);
				var upVec = new THREE.Vector3(Math.cos(angleRad), Math.sin(angleRad), 0.0).applyQuaternion(quat);
				quat = QuaternionHelper.LookAtQuaternion(Vector3.Zero, tangent, upVec);

				var angleRad = MathHelper.ToRad(this.angle);
				/*var pos2D = new THREE.Vector3(Math.cos(angleRad), Math.sin(angleRad), 0.0);
				pos2D.multiplyScalar(this.radius);
				var finalPos = pos2D.applyQuaternion(quat).add(pos3D);
				var upVector = pos3D.sub(finalPos);
				var rotationQuat = QuaternionHelper.LookAtQuaternion(Vector3.Zero, tangent, upVector);
	
				this.object3D.quaternion.copy(rotationQuat);
				this.object3D.position.copy(finalPos);
				this.object3D.updateMatrix();*/
				//var upVec = new THREE.Vector3(0, 1.0, 0.0).applyQuaternion(quat);
				this.object3D.setRotationFromQuaternion(quat);
				this.object3D.position.copy(pos3D);
				//				this.object3D.rotation.z = angleRad;
			}
			else {
				if (curve.next != undefined) {
				//	console.log("NEXT CURVE:" + curve.next);
					this.SetParent(curve.next);
					this.updateAngle = true;
					this.updateAngle = false;
				}
			}


			this.mesh.updateMatrixWorld(true);

			var worldPosition = this.mesh.getWorldPosition(new THREE.Vector3());
			var worldQuaternion = this.mesh.getWorldQuaternion(new THREE.Quaternion());


			if (!Gameplay.Main.editMode) {
				Gameplay.Main.camera.position.copy(worldPosition.clone());
				Gameplay.Main.camera.quaternion.copy(worldQuaternion.clone().multiply(new THREE.Quaternion().setFromAxisAngle(Vector3.Up, Math.PI)));
				Gameplay.Main.camera.updateMatrix();
			}
			/*	if ((game.inputManager.mouse.isDown && game.inputManager.mouse.downPosition.x >= game.width / 2)) {
					Gameplay.Main.xDirection = -1;
					//console.error("RIGHT SIDE.");
	
	
				} else if ((game.inputManager.mouse.isDown && game.inputManager.mouse.downPosition.x < game.width / 2)) {
					Gameplay.Main.xDirection = 1;
					
					//console.error("LEFT SIDE.");
				}
			*/









			/*	this.farTest.parent.updateMatrixWorld(true);
				this.farTest.updateMatrixWorld(true);
				var worldPos = this.farTest.getWorldPosition(new THREE.Vector3());
				var worldQuat = this.farTest.getWorldQuaternion(new THREE.Quaternion());
				this.farTest2.position.copy(worldPos);
				this.farTest2.quaternion.copy(worldQuat);
				this.farTest2.updateMatrix();
				this.farTest2.updateMatrixWorld(true);
	*/

			this.customRenderTarget.Render(false);
			(this.testPlane.material as THREE.MeshBasicMaterial).map = this.customRenderTarget.lastTextureOutput;
			(this.testPlane.material as THREE.MeshBasicMaterial).needsUpdate = true;
			(Gameplay.Main.curveMaterial as THREE.RawShaderMaterial).uniforms.depthTex.value = this.customRenderTarget.lastTextureOutput;
			(Gameplay.Main.curveMaterial as THREE.RawShaderMaterial).needsUpdate = true;


			this.sphere.position.copy(worldPosition);
			this.sphere.quaternion.copy(worldQuaternion);
			//this.simulation.updatePositionAndRotation(this.sphereBody);
			this.simulation.updatePositionAndRotation(this.sphereBody, this.sphere.position, this.sphere.quaternion);


		}
		private lastObstaclePosition: number = 20;
		private firstGeneration: boolean = true;
		Update0(deltaTime: number): void {
			var pool = Gameplay.Main.obstaclePool;
			var availableItems = pool.availableItems;
			var k: number = 0;
			while (availableItems > 0)
			{
				var patternTypes = [SpaceSpeedBlockType2.Bars, SpaceSpeedBlockType2.Circles, SpaceSpeedBlockType2.Cross];
				var randomPattern = MathHelper.randomValue(patternTypes);
				if (this.firstGeneration && k < patternTypes.length)
				{
					randomPattern = patternTypes[k];
				}
				k++;
			
				//randomPattern = SpaceSpeedBlockType2.Cross;
				var randomMode = MathHelper.randomInRange(0, 4);

				var randomAngle = MathHelper.randomInRange(0, 360);
			
				var obstacle = pool.GetOne(randomPattern, {angle: randomAngle, mode: randomMode, parent: BezierCurve.LastCurve, position: this.lastObstaclePosition});
				
				/*var cross = SpaceSpeedBlockGenerator.GeneratePattern2(randomPattern);
				var root: THREE.Object3D = new THREE.Object3D();
				for (var i = 0; i < cross.length; i++) {
					// rows
					for (var j = 0; j < cross[i].length; j++) {
						// columns
						for (var k = 0; k < cross[i][j].length; k++) {
							var mesh = new THREE.Mesh(cross[i][j][k], BezierCurve.ObstaclesMaterials);
							mesh.rotateX(MathHelper.ToRad(90));
							(mesh as any).obstacleInfo = <CurveObstacleInfo>{ columnIndex: j, rowIndex: i, actualAngle: 0 };
							var parent = new THREE.Object3D();
							parent.add(mesh);
							root.add(parent);

						}
					}
				}*/

			
				var randomDist = MathHelper.fit01(MathHelper.Random(), 10, 10);
				this.lastObstaclePosition += randomDist;
				
				availableItems--;
			}
			this.firstGeneration = false;
			//console.log("HELLO");
			//throw new Error("Method not implemented.");
		}
		Render0(i: number): void {
			//throw new Error("Method not implemented.");
		}
		Destroy0(): void {
			//throw new Error("Method not implemented.");
		}


	}
}