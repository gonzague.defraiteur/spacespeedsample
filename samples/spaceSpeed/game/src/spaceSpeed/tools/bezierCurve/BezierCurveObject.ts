module GAME {


	export abstract class BezierCurveObject extends GameEntity {
		position: number = 0;
		public abstract OnCurveUpdate(curve: BezierCurve): void;
		public constructor(parent: BezierCurve) {
			super(parent);
			parent.bezierCurveObjects.push(this);
		}
		public Destroy() {
			super.Destroy();
			if (this.parent != undefined) {
				var array = (this.parent as BezierCurve).bezierCurveObjects;
				var index = array.indexOf(this);
				if (index != -1) {
					array.splice(index, 1);
				}
			}
		}
		SetParent(parent: BezierCurve) {
			
			var curve = this.parent as BezierCurve;
			super.SetParent(parent);
			
			if (curve != undefined) {
				var objects = curve.bezierCurveObjects;
				var index = objects.indexOf(this);
				if (index != -1) {
					objects.splice(index, 1);
				}
			}
			if (parent != undefined) {
				parent.bezierCurveObjects.push(this);
			}

		}

	}
}