///<reference path="../LinkedList.ts"/>
///<reference path="./BezierCurveObject.ts"/>
module GAME {
	export class BezierCurves {
	}

	export class CylinderCurveObject extends BezierCurveObject {
		// ce sera certainement mieux avec un buffer machin la.
		public OnCurveUpdate(curve: BezierCurve): void { }
		Start0(): void { }
		Update0(deltaTime: number): void { }
		Render0(i: number): void { }
		Destroy0(): void { }
		mesh: THREE.Mesh = undefined;
	}

	export class BezierCurveManager {
		private AttachCurveToPrevious(curve: BezierCurve, previousCurve: BezierCurve) {
			curve.AlignNormalsWith(previousCurve);
		}
		private OnNodeInserted(args: {
			elem: LinkedList<BezierCurve>, next: LinkedList<BezierCurve>,
			prev: LinkedList<BezierCurve>
		}): void {
			this.AttachCurveToPrevious(args.elem.data, args.prev.data);

		}
		private OnNodeRemoved(args: {
			elem: LinkedList<BezierCurve>, next: LinkedList<BezierCurve>,
			prev: LinkedList<BezierCurve>, isRange: boolean
		}): void {

		}
		private currentNode: LinkedList<BezierCurve> = new LinkedList<BezierCurve>(undefined, true);
		public constructor() {
			this.currentNode.OnLinkedListInsertedEvent.AddListener(this, this.OnNodeInserted);
			this.currentNode.OnLinkedListRemovedEvent.AddListener(this, this.OnNodeRemoved);
		}
	}

	export abstract class BezierCurve extends GameEntity {

		GetFrameAtPosition(position: number): {frame: VectorFrame, curve: BezierCurve} {
			var distance = position;
			var curve = this as BezierCurve;
			var frames = curve.activeData;
			var i = 0;
			var activeIndex = 0;
			var updateAngle = false;
			var resTangent: THREE.Vector3;
			var resNormal: THREE.Vector3;
			var resOrigin: THREE.Vector3;
			var resAngle: number = 0;
			var dist = 0;

			while (frames[0].distance > distance) {
				if (curve.previous != undefined) {
					curve = curve.previous;
					frames = curve.activeData;
					//	console.log("GOES TO PREVIOUS CURVE");
				}
				else {
					
					break;
				}
			}
			// debugger;
			//console.log("BEGIN.");
		
			while (dist < position) {
				for (i = activeIndex; i < frames.length && position >= frames[i].distance; i++) {

				}

				//console.log("FRAMES LEN:" + frames.length);
				//console.log("frame 0 dist:" + frames[0].distance);
				var nextIndex = i;
				var lastIndex = i - 1;
				/*	if (lastIndex < 0) {
						console.error("LAST INDEX:" + lastIndex);
						console.error("POS:" + position);
						console.error("DIST:" + frames[i].distance);
					}*/
				if (nextIndex < frames.length) {
					//console.log("LAST INDEX:" + lastIndex);
					//console.log("NEXT INDEX:" + nextIndex);
					var diff = position - frames[lastIndex].distance;
					var interpolation = diff / (frames[nextIndex].distance - frames[lastIndex].distance);

					var res = VectorFrame.Lerp(frames[lastIndex], frames[nextIndex], interpolation);
					dist = frames[i].distance;
					// debugger;
					
					//console.error('RES DIST:' + dist);
					return ({frame: res, curve: curve});
					//console.log("FRAME DIST:" + dist);
				}
				else {
					if (curve.next == undefined) {
						curve.ConstructNextCurve();
					}
					if (curve.next != undefined) {
						curve = curve.next;
						frames = curve.activeData;
					}
					else {
						
						//console.error("ERROR.");
						break;
					}
				}
			}
			
			if (position == 0 && this.activeData[0].distance == 0)
			{
				// debugger;
				return ({frame: this.activeData[0].clone(), curve: this});
			}
			// debugger;
			return (undefined);
		}
		procMesh: ProceduralMesh;
		mesh: THREE.Mesh;
		lastGenerationPoints: THREE.Vector3[];
		lastObstaclePosition: any = 0;
		startFrame: VectorFrame;
		PlaceOnCurve(obj: THREE.Object3D, distance: number, angle: number = 0): { curve: BezierCurve, tangent: THREE.Vector3, normal: THREE.Vector3, origin: THREE.Vector3, position: number, angle: number } {
			var position = distance;
			var curve = this as BezierCurve;
			var frames = curve.activeData;
			var i = 0;
			var activeIndex = 0;
			var updateAngle = false;
			var resTangent: THREE.Vector3;
			var resNormal: THREE.Vector3;
			var resOrigin: THREE.Vector3;
			var resAngle: number = 0;
			var dist = 0;

			while (frames[0].distance > distance) {
				if (curve.previous != undefined) {
					curve = curve.previous;
					frames = curve.activeData;
					//	console.log("GOES TO PREVIOUS CURVE");
				}
				else {
					break;
				}
			}
			//console.log("BEGIN.");
			while (dist < position) {
				for (i = activeIndex; i < frames.length && position >= frames[i].distance; i++) {

				}

				//console.log("FRAMES LEN:" + frames.length);
				//console.log("frame 0 dist:" + frames[0].distance);
				var nextIndex = i;
				var lastIndex = i - 1;
				/*	if (lastIndex < 0) {
						console.error("LAST INDEX:" + lastIndex);
						console.error("POS:" + position);
						console.error("DIST:" + frames[i].distance);
					}*/
				if (nextIndex < frames.length) {
					//console.log("LAST INDEX:" + lastIndex);
					//console.log("NEXT INDEX:" + nextIndex);
					var diff = position - frames[lastIndex].distance;
					var interpolation = diff / (frames[nextIndex].distance - frames[lastIndex].distance);
					var pos3D = frames[nextIndex].origin.clone().sub(frames[lastIndex].origin).multiplyScalar(interpolation).add(frames[lastIndex].origin);
					var normal = frames[nextIndex].normal.clone().sub(frames[lastIndex].normal).multiplyScalar(interpolation).add(frames[lastIndex].normal);
					var tangent = frames[nextIndex].tangent.clone().sub(frames[lastIndex].tangent).multiplyScalar(interpolation).add(frames[lastIndex].tangent);


					resNormal = normal.clone();
					resOrigin = pos3D.clone();
					resAngle = angle;
					resTangent = tangent.clone();

					normal = normal.applyAxisAngle(tangent, MathHelper.ToRad(angle));
					var quat = QuaternionHelper.LookAtQuaternion(Vector3.Zero, tangent, normal);
					obj.position.copy(pos3D);
					obj.quaternion.copy(quat);
					dist = frames[i].distance;
					//console.log("FRAME DIST:" + dist);
				}
				else {
					if (curve.next == undefined) {
						curve.ConstructNextCurve();
					}
					if (curve.next != undefined) {
						curve = curve.next;
						frames = curve.activeData;
						//	console.log("GOES TO NEXT CURVE");
					}
					else {

						//	console.error("COULD NOT ADD CURVE:" + dist);
						//	console.error("CURVE POS:" + position);
						break;
					}
				}
			}
			//console.log("END:" + dist);
			return ({
				curve: curve, tangent: resTangent, normal: resNormal,
				origin: resOrigin, position: position, angle: resAngle
			});
		}
		next: BezierCurve;
		previous: BezierCurve;
		SwitchData(newData: VectorFrame[]) {
			this.activeData = newData;
			this.ConstructMeshes(this.activeData);
			for (var i = 0; i < this.bezierCurveObjects.length; i++) {
				this.bezierCurveObjects[i].OnCurveUpdate(this);
			}
		}
		AlignNormalsWith(previous: BezierCurve): VectorFrame[] {
	
/*
		var endFrame = previous.activeData[previous.activeData.length - 1];
			var endNormal = endFrame.normal.clone();

			var firstFrame = this.activeData[this.activeData.length - 1];
			var firstNormal = firstFrame.normal.clone();
			var firstTangent = firstFrame.tangent.clone();

			var quat = QuaternionHelper.LookAtQuaternion(Vector3.Zero, firstTangent, firstNormal);


			var inverse = quat.clone().inverse();
			var upDir = endNormal;

			var pos2D = upDir.applyQuaternion(inverse);
			pos2D.z = 0;
			pos2D.normalize();
			var angleRad = Math.atan2(pos2D.y, pos2D.x);



			for (var i = 0; i < this.activeData.length; i++) {

				var quat = QuaternionHelper.LookAtQuaternion(Vector3.Zero, this.activeData[i].tangent, this.activeData[i].normal);
				var upVec = new THREE.Vector3(Math.cos(angleRad), Math.sin(angleRad), 0.0).applyQuaternion(quat);
				upVec.normalize();
				this.activeData[i].normal = upVec;
			}

			*/
			this.ConstructMeshes(this.activeData);
			var dist = 0;
			//this.activeData[0].distance = 0;
			for (var i = 1; i < this.activeData.length; i++) {
				dist += this.activeData[i - 1].origin.distanceTo(this.activeData[i].origin);
				this.activeData[i].distance = dist;
			}
			for (var i = 0; i < this.bezierCurveObjects.length; i++) {
				this.bezierCurveObjects[i].OnCurveUpdate(this);
			}
			this.previous = previous;
			this.previous.next = this;

			for (var i = 0; i < this.activeData.length; i++) {
				this.activeData[i].distance += this.previous.activeData[this.previous.activeData.length - 1].distance;
			}
		/*	var next = this.activeData[1];
			var prev = previous.activeData[previous.activeData.length - 2];
			this.activeData[0] = VectorFrame.Lerp(prev, next, 0.5);
			previous.activeData[previous.activeData.length - 1] = this.activeData[0];*/
			return (this.activeData);
		}
		needsUpdate: boolean = false;
		controlObjects: THREE.Object3D[] = [];
		pts: THREE.Vector3[] = [];
		public get p0(): THREE.Object3D {
			return (this.controlObjects[0]);
		}
		public get p1(): THREE.Object3D {
			return (this.controlObjects[1]);
		}
		normalsMesh: THREE.LineSegments;
		lineMesh: THREE.Line;
		curveMesh: THREE.Line;
		bezierCurveObjects: BezierCurveObject[] = [];
		activeData: VectorFrame[];
		abstract InitializeMeshes(pts: THREE.Vector3[]): void;
		abstract GetFrames(pts: THREE.Vector3[]): VectorFrame[];
		private _GetFrames(pts: THREE.Vector3[]): VectorFrame[] {
			this.activeData = this.GetFrames(pts);
			var dist = 0;
			this.activeData[0].distance = 0;
			for (var i = 1; i < this.activeData.length; i++) {
				dist += this.activeData[i - 1].origin.distanceTo(this.activeData[i].origin);
				this.activeData[i].distance = dist;
			}
			return (this.activeData);
		}
		ConstructMeshes(frames: VectorFrame[]): void {
			//var frames = VectorFrame.GetQuadraticRMF(pts[0], pts[1], pts[2], this.divisions);
			for (var i = 0; i < frames.length; i++) {
				(this.curveMesh.geometry as THREE.Geometry).vertices[i]
					= frames[i].origin;
				(this.normalsMesh.geometry as THREE.Geometry).vertices[(i * 2) + 0]
					= frames[i].origin;
				(this.normalsMesh.geometry as THREE.Geometry).vertices[(i * 2) + 1]
					= frames[i].origin.clone().add(frames[i].normal.clone().multiplyScalar(1.0));
			}
			(this.lineMesh.geometry as THREE.Geometry).vertices[0] = this.pts[0];
			(this.lineMesh.geometry as THREE.Geometry).vertices[1] = this.pts[1];
			(this.lineMesh.geometry as THREE.Geometry).verticesNeedUpdate = true;
			(this.normalsMesh.geometry as THREE.Geometry).verticesNeedUpdate = true;
			(this.curveMesh.geometry as THREE.Geometry).verticesNeedUpdate = true;
		}
		public static LastCurve: BezierCurve = undefined;
		private _ConstructNextCurve() {
			var list: THREE.Vector3[][] = [];
			if (this.previous != undefined) {
				list = [this.previous.pts];
			}
			list.push(this.pts);
			var resInfo = CubicBezierUtil.GenerateRandomCurves(1, this.lastGenerationPoints);
			var res = resInfo.res;
			var data = res[0];
			var nextCurve = new CubicBezierCurve(this.parent, data, 100, this.activeData[this.activeData.length - 1], false);
			nextCurve.lastGenerationPoints = resInfo.pts;

			let oldData: VectorFrame[] = [];
			let newData: VectorFrame[];
			let curve: BezierCurve;
			var tmp = this.activeData;
			for (var j = 0; j < tmp.length; j++) {
				oldData.push(tmp[j].clone());
			}
			newData = nextCurve.AlignNormalsWith(this);
			curve = nextCurve;
			/*
			
			
						 console.error("LAST FRAME NORMAL:" + this.activeData[this.activeData.length - 1].normal.toArray());
					
						 console.error("NEXT FRAME NORMAL:" + this.activeData[this.activeData.length - 2].normal.toArray());
						 var index = this.activeData.length - 3;
						 var frameDat = this.activeData[index];
						 var oldFrameDat = this.activeData[(index - 1)];
						 var dist = frameDat.distance - oldFrameDat.distance; 
						 console.error("LASt FRAME DISTANCE:" + dist);
						 var index = this.activeData.length - 2;
						 var frameDat = this.activeData[index];
						 var oldFrameDat = this.activeData[(index - 1)];
						 var dist = frameDat.distance - oldFrameDat.distance; 
						 console.error("LASt FRAME DISTANCE:" + dist);
						 var index = this.activeData.length - 1;
						 var frameDat = this.activeData[index];
						 var oldFrameDat = this.activeData[(index - 1)];
						 var dist = frameDat.distance - oldFrameDat.distance; 
			
						 console.error("NEXT FRAME DISTANCE:" + dist);
						 console.error("NEXT FRAME DISTANCE:" + (nextCurve.activeData[0].distance - frameDat.distance));
			
						 console.error("LAST FRAME POSITION:" + this.activeData[this.activeData.length - 2].origin.toArray());
						 console.error("NEXT FRAME POSITION:" + this.activeData[this.activeData.length - 1].origin.toArray());
			*/
		/*	var procMesh = SpaceSpeedCylinderGenerator.GenerateCylinder(nextCurve, this.procMesh, 0.3, 10, Gameplay.Main.curveRadius * 1.1);
			nextCurve.procMesh = procMesh;
			var geom = procMesh.GenerateTHREEGeometry();
	
			geom.computeBoundingBox();
			var center = geom.boundingBox.getCenter(new THREE.Vector3());

			var mesh = new THREE.Mesh();
			mesh.position.copy(center.clone());
			var mat = mesh.matrix;
			var inv = mat.getInverse(new THREE.Matrix4());
			for (var j = 0; j < geom.vertices.length; j++) {
				geom.vertices[j].sub(mesh.position);
			}
			mesh.renderOrder = this.mesh.renderOrder - 1;
			mesh.geometry = geom;
			nextCurve.mesh = mesh;
			mesh.material = this.mesh.material;
			//var center = geom.center();

			Gameplay.Main.scene.add(mesh);*/
			this.next = nextCurve;
			this.next.lastObstaclePosition = this.lastObstaclePosition;
			this.next.Start();
			//this.next.GenerateRandomObstacles();
		}
		ConstructNextCurve(count: number = 1) {

			this._ConstructNextCurve();
			var next = this.next;
			for (var i = 0; i < count - 1; i++) {
				next._ConstructNextCurve();
				next = next.next;
			}
			if (BezierCurve.ObstaclesMaterials == undefined) {
				BezierCurve.ObstaclesMaterials = [new THREE.MeshPhongMaterial({ color: 0xFFFF00 }), new THREE.MeshPhongMaterial({ color: 0xFF0000 }), new THREE.MeshPhongMaterial({ color: 0xFF0000 })];
			
			}
			//this.controlList.push(mesh);

			//lastProcMesh = procMesh;

		}

		public static ObstaclesMaterials: THREE.MeshPhongMaterial[] = undefined;
		GenerateRandomObstacles(): void {
			

			var randomDist = MathHelper.fit01(MathHelper.Random(), 10, 10);
			//console.log("RANDOM DIST:" + randomDist);
			//console.log("NEW POSITION:" + Gameplay.Main.lastObstaclePosition);
			Gameplay.Main.lastObstaclePosition += randomDist;

			var lastPos = this.activeData[this.activeData.length - 1].distance;

			while (Gameplay.Main.lastObstaclePosition < lastPos) {
				var patternTypes = [SpaceSpeedBlockType2.Bars, SpaceSpeedBlockType2.Circles, SpaceSpeedBlockType2.Cross];
				var randomPattern = MathHelper.randomValue(patternTypes);
				//randomPattern = SpaceSpeedBlockType2.Cross;
				var randomMode = MathHelper.randomInRange(0, 4);

				var randomAngle = MathHelper.randomInRange(0, 360);
				var startPosition: number = this.lastObstaclePosition;

				var cross = SpaceSpeedBlockGenerator.GeneratePattern2(randomPattern);
				var root: THREE.Object3D = new THREE.Object3D();
				for (var i = 0; i < cross.length; i++) {
					// rows
					for (var j = 0; j < cross[i].length; j++) {
						// columns
						for (var k = 0; k < cross[i][j].length; k++) {
							var mesh = new THREE.Mesh(cross[i][j][k], BezierCurve.ObstaclesMaterials);
							mesh.rotateX(MathHelper.ToRad(90));
							(mesh as any).obstacleInfo = <CurveObstacleInfo>{ columnIndex: j, rowIndex: i, actualAngle: 0 };
							var parent = new THREE.Object3D();
							parent.add(mesh);
							root.add(parent);

						}
					}
				}

				var test = new CurveObstacle(root, Gameplay.Main.lastObstaclePosition, randomAngle, randomMode, randomPattern, this);
				var randomDist = MathHelper.fit01(MathHelper.Random(), 10, 10);
				//console.log("RANDOM DIST:" + randomDist);
				//this.lastObstaclePosition = Gameplay.Main.lastObstaclePosition;
				//this.next.lastObstaclePosition = Gameplay.Main.lastObstaclePosition;
				Gameplay.Main.lastObstaclePosition += randomDist;
				//console.log("NEW POSITION:" + Gameplay.Main.lastObstaclePosition);
			}


		}
		private static curveLineMat: THREE.LineBasicMaterial = undefined;
		private static normalsMat: THREE.LineBasicMaterial = undefined;
		private static linesMat: THREE.LineBasicMaterial = undefined;

		constructor(parent: EntityContainer, pts: THREE.Vector3[], protected divisions: number = 100, startFrame: VectorFrame = undefined, protected debug: boolean = true,
			protected debugOptions = {
				debugLine: true, debugCurve: true, debugNormals: true,
				debugControls: true,
				lineColor: 0xFFFFFF, curveColor: 0xFFFF00, normalColor: 0xFF0000
			}) {
			super(parent);
			this.pts = pts;
			this.startFrame = startFrame;

			var geom = new THREE.Geometry();
			for (var i = 0; i < this.divisions; i++) {
				geom.vertices.push(new THREE.Vector3());
			}
			if (BezierCurve.curveLineMat == undefined) {
				BezierCurve.curveLineMat = new THREE.LineBasicMaterial({ color: this.debugOptions.curveColor });
				BezierCurve.normalsMat = new THREE.LineBasicMaterial({ color: this.debugOptions.normalColor });
				BezierCurve.linesMat = new THREE.LineBasicMaterial({ color: this.debugOptions.lineColor });
			}
			this.curveMesh = new THREE.Line(geom, BezierCurve.curveLineMat);

			var geom = new THREE.Geometry();
			for (var i = 0; i < this.divisions; i++) {
				geom.vertices.push(new THREE.Vector3());
				geom.vertices.push(new THREE.Vector3());
			}
			this.normalsMesh = new THREE.LineSegments(geom, BezierCurve.normalsMat);
			var geom = new THREE.Geometry();
			geom.vertices.push(new THREE.Vector3());
			geom.vertices.push(new THREE.Vector3());
			this.lineMesh = new THREE.LineSegments(geom, BezierCurve.linesMat);
			if (!(this.debug && this.debugOptions.debugCurve)) {
				this.curveMesh.visible = false;
			}
			if (!(this.debug && this.debugOptions.debugLine)) {
				this.lineMesh.visible = false;
			}
			if (!(this.debug && this.debugOptions.debugNormals)) {
				this.normalsMesh.visible = false;
			}
			if (debug) {
				Gameplay.Main.scene.add(this.curveMesh);
				Gameplay.Main.scene.add(this.lineMesh);
				Gameplay.Main.scene.add(this.normalsMesh);
			}
			for (var i = 0; i < pts.length; i++) {
				this.controlObjects.push(new THREE.Mesh(new THREE.CubeGeometry(0.5, 0.5, 0.5), new THREE.MeshPhongMaterial({ color: i < 2 ? 0x0000FF : 0x00FF00 })));
				this.controlObjects[i].position.copy(pts[i].clone());//.clone();
				if (debug) {

					Gameplay.Main.scene.add(this.controlObjects[i]);


					Gameplay.Main.controlList.push(this.controlObjects[i]);
				}
				if (!(this.debug && this.debugOptions.debugControls)) {
					//	console.log("NOT VISIBLE.");
					this.controlObjects[i].visible = true;
				}
			}

			this.InitializeMeshes(pts);

			this.ConstructMeshes(this._GetFrames(pts));
			BezierCurve.LastCurve = this;

		}
		UpdateFromPoints() {
			for (var i = 0; i < this.controlObjects.length; i++) {
				this.controlObjects[i].position.copy(this.pts[i]);
			}

			this.UpdateAll();
		}
		SetPoints(pts: THREE.Vector3[]) {
			for (var i = 0; i < this.controlObjects.length; i++) {
				this.controlObjects[i].position.copy(pts[i]);
			}
			this.UpdateAll();
		}
		UpdateAll() {
			console.log("UPDATE ALL.");
			this.UpdatePoints();
			this.ConstructMeshes(this._GetFrames(this.pts));
			for (var i = 0; i < this.bezierCurveObjects.length; i++) {
				this.bezierCurveObjects[i].OnCurveUpdate(this);
			}
			this.needsUpdate = false;
		}
		Update(deltaTime: number) {

			if (this.needsUpdate) {
				console.log("NEEDS UPDATE.");
				this.UpdateAll();
			}
			super.Update(deltaTime);
			if (Gameplay.Main.follower.position - this.activeData[this.activeData.length - 1].distance > 1.0) {
				this.Destroy();
			}
		}
		UpdatePoints(): any {
			for (var i = 0; i < this.controlObjects.length; i++) {
				this.pts[i] = this.controlObjects[i].position.clone();
			}
		}
		Destroy() {
			super.Destroy();

			var meshes = [this.mesh, this.curveMesh, this.lineMesh, this.normalsMesh];
			for (var i = 0; i < meshes.length; i++) {
				if (meshes[i] != undefined) {
					if (meshes[i].parent != undefined) {
						meshes[i].parent.remove(meshes[i]);
					}
					meshes[i].geometry.dispose()
				}
			}
			if (this.mesh != undefined) {
				if (this.mesh.parent != undefined) {
					this.mesh.parent.remove(this.mesh);
				}
				this.mesh.geometry.dispose();
			}
			if (this.next != undefined) {
				this.next.previous = undefined;
			}
			if (this.previous != undefined) {
				this.previous.next = undefined;
			}
		}
	}
	export class QuadraticBezierCurve extends BezierCurve {
		GetFrames(pts: THREE.Vector3[]): VectorFrame[] {
			var frames = VectorFrame.GetQuadraticRMF(pts[0], pts[1], pts[2], this.divisions);
			return (frames);
		}
		InitializeMeshes(pts: THREE.Vector3[]): void {
			if (pts.length != 3) {
				console.error("Cannot initialize Quadratic Bezier Curve: must have 3 points.");
			}
		}
		Start0(): void { }
		Update0(deltaTime: number): void { }
		Render0(i: number): void { }
		Destroy0(): void { }
		public get controlPoint(): THREE.Object3D {
			return (this.controlObjects[2]);
		}
	}


	export class CubicBezierCurve extends BezierCurve {
		GetFrames(pts: THREE.Vector3[]): VectorFrame[] {
			var frames : VectorFrame[] = [];
			
			frames = VectorFrame.GetCubicRMF(pts[0], pts[1], pts[2], pts[3], this.divisions, this.startFrame);
			return (frames);
		}
		InitializeMeshes(pts: THREE.Vector3[]): void {
			if (pts.length != 4) {
				console.error("Cannot initialize Quadratic Bezier Curve: must have 4 points.");
			}
		}
		Start0(): void { }
		Update0(deltaTime: number): void { }
		Render0(i: number): void { }
		Destroy0(): void { }
		public get controlPoint0(): THREE.Object3D {
			return (this.controlObjects[2]);
		}
		public get controlPoint1(): THREE.Object3D {
			return (this.controlObjects[3]);
		}
	}
}