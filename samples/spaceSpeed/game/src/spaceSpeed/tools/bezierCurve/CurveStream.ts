module GAME {
	export class CurveStream extends BezierCurveObject {
		public actualEnd: number = 0;
		public actualEndFrame: VectorFrame = undefined;
		public actualPosition: number = 0;
		public geometry: THREE.Geometry = undefined;
		public actualVertRangeIndex: number = 0;
		elemCount: number;
		mesh: THREE.Mesh;
	
		public OnCurveUpdate(curve: BezierCurve): void {

		}
		UpdateGeometry() {
			// 1 - j'update la position par rapport a la courbe.
			// 2 - j'enleve les vertices qui sont avant la position du début.
			// 2 - je trouve la fin, je rajoute 
		
			var availableVertRangeCount: number = 0;
			while (this.actualPosition <= (this.position - (1 / this.divisionsByLen))) {
				this.actualPosition += (1 / this.divisionsByLen);
				// prepare a range of vertices to be moved to the end.
				availableVertRangeCount++;
				

			}
			//console.log("VERT RANGE:" + availableVertRangeCount);


			while (availableVertRangeCount > 0) {
				this.actualEnd += (1 / this.divisionsByLen);
				var currentFrame = this.actualEndFrame;
				var nextFrameData = (this.parent as BezierCurve).GetFrameAtPosition(this.actualEnd);
				var nextFrame = nextFrameData.frame;
				this.actualEndFrame = frame;

				this.actualEndFrame = nextFrameData.frame;
				if (this.parent != nextFrameData.curve)
				{
					this.SetParent(nextFrameData.curve);
				}

				for (var i = (this.actualVertRangeIndex * this.segments); i < (this.actualVertRangeIndex + 1) * this.segments; i++) {


					var frame = currentFrame;

					var lookatQuat = QuaternionHelper.LookAtQuaternion(Vector3.Zero,
						frame.tangent, frame.normal);
					var pos: THREE.Vector3 = frame.origin.clone();

					var angle = (i / this.segments) * 360.0;
					var rad = MathHelper.ToRad(angle);
					var x = Math.cos(rad);
					var y = Math.sin(rad);

					var vec = new THREE.Vector3(x, y, 0.0).multiplyScalar(this.radius);

					var worldDirection = vec.clone().applyQuaternion(lookatQuat);
					var worldPosition = worldDirection.clone().add(pos);


					this.geometry.vertices[(i * 2)].copy(worldPosition);


					var frame = nextFrame;

					var lookatQuat = QuaternionHelper.LookAtQuaternion(Vector3.Zero,
						frame.tangent, frame.normal);
					var pos: THREE.Vector3 = frame.origin.clone();


					var angle = (i / this.segments) * 360.0;
					var rad = MathHelper.ToRad(angle);
					var x = Math.cos(rad);
					var y = Math.sin(rad);

					var vec = new THREE.Vector3(x, y, 0.0).multiplyScalar(this.radius);

					var worldDirection = vec.clone().applyQuaternion(lookatQuat);
					var worldPosition = worldDirection.clone().add(pos);


					this.geometry.vertices[(i * 2) + 1].copy(worldPosition);

				}
				
				this.actualVertRangeIndex++;
				this.actualVertRangeIndex = this.actualVertRangeIndex % this.elemCount;
				availableVertRangeCount--;
			}
			this.geometry.verticesNeedUpdate = true;
		

		}
		InitializeGeometry() {
			if (this.divisionsByLen < 1 / 10) {
				this.divisionsByLen = 1 / 10;
			}
			this.geometry = new THREE.Geometry();
			var vertices: THREE.Vector3[] = [];
			//var normals: THREE.Vector3[] = [];
			//this.geometry.faces
			var faces: THREE.Face4[] = [];
			this.geometry.vertices = vertices;
			this.geometry.faces = faces;
			var iiuv = [new THREE.Vector2(0, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, 1), new THREE.Vector2(0, 1)];
			var elemIndex: number = 0;
			var uvs: THREE.Vector2[][][];
			var uv0 : THREE.Vector2[][] = [];
			while (this.actualEnd <= this.position + this.length) {
				// if (this.actualEndFrame == undefined)
				// get the tangent/normal/etc... from this.actualEnd
				// put it in this.actualEndFrame
				if (this.actualEndFrame == undefined) {
					
					this.actualEndFrame = (this.parent as BezierCurve).GetFrameAtPosition(this.actualEnd).frame;
			
					//debugger;
				}
				this.actualEnd += (1 / this.divisionsByLen);
				var currentFrame = this.actualEndFrame;
				//debugger;
				var nextFrame = (this.parent as BezierCurve).GetFrameAtPosition(this.actualEnd).frame;
				this.actualEndFrame = nextFrame;



				var endIndices: number[] = [];
				var startIndices: number[] = [];
				var faceIds: number[] = [];
				var startFaces: number[] = [];
				var endFaces: number[] = [];


				for (var i = 0; i < this.segments; i++) {

					var frame = currentFrame;
					var vertPositions: THREE.Vector3[] = [];
					var lookatQuat = QuaternionHelper.LookAtQuaternion(Vector3.Zero,
						frame.tangent, frame.normal);
					var pos: THREE.Vector3 = frame.origin.clone();


					var angle = (i / this.segments) * 360.0;
					var rad = MathHelper.ToRad(angle);
					var x = Math.cos(rad);
					var y = Math.sin(rad);

					var vec = new THREE.Vector3(x, y, 0.0).multiplyScalar(this.radius);

					var worldDirection = vec.clone().applyQuaternion(lookatQuat);
					var worldPosition = worldDirection.clone().add(pos);

					startIndices.push(this.geometry.vertices.length);
					this.geometry.vertices.push(new THREE.Vector3().copy(worldPosition)); // un vertice de start

					var frame = nextFrame;
					var vertPositions: THREE.Vector3[] = [];
					var lookatQuat = QuaternionHelper.LookAtQuaternion(Vector3.Zero,
						frame.tangent, frame.normal);
					var pos: THREE.Vector3 = frame.origin.clone();

					var angle = (i / this.segments) * 360.0;
					var rad = MathHelper.ToRad(angle);
					var x = Math.cos(rad);
					var y = Math.sin(rad);

					var vec = new THREE.Vector3(x, y, 0.0).multiplyScalar(this.radius);

					var worldDirection = vec.clone().applyQuaternion(lookatQuat);
					var worldPosition = worldDirection.clone().add(pos);

					endIndices.push(this.geometry.vertices.length);
					this.geometry.vertices.push(new THREE.Vector3().copy(worldPosition));// un vertice de end

					startFaces.push(this.geometry.faces.length);
					//uv0.push([]);
					this.geometry.faces.push(new THREE.Face3(0, 0, 0));// une face deux vertices de start un de end

					endFaces.push(this.geometry.faces.length);
					//uv0.push([]);
					this.geometry.faces.push(new THREE.Face3(0, 0, 0));//une face deux vertices de end, un de start.
				}




				for (var i = 0; i < this.segments; i++) {

					var i0 = startIndices[i];
					var ii0 = 0;

					var i1 = endIndices[(i + 1) % endIndices.length];
					var ii1 = 2;

					var i2 = endIndices[(i) % endIndices.length];
					var ii2 = 1;


					var face = this.geometry.faces[startFaces[i]];
					face.a = i2;
					face.b = i1;
					face.c = i0;

					// console.log("START FACE ID:" + startFaces[i]);
					uv0.push([iiuv[ii0], iiuv[ii1], iiuv[ii2]].reverse());
					for (var j = 0; j < uv0[uv0.length - 1].length; j++)
					{
						if (uv0[uv0.length - 1][j] == undefined)
						{

							console.error("UNDEFINED UV.");
							debugger;
						}
					}
					var color = new THREE.Color(0, 1, 0).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
					if (i % 2 == 1) {
						var color = new THREE.Color(0, 1, 0).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
						if (elemIndex % 2 == 1) {
							color = new THREE.Color(0.75, 0, 0.25);
						}

					}
					else {
						color = new THREE.Color(1, 0, 1).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
					}
					this.geometry.faces[startFaces[i]].vertexColors = [color, color, color];


					i0 = startIndices[i];
					ii0 = 0;
					i1 = startIndices[(i + 1) % startIndices.length];
					ii1 = 3;
					i2 = endIndices[(i + 1) % endIndices.length];
					ii2 = 2;

					var face = this.geometry.faces[endFaces[i]];
					face.a = i2;
					face.b = i1;
					face.c = i0;

					// console.log("END FACE ID:" + endFaces[i]);
					uv0.push([iiuv[ii0], iiuv[ii1], iiuv[ii2]].reverse());
					for (var j = 0; j < uv0[uv0.length - 1].length; j++)
					{
						if (uv0[uv0.length - 1][j] == undefined)
						{

							console.error("UNDEFINED UV.");
							debugger;
						}
					}
					//uv0[endFaces[i]] = [new THREE.Vector2(0.5, 0.5), new THREE.Vector2(0.5, 0.5), new THREE.Vector2(0.5, 0.5)];
					var color = new THREE.Color(0, 1, 0).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
					if (i % 2 == 1) {
						var color = new THREE.Color(0, 1, 0).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
						if (elemIndex % 2 == 1) {
							color = new THREE.Color(0.75, 0, 0.25);
						}
					}
					else {
						color = new THREE.Color(1, 0, 1).multiplyScalar(0.45).add(new THREE.Color(0, 0, 0.45));
					}
					this.geometry.faces[endFaces[i]].vertexColors = [color, color, color];
				}
				
	
				this.geometry.faceVertexUvs = uvs;
				this.geometry.uvsNeedUpdate = true;
				
				elemIndex++;

				// add one range of vertices/faces at the end:
				// get the tangent/normal/etc.. from this.actualEndFrame
				// now get the tangent/normal/etc... from this.actualEnd
				// add and place vertices/faces
				// update this.actualEndFrame with new values.
			}
			uvs = [uv0];
			this.geometry.faceVertexUvs = uvs;
			this.elemCount = elemIndex;
		}

		Start0(): void {
			if (Math.floor(this.length * this.divisionsByLen) % 2 == 0)
			{
				this.length += (1 / this.divisionsByLen);
			}
			this.InitializeGeometry();
			this.mesh = new THREE.Mesh(this.geometry, Gameplay.Main.curveMaterial);
			Gameplay.Main.scene.add(this.mesh);
			this.mesh.renderOrder = 3000;
			this.mesh.frustumCulled = false;
		}
		Update0(deltaTime: number): void {
			this.position = Gameplay.Main.follower.position - 2.0;
		//	console.log("UPDATED POSITION:" + this.position);
			this.UpdateGeometry();
		}
		Render0(i: number): void {

		}
		Destroy0(): void {

		}

		public constructor(parent: BezierCurve, public length: number, public radius: number = 1.3, public divisionsByLen: number = 0.30, public segments: number = 10) {
			
			super(parent);
			
			this.length = length;
		}
	}
}