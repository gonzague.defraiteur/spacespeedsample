module GAME {

    export class TimeCount extends plume.Container {

        time: number;
        protected textureList: string[];
        updatable: boolean = false;
        protected pause: boolean;
        protected color: number;
        protected size: number;
        protected cont: plume.Container;
        protected scaleStart: number;
        protected scaleEnd: number;
        protected alphaStart: number;
        protected alphaEnd: number;

        protected _onEnd: plume.ContextualCallback;
        protected updater: UpdaterH5;

        protected soundToPlay: string;

        constructor(seconde: number, size?: number, color?: number) {
            super();
            this.soundToPlay = null;
            this.scaleStart = 1;
            this.scaleEnd = 1;
            this.alphaStart = 1;
            this.alphaEnd = 1;
            this.pause = false;
            this.cont = null;
            this._onEnd = null;
            this.size = size != undefined ? size : 42;
            this.color = color != undefined ? color : null;
            this.time = seconde;
            this.buildNumber();

            this.updater = new UpdaterH5(this);
            this.updater.addUpdatable();

            this.cont.setSizeFromChildren();
            this.setSizeFromChildren();
        }

        setSound(sound: string) {
            this.soundToPlay = sound;
        }

        protected buildNumber() {
            if (this.cont != null) this.cont.removeSelf();
            let t = "" + Math.floor(this.time)
            var text = factory.newText(t, this.size, this.color);
            text.x = -text.width / 2;
            text.y = -text.height / 2;
            let volume = factory.newText(t, this.size, 0XAAAAAA);
            volume.x = text.x;
            volume.y = text.y + 4;
            let shadow = factory.newText(t, this.size, 0);
            shadow.x = text.x;
            shadow.y = text.y + 12;
            if (this.color)
                text.tint = this.color;
            this.cont = new plume.Container();
            this.cont.addChild(shadow);
            this.cont.addChild(volume);
            this.cont.addChild(text);
            this.addChild(this.cont);
        }

        setScaleEffect(start: number, end: number) {
            this.scaleStart = start;
            this.scaleEnd = end;
        }

        setAlphaEffect(start: number, end: number) {
            this.alphaStart = Math.max(0, Math.min(start, 1));
            this.alphaEnd = Math.max(0, Math.min(start, 1));
        }

        setPause(pauseON: boolean) {
            this.pause = pauseON;
        }

        setClassicEffect() {
            this.setScaleEffect(1.2, 1);
            this.setAlphaEffect(1, 0.3);
        }

        onEnd(callback: Function, context?: any) {
            if (callback)
                this._onEnd = new plume.ContextualCallback(callback, context);
            else
                this._onEnd = null;
        }

        destroy() {
            if (this._onEnd)
                this._onEnd.execute();
            this.updater.stopUpdatable();
            super.removeSelf();
        }

        protected updateEffect() {
            var ratio = Math.abs(Math.floor(this.time) - this.time);
            var ratioScale = 1 - ratio * ratio;
            var ratioAlpha = 1 - ratio;
            this.cont.scaleXY = this.scaleStart + (this.scaleEnd - this.scaleStart) * ratioScale;
            this.cont.alpha = this.alphaStart + (this.alphaEnd - this.alphaStart) * ratioAlpha;

        }

        update(delta: number) {
            let dt = delta * 0.001;
            if (this.pause != true) {
                let old = this.time;
                this.time = Math.max(0, this.time - 1 * dt);
                if (Math.floor(this.time) != Math.floor(old))
                    this.buildNumber();
                if (this.time <= 0) this.destroy();
                this.updateEffect();
            }
        }
    }
}