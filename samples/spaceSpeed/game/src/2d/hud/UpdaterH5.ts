module GAME {

    export class UpdaterH5 {

        protected updatable: boolean;
        protected objToUpdt: { update: (delta?: number) => void };

        constructor(objToUpdt: { update: (delta?: number) => void }, dontStopAuto?: boolean) {
            this.updatable = false;
            this.objToUpdt = objToUpdt;

            let detachListener: plume.Handler<any> = objToUpdt['detachListener'];
            if (detachListener && dontStopAuto != true)
                detachListener.add(this.stopUpdatable, this);
        }


        addUpdatable() {
            if (this.updatable != true) {
                this.updatable = true;
                Game.get().addUpdatable(this.objToUpdt);
            }
        }

        stopUpdatable() {
            if (this.updatable == true) {
                this.updatable = false;
                Game.get().removeUpdatable(this.objToUpdt);
            }
        }
    }
}