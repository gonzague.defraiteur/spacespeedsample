module GAME {

    export class GameParameters {

        fpsHelper = true;
        datGUI = true;

        constructor() {
            this.fpsHelper = this.getBool("fpsHelper", true);
            this.datGUI = this.getBool("config", false);

            logger.debug("--- Debug Parameters ---");
            logger.debug("fpsHelper: " + this.fpsHelper);
            logger.debug("datGUI: " + this.datGUI);
            logger.debug("--- ---------------- ---");
        }

        private getBool(key: string, defValue: boolean): boolean {
            let v = plume.StartupParams.get(key);
            if (v == null) return defValue;
            return (v == "true");
        }
        private getNumber(key: string, defValue: number): number {
            let v = plume.StartupParams.get(key);
            if (v == null) return defValue;
            return (parseFloat(v));
        }
    }

}