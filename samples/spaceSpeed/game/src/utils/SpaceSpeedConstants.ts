module GAME {

	export const enum Constant {
		DebugBorder = 0,
	}

	export class Colors {
		static Initialize() {
			// if (adsHandler.gameplay.ballColorArray != undefined) {
			// 	this.BallYellowColorArray = adsHandler.gameplay.ballColorArray;
			// }

			// if (adsHandler.gameplay.obstaclesColorArray != undefined) {
			// 	this.EnemyRedColorArray = adsHandler.gameplay.obstaclesColorArray;
			// }

			// if (adsHandler.gameplay.platformColorArray != undefined) {
			// 	this.PlatformBlackColorArray = adsHandler.gameplay.platformColorArray;
			// }

			// if (adsHandler.gameplay.postColorArray != undefined) {
			// 	this.PostWhiteColorArray = adsHandler.gameplay.postColorArray;
			// }

			// if (adsHandler.gameplay.perfectRedColor != undefined) {
			// 	this.RedColorArray = [adsHandler.gameplay.perfectRedColor];
			// }
		}
		private static _paletteIndex: number = 0;
		public static get paletteIndex() {
			return (this._paletteIndex);
		}
		public static set paletteIndex(value: number) {
			this._paletteIndex = value;

		}
		public static get Red() {
			return (this.RedColorArray[this.paletteIndex % this.RedColorArray.length])
		}
		private static RedColorArray: number[] = [0xFF0000];
		public static get BallYellow() {
			return (this.BallYellowColorArray[this.paletteIndex % this.BallYellowColorArray.length])
		}
		private static BallYellowColorArray: number[] = [0xEEBE2E, 0x35B9E0, 0xBBC13F, 0xFF5A41,
			0x6B51D0, 0xF9FAFE, 0x394B8A, 0x2A262B, 0xC78EF5];
		public static get PostWhite() {

			return (this.PostWhiteColorArray[this.paletteIndex % this.PostWhiteColorArray.length])
		}
		private static PostWhiteColorArray: number[] = [0xD9D5D9, 0xE1DEAD, 0xF5DB90, 0xC47574,
			0x99CEB8, 0x47403E, 0xCBE3FA, 0xF4E4C0, 0x412255];
		public static get PlatformBlack() {

			return (this.PlatformBlackColorArray[this.paletteIndex % this.PlatformBlackColorArray.length])
		}
		private static PlatformBlackColorArray: number[] = [0x302C31, 0x565C90, 0xF9D059, 0x833969,
			0x81AF11, 0xD4834D, 0x3FA5E0, 0xFAAE28, 0xEABA92];
		public static get EnemyRed() {

			return (this.EnemyRedColorArray[this.paletteIndex % this.EnemyRedColorArray.length])
		}
		private static EnemyRedColorArray: number[] = [0xE83A26, 0xEA6626, 0xFD651E, 0xC03763,
			0xE1BC22, 0xF55641, 0xE781B5, 0xF15F06, 0xDE5317];
	}

	// export class Gradients {
	// 	public static Initialize() {
	// 		// if (adsHandler.gameplay.backgrounds != undefined) {
	// 		// 	this._gradients = adsHandler.gameplay.backgrounds;
	// 		// }
	// 	}
	// 	private static _gradients = ["-webkit-gradient(linear, center top, center bottom, from(#878188), to(#3E3A3F))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#D5D0B9), to(#646891))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#D5DE8B), to(#B8C058))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#B29E8A), to(#64827C))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#AA9AD3), to(#588D9E))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#CBC48E), to(#9FA26F))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#B797D6), to(#513A99))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#F39E66), to(#E4D08A))",
	// 		"-webkit-gradient(linear, center top, center bottom, from(#573E60), to(#D79E7D))"];
	// 	// public static get gradients(): string[] {
	// 	// 	if (adsHandler.gameplay.backgrounds != undefined) {
	// 	// 		this._gradients = adsHandler.gameplay.backgrounds;
	// 	// 	}
	// 	// 	return (this._gradients);
	// 	// }
	// 	// public static get Length() {
	// 	// 	return (this.gradients.length);
	// 	// }
	// }

}