/**
 * Example Vertex Shader
 * Sets the position of the vertex by setting gl_Position
 */

// Set the precision for data types used in this shader
precision highp float;
precision highp int;

// Default THREE.js uniforms available to both fragment and vertex shader
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 boundingBox;
uniform vec3 nbRows;
// Default uniforms provided by ShaderFrog.
uniform vec3 cameraPosition;
uniform float time;
uniform sampler2D texture1;
uniform sampler2D depthTex;

// Default attributes provided by THREE.js. Attributes are only available in the
// vertex shader. You can pass them to the fragment shader using varyings
attribute vec3 position;
attribute vec3 normal;
attribute vec2 uv;
attribute vec2 uv2;
attribute vec4 color;

// Examples of variables passed from vertex to fragment shader
varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;
varying vec2 vUv2;
varying vec4 vColor;
varying vec4 vProjectedPos;
varying vec2 vScreenPos;



void main() {

  // To pass variables to the fragment shader, you assign them here in the
  // main function. Traditionally you name the varying with vAttributeName
  vNormal = normal;
  vUv = uv * vec2(1.1, 1.2);
  vUv2 = uv2;
  vPosition = position;
  vColor = color;

			gl_Position = vec4(position.x, position.y, 1.0, 1.0);
		
}