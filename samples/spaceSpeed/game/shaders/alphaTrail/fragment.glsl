/**
 * Example Fragment Shader
 * Sets the color and alpha of the pixel by setting gl_FragColor
 */
#define M_PI 3.1415926535897932384626433832795
// Set the precision for data types used in this shader
precision highp float;
precision highp int;

// Default THREE.js uniforms available to both fragment and vertex shader
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 boundingBox;
uniform vec4 lineColor;
uniform int useUvs;
uniform float uvRotation;
uniform sampler2D texture1;
uniform vec2 uvScale;

// Default uniforms provided by ShaderFrog.
uniform vec3 cameraPosition;
uniform float time;
uniform float alphaMultiplier;

// A uniform unique to this shader. You can modify it to the using the form
// below the shader preview. Any uniform you add is automatically given a form
uniform vec4 color;
uniform vec3 lightPosition;

// Example varyings passed from the vertex shader
varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;
varying vec2 vUv2;
varying vec4 vColor;
float floatmod(float nb, float modulo) {

  float floored = floor(nb / modulo);
  float res = nb - (float(floored) * modulo);
  return (res);
}

float GetDistanceFromGrid(float position, float gridSize, float minDist) {
  float mod1 = mod(position, gridSize);
  float res = mod1;
  float mod2 = abs(gridSize - mod1);

  { res = min(abs(mod1), abs(mod2)); }
  return (res);
}

vec2 GetGridUVS(vec3 position, float gridSizes2, float minDist) {

  float res = 0.0;
  vec3 gridSizes = vec3(gridSizes2, gridSizes2, gridSizes2);
  gridSizes = vec3(1.0, 1.0, 1.0);
  float boxSizeX = 0.0;
  float boxSizeY = 0.0;
	float baseRot = uvRotation;
	float uvRotX = baseRot / 180.0 * 3.1415926535897932384626433832795;
	float uvRotY = uvRotX + (90.0 / 180.0)  * 3.1415926535897932384626433832795;
	vec2 dirX =  vec2(cos(uvRotX), sin(uvRotX));
	vec2 dirY = vec2(cos(uvRotY), sin(uvRotY));
	vec2 rotatedUv = dirX * uvScale.x * vUv.x + dirY * uvScale.y * vUv.y;
	float Uvx = rotatedUv.x;
	float Uvy = rotatedUv.y;

	float x = mod(Uvx  + (-(boxSizeX / 2.0)), gridSizes.x);
	float y = mod(Uvy  + (-(boxSizeY / 2.0)), gridSizes.y);
	
	
/*  float x =
      GetDistanceFromGrid(vUv.x + (-(boxSizeX / 2.0)), gridSizes.x, minDist);
  float y =
      GetDistanceFromGrid(vUv.y + (-(boxSizeY / 2.0)), gridSizes.y, minDist);*/
  vec3 resVec = vec3(x, y, 0);

  return (vec2(x, y));
}

float GetDistanceFromGridUVS(vec3 position, float gridSizes2, float minDist) {

  float res = 0.0;
  vec3 gridSizes = vec3(gridSizes2, gridSizes2, gridSizes2);
  gridSizes = vec3(1.0, 1.0, 1.0);
  float boxSizeX = 0.0;
  float boxSizeY = 0.0;
  float x =
      GetDistanceFromGrid(vUv.x + (-(boxSizeX / 2.0)), gridSizes.x, minDist);
  float y =
      GetDistanceFromGrid(vUv.y + (-(boxSizeY / 2.0)), gridSizes.y, minDist);
  vec3 resVec = vec3(x, y, 0);

  if ((x <= minDist || y <= minDist)) {
    return (min(x, y));
  }

  return (minDist * 2.0);
}

float GetDistanceFromGrid(vec3 position, float gridSizes2, float minDist) {

  float res = 0.0;
  vec3 gridSizes = vec3(gridSizes2, gridSizes2, gridSizes2);
  gridSizes = vec3(0.5, 0.5, 0.5);
  float boxSizeX = 0.0;
  float boxSizeY = 0.0;
  float x = GetDistanceFromGrid(position.x + (-(boxSizeX / 2.0)), gridSizes.x,
                                minDist);
  float y = GetDistanceFromGrid(position.y + (-(boxSizeY / 2.0)), gridSizes.y,
                                minDist);
  float z = GetDistanceFromGrid(position.z + (-(boxSizeX / 2.0)), gridSizes.z,
                                minDist);
  vec3 resVec = vec3(x, y, z);

  if ((z <= minDist || y <= minDist) && (z > 0.00001 || x <= minDist)) {
    return (0.0);
  }
  if ((x <= minDist || y <= minDist) && (x > 0.00001 || z <= minDist)) {
    return (0.0);
  }
  return (minDist * 2.0);
}

void main() {

  // Calculate the real position of this pixel in 3d space, taking into account
  // the rotation and scale of the model. It's a useful formula for some
  // effects. This could also be done in the vertex shader
  gl_FragColor = vec4(0, 1, 0, (1.0 - vUv.x) * alphaMultiplier);
 /* vec3 worldPosition = (modelMatrix * vec4(vPosition, 1.0)).xyz;

  // Calculate the normal including the model rotation and scale
  vec3 worldNormal = normalize(vec3(modelMatrix * vec4(vNormal, 0.0)));

  vec3 lightVector = normalize(lightPosition - worldPosition);

  // An example simple lighting effect, taking the dot product of the normal
  // (which way this pixel is pointing) and a user generated light position
  float brightness = dot(worldNormal, lightVector);
  float minDist = 0.045;
  float dist = 0.0;
  if (useUvs == 0) {
    minDist *= 0.35;
    dist = GetDistanceFromGrid(vPosition, 0.5, minDist);
  } else if (useUvs == 1) {
    dist = GetDistanceFromGridUVS(vPosition, 0.5, minDist);
  }

  if (useUvs == 2) {
    gl_FragColor = texture2D(texture1, vUv) * 3.5;
    // gl_FragColor = vec4(1.0, 0, 0, 1.0);
  }
  else if (useUvs == 3)
  {
	  //gl_FragColor = vec4(0, 0, 0, 1);
	
	   gl_FragColor = texture2D(texture1, GetGridUVS(vPosition, 0.5, minDist)) ;
  } 
  else {
    if (dist < minDist) {
      gl_FragColor = lineColor;
    } else {
      gl_FragColor = vec4(0, 0, 0, 1.0);
    }
  }
  // Fragment shaders set the gl_FragColor, which is a vector4 of
  // ( red, green, blue, alpha ).
  // gl_FragColor = vec4(1, 0, 0, 1);// vec4( color * brightness, 1.0 );*/
}