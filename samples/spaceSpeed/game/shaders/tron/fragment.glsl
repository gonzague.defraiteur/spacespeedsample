/**
 * Example Fragment Shader
 * Sets the color and alpha of the pixel by setting gl_FragColor
 */
#define M_PI 3.1415926535897932384626433832795
// Set the precision for data types used in this shader
precision highp float;
precision highp int;

// Default THREE.js uniforms available to both fragment and vertex shader
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 boundingBox;
uniform vec4 lineColor;
uniform int useUvs;
uniform float uvRotation;
uniform sampler2D texture1;
uniform vec2 uvScale;
uniform sampler2D depthTex;

// Default uniforms provided by ShaderFrog.
uniform vec3 cameraPosition;
uniform float time;

// A uniform unique to this shader. You can modify it to the using the form
// below the shader preview. Any uniform you add is automatically given a form
uniform vec3 lightPosition;

// Example varyings passed from the vertex shader
varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;
varying vec2 vUv2;
varying vec4 vColor;
varying vec4 vProjectedPos;
varying vec2 vScreenPos;
float floatmod(float nb, float modulo) {

  float floored = floor(nb / modulo);
  float res = nb - (float(floored) * modulo);
  return (res);
}

float GetDistanceFromGrid(float position, float gridSize, float minDist) {
  float mod1 = mod(position, gridSize);
  float res = mod1;
  float mod2 = abs(gridSize - mod1);

  { res = min(abs(mod1), abs(mod2)); }
  return (res);
}

vec2 GetGridUVS(vec3 position, float gridSizes2, float minDist) {

  float res = 0.0;
  vec3 gridSizes = vec3(gridSizes2, gridSizes2, gridSizes2);
  gridSizes = vec3(1.0, 1.0, 1.0);
  float boxSizeX = 0.0;
  float boxSizeY = 0.0;
	float baseRot = uvRotation;
	float uvRotX = baseRot / 180.0 * 3.1415926535897932384626433832795;
	float uvRotY = uvRotX + (90.0 / 180.0)  * 3.1415926535897932384626433832795;
	vec2 dirX =  vec2(cos(uvRotX), sin(uvRotX));
	vec2 dirY = vec2(cos(uvRotY), sin(uvRotY));
	vec2 rotatedUv = dirX * uvScale.x * vUv.x + dirY * uvScale.y * vUv.y;
	float Uvx = rotatedUv.x;
	float Uvy = rotatedUv.y;

	float x = mod(Uvx  + (-(boxSizeX / 2.0)), gridSizes.x);
	float y = mod(Uvy  + (-(boxSizeY / 2.0)), gridSizes.y);
	
	
/*  float x =
      GetDistanceFromGrid(vUv.x + (-(boxSizeX / 2.0)), gridSizes.x, minDist);
  float y =
      GetDistanceFromGrid(vUv.y + (-(boxSizeY / 2.0)), gridSizes.y, minDist);*/
  vec3 resVec = vec3(x, y, 0);
//
  return (vec2(x, y));
}

float GetDistanceFromGridUVS(vec3 position, float gridSizes2, float minDist) {

  float res = 0.0;
  vec3 gridSizes = vec3(gridSizes2, gridSizes2, gridSizes2);
  gridSizes = vec3(1.0, 1.0, 1.0);
  float boxSizeX = 0.0;
  float boxSizeY = 0.0;
  float x =
      GetDistanceFromGrid(vUv.x + (-(boxSizeX / 2.0)), gridSizes.x, minDist);
  float y =
      GetDistanceFromGrid(vUv.y + (-(boxSizeY / 2.0)), gridSizes.y, minDist);
  vec3 resVec = vec3(x, y, 0);

  if ((x <= minDist || y <= minDist)) {
    return (min(x, y));
  }

  return (minDist * 2.0);
}

float GetDistanceFromGrid(vec3 position, float gridSizes2, float minDist) {

  float res = 0.0;
  vec3 gridSizes = vec3(gridSizes2, gridSizes2, gridSizes2);
  gridSizes = vec3(0.5, 0.5, 0.5);
  float boxSizeX = 0.0;
  float boxSizeY = 0.0;
  float x = GetDistanceFromGrid(position.x + (-(boxSizeX / 2.0)), gridSizes.x,
                                minDist);
  float y = GetDistanceFromGrid(position.y + (-(boxSizeY / 2.0)), gridSizes.y,
                                minDist);
  float z = GetDistanceFromGrid(position.z + (-(boxSizeX / 2.0)), gridSizes.z,
                                minDist);
  vec3 resVec = vec3(x, y, z);

  if ((z <= minDist || y <= minDist) && (z > 0.00001 || x <= minDist)) {
    return (0.0);
  }
  if ((x <= minDist || y <= minDist) && (x > 0.00001 || z <= minDist)) {
    return (0.0);
  }
  return (minDist * 2.0);
}

const vec4 bitSh = vec4(256. * 256. * 256., 256. * 256., 256., 1.);
const vec4 bitMsk = vec4(0.,vec3(1./256.0));
const vec4 bitShifts = vec4(1.) / bitSh;
const float rangeMin = -1.;
const float rangeMax = -1.;

float DecodeFloatRGBA( vec4 rgba ) {
 return dot( rgba, vec4(1.0, 1.0/255.0, 1.0/65025.0, 1.0/16581375.0) );
}

vec4 EncodeFloatRGBA( float v ) {
  vec4 enc = vec4(1.0, 255.0, 65025.0, 16581375.0) * v;
  enc = fract(enc);
  enc -= enc.yzww * vec4(1.0/255.0,1.0/255.0,1.0/255.0,0.0);
  return enc;
}

vec4 pack (float value) {
    vec4 comp = fract(value * bitSh);
    comp -= comp.xxyz * bitMsk;
    return comp;
}

float unpack (vec4 color) {
    return dot(color , bitShifts);
}

float convertFromColorToRange(vec4 color) {
   float zeroToOne = unpack(color);
   return rangeMin + zeroToOne * (rangeMax - rangeMin);
}

vec4 convertFromRangeToColor(float value) {
   float zeroToOne = (value - rangeMin) / (rangeMax - rangeMin);
   return pack(value);
}

void main() {

  // Calculate the real position of this pixel in 3d space, taking into account
  // the rotation and scale of the model. It's a useful formula for some
  // effects. This could also be done in the vertex shader
  vec3 worldPosition = (modelMatrix * vec4(vPosition, 1.0)).xyz;

  // Calculate the normal including the model rotation and scale
  vec3 worldNormal = normalize(vec3(modelMatrix * vec4(vNormal, 0.0)));

  vec3 lightVector = normalize(lightPosition - worldPosition);

  // An example simple lighting effect, taking the dot product of the normal
  // (which way this pixel is pointing) and a user generated light position
  float brightness = dot(worldNormal, lightVector);
  float minDist = 0.065;
  float dist = 0.0;
  if (useUvs == 0) {
    minDist *= 0.35;
    dist = GetDistanceFromGrid(vPosition, 0.5, minDist);
  } else if (useUvs == 1) {
    dist = GetDistanceFromGridUVS(vPosition, 0.5, minDist);
  }

  if (useUvs == 2) {
    gl_FragColor = texture2D(texture1, vUv) * 3.5;
    // gl_FragColor = vec4(1.0, 0, 0, 1.0);
  }
  else if (useUvs == 3)
  {
	  //gl_FragColor = vec4(0, 0, 0, 1);
	
	   gl_FragColor = texture2D(texture1, GetGridUVS(vPosition, 0.5, minDist)) ;
  } 
  else {
    if (dist < minDist) {
      gl_FragColor = vec4(vColor.rgb, 0.7);
    } else {
      gl_FragColor = vec4(0, 0, 0, 0.02);
    }
  }
  if (vUv.x > 1.0 || vUv.y > 1.0)
  {
	   gl_FragColor = vec4(0, 0, 0, 0.02);
  }
  float zPos = ((gl_FragCoord.z / gl_FragCoord.w) / 1000.0) * 10.0;
  
  vec2 scPos = vProjectedPos.xy / vProjectedPos.w;
	scPos = scPos + 1.0;
	scPos = scPos / 2.0;

	float decode = 0.0;
	vec4 colorTest = texture2D(depthTex, scPos);

	if (colorTest.r != 0.0)
	{
		if (zPos > colorTest.r)
		{
			gl_FragColor = vec4(0, 0, 0, 0.0);
		}
	}	
	
	
	//gl_FragColor = colorTest;
	//float originalZ = gl_FragCoord.z / gl_FragCoord.w;
	/*if (decode * 1000.0 < zPos)
	{
		gl_FragColor = vec4(0, 0, 0, 0);
	}*/
//gl_FragColor = EncodeFloatRGBA(zPos);

/*
  vec2 coord = scPos;
						//vec2 divider = vec2(viewport.z, viewport.w);
						//coord = coord / divider;
coord = coord + 1.0;
coord = coord / 2.0;*/
/*	float originalZ = gl_FragCoord.z / gl_FragCoord.w;
	*/
	//vec4 coord = vec4(gl_FragCoord);
	//float colorTest = texture2D(depthTex, coord);
/*
if (depth.r < originalZ)
{
	gl_FragColor = vec4(0, 0, 0, 0.0);
}*/
//gl_FragColor = depth;
//gl_FragColor = depth;
 /* else
  {
	  gl_FragColor = vec4(1.0, 0, 0, 1.0);
  }*/
  
  //gl_FragColor = vec4(vUv.x, vUv.y, 0.0, 1.0);
  /*if (length(vUv - vec2(0, 0)) < 0.1)
  {
	  gl_FragColor = vec4(1.0, 0, 0, 1.0);
  }*/
  // Fragment shaders set the gl_FragColor, which is a vector4 of
  // ( red, green, blue, alpha ).
  // gl_FragColor = vec4(1, 0, 0, 1);// vec4( color * brightness, 1.0 );
}