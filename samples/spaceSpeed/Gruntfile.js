module.exports = function (grunt) {
    'use strict';

    var vendorPath = '../../plume3d/vendor';
    var vendorPath2 = '../vendor';
    var minisdkPath = '../../integration/minisdk/export';
	var buildPath = '../../plume3d/export/latest';
	var currentPath = './game/js/';

    var publisher = grunt.option('to') || 'dummy';
    grunt.log.writeln('Building publisher #' + publisher);
    grunt.option("subproject", "plume");

    var build = {
        depsFiles: [
            buildPath + '/plume3d.core.js',
            buildPath + '/plume3d.gui.js',
            buildPath + '/plume3d.tween.js',
            buildPath + '/plume3d.cannon.js',
            vendorPath + '/three.min.js',
			vendorPath + '/three.gltfloader.js',
            // vendorPath + '/three.dracoloader.js',
            vendorPath + '/cannon.min.js',
            minisdkPath + '/vads.js',
			vendorPath + '/pako_inflate.min.js',
			currentPath + '/random-seed.js',
			currentPath + '/three-editor-controls.js',
			currentPath + '/three-transform-controls.js',
			vendorPath + '/howler.core.min.js',

        ],
        jsLibs: [
            // { path: vendorPath, file: 'draco/**' },
        ],
        packagejson: grunt.file.readJSON("package.json"),
        publisherJsonConfig: grunt.file.readJSON("publisher.json"),
        publisher: publisher,
        fonts: {
            inputs: ['assetsSource/fonts/arcade.fnt'],
            xml: true
        },
        shaders: {
            inputs: ['game/shaders'],
        },
        assets: {
            copyMobile: false
        },
        ads: {
            zipJson: true,
            zipBin: true,
            zipImages: false,
            inputs: [
                'index.html',
				'build/game.min.js',
				'game/assets/textures/mainmap.png',
				'game/assets/3d/man_walk_optimized.gltf',
				'game/assets/3d/animations/skinnedmeshes/grandma_optimized.gltf',
				'game/assets/3d/animations/skinnedmeshes/grandpa_optimized.gltf',
				'game/assets/3d/animations/skinnedmeshes/woman_optimized.gltf',
				'game/assets/3d/animations/skinnedmeshes/kid_optimized.gltf',
                'game/assets/3d/gonzague48_optimized.gltf',
				'game/assets/3d/gonzague48_optimized.bin',
                'game/assets/3d/crown.gltf',
                'game/assets/3d/crown.bin',
                'game/assets/3d/Assets/Arena_1/Texture2D/BillboardAds.jpg',
                'game/assets/3d/Assets/Arena_1/Texture2D/PolygonCity_Road_01.jpg',
                'game/assets/3d/Assets/Arena_1/Texture2D/PolygonCity_Texture_01_A.jpg',
                'game/assets/3d/Assets/Arena_1/Texture2D/PolygonCity_Texture_01_C.jpg',
                'game/assets/3d/Assets/Arena_1/Texture2D/PolygonCity_Texture_02_B.jpg',
                'game/assets/3d/Assets/Arena_1/Texture2D/PolygonCity_Texture_03_B.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_1.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_2.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_3.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_6.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_8.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_9.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_15.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_16.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_18.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_19.jpg',
                'game/assets/3d/Assets/optimized_assets/NewAssets/BLD_21.jpg',
            ],
            boot: [
            ]
        }
    };
    grunt.build = build;


    grunt.loadTasks('../../build/grunt');
    grunt.loadTasks('../build/grunt');


    grunt.config('ads', {
        fb: {
        }
    });
};
