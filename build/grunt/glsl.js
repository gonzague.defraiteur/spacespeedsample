module.exports = function (grunt) {

    var os = require('os')
    var fs = require('fs');

    var tab = 0;
    var currentFile;
    var isWin = /^win/.test(os.platform());
    var multilineComment = /^[\t\s]*\/\*\*?[^!][\s\S]*?\*\/[\r\n]/gm
    var singleLineComment = /^[\t\s]*(\/\/)[^\n\r]*[\n\r]/gm
    var lineEndings = (isWin ? "\r\n" : "\n");
    var trim = true;
    var stripComments = true;

    function addLine(text) {
        var tabs = "";
        for (var i = 0; i < tab; i++) {
            tabs += "\t";
        }
        fs.writeSync(currentFile, tabs + text + '\n');
    }

    function processShader(file, shadername, vertex) {
        var src = grunt.util.normalizelf(grunt.file.read(file)).trim();

        // Remove comments
        if (stripComments) {
            src = src.replace(multilineComment, '');
            src = src.replace(singleLineComment, '');
        }

        var shaderArrSrc = src.split(lineEndings);
        if (trim) {
            shaderArrSrc = shaderArrSrc.map(function (line) {
                return line.trim()
            });
        }

        // Remove empty lines
        shaderArrSrc = shaderArrSrc.filter(function (line) {
            return (line.length > 0);
        });

        var name = shadername + "_" + (vertex ? "vertex" : "fragment");
        var out = "export let " + name + " = '" + shaderArrSrc.join('\\n') + "';";

        // console.log("out => " + out);
        addLine(out);
    }

    function processFile(path, name, shadername) {
        var file = path + "/" + name;
        if (name == "fragment.glsl") {
            processShader(file, shadername, false);
        } else if (name == "vertex.glsl") {
            processShader(file, shadername, true);
        } else {
            grunt.log.console.warn("Ignoring shader " + file);
        }
    }

    function processFolder(path, name) {
        //console.log("Processing folder " + name + " in " + path);
        var folderpath = path + "/" + name;
        fs.readdirSync(folderpath).forEach(file => {
            processFile(folderpath, file, name);
        });
    }

    grunt.registerMultiTask('glsl', 'Generate ts file with shaders', function (prop) {
        var folders = this.data.inputs;
        var output = this.data.output;
        var namespace = this.data.namespace || "GAME";

        if (typeof folders === "undefined" || folders.length == 0) {
            grunt.log.error("No folder provided");
            return;
        }

        if (folders.length == 0) {
            grunt.log.writeln("No shader to process.")
            return;
        }

        for (var i = 0; i < folders.length; i++) {
            let folder = folders[i];
            var ns = (typeof namespace == "object" ? namespace[i] : namespace);
            var out = (typeof output == "object" ? output[i] : output);

            // create dir if necessary
            if (!fs.existsSync(out)) {
                fs.mkdirSync(out, '0777', true);
            }

            var outputFile = out + '/Shaders.ts';

            tab = 0;
            currentFile = fs.openSync(outputFile, 'w');
            addLine('// this file is auto-generated.');
            addLine('module ' + ns + '.shaders {');
            addLine('');
            tab++;

            fs.readdirSync(folder).forEach(file => {
                processFolder(folder, file);
            });

            addLine('');
            tab--;
            addLine('}')
        }
    });

};
