﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{

    public static Vector3 Round(this Vector3 vector3, int decimalPlaces)
    {
        float multiplier = 1;
        for (int i = 0; i < decimalPlaces; i++)
        {
            multiplier *= 10f;
        }
        return new Vector3(
            Mathf.Round(vector3.x * multiplier) / multiplier,
            Mathf.Round(vector3.y * multiplier) / multiplier,
            Mathf.Round(vector3.z * multiplier) / multiplier);
    }

    public static Quaternion Round(this Quaternion q, int decimalPlaces)
    {
        float multiplier = 1;
        for (int i = 0; i < decimalPlaces; i++)
        {
            multiplier *= 10f;
        }
        return new Quaternion(
            Mathf.Round(q.x * multiplier) / multiplier,
            Mathf.Round(q.y * multiplier) / multiplier,
            Mathf.Round(q.z * multiplier) / multiplier,
            Mathf.Round(q.w * multiplier) / multiplier);
    }

}
