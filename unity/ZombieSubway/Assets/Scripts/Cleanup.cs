﻿#if (UNITY_EDITOR)

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityExporter;

namespace Subway
{

    public class Cleanup : EditorWindow
    {

        [MenuItem("Subway/Cleanup/Remove inactive node (in selection)")]
        static void RemoveInactiveNode()
        {
            var selections = Selection.transforms;
            foreach (var t in selections)
            {
                Debug.Log("Cleaning inactive in " + t.name);
                RemoveInactiveNode0(t.gameObject);
            }
        }

        private static void RemoveInactiveNode0(GameObject gameObject)
        {
            SceneHelper.traverse(gameObject, node =>
            {
                if (!node.activeSelf)
                {
                    DestroyImmediate(node);
                }
            });
        }

        [MenuItem("Subway/Cleanup/Remove un-rendered leaf (in selection)")]
        static void RemoveUnrenderedLeaf()
        {
            var selections = Selection.transforms;
            foreach (var t in selections)
            {
                Debug.Log("Cleaning inactive in " + t.name);
                for (var i = 0; i < 5; i++)
                {
                    RemoveUnrenderedLeaf0(t.gameObject);
                }
            }
        }

        private static void RemoveUnrenderedLeaf0(GameObject gameObject)
        {
            SceneHelper.traverse(gameObject, node =>
            {
                if (node.transform.childCount <= 0)
                {
                    // leaf
                    var renderer = node.GetComponent<Renderer>();
                    if (renderer != null)
                        return;

                    var light = node.GetComponent<Light>();
                    if (light != null)
                        return;

                    DestroyImmediate(node);
                }
            });
        }

    }

}

#endif