﻿#if (UNITY_EDITOR)

using System;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityExporter
{
    [Serializable, CanEditMultipleObjects]
    public abstract class EditorScriptComponent : MonoBehaviour
    {
        void Start() { }

        protected EditorScriptComponent()
        {
        }

        public abstract void Serialize(Newtonsoft.Json.Linq.JObject jso);
    }
}


#endif
