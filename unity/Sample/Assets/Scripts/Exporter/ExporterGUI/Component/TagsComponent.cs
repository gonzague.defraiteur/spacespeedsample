﻿#if (UNITY_EDITOR)

using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

namespace UnityExporter
{
    [AddComponentMenu("Gamebuilt/Components/Component Tags", 1)]
    public sealed class TagsComponent : EditorScriptComponent
    {
        public List<string> tags;

        override public void Serialize(JObject jso)
        {
            if (tags == null || tags.Count <= 0) return;

            jso.Add(new JProperty("tags", new JArray(tags)));
        }
    }
}


#endif
