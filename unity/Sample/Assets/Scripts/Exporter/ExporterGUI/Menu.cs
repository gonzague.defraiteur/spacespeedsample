﻿#if (UNITY_EDITOR)

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityExporter
{
    public class Menu : EditorWindow
    {
        private static bool prefRead = false;

        [MenuItem("Gamebuiltv2/Settings")]
        static void Init()
        {
            Menu window = (Menu)EditorWindow.GetWindow(typeof(Menu), false, "Export Settings");
            window.Show();
        }

        void OnGUI()
        {
            Menu.ReadPref();

            EditorGUILayout.LabelField("Exporter", EditorStyles.boldLabel);

            // Default options
            ExporterOptions.ExportOnlyActive = EditorGUILayout.Toggle("Export only active nodes", ExporterOptions.ExportOnlyActive);
            ExporterOptions.ExportLightmap = EditorGUILayout.Toggle("Lightmaps: export lightmap", ExporterOptions.ExportLightmap);
            ExporterOptions.ExporMeshUv2 = EditorGUILayout.Toggle("Mesh: export uv2", ExporterOptions.ExporMeshUv2);
            ExporterOptions.ExporMeshNormal = EditorGUILayout.Toggle("Mesh: export normal", ExporterOptions.ExporMeshNormal);
            ExporterOptions.ExporMeshTangent = EditorGUILayout.Toggle("Mesh: export tangent", ExporterOptions.ExporMeshTangent);
            ExporterOptions.JpgEncoding = EditorGUILayout.Toggle("Jpeg encoding", ExporterOptions.JpgEncoding);
            ExporterOptions.JpgQuality = (int)EditorGUILayout.Slider("Jpeg quality", ExporterOptions.JpgQuality, 1, 100);


            if (GUILayout.Button("Save"))
            {
                var error = false;
                if (ExporterOptions.ExportLightmap && !ExporterOptions.ExporMeshUv2)
                {
                    ExporterOptions.ExporMeshUv2 = true; // Force to export uv2 for lightmap
                    error = true;
                }

                if (!error)
                {
                    Menu.SavePref();
                    this.Close();
                }
            }
        }

        private static void SavePref()
        {
            EditorPrefs.SetBool("ExporterOptions.ExportOnlyActive", ExporterOptions.ExportOnlyActive);
            EditorPrefs.SetBool("ExporterOptions.ExportLightmap", ExporterOptions.ExportLightmap);
            EditorPrefs.SetBool("ExporterOptions.ExporMeshUv2", ExporterOptions.ExporMeshUv2);
            EditorPrefs.SetBool("ExporterOptions.ExporMeshNormal", ExporterOptions.ExporMeshNormal);
            EditorPrefs.SetBool("ExporterOptions.ExporMeshTangent", ExporterOptions.ExporMeshTangent);
            EditorPrefs.SetBool("ExporterOptions.JpgEncoding", ExporterOptions.JpgEncoding);
            EditorPrefs.SetInt("ExporterOptions.JpgQuality", ExporterOptions.JpgQuality);
        }

        private static void ReadPref()
        {
            if (Menu.prefRead) return;

            ExporterOptions.ExportOnlyActive = EditorPrefs.GetBool("ExporterOptions.ExportOnlyActive", true);
            ExporterOptions.ExportLightmap = EditorPrefs.GetBool("ExporterOptions.ExportLightmap", false);
            ExporterOptions.ExporMeshUv2 = EditorPrefs.GetBool("ExporterOptions.ExporMeshUv2", false);
            ExporterOptions.ExporMeshNormal = EditorPrefs.GetBool("ExporterOptions.ExporMeshNormal", false);
            ExporterOptions.ExporMeshTangent = EditorPrefs.GetBool("ExporterOptions.ExporMeshTangent", false);
            ExporterOptions.JpgEncoding = EditorPrefs.GetBool("ExporterOptions.JpgEncoding", true);
            ExporterOptions.JpgQuality = EditorPrefs.GetInt("ExporterOptions.JpgQuality", 90);

            Menu.prefRead = true;
        }


        private static string getExportPath()
        {
            string lastPath = EditorPrefs.GetString("gbexporter_lastPath", "");
            string path = EditorUtility.OpenFolderPanel("Export Path", lastPath, "");
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            EditorPrefs.SetString("gbexporter_lastPath", path);
            return path;
        }

        [MenuItem("Gamebuiltv2/Export Scene")]
        public static void ExportScene()
        {
            Menu.ReadPref();

            var scene = SceneManager.GetActiveScene();
            var gameObjects = scene.GetRootGameObjects();
            var transforms = Array.ConvertAll(gameObjects, gameObject => gameObject.transform);

            var exporter = new GLTFExporter(transforms);
            string path = getExportPath();
            if (string.IsNullOrEmpty(path))
            {
                Debug.LogError("No export path specified");
                return;
            }
            exporter.SaveGLTFandBin(path, scene.name);
        }
    }
}

#endif
