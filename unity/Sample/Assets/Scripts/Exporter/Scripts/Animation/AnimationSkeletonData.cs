﻿#if (UNITY_EDITOR)

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace UnityExporter
{

    class AnimationSkeletonDataCache
    {
        private GLTFExporter exporter;

        ListCache<float> fcache = new ListCache<float>();
        ListCache<Vector3> v3cache = new ListCache<Vector3>();
        ListCache<Quaternion> qcache = new ListCache<Quaternion>();

        public AnimationSkeletonDataCache(GLTFExporter exporter)
        {
            this.exporter = exporter;
        }

        public GLTF.Schema.AccessorId GetAccessorId(List<float> datas)
        {
            var id = fcache.Contains(datas);
            if (id != null) return id;

            id = this.exporter.accessorExporter.Export(datas);
            fcache.Add(datas, id);
            return id;
        }
        public GLTF.Schema.AccessorId GetAccessorId(List<Vector3> datas)
        {
            var id = v3cache.Contains(datas);
            if (id != null) return id;

            id = this.exporter.accessorExporter.Export(datas);
            v3cache.Add(datas, id);
            return id;
        }
        public GLTF.Schema.AccessorId GetAccessorId(List<Quaternion> datas)
        {
            var id = qcache.Contains(datas);
            if (id != null) return id;

            id = this.exporter.accessorExporter.Export(datas);
            qcache.Add(datas, id);
            return id;
        }

    }

    class ListCache<T>
    {
        List<List<T>> cache = new List<List<T>>();
        List<GLTF.Schema.AccessorId> ids = new List<GLTF.Schema.AccessorId>();

        public GLTF.Schema.AccessorId Contains(List<T> input)
        {
            for (var i = 0; i < cache.Count; i++)
            {
                var equals = input.SequenceEqual(cache[i]);
                if (equals)
                {
                    return ids[i];
                }
            }
            return null;
        }

        public void Add(List<T> input, GLTF.Schema.AccessorId id)
        {
            cache.Add(input);
            ids.Add(id);
        }
    }

    //
    // Data by bone
    // On export, generate rotation/translation/scale sampler
    //
    class AnimationSkeletonData
    {
        public GLTF.Schema.SamplerId SamplerId { get; private set; }

        private GLTFExporter exporter;
        private AnimationClip clip;
        private Transform bone;
        private SkinnedMeshRenderer skinnedMesh;
        private AnimationSkeletonDataCache cache;
        // private AnimationClipSettings clipSettings;

        private List<float> time = new List<float>();
        private List<Vector3> scale = new List<Vector3>();
        private List<Vector3> position = new List<Vector3>();
        private List<Quaternion> rotation = new List<Quaternion>();

        private Vector3 initialPosition;
        private Vector3 initialScale;
        private Quaternion initialRotation;
        private bool saveT = false;
        private bool saveR = false;
        private bool saveS = false;

        public AnimationSkeletonData(GLTFExporter exporter, AnimationClip clip, SkinnedMeshRenderer skinnedMesh, Transform bone, AnimationSkeletonDataCache cache)
        {
            this.exporter = exporter;
            this.clip = clip;
            this.bone = bone;
            this.skinnedMesh = skinnedMesh;
            this.cache = cache;

            this.initialPosition = new Vector3(this.bone.localPosition.x, this.bone.localPosition.y, this.bone.localPosition.z);
            this.initialScale = new Vector3(this.bone.localScale.x, this.bone.localScale.y, this.bone.localScale.z);
            this.initialRotation = new Quaternion(this.bone.localRotation.x, this.bone.localRotation.y, this.bone.localRotation.z, this.bone.localRotation.w);
            // this.clipSettings = UnityEditor.AnimationUtility.GetAnimationClipSettings(clip);
        }

        public void SaveSamplingData(float time)
        {
            this.time.Add(time);
            this.position.Add(new Vector3(this.bone.localPosition.x, this.bone.localPosition.y, this.bone.localPosition.z));
            this.scale.Add(new Vector3(this.bone.localScale.x, this.bone.localScale.y, this.bone.localScale.z));
            this.rotation.Add(new Quaternion(this.bone.localRotation.x, this.bone.localRotation.y, this.bone.localRotation.z, this.bone.localRotation.w));

            var size = this.position.Count;
            if (this.position.Count > 1)
            {
                if (!this.saveT)
                    this.saveT = (this.position[size - 1] != this.position[size - 2]);
                if (!this.saveR)
                    this.saveR = (this.rotation[size - 1] != this.rotation[size - 2]);
                if (!this.saveS)
                    this.saveS = (this.scale[size - 1] != this.scale[size - 2]);
            }
        }

        public void ExportSampler(List<GLTF.Schema.AnimationChannel> channels, List<GLTF.Schema.AnimationSampler> samplers)
        {
            // reset bones
            this.bone.localPosition = this.initialPosition;
            this.bone.localScale = this.initialScale;
            this.bone.localRotation = this.initialRotation;

            if (!this.saveT && !this.saveR && !this.saveS)
            {
                return;
            }

            var nodeId = this.exporter.nodeIdByTransform[this.bone];

            if (this.saveT)
            {
                var sampler = this.AddChannelAndGetSampler(nodeId, channels, samplers, GLTF.Schema.GLTFAnimationChannelPath.translation);
                this.ExportData<Vector3>(
                    sampler,
                    this.position,
                    elem => GetRightHandedVector(elem),
                    values => this.cache.GetAccessorId(values));
            }
            if (this.saveS)
            {
                var sampler = this.AddChannelAndGetSampler(nodeId, channels, samplers, GLTF.Schema.GLTFAnimationChannelPath.scale);
                this.ExportData(
                    sampler,
                    this.scale,
                    elem => elem,
                    values => this.cache.GetAccessorId(values));
            }
            if (this.saveR)
            {
                var sampler = this.AddChannelAndGetSampler(nodeId, channels, samplers, GLTF.Schema.GLTFAnimationChannelPath.rotation);
                this.ExportData(
                    sampler,
                    this.rotation,
                    elem => GetRightHandedQuaternion(CreateNormalizedQuaternion(elem)),
                    values => this.cache.GetAccessorId(values));
            }
        }

        private void ExportData<T>(GLTF.Schema.AnimationSampler sampler,
            List<T> samples,
            Func<T, T> convert,
            Func<List<T>, GLTF.Schema.AccessorId> exportData)
        {

            var output = new List<T>();
            for (var i = 0; i < this.time.Count; i++)
            {
                output.Add(convert(samples[i]));
            }

            var inputId = this.cache.GetAccessorId(this.time);

            sampler.Input = inputId;
            sampler.Interpolation = GLTF.Schema.InterpolationType.LINEAR;
            sampler.Output = exportData(output);

        }

        private GLTF.Schema.AnimationSampler AddChannelAndGetSampler(GLTF.Schema.NodeId nodeIndex, List<GLTF.Schema.AnimationChannel> channels, List<GLTF.Schema.AnimationSampler> samplers, GLTF.Schema.GLTFAnimationChannelPath property)
        {
            GLTF.Schema.SamplerId samplerId = new GLTF.Schema.SamplerId()
            {
                Id = samplers.Count,
                Root = this.exporter._root,
            };

            var sampler = new GLTF.Schema.AnimationSampler();
            samplers.Add(sampler);

            var channel = new GLTF.Schema.AnimationChannel
            {
                Sampler = samplerId,
                Target = new GLTF.Schema.AnimationChannelTarget
                {
                    Node = nodeIndex,
                    Path = property,
                },
            };
            channels.Add(channel);

            return sampler;
        }

        private static Vector3 GetRightHandedVector(Vector3 value)
        {
            return SchemaExtensions.ToGltfVector3Convert(value).ToUnityVector3Raw();
        }
        private static Quaternion GetRightHandedQuaternion(Quaternion value)
        {
            return SchemaExtensions.ToGltfQuaternionConvert(value).ToUnityQuaternionRaw();
        }
        private static Quaternion CreateNormalizedQuaternion(Quaternion q)
        {
            q.Normalize();
            return q;
        }
    }

}

#endif
