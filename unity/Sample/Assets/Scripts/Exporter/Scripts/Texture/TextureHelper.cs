#if (UNITY_EDITOR) 

using System;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEditor;

namespace UnityExporter
{

    public static class TextureHelper
    {

        public static bool HasAlpha(this Texture2D source)
        {
            bool hasAlpha = source.alphaIsTransparency;
            Color[] apixels = source.GetPixels(0, 0, source.width, source.height);
            for (int index = 0; index < apixels.Length; index++)
            {
                hasAlpha |= apixels[index].a <= 0.99999f;
            }
            return hasAlpha;
        }
    }
}

#endif
