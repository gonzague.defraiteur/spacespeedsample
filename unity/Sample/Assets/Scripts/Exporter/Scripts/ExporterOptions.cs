﻿#if (UNITY_EDITOR)

using UnityEngine;
using GLTF.Schema;

namespace UnityExporter
{
    public class ExporterOptions
    {
        public static CustomGLTFNodeExporter nodeExporterDelegate;
        public static CustomGLTFNodeFilter nodeFilterDelegate = new DefaultGLTFNodeFilter();
        public static bool ExportOnlyActive = true;
        public static bool ExportLightmap = true;
        public static bool RequireExtensions = true;
        public static bool ExportFullPath = true;

        // Mesh accessors
        public static bool ExporMeshUv2 = false;
        public static bool ExporMeshTangent = false;
        public static bool ExporMeshNormal = false;

        public static bool JpgEncoding = true;
        public static int JpgQuality = 90;
    }

}

#endif