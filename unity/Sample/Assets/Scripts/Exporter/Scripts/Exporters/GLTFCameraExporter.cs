﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFCameraExporter : GLTFExporterBase
    {
        public GLTFCameraExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.CameraId Export(Camera unityCamera)
        {
            GLTF.Schema.GLTFCamera camera = new GLTF.Schema.GLTFCamera();
            camera.Name = unityCamera.name;

            //type
            bool isOrthographic = unityCamera.orthographic;
            camera.Type = isOrthographic ? GLTF.Schema.CameraType.orthographic : GLTF.Schema.CameraType.perspective;
            Matrix4x4 matrix = unityCamera.projectionMatrix;

            //matrix properties: compute the fields from the projection matrix
            if (isOrthographic)
            {
                GLTF.Schema.CameraOrthographic ortho = new GLTF.Schema.CameraOrthographic();
                ortho.XMag = 1 / matrix[0, 0];
                ortho.YMag = 1 / matrix[1, 1];

                float farClip = (matrix[2, 3] / matrix[2, 2]) - (1 / matrix[2, 2]);
                float nearClip = farClip + (2 / matrix[2, 2]);
                ortho.ZFar = farClip;
                ortho.ZNear = nearClip;
                camera.Orthographic = ortho;
            }
            else
            {
                GLTF.Schema.CameraPerspective perspective = new GLTF.Schema.CameraPerspective();
                float fov = 2 * Mathf.Atan(1 / matrix[1, 1]);
                float aspectRatio = matrix[1, 1] / matrix[0, 0];
                perspective.YFov = fov;
                perspective.AspectRatio = aspectRatio;

                if (matrix[2, 2] == -1)
                {
                    //infinite projection matrix
                    float nearClip = matrix[2, 3] * -0.5f;
                    perspective.ZNear = nearClip;
                }
                else
                {
                    //finite projection matrix
                    float farClip = matrix[2, 3] / (matrix[2, 2] + 1);
                    float nearClip = farClip * (matrix[2, 2] + 1) / (matrix[2, 2] - 1);
                    perspective.ZFar = farClip;
                    perspective.ZNear = nearClip;
                }
                camera.Perspective = perspective;
            }

            var id = new GLTF.Schema.CameraId
            {
                Id = _root.Cameras.Count,
                Root = _root
            };

            _root.Cameras.Add(camera);

            return id;
        }
    }
}

#endif