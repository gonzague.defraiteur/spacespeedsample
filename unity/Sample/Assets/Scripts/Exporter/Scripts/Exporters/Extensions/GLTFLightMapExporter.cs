#if (UNITY_EDITOR)

using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

namespace UnityExporter
{
    class LightmapData
    {
        public int unityIndex;
        public int gltfIndex;
    }

    // Root: List lightmap textures
    // Node: Reference to lightmap, uv offset / scale

    class GLTFLightMapExporter : GLTFExporterBase
    {
        public static string EXTENSION_NAME = "GB_lightmaps";
        private bool extensionAdded = false;
        private LightMapRootExtension extension;

        public GLTFLightMapExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public void ExportLightMap(Renderer renderer, GLTF.Schema.Node node)
        {
            if (!ExporterOptions.ExportLightmap) return;

            var lightmapIndex = renderer.lightmapIndex;
            var lightmapScaleOffset = renderer.lightmapScaleOffset;

            if (lightmapIndex < 0) return; // No lightmap on node
            if (lightmapIndex >= 0xFFFE)
            {
                // https://docs.unity3d.com/ScriptReference/Renderer-lightmapIndex.html
                Debug.LogWarning("Ignoring lightmap with index >= 0xFFFE (" + lightmapIndex + ")");
                return;
            }

            // Register and create extension root
            // Register light at root level
            if (this.extension == null)
            {
                this.RegisterExtension();
            }

            var gltfIndex = this.AddLightmap(lightmapIndex);

            // Create extension at node level
            // Register light at node level (will have position / rotation attached)
            if (node.Extensions == null)
            {
                node.Extensions = new Dictionary<string, GLTF.Schema.IExtension>();
            }
            node.Extensions[EXTENSION_NAME] = new LightMapNodeExtension(gltfIndex, lightmapScaleOffset);
        }

        private int AddLightmap(int lightmapIndex)
        {
            LightmapData existingData;
            if (this.exporter.lightmapDataByIndex.TryGetValue(lightmapIndex, out existingData))
            {
                return existingData.gltfIndex;
            }
            else
            {
                existingData = new LightmapData();
                this.exporter.lightmapDataByIndex.Add(lightmapIndex, existingData);
            }

            var lightmapCount = this.extension.lightmaps.Count;
            existingData.gltfIndex = lightmapCount;
            existingData.unityIndex = lightmapIndex;

            var lightmap = LightmapSettings.lightmaps[lightmapIndex];
            var lightmapColor = lightmap.lightmapColor;
            var textureInfo = this.exporter.textureExporter.ExportTextureInfo(lightmapColor, TextureMapType.Light, false);

            JObject texture = new JObject();
            texture.Add("index", textureInfo.Index.Id);

            JObject o = new JObject();
            o.Add("texture", texture);
            this.extension.lightmaps.Add(o);

            return existingData.gltfIndex;
        }

        private void RegisterExtension()
        {
            if (this.extensionAdded) return;

            if (_root.Extensions == null)
            {
                _root.Extensions = new Dictionary<string, GLTF.Schema.IExtension>();
            }
            extension = new LightMapRootExtension();
            _root.Extensions.Add(EXTENSION_NAME, extension);

            if (_root.ExtensionsUsed == null)
            {
                _root.ExtensionsUsed = new List<string>(new[] { EXTENSION_NAME });
            }
            else
            {
                _root.ExtensionsUsed.Add(EXTENSION_NAME);
            }

            if (ExporterOptions.RequireExtensions)
            {
                if (_root.ExtensionsRequired == null)
                {
                    _root.ExtensionsRequired = new List<string>(new[] { EXTENSION_NAME });
                }
                else
                {
                    _root.ExtensionsRequired.Add(EXTENSION_NAME);
                }
            }

            this.extensionAdded = true;
        }
    }

    // Definition at root levels (list of lights with details)
    class LightMapRootExtension : GLTF.Schema.IExtension
    {
        public JArray lightmaps;

        public LightMapRootExtension()
        {
            this.lightmaps = new JArray();
        }
        public GLTF.Schema.IExtension Clone(GLTF.Schema.GLTFRoot root)
        {
            return new LightMapRootExtension(); // TODO
        }
        public JProperty Serialize()
        {
            JObject ext = new JObject();
            ext.Add(new JProperty("lightmaps", this.lightmaps));

            return new JProperty(GLTFLightMapExporter.EXTENSION_NAME, ext);
        }
    }

    // Definition at node level
    class LightMapNodeExtension : GLTF.Schema.IExtension
    {
        private int id;
        private Vector4 lightmapScaleOffset;

        public LightMapNodeExtension(int id, Vector4 lightmapScaleOffset)
        {
            this.id = id;
            this.lightmapScaleOffset = lightmapScaleOffset;
        }
        public GLTF.Schema.IExtension Clone(GLTF.Schema.GLTFRoot root)
        {
            return new LightMapNodeExtension(this.id, this.lightmapScaleOffset);
        }
        public JProperty Serialize()
        {
            JObject ext = new JObject();
            ext.Add(new JProperty("lightmap", this.id));
            ext.Add(new JProperty("uvScale", new JArray { lightmapScaleOffset.x, lightmapScaleOffset.y }));
            ext.Add(new JProperty("uvOffset", new JArray { lightmapScaleOffset.z, lightmapScaleOffset.w }));

            return new JProperty(GLTFLightMapExporter.EXTENSION_NAME, ext);
        }
    }

}

#endif