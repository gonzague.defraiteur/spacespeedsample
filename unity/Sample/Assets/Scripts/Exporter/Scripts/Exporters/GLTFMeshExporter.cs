﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFMeshExporter : GLTFExporterBase
    {
        public GLTFMeshExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.MeshId Export(string name, GameObject[] primitives)
        {
            // check if this set of primitives is already a mesh
            GLTF.Schema.MeshId existingMeshId = null;
            var key = new PrimKey();
            foreach (var prim in primitives)
            {
                var filter = prim.GetComponent<MeshFilter>();
                var renderer = prim.GetComponent<MeshRenderer>();
                var skinnedMeshRenderer = prim.GetComponent<SkinnedMeshRenderer>();
                if (skinnedMeshRenderer != null)
                {
                    key.Mesh = skinnedMeshRenderer.sharedMesh;
                    key.Material = skinnedMeshRenderer.sharedMaterial;
                }
                else
                {
                    key.Mesh = filter.sharedMesh;
                    key.Material = renderer.sharedMaterial;
                }

                GLTF.Schema.MeshId tempMeshId;
                if (this.exporter.primOwner.TryGetValue(key, out tempMeshId) && (existingMeshId == null || tempMeshId == existingMeshId))
                {
                    existingMeshId = tempMeshId;
                }
                else
                {
                    existingMeshId = null;
                    break;
                }
            }

            // if so, return that mesh id
            if (existingMeshId != null)
            {
                return existingMeshId;
            }

            // if not, create new mesh and return its id
            var mesh = new GLTF.Schema.GLTFMesh();
            mesh.Name = name;

            mesh.Primitives = new List<GLTF.Schema.MeshPrimitive>(primitives.Length);
            foreach (var prim in primitives)
            {
                var export = this.exporter.primitiveExporter.Export(prim);
                mesh.Primitives.AddRange(export);
            }

            if (key.Mesh.blendShapeCount > 0)
            {
                var extra = this.exporter.getExtra(mesh);
                var targetNames = new Newtonsoft.Json.Linq.JArray();
                extra.Add("targetNames", targetNames);
                mesh.Weights = new List<double>();

                for (int j = 0; j < key.Mesh.blendShapeCount; ++j)
                {
                    var morphTarget = this.ExportMorphTarget(key.Mesh, j);

                    //
                    // all primitive has same blendShape
                    //
                    for (int k = 0; k < mesh.Primitives.Count; ++k)
                    {
                        if (mesh.Primitives[k].Targets == null)
                        {
                            mesh.Primitives[k].Targets = new List<Dictionary<string, GLTF.Schema.AccessorId>>();
                        }
                        mesh.Primitives[k].Targets.Add(morphTarget);
                        // mesh.Primitives[k].extras.targetNames.Add(mesh.GetBlendShapeName(j));
                    }

                    var firstPrimitive = primitives[0];
                    var skinnedMeshRenderer = firstPrimitive.GetComponent<SkinnedMeshRenderer>();
                    var weight0 = skinnedMeshRenderer.GetBlendShapeWeight(j) / 100;
                    var name0 = key.Mesh.GetBlendShapeName(j);
                    mesh.Weights.Add(weight0);
                    targetNames.Add(name0);
                }
            }

            var id = new GLTF.Schema.MeshId
            {
                Id = _root.Meshes.Count,
                Root = _root
            };
            _root.Meshes.Add(mesh);

            return id;
        }


        private Dictionary<string, GLTF.Schema.AccessorId> ExportMorphTarget(Mesh mesh, int blendShapeIdx)
        {
            var dictionnary = new Dictionary<string, GLTF.Schema.AccessorId>();

            var blendShapeVertices = mesh.vertices;
            var usePosition = blendShapeVertices != null && blendShapeVertices.Length > 0;

            var blendShapeNormals = mesh.normals;
            var useNormal = ExporterOptions.ExporMeshNormal && (usePosition && blendShapeNormals != null && blendShapeNormals.Length == blendShapeVertices.Length);

            // var blendShapeTangents = mesh.tangents.Select(y => (Vector3)y).ToArray();
            var useTangent = ExporterOptions.ExporMeshTangent && false;

            var frameCount = mesh.GetBlendShapeFrameCount(blendShapeIdx);
            mesh.GetBlendShapeFrameVertices(blendShapeIdx, frameCount - 1, blendShapeVertices, blendShapeNormals, null);

            if (usePosition)
            {
                blendShapeVertices = SchemaExtensions.ConvertVector3CoordinateSpaceAndCopy(blendShapeVertices, SchemaExtensions.CoordinateSpaceConversionScale);

                var blendShapePositionAccessorIndex = this.exporter.accessorExporter.Export(blendShapeVertices);
                dictionnary.Add(GLTF.Schema.SemanticProperties.POSITION, blendShapePositionAccessorIndex);
                // gltf.accessors[blendShapePositionAccessorIndex].min = blendShapeVertices.Aggregate(blendShapeVertices[0], (a, b) => new Vector3(Mathf.Min(a.x, b.x), Math.Min(a.y, b.y), Mathf.Min(a.z, b.z))).ToArray();
                // gltf.accessors[blendShapePositionAccessorIndex].max = blendShapeVertices.Aggregate(blendShapeVertices[0], (a, b) => new Vector3(Mathf.Max(a.x, b.x), Math.Max(a.y, b.y), Mathf.Max(a.z, b.z))).ToArray();
            }

            if (useNormal)
            {
                blendShapeNormals = SchemaExtensions.ConvertVector3CoordinateSpaceAndCopy(blendShapeNormals, SchemaExtensions.CoordinateSpaceConversionScale);

                var blendShapeNormalAccessorIndex = this.exporter.accessorExporter.Export(blendShapeNormals);
                dictionnary.Add(GLTF.Schema.SemanticProperties.NORMAL, blendShapeNormalAccessorIndex);
            }

            if (useTangent)
            {
                // blendShapeTangents = SchemaExtensions.ConvertVector3CoordinateSpaceAndCopy(blendShapeTangents, SchemaExtensions.TangentSpaceConversionScale);

                // var blendShapeTangentAccessorIndex = this.exporter.accessorExporter.Export(blendShapeTangents);
                // dictionnary.Add(GLTF.Schema.SemanticProperties.TANGENT, blendShapeTangentAccessorIndex);
            }

            return dictionnary;
        }
    }
}

#endif