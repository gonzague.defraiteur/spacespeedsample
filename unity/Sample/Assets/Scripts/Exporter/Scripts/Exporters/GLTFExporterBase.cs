﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFExporterBase
    {
        protected GLTFExporter exporter;
        public GLTFExporterBase(GLTFExporter exporter)
        {
            this.exporter = exporter;
        }

        protected bool doExport(Transform transform)
        {
            return this.exporter.nodeFilter.exportNode(transform);
        }

        protected GLTF.Schema.GLTFRoot _root
        {
            get { return this.exporter._root; }
        }
    }
}

#endif