﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFSamplerExporter : GLTFExporterBase
    {
        public GLTFSamplerExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.SamplerId Export(Texture texture)
        {
            var samplerId = this.exporter.GetSamplerId(_root, texture);
            if (samplerId != null)
                return samplerId;

            var sampler = new GLTF.Schema.Sampler();

            if (texture.wrapMode == TextureWrapMode.Clamp)
            {
                sampler.WrapS = GLTF.Schema.WrapMode.ClampToEdge;
                sampler.WrapT = GLTF.Schema.WrapMode.ClampToEdge;
            }
            else
            {
                sampler.WrapS = GLTF.Schema.WrapMode.Repeat;
                sampler.WrapT = GLTF.Schema.WrapMode.Repeat;
            }

            if (texture.filterMode == FilterMode.Point)
            {
                sampler.MinFilter = GLTF.Schema.MinFilterMode.NearestMipmapNearest;
                sampler.MagFilter = GLTF.Schema.MagFilterMode.Nearest;
            }
            else if (texture.filterMode == FilterMode.Bilinear)
            {
                sampler.MinFilter = GLTF.Schema.MinFilterMode.NearestMipmapLinear;
                sampler.MagFilter = GLTF.Schema.MagFilterMode.Linear;
            }
            else
            {
                sampler.MinFilter = GLTF.Schema.MinFilterMode.LinearMipmapLinear;
                sampler.MagFilter = GLTF.Schema.MagFilterMode.Linear;
            }

            samplerId = new GLTF.Schema.SamplerId
            {
                Id = _root.Samplers.Count,
                Root = _root
            };

            _root.Samplers.Add(sampler);

            return samplerId;
        }
    }
}

#endif