﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFNodeExporter : GLTFExporterBase
    {
        public GLTFNodeExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.NodeId Export(Transform nodeTransform)
        {
            var node = new GLTF.Schema.Node();
            node.Name = nodeTransform.name;

            // Custom GB components
            var tags = nodeTransform.GetComponent<TagsComponent>();
            if (tags != null)
            {
                tags.Serialize(this.exporter.getExtra(node));
            }


            //export camera attached to node
            Camera unityCamera = nodeTransform.GetComponent<Camera>();
            if (unityCamera != null)
            {
                node.Camera = this.exporter.cameraExporter.Export(unityCamera);
            }

            // check for light
            Light light = nodeTransform.GetComponent<Light>();
            if (light != null && light.isActiveAndEnabled)
            {
                this.exporter.lightExporter.ExportLight(light, node);
            }

            // check for lightmap
            var meshRenderer = nodeTransform.GetComponent<Renderer>();
            if (meshRenderer != null)
            {
                if (ExporterOptions.ExportLightmap && meshRenderer.lightmapIndex >= 0)
                {
                    this.exporter.lightmapExporter.ExportLightMap(meshRenderer, node);
                }
            }

            node.SetUnityTransform(nodeTransform);

            var id = new GLTF.Schema.NodeId
            {
                Id = _root.Nodes.Count,
                Root = _root
            };
            _root.Nodes.Add(node);

            this.exporter.nodeIdsByNode.Add(node, id);
            this.exporter.nodeIdByTransform.Add(nodeTransform, id);

            // children that are primitives get put in a mesh
            GameObject[] primitives, nonPrimitives;
            FilterPrimitives(nodeTransform, out primitives, out nonPrimitives);
            if (primitives.Length > 0)
            {
                node.Mesh = this.exporter.meshExporter.Export(nodeTransform.name, primitives);

                // associate unity meshes with gltf mesh id
                foreach (var prim in primitives)
                {
                    var filter = prim.GetComponent<MeshFilter>();
                    var renderer = prim.GetComponent<MeshRenderer>();
                    var skinnedMeshRenderer = prim.GetComponent<SkinnedMeshRenderer>();

                    PrimKey key;
                    if (skinnedMeshRenderer != null)
                    {
                        key = new PrimKey { Mesh = skinnedMeshRenderer.sharedMesh, Material = skinnedMeshRenderer.sharedMaterial };
                    }
                    else
                    {
                        key = new PrimKey { Mesh = filter.sharedMesh, Material = renderer.sharedMaterial };
                    }
                    this.exporter.primOwner[key] = node.Mesh;
                }
            }

            // children that are not primitives get added as child nodes
            if (nonPrimitives.Length > 0)
            {
                node.Children = new List<GLTF.Schema.NodeId>(nonPrimitives.Length);
                foreach (var child in nonPrimitives)
                {
                    if (!doExport(child.transform))
                        continue;

                    var childExport = this.exporter.nodeExporter.Export(child.transform);
                    node.Children.Add(childExport);
                }
            }

            // Unity to threejs data
            this.exporter.unityNodeExporter.exportNode(nodeTransform, node);

            // Allow a client to add custom data in exporter json
            this.exporter.customDataExporter.exportNode(nodeTransform, node);

            // check for animation
            var animator = nodeTransform.GetComponent<Animator>();
            if (animator != null)
            {
                this.exporter.animationExporter.Export(nodeTransform, animator, id);
            }

            return id;
        }

        private void FilterPrimitives(Transform transform, out GameObject[] primitives, out GameObject[] nonPrimitives)
        {
            var childCount = transform.childCount;
            var prims = new List<GameObject>();
            var nonPrims = new List<GameObject>();

            // add another primitive if the root object also has a mesh
            if (transform.gameObject.GetComponent<MeshFilter>() != null
                && transform.gameObject.GetComponent<MeshRenderer>() != null)
            {
                prims.Add(transform.gameObject);
            }
            else if (transform.gameObject.GetComponent<SkinnedMeshRenderer>() != null)
            {
                prims.Add(transform.gameObject);
            }

            for (var i = 0; i < childCount; i++)
            {
                var go = transform.GetChild(i).gameObject;
                nonPrims.Add(go);
            }

            primitives = prims.ToArray();
            nonPrimitives = nonPrims.ToArray();
        }

        // private static bool IsPrimitive(GameObject gameObject)
        // {
        //     /*
        // 	 * Primitives have the following properties:
        // 	 * - have no children
        // 	 * - have no non-default local transform properties
        // 	 * - have MeshFilter and MeshRenderer components
        // 	 */
        //     return gameObject.transform.childCount == 0
        //         && gameObject.transform.localPosition == Vector3.zero
        //         && gameObject.transform.localRotation == Quaternion.identity
        //         && gameObject.transform.localScale == Vector3.one
        //         && gameObject.GetComponent<MeshFilter>() != null
        //         && gameObject.GetComponent<MeshRenderer>() != null;
        // }
    }
}

#endif