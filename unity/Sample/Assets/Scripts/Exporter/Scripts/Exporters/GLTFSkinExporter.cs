﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFSkinExporter : GLTFExporterBase
    {
        private readonly Dictionary<Skin, GLTF.Schema.SkinId> _skinCache = new Dictionary<Skin, GLTF.Schema.SkinId>();

        public GLTFSkinExporter(GLTFExporter exporter) : base(exporter)
        {
        }


        public void Export(Transform[] rootObjTransforms)
        {
            foreach (var transform in rootObjTransforms)
            {
                GameObject gameObject = transform.gameObject;
                foreach (var skinnedMeshRenderer in gameObject.GetComponentsInChildren<SkinnedMeshRenderer>())
                {
                    if (!doExport(skinnedMeshRenderer.transform))
                        continue;

                    var nodeId = this.exporter.nodeIdByTransform[skinnedMeshRenderer.transform];
                    //if: exist bind poses + a bone, OR exist bones
                    if ((skinnedMeshRenderer.sharedMesh.bindposes != null && skinnedMeshRenderer.sharedMesh.bindposes.Any() && skinnedMeshRenderer.rootBone != null) ||
                        (skinnedMeshRenderer.bones != null && skinnedMeshRenderer.bones.Any()))
                    {
                        // Debug.Log("Export skin on " + gameObject.name);
                        _root.Nodes[nodeId.Id].Skin = ExportSkin(skinnedMeshRenderer);
                    }
                }
            }
        }

        private GLTF.Schema.SkinId ExportSkin(SkinnedMeshRenderer skinnedMeshRenderer)
        {
            var skin = new Skin
            {
                BindPoses = skinnedMeshRenderer.sharedMesh.bindposes,
                RootBone = skinnedMeshRenderer.rootBone,
                Bones = skinnedMeshRenderer.bones,
            };

            this.ExportSkeletonAnimation(skinnedMeshRenderer);

            GLTF.Schema.SkinId skinId;
            if (_skinCache.TryGetValue(skin, out skinId))
            {
                //already exported this skin, return it
                return skinId;
            }
            skinId = new GLTF.Schema.SkinId
            {
                Id = _root.Skins.Count,
                Root = this._root
            };

            Transform parentTransform = skinnedMeshRenderer.transform.parent;
            if (parentTransform != null && parentTransform.localScale != Vector3.one)
            {
                Debug.LogWarning("Skinning - not supported: parent node (" + parentTransform.name + ") has a local scale (" + parentTransform.localScale + ")!");
            }

            var bindposeConverted = skin.BindPoses.Select(bindpose => GetRightHandedMatrix(bindpose));
            var bindposeAccessor = this.exporter.accessorExporter.Export(bindposeConverted);

            _root.Skins.Add(new GLTF.Schema.Skin
            {
                InverseBindMatrices = bindposeAccessor,
                Joints = skin.Bones.Select(bone => this.exporter.nodeIdByTransform[bone.transform]).ToList(),
                Skeleton = this.exporter.nodeIdByTransform[skin.RootBone]
            });
            return skinId;
        }


        //Skin struct for caching purposes
        private struct Skin
        {

            public Matrix4x4[] BindPoses;
            public Transform RootBone;
            public Transform[] Bones;

            public override bool Equals(object obj)
            {
                var skin = (Skin)obj;
                return
                    this.BindPoses.SequenceEqual(skin.BindPoses) &&
                    this.RootBone == skin.RootBone &&
                    this.Bones.SequenceEqual(skin.Bones);
            }

            public override int GetHashCode()
            {
                int hashCode = this.RootBone.GetHashCode();
                foreach (var bindPose in this.BindPoses)
                {
                    hashCode ^= bindPose.GetHashCode();
                }
                foreach (var bone in this.Bones)
                {
                    hashCode ^= bone.GetHashCode();
                }
                return hashCode;
            }
        }

        private static Matrix4x4 GetRightHandedMatrix(Matrix4x4 matrix)
        {
            return SchemaExtensions.ToGltfMatrix4x4Convert(matrix).ToUnityMatrix4x4Raw();
        }

        private void ExportSkeletonAnimation(SkinnedMeshRenderer skinnedMesh)
        {
            var parent = skinnedMesh.rootBone.parent;
            if (parent != null)
            {
                var animator = parent.gameObject.GetComponent<Animator>();
                if (animator != null && animator.isHuman)
                {
                    this.exporter.animationExporter.Export(skinnedMesh, animator);
                }
            }
        }

    }
}

#endif
