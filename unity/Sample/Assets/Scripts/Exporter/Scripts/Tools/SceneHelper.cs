﻿#if (UNITY_EDITOR)

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{

    public delegate void NodeAction(GameObject node);

    public class SceneHelper
    {

        public static void traverse(GameObject gameObject, NodeAction action)
        {
            var traverser = new SceneTraverser(gameObject);
            foreach (var node in traverser.all)
            {
                action(node);
            }
        }

    }

    public class SceneTraverser
    {
        public GameObject root;
        public List<GameObject> all = new List<GameObject>();

        public SceneTraverser(GameObject gameObject)
        {
            AddAll(gameObject);
        }

        private void AddAll(GameObject gameObject)
        {
            all.Add(gameObject);

            for (var i = 0; i < gameObject.transform.childCount; i++)
            {
                AddAll(gameObject.transform.GetChild(i).gameObject);
            }
        }
    }
}

#endif