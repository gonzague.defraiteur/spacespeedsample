﻿#if (UNITY_EDITOR)

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    struct PrimKey
    {
        public Mesh Mesh;
        public Material Material;
    }
    struct ImageInfo
    {
        public Texture2D texture;
        public TextureMapType textureMapType;
        public bool jpg;
    }
    enum IMAGETYPE
    {
        RGB,
        RGBA,
        R,
        G,
        B,
        A,
        G_INVERT
    }

    enum TextureMapType
    {
        Main,
        Bump,
        SpecGloss,
        Emission,
        MetallicGloss,
        Light,
        Sprite,
        Occlusion
    }


    class GLTFExporter
    {

        public GLTF.Schema.GLTFRoot _root;
        public BinaryWriter _bufferWriter;
        public GLTF.Schema.BufferId _bufferId;
        GLTF.Schema.GLTFBuffer _buffer;

        public List<ImageInfo> _imageInfos;
        public List<Texture> _textures;
        public List<Material> _materials;

        private Transform[] _rootTransforms;

        private readonly Dictionary<PrimKey, GLTF.Schema.MeshId> _primOwner = new Dictionary<PrimKey, GLTF.Schema.MeshId>();
        private readonly Dictionary<Mesh, GLTF.Schema.MeshPrimitive[]> _meshToPrims = new Dictionary<Mesh, GLTF.Schema.MeshPrimitive[]>();
        public readonly Dictionary<int, LightmapData> lightmapDataByIndex = new Dictionary<int, LightmapData>();

        // Can be overloaded
        public CustomGLTFNodeExporter customDataExporter;
        public CustomGLTFNodeFilter nodeFilter;
        public GLTFSceneExporter sceneExporter;
        public GLTFNodeExporter nodeExporter;
        public GLTFCameraExporter cameraExporter;
        public GLTFMeshExporter meshExporter;
        public GLTFPrimitiveExporter primitiveExporter;
        public GLTFAccessorExporter accessorExporter;
        public GLTFMaterialExporter materialExporter;
        public GLTFTextureExporter textureExporter;
        public GLTFImageExporter imageExporter;
        public GLTFSamplerExporter samplerExporter;
        public GLTFSpriteExporter spriteExporter;

        // Extensions
        public GLTFLightExporter lightExporter;
        public GLTFLightMapExporter lightmapExporter;
        public GLTFUnityNodeExporter unityNodeExporter;
        public GLTFAnimationExporter animationExporter;
        public GLTFSkinExporter skinExporter;

        // Caches
        public readonly Dictionary<GLTF.Schema.Node, GLTF.Schema.NodeId> nodeIdsByNode = new Dictionary<GLTF.Schema.Node, GLTF.Schema.NodeId>();
        public readonly Dictionary<Transform, GLTF.Schema.NodeId> nodeIdByTransform = new Dictionary<Transform, GLTF.Schema.NodeId>();

        public GLTFExporter(Transform[] rootTransforms)
        {
            Debug.Log("Exporting " + rootTransforms.Length + " scenes");

            // Init default exporter
            customDataExporter = new DefaultCustomGLTFNodeExporter();
            nodeFilter = new DefaultGLTFNodeFilter();
            sceneExporter = new GLTFSceneExporter(this);
            nodeExporter = new GLTFNodeExporter(this);
            cameraExporter = new GLTFCameraExporter(this);
            meshExporter = new GLTFMeshExporter(this);
            primitiveExporter = new GLTFPrimitiveExporter(this);
            accessorExporter = new GLTFAccessorExporter(this);
            materialExporter = new GLTFMaterialExporter(this);
            textureExporter = new GLTFTextureExporter(this);
            imageExporter = new GLTFImageExporter(this);
            samplerExporter = new GLTFSamplerExporter(this);
            spriteExporter = new GLTFSpriteExporter(this);

            lightExporter = new GLTFLightExporter(this);
            lightmapExporter = new GLTFLightMapExporter(this);
            unityNodeExporter = new GLTFUnityNodeExporter(this);
            animationExporter = new GLTFAnimationExporter(this);
            skinExporter = new GLTFSkinExporter(this);

            _rootTransforms = rootTransforms;
            _root = new GLTF.Schema.GLTFRoot
            {
                Accessors = new List<GLTF.Schema.Accessor>(),
                Asset = new GLTF.Schema.Asset
                {
                    Version = "2.0"
                },
                Buffers = new List<GLTF.Schema.GLTFBuffer>(),
                BufferViews = new List<GLTF.Schema.BufferView>(),
                Cameras = new List<GLTF.Schema.GLTFCamera>(),
                Images = new List<GLTF.Schema.GLTFImage>(),
                Materials = new List<GLTF.Schema.GLTFMaterial>(),
                Meshes = new List<GLTF.Schema.GLTFMesh>(),
                Nodes = new List<GLTF.Schema.Node>(),
                Samplers = new List<GLTF.Schema.Sampler>(),
                Scenes = new List<GLTF.Schema.GLTFScene>(),
                Textures = new List<GLTF.Schema.GLTFTexture>(),
                Animations = new List<GLTF.Schema.GLTFAnimation>(),
                Skins = new List<GLTF.Schema.Skin>(),
            };

            _imageInfos = new List<ImageInfo>();
            _materials = new List<Material>();
            _textures = new List<Texture>();

            _buffer = new GLTF.Schema.GLTFBuffer();
            _bufferId = new GLTF.Schema.BufferId
            {
                Id = _root.Buffers.Count,
                Root = _root
            };
            _root.Buffers.Add(_buffer);
        }


        public void SaveGLTFandBin(string path, string fileName)
        {
            var binFile = File.Create(Path.Combine(path, fileName + ".bin"));
            _bufferWriter = new BinaryWriter(binFile);

            _root.Scene = sceneExporter.Export(fileName, _rootTransforms);

            _buffer.Uri = fileName + ".bin";
            _buffer.ByteLength = (int)_bufferWriter.BaseStream.Length;

            var gltfFile = File.CreateText(Path.Combine(path, fileName + ".gltf"));
            _root.Serialize(gltfFile);

#if WINDOWS_UWP
            			gltfFile.Dispose();
            			binFile.Dispose();
#else
            gltfFile.Close();
            binFile.Close();
#endif

            this.imageExporter.Export(path, _imageInfos);
        }


        public Dictionary<PrimKey, GLTF.Schema.MeshId> primOwner
        {
            get { return this._primOwner; }
        }
        public Dictionary<Mesh, GLTF.Schema.MeshPrimitive[]> meshToPrims
        {
            get { return this._meshToPrims; }
        }

        public GLTF.Schema.MaterialId GetMaterialId(GLTF.Schema.GLTFRoot root, Material materialObj)
        {
            for (var i = 0; i < this._materials.Count; i++)
            {
                if (this._materials[i] == materialObj)
                {
                    return new GLTF.Schema.MaterialId
                    {
                        Id = i,
                        Root = root
                    };
                }
            }
            return null;
        }

        public GLTF.Schema.TextureId GetTextureId(GLTF.Schema.GLTFRoot root, Texture textureObj)
        {
            for (var i = 0; i < _textures.Count; i++)
            {
                if (_textures[i] == textureObj)
                {
                    return new GLTF.Schema.TextureId
                    {
                        Id = i,
                        Root = root
                    };
                }
            }
            return null;
        }

        public GLTF.Schema.ImageId GetImageId(GLTF.Schema.GLTFRoot root, Texture imageObj)
        {
            for (var i = 0; i < _imageInfos.Count; i++)
            {
                if (_imageInfos[i].texture == imageObj)
                {
                    return new GLTF.Schema.ImageId
                    {
                        Id = i,
                        Root = root
                    };
                }
            }

            return null;
        }

        public GLTF.Schema.SamplerId GetSamplerId(GLTF.Schema.GLTFRoot root, Texture textureObj)
        {
            for (var i = 0; i < root.Samplers.Count; i++)
            {
                bool filterIsNearest = root.Samplers[i].MinFilter == GLTF.Schema.MinFilterMode.Nearest
                    || root.Samplers[i].MinFilter == GLTF.Schema.MinFilterMode.NearestMipmapNearest
                    || root.Samplers[i].MinFilter == GLTF.Schema.MinFilterMode.LinearMipmapNearest;

                bool filterIsLinear = root.Samplers[i].MinFilter == GLTF.Schema.MinFilterMode.Linear
                    || root.Samplers[i].MinFilter == GLTF.Schema.MinFilterMode.NearestMipmapLinear;

                bool filterMatched = textureObj.filterMode == FilterMode.Point && filterIsNearest
                    || textureObj.filterMode == FilterMode.Bilinear && filterIsLinear
                    || textureObj.filterMode == FilterMode.Trilinear && root.Samplers[i].MinFilter == GLTF.Schema.MinFilterMode.LinearMipmapLinear;

                bool wrapMatched = textureObj.wrapMode == TextureWrapMode.Clamp && root.Samplers[i].WrapS == GLTF.Schema.WrapMode.ClampToEdge
                    || textureObj.wrapMode == TextureWrapMode.Repeat && root.Samplers[i].WrapS != GLTF.Schema.WrapMode.ClampToEdge;

                if (filterMatched && wrapMatched)
                {
                    return new GLTF.Schema.SamplerId
                    {
                        Id = i,
                        Root = root
                    };
                }
            }

            return null;
        }

        public string RetrieveTexturePath(UnityEngine.Texture texture)
        {
            return UnityEditor.AssetDatabase.GetAssetPath(texture);
        }

        public Newtonsoft.Json.Linq.JObject getExtra(GLTF.Schema.GLTFProperty property)
        {
            if (property.Extras == null)
            {
                property.Extras = new Newtonsoft.Json.Linq.JObject();
            }
            return (Newtonsoft.Json.Linq.JObject)property.Extras;

        }
    }
}

#endif