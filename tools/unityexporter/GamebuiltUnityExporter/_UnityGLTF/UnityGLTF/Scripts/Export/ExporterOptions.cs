﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GLTF.Schema;
using UnityEngine;
using UnityEngine.Rendering;
using UnityGLTF.Extensions;
using CameraType = GLTF.Schema.CameraType;
using WrapMode = GLTF.Schema.WrapMode;

// MBA Custom
namespace UnityGLTF
{
    public interface CustomGLTFNodeExporter
    {
        void exportNode(Transform transform, Node node);
    }

    public interface CustomGLTFNodeFilter
    {
        bool exportNode(Transform transform);
    }

    public class ExporterOptions
    {

        // MBA CUSTOM export options
        public static CustomGLTFNodeExporter nodeExporterDelegate;
        public static CustomGLTFNodeFilter nodeFilterDelegate = new DefaultGLTFNodeFilter();
        public static bool ExportOnlyActive = true;
        public static bool ExportPrimitiveVertexColors = true;

    }

    public class DefaultGLTFNodeFilter : CustomGLTFNodeFilter
    {
        public bool exportNode(Transform transform)
        {
            if (ExporterOptions.ExportOnlyActive)
            {
                if (!transform.gameObject.activeSelf) return false;
            }

            return true;
        }
    }
}
