﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Gamebuilt.Exporter {

    public class Exporter {

        protected string path;

        public static string RetrieveTexturePath(UnityEngine.Texture texture)
        {
            return AssetDatabase.GetAssetPath(texture);
        }

        public Exporter (string path) {
            this.path = path;
        }
    }
}