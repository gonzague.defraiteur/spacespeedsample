﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityGLTF;
using UnityEngine.SceneManagement;
using System;

namespace Gamebuilt.Exporter
{

    public class GBExportMenu : EditorWindow
    {
        [MenuItem("Gamebuilt/Settings")]
        static void Init()
        {
            GBExportMenu window = (GBExportMenu)EditorWindow.GetWindow(typeof(GBExportMenu), false, "GLTF Export Settings");
            window.Show();
        }

        void OnGUI()
        {
            EditorGUILayout.LabelField("Exporter", EditorStyles.boldLabel);

            // Default options
            //GLTFSceneExporter.ExportFullPath = EditorGUILayout.Toggle("Export using original path", GLTFSceneExporter.ExportFullPath);
            //GLTFSceneExporter.ExportNames = EditorGUILayout.Toggle("Export names of nodes", GLTFSceneExporter.ExportNames);
            //GLTFSceneExporter.RequireExtensions = EditorGUILayout.Toggle("Require extensions", GLTFSceneExporter.RequireExtensions);

            ExporterOptions.ExportOnlyActive = EditorGUILayout.Toggle("Export only active nodes", ExporterOptions.ExportOnlyActive);
            ExporterOptions.ExportPrimitiveVertexColors = EditorGUILayout.Toggle("Export primitive vertex color ", ExporterOptions.ExportPrimitiveVertexColors);
        }

        private static string getExportPath()
        {
            string lastPath = EditorPrefs.GetString("gbexporter_lastPath", "");
            string path = EditorUtility.OpenFolderPanel("Export Path", lastPath, "");
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            EditorPrefs.SetString("gbexporter_lastPath", path);

            return path;
        }

        [MenuItem("Gamebuilt/Export Mesh")]
        public static void ExportSelected()
        {
            string path = getExportPath();
            if (string.IsNullOrEmpty(path))
            {
                return;
            }


            MeshExporter exporter = new MeshExporter(path);

            UnityEngine.Object[] objects = Selection.objects;
            for (int i = 0; i < objects.Length; i++)
            {
                UnityEngine.Object t = objects[i];
                //Debug.Log("Object " + i + ": " + type);
                if (t is GameObject)
                {
                    GameObject go = (GameObject)t;
                    exporter.export(go);
                }
            }

            //string name;
            //if (Selection.transforms.Length > 1)
            //    name = SceneManager.GetActiveScene().name;
            //else if (Selection.transforms.Length == 1)
            //    name = Selection.activeGameObject.name;
            //else
            //    throw new Exception("No objects selected, cannot export.");

            //var exporter = new GLTFSceneExporter(Selection.transforms, RetrieveTexturePath);

            //var path = EditorUtility.OpenFolderPanel("glTF Export Path", "", "");
            //if (!string.IsNullOrEmpty(path))
            //{
            //    exporter.SaveGLTFandBin(path, name);
            //}
        }

        [MenuItem("Gamebuilt/Export Scene")]
        public static void ExportScene()
        {
            var scene = SceneManager.GetActiveScene();
            var gameObjects = scene.GetRootGameObjects();
            var transforms = Array.ConvertAll(gameObjects, gameObject => gameObject.transform);

            var exporter = new GLTFSceneExporter(transforms, Exporter.RetrieveTexturePath);
            string path = getExportPath();
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            exporter.SaveGLTFandBin(path, scene.name);
        }
    }
}